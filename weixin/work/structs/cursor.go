package structs

type Cursor struct {
	Limit  int    `json:"limit"`
	Cursor string `json:"cursor,omitempty"`
}
