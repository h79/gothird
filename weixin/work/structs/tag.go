package structs

type Tag struct {
	TagId   int64  `json:"tagid,omitempty"`
	TagName string `json:"tagname,omitempty"`
}

type UserTag struct {
	Tag
	UserList  []User `json:"userlist,omitempty"`
	PartyList []int  `json:"partylist,omitempty"`
}

type User struct {
	Userid string `json:"userid"`
	Name   string `json:"name"`
}
