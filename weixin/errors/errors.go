package errors

// 小程序消息错误
const (
	ErrException              = 5
	ErrToken                  = 40001 // 获取 access_token 时 AppSecret 错误，或者 access_token 无效。请开发者认真比对 AppSecret 的正确性，或查看是否正在为恰当的小程序调用接口
	ErrIllegal                = 40002 // 不合法的凭证类型
	ErrOpenid                 = 40003 // 不合法的 OpenId，请开发者确认 OpenId 是否是其他小程序的 OpenId
	ErrAppid                  = 40013 // 不合法的 AppID ，请开发者检查 AppID 的正确性，避免异常字符，注意大小写
	ErrTimeout                = 45015 // 回复时间超过限制
	ErrIllegalAuthCode        = 40029 // 无效的 oauth_code
	ErrInvalidRefreshToken    = 40030 // invalid refresh_token
	ErrTimeoutAcsToken        = 42001 // access_token 超时，请检查 access_token 的有效期，请参考基础支持 - 获取 access_token 中，对 access_token 的详细机制说明
	ErrTimeoutRefToken        = 42002 // refresh_token 超时
	ErrTimeoutAuthCode        = 42003 // oauth_code 超时
	ErrLimit                  = 45047 // 客服接口下行条数超过上限
	ErrAuth                   = 48001 // API 功能未授权，请确认小程序已获得该接口
	ErrNotComponentAuthorized = 61003 // component is not authorized by this account
)

func IsExpired(code int32) bool {
	return code == ErrInvalidRefreshToken ||
		code == ErrTimeoutAcsToken ||
		code == ErrTimeoutRefToken ||
		code == ErrNotComponentAuthorized
}

func IsInvalid(code int32) bool {
	return ErrToken == code || ErrIllegal == code
}

//https://developers.weixin.qq.com/doc/offiaccount/Getting_Started/Global_Return_Code.html

//https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/login/auth.code2Session.html
//https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140842
