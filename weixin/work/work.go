package work

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/token/access"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	workaccess "gitee.com/h79/gothird/weixin/work/access"
	"gitee.com/h79/goutils/common/data"
	"gitee.com/h79/goutils/common/http"
)

func GetAccessToken(tk token.Token, d data.D) (access.Token, error) {
	var appId = tk.GetAppId()
	var secret = tk.GetSecret()
	if appId == "" {
		return nil, fmt.Errorf("corp id is empty")
	}
	if secret == "" {
		return nil, fmt.Errorf("corp secret is empty")
	}
	url := fmt.Sprintf("%s/cgi-bin/gettoken?corpid=%s&corpsecret=%s", consts.WorkApiPrefixUrl, appId, secret)
	hp := http.Http{}
	body, err := hp.DoBytes("GET", url, nil)
	if err != nil {
		return nil, err
	}

	type tokenResult struct {
		response.Response
		workaccess.Token
	}

	res := tokenResult{}

	if err = json.Unmarshal(body, &res); err != nil {
		return nil, err
	}
	return &res.Token, res.ErrorIf()
}
