package media

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	utilapi "gitee.com/h79/goutils/api"
	"gitee.com/h79/goutils/common/http"
)

// 素材管理
//https://developers.weixin.qq.com/doc/offiaccount/Asset_Management/New_temporary_materials.html
//https://developers.weixin.qq.com/doc/offiaccount/Asset_Management/Adding_Permanent_Assets.html

// Upload 上传临时素材
// 素材上传得到media_id，该media_id仅三天内有效
// media_id在同一企业内应用之间可以共享
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/media/upload?access_token=ACCESS_TOKEN&type=TYPE
// 使用multipart/form-data POST上传文件， 文件标识名为"media"
// fileType 分别有图片（image）、语音（voice）、视频（video），普通文件（file）
func Upload(api *token.Api, domain, fileType, filename string) (*ID, error) {
	var img = Image{
		Field:    "media",
		FileName: filename,
	}
	return UploadV2(api, domain, fileType, &img)
}

func UploadV2(api *token.Api, domain, fileType string, img token.Form) (*ID, error) {
	uri := fmt.Sprintf("%s/cgi-bin/media/upload?type=%s&", domain, fileType)
	var res = struct {
		response.Response
		ID
	}{}
	er := api.Upload(uri, fileType, img, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})
	return &res.ID, er
}

// UpdateImage 上传图片
// 上传图片得到图片URL，该URL永久有效
// 返回的图片URL，仅能用于图文消息正文中的图片展示，或者给客户发送欢迎语等；若用于非企业微信环境下的页面，图片将被屏蔽。
// 每个企业每月最多可上传3000张图片，每天最多可上传1000张图片
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/media/uploadimg?access_token=ACCESS_TOKEN
func UpdateImage(api *token.Api, domain string, img *Image) (string, error) {
	return UpdateImageV2(api, domain, "", img)
}

// UpdateImageV2 上传图片
// 上传图片得到图片URL，该URL永久有效
// 返回的图片URL，仅能用于图文消息正文中的图片展示，或者给客户发送欢迎语等；若用于非企业微信环境下的页面，图片将被屏蔽。
// 每个企业每月最多可上传3000张图片，每天最多可上传1000张图片
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/media/uploadimg?access_token=ACCESS_TOKEN
func UpdateImageV2(api *token.Api, domain string, fileType string, img token.Form) (string, error) {
	uri := fmt.Sprintf("%s/cgi-bin/media/uploadimg?", domain)
	var res = struct {
		response.Response
		Url string `json:"url"`
	}{}
	er := api.Upload(uri, fileType, img, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})
	return res.Url, er
}

// Get 获取临时素材
// GET, https://api.weixin.qq.com/cgi-bin/media/get?access_token=ACCESS_TOKEN&media_id=MEDIA_ID
// GET, https://qyapi.weixin.qq.com/cgi-bin/media/get?access_token=ACCESS_TOKEN&media_id=MEDIA_ID for 企业微信
func Get(api *token.Api, domain string, mediaId string) ([]byte, error) {

	uri := fmt.Sprintf("%s/cgi-bin/media/get?media_id=%s&", domain, mediaId)

	return getMedia(api, uri)
}

// GetHD 获取高清语音素材
// 可以使用本接口获取从JSSDK的uploadVoice接口上传的临时语音素材，格式为speex，16K采样率。该音频比上文的临时素材获取接口（格式为amr，8K采样率）更加清晰，适合用作语音识别等对音质要求较高的业务。
// 请求方式：GET（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/media/get/jssdk?access_token=ACCESS_TOKEN&media_id=MEDIA_ID
func GetHD(api *token.Api, domain string, mediaId string) ([]byte, error) {

	uri := fmt.Sprintf("%s/cgi-bin/media/get/jssdk?media_id=%s&", domain, mediaId)

	return getMedia(api, uri)
}

func getMedia(api *token.Api, uri string) ([]byte, error) {
	var buff []byte
	var res = response.Response{}
	if er := api.Request("GET", uri, nil, func(hp *http.Http, body []byte) error {
		ll := len(body)
		if ll > 100 {
			buff = body
			return nil
		}
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	}); er != nil {
		return nil, er
	}
	if res.ErrCode != 0 {
		return nil, res
	}
	return buff, nil
}

// UploadASync 生成异步上传任务
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/media/upload_by_url?access_token=ACCESS_TOKEN
func UploadASync(api *token.Api, domain string, file *BigFile) (string, error) {
	url := fmt.Sprintf("%s/cgi-bin/media/upload_by_url?", domain)
	buf, err := json.Marshal(file)
	if err != nil {
		return "", err
	}
	var res = struct {
		response.Response
		JobId string `json:"jobid"`
	}{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return "", err
	}
	return res.JobId, res.ErrorIf()
}

// ASyncJob 查询异步任务结果
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/media/get_upload_by_url_result?access_token=ACCESS_TOKEN
func ASyncJob(api *token.Api, domain, jobId string) (BigJob, error) {
	url := fmt.Sprintf("%s/cgi-bin/media/get_upload_by_url_result?", domain)
	var job = struct {
		JobId string `json:"jobid"`
	}{JobId: jobId}
	buf, err := json.Marshal(&job)
	if err != nil {
		return BigJob{}, err
	}
	var res = struct {
		response.Response
		BigJob
	}{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return BigJob{}, err
	}
	return res.BigJob, res.ErrorIf()
}

// UploadProvider 服务商上传临时素材
// 素材上传得到media_id，该media_id仅三天内有效
// media_id在同一企业内应用之间可以共享
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/service/media/upload?provider_access_token=ACCESS_TOKEN&type=TYPE&attachment_type=3
// 使用multipart/form-data POST上传文件， 文件标识名为"media"
func UploadProvider(api *token.Api, fileType string, attachmentType int, img token.Form) (*ID, error) {
	uri := fmt.Sprintf("%s/cgi-bin/media/service/upload?type=%s&attachment_type=%d&", consts.WorkApiPrefixUrl, fileType, attachmentType)
	var res = struct {
		response.Response
		ID
	}{}
	er := api.Upload(uri, "media", img, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})
	return &res.ID, er
}

// AddNews 新增永久图文素材
// POST，https://api.weixin.qq.com/cgi-bin/material/add_news?access_token=ACCESS_TOKEN
func AddNews(api *token.Api, news *Articles) (*ID, error) {

	uri := fmt.Sprintf("%s/cgi-bin/material/add_news?", consts.ApiPrefixUrl)
	buf, err := json.Marshal(news)
	if err != nil {
		return nil, err
	}
	type mediaResult struct {
		response.Response
		ID
	}
	res := mediaResult{}
	er := api.Request("POST", uri, buf, func(hp *http.Http, body []byte) error {
		if err = json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})
	return &res.ID, er
}

// GetMaterial 获取永久素材
// POST,https协议 https://api.weixin.qq.com/cgi-bin/material/get_material?access_token=ACCESS_TOKEN
func GetMaterial(api *token.Api, mediaId string) (*ArticlesResult, error) {

	uri := fmt.Sprintf("%s/cgi-bin/material/get_material?", consts.ApiPrefixUrl)
	id := ID{
		MediaId: mediaId,
	}
	buf, err := json.Marshal(&id)
	if err != nil {
		return nil, err
	}
	type mediaResult struct {
		response.Response
		ArticlesResult
	}
	res := mediaResult{}
	er := api.Request("POST", uri, buf, func(hp *http.Http, body []byte) error {
		resp := hp.GetResponse()
		if resp.IsFile() {
			res.Raw = body
			res.File = resp.FileName()
		}
		if utilapi.ContentTypeTo(resp.GetContentType()) == utilapi.JSON {
			if err = json.Unmarshal(body, &res); err != nil {
				return err
			}
		}
		return res.ReturnIf(api)
	})
	return &res.ArticlesResult, er
}

//删除永久图文素材
//POST https://api.weixin.qq.com/cgi-bin/material/del_material?access_token=ACCESS_TOKEN

//修改永久图文素材
//POST https://api.weixin.qq.com/cgi-bin/material/update_news?access_token=ACCESS_TOKEN
