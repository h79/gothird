package sign

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
)

// Sign 计算签名
func Sign(appKey, appSecret, method string, timestamp string, paramJson string) string {
	// 按给定规则拼接参数
	paramPattern := "app_key" + appKey + "method" + method + "param_json" + paramJson + "timestamp" + timestamp + "v2"
	signPattern := appSecret + paramPattern + appSecret
	return Hmac(signPattern, appSecret)
}

// Hmac 计算hmac
func Hmac(s string, appSecret string) string {
	h := hmac.New(sha256.New, []byte(appSecret))
	_, _ = h.Write([]byte(s))
	return hex.EncodeToString(h.Sum(nil))
}
