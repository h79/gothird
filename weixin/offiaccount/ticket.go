package offiaccount

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/goutils/common/data"
	"gitee.com/h79/goutils/common/http"
)

// GetTicket 由于获取api_ticket 的api 调用次数非常有限，频繁刷新api_ticket 会导致api调用受限，影响自身业务，开发者需在自己的服务存储与更新api_ticket
func GetTicket(tk token.Token, d data.D) (*token.Ticket, error) {
	ticketType := d.String(token.TicketType)
	if ticketType == "" {
		ticketType = "jsapi"
	}
	uri := fmt.Sprintf("%s/cgi-bin/ticket/getticket?type=%s&", consts.ApiPrefixUrl, ticketType)

	var res = struct {
		response.Response
		Ticket   string `json:"ticket"`
		ExpireIn int64  `json:"expires_in"`
	}{}
	var api = token.NewApi(tk, token.WithAppendData(d))
	err := api.Request("GET", uri, nil, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	return &token.Ticket{
		Ticket:   res.Ticket,
		ExpireIn: res.ExpireIn,
	}, nil
}
