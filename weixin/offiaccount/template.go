/*
*
  - https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1433751277

第一类 模板消息
  - 1 设置所属行业
  - 2 获取设置的行业信息
  - 3 获得模板ID
  - 4 获取模板列表
  - 5 删除模板
  - 6 发送模板消息
*/
package offiaccount

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/goutils/common/http"
)

// SetIndustry 设置所属行业,每月可修改行业1次
// 数据示例如下：
//
//	{
//	   "industry_id1":"1",
//	   "industry_id2":"4"
//	}
//
// POST https://api.weixin.qq.com/cgi-bin/template/api_set_industry?access_token=ACCESS_TOKEN
func SetIndustry(api *token.Api, req *IndustryReq) error {
	uri := fmt.Sprintf("%s/cgi-bin/template/api_set_industry?", consts.ApiPrefixUrl)

	buf, err := json.Marshal(&req)
	if err != nil {
		return err
	}
	res := response.Response{}
	return api.Request("POST", uri, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
}

// GetIndustry 获取设置的行业信息
// GET https://api.weixin.qq.com/cgi-bin/template/get_industry?access_token=ACCESS_TOKEN
// ErrorIf
// {
// "primary_industry":{"first_class":"运输与仓储","second_class":"快递"},
// "secondary_industry":{"first_class":"IT科技","second_class":"互联网|电子商务"}
// }
func GetIndustry(api *token.Api) (*IndustryS, error) {
	uri := fmt.Sprintf("%s/cgi-bin/template/get_industry?", consts.ApiPrefixUrl)

	type industryResult struct {
		response.Response
		IndustryS
	}

	res := industryResult{}
	er := api.Request("GET", uri, nil, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})
	return &res.IndustryS, er
}

// GetTemplateId 获得模板ID
// POST https://api.weixin.qq.com/cgi-bin/template/api_add_template?access_token=ACCESS_TOKEN
func GetTemplateId(api *token.Api, tmShortId string) (string, error) {
	uri := fmt.Sprintf("%s/cgi-bin/template/api_add_template?", consts.ApiPrefixUrl)

	type idReq struct {
		ShortId string `json:"template_id_short"`
	}
	req := idReq{
		ShortId: tmShortId,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return "", err
	}

	type idResult struct {
		response.Response
		Id string `json:"template_id"`
	}
	res := idResult{}
	er := api.Request("POST", uri, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	return res.Id, er
}

// GetTemplateList 获取模板列表
// GET https://api.weixin.qq.com/cgi-bin/template/get_all_private_template?access_token=ACCESS_TOKEN
func GetTemplateList(api *token.Api) ([]TemplateInfo, error) {
	uri := fmt.Sprintf("%s/cgi-bin/template/get_all_private_template?", consts.ApiPrefixUrl)
	type listResult struct {
		response.Response
		TMList []TemplateInfo `json:"template_list"`
	}
	res := listResult{}
	er := api.Request("GET", uri, nil, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})
	return res.TMList, er
}

// DeleteTemplate 删除模板
// POST https://api.weixin.qq.com/cgi-bin/template/del_private_template?access_token=ACCESS_TOKEN
// {
// "template_id" : "Dyvp3-Ff0cnail_CDSzk1fIc6-9lOkxsQE7exTJbwUE"
// }
func DeleteTemplate(api *token.Api, tmId string) error {
	uri := fmt.Sprintf("%s/cgi-bin/template/del_private_template?", consts.ApiPrefixUrl)

	type delReq struct {
		Id string `json:"template_id"`
	}
	req := delReq{
		Id: tmId,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return err
	}
	res := response.Response{}
	return api.Request("POST", uri, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
}
