package work

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/gothird/weixin/work/structs"
	"gitee.com/h79/goutils/common/http"
)

// TAGCreate 创建标签
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/tag/create?access_token=ACCESS_TOKEN
func TAGCreate(api *token.Api, tag *structs.Tag) (int64, error) {
	url := fmt.Sprintf("%s/cgi-bin/tag/create?", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(tag)
	if err != nil {
		return 0, err
	}

	var res = struct {
		response.Response
		TagId int64 `json:"tagid"`
	}{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return 0, err
	}
	return res.TagId, res.ErrorIf()
}

// TAGUpdate 更新标签名字
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/tag/update?access_token=ACCESS_TOKEN
func TAGUpdate(api *token.Api, tag *structs.Tag) error {
	url := fmt.Sprintf("%s/cgi-bin/tag/update?", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(tag)
	if err != nil {
		return err
	}

	var res = response.Response{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return err
	}
	if res.ErrCode != 0 {
		return res
	}
	return nil
}

// TAGDelete 删除标签
// 请求方式：GET（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/tag/delete?access_token=ACCESS_TOKEN&tagid=TAGID
func TAGDelete(api *token.Api, tagId int64) error {
	url := fmt.Sprintf("%s/cgi-bin/tag/delete?tagid=%v&", consts.WorkApiPrefixUrl, tagId)

	var res = response.Response{}
	if err := api.Request("GET", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return err
	}
	if res.ErrCode != 0 {
		return res
	}
	return nil
}

// TAGUser 获取标签成员
// 请求方式：GET（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/tag/get?access_token=ACCESS_TOKEN&tagid=TAGID
func TAGUser(api *token.Api, tagId int64) (*structs.UserTag, error) {
	url := fmt.Sprintf("%s/cgi-bin/tag/get?tagid=%v&", consts.WorkApiPrefixUrl, tagId)

	var res = struct {
		response.Response
		structs.UserTag
	}{}
	if err := api.Request("GET", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return nil, err
	}
	if res.ErrCode != 0 {
		return nil, res
	}
	return &res.UserTag, nil
}

// TAGAddUser 增加标签成员
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/tag/addtagusers?access_token=ACCESS_TOKEN
func TAGAddUser(api *token.Api, tag *structs.UserTag) error {
	url := fmt.Sprintf("%s/cgi-bin/tag/addtagusers?", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(tag)
	if err != nil {
		return err
	}

	var res = response.Response{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return err
	}
	if res.ErrCode != 0 {
		return res
	}
	return nil
}

// TAGDeleteUser 删除标签成员
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/tag/deltagusers?access_token=ACCESS_TOKEN
func TAGDeleteUser(api *token.Api, tag *structs.UserTag) error {
	url := fmt.Sprintf("%s/cgi-bin/tag/deltagusers?", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(tag)
	if err != nil {
		return err
	}

	var res = response.Response{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return err
	}
	if res.ErrCode != 0 {
		return res
	}
	return nil
}

// TAGGetList 获取标签列表
// 请求方式：GET（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/tag/list?access_token=ACCESS_TOKEN
func TAGGetList(api *token.Api) ([]structs.Tag, error) {
	url := fmt.Sprintf("%s/cgi-bin/tag/list?", consts.WorkApiPrefixUrl)

	var res = struct {
		response.Response
		Tags []structs.Tag `json:"taglist"`
	}{}
	if err := api.Request("GET", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return nil, err
	}
	if res.ErrCode != 0 {
		return nil, res
	}
	return res.Tags, nil
}
