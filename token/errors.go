package token

var (
	ErrNotSupported = Result{Code: -1, SubCode: "support", Msg: "不支持"}
	ErrTokenExpired = Result{Code: -2, SubCode: "token", Msg: "access token过期"}
	ErrTokenInvalid = Result{Code: -3, SubCode: "token", Msg: "access token无效"}
	ErrObjectNil    = Result{Code: -4, SubCode: "object", Msg: "对象为空"}
	ErrParam        = Result{Code: -5, SubCode: "param", Msg: "参数错误"}
)

func ErrEq(err error, o Result) bool {
	if res, ok := err.(Result); ok {
		return res.Code == o.Code || (o.SubCode != "" && res.SubCode == o.SubCode)
	}
	return false
}
