package feishu

import (
	"gitee.com/h79/gothird/config"
	"gitee.com/h79/gothird/token"
)

type FillContentCallback func(*config.Options, interface{}) string

func Notify(option *config.Options, content interface{}, call FillContentCallback) error {

	tk, err := token.GetToken(option.Group.CorpId)
	if err != nil {
		return err
	}
	api := token.NewApi(tk)

	head := MsgHead{
		MsgType: "text",
		ChatId:  option.Group.ChatId,
	}
	_, err = SendMessage(api,
		&MsgText{MsgHead: head,
			Content: ContentText{
				Text: call(option, content),
			},
		})
	return err
}
