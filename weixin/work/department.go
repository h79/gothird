package work

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/gothird/weixin/work/structs"
	"gitee.com/h79/goutils/common/http"
)

// DepartCreate 创建部门
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/department/create?access_token=ACCESS_TOKEN
func DepartCreate(api *token.Api, dep *structs.Department) (int64, error) {
	return DepartCreateV2(api, dep)
}

// DepartCreateV2 创建部门
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/department/create?access_token=ACCESS_TOKEN
func DepartCreateV2(api *token.Api, dep any) (int64, error) {
	var url = fmt.Sprintf("%s/cgi-bin/department/create?", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(dep)
	if err != nil {
		return 0, err
	}

	var res = struct {
		response.Response
		ID int64 `json:"id"`
	}{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return 0, err
	}
	return res.ID, res.ErrorIf()
}

// DepartUpdate 更新部门
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/department/update?access_token=ACCESS_TOKEN
func DepartUpdate(api *token.Api, dep *structs.Department) error {
	return DepartUpdateV2(api, dep)
}

// DepartUpdateV2 更新部门
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/department/update?access_token=ACCESS_TOKEN
func DepartUpdateV2(api *token.Api, dep any) error {
	var url = fmt.Sprintf("%s/cgi-bin/department/update?", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(dep)
	if err != nil {
		return err
	}

	var res = response.Response{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return err
	}
	return res.ErrorIf()
}

// DepartDelete 删除部门
// 请求方式：GET（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/department/delete?access_token=ACCESS_TOKEN&id=ID
func DepartDelete(api *token.Api, departId string) error {
	var url = fmt.Sprintf("%s/cgi-bin/department/delete?id=%s&", consts.WorkApiPrefixUrl, departId)

	var res = response.Response{}
	if err := api.Request("GET", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return err
	}

	return res.ErrorIf()
}

// DepartGetList 获取部门列表
// 企业通讯录安全特别重要，企业微信将持续升级加固通讯录接口的安全机制，以下是关键的变更点：
// 【重要】从2022年8月15日10点开始，“企业管理后台 - 管理工具 - 通讯录同步”的新增IP将不能再调用此接口，企业可通过「获取部门ID列表」接口获取部门ID列表。查看调整详情。
// 请求方式：GET（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/department/list?access_token=ACCESS_TOKEN&id=ID
func DepartGetList(api *token.Api, id string) ([]structs.Department, error) {
	var url = fmt.Sprintf("%s/cgi-bin/department/list?", consts.WorkApiPrefixUrl)
	if id != "" {
		url += fmt.Sprintf("id=%s&", id)
	}
	var res = struct {
		response.Response
		Department []structs.Department `json:"department"`
	}{}
	err := api.Request("GET", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	return res.Department, res.ErrorIf()
}

// DepartGetSimpleList 获取子部门ID列表
// 请求方式：GET（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/department/simplelist?access_token=ACCESS_TOKEN&id=ID
func DepartGetSimpleList(api *token.Api, id string) ([]structs.Department, error) {
	var url = fmt.Sprintf("%s/cgi-bin/department/simplelist?", consts.WorkApiPrefixUrl)
	if id != "" {
		url += fmt.Sprintf("id=%s&", id)
	}
	var res = struct {
		response.Response
		Department []structs.Department `json:"department_id"`
	}{}
	err := api.Request("GET", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	return res.Department, res.ErrorIf()
}

// DepartGetDetail 获取单个部门详情
// 企业通讯录安全特别重要，企业微信将持续升级加固通讯录接口的安全机制，以下是关键的变更点：
// 【重要】从2022年8月15日10点开始，“企业管理后台 - 管理工具 - 通讯录同步”的新增IP将不能再调用此接口，企业可通过「获取部门ID列表」接口获取部门ID列表。查看调整详情。
// 请求方式：GET（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/department/get?access_token=ACCESS_TOKEN&id=ID
func DepartGetDetail(api *token.Api, id string) (*structs.Department, error) {
	url := fmt.Sprintf("%s/cgi-bin/department/get?id=%s&", consts.WorkApiPrefixUrl, id)
	var res = struct {
		response.Response
		Department structs.Department `json:"department"`
	}{}
	err := api.Request("GET", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	return &res.Department, res.ErrorIf()
}
