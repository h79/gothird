package mini

import (
	"encoding/json"
	"gitee.com/h79/gothird/weixin/iv"
)

func GetUser(encryptData, sessionKey, ivStr string) (*User, error) {

	d, err := iv.Decode(encryptData, sessionKey, ivStr)
	if err != nil {
		return &User{}, err
	}
	user := User{}
	if err = json.Unmarshal(d, &user); err != nil {
		return nil, err
	}
	return &user, nil
}

func GetPhone(encryptData, sessionKey, ivStr string) (*Phone, error) {

	d, err := iv.Decode(encryptData, sessionKey, ivStr)
	if err != nil {
		return nil, err
	}
	phone := Phone{}
	if err = json.Unmarshal(d, &phone); err != nil {
		return nil, err
	}
	return &phone, nil
}
