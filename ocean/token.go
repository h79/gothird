package ocean

import (
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/goutils/common/data"
	"mime/multipart"
	"net/http"
)

var firstService *acsReq
var advService *acsReq

func init() {
	firstService = &acsReq{
		get:    token.GetEmpty,
		ref:    token.GetEmpty,
		oauth2: GetAccessToken,
	}

	advService = &acsReq{
		get:    token.GetEmpty,
		ref:    RefreshToken,
		oauth2: token.GetEmpty,
	}
}

func GetService() token.Service {
	return firstService
}

func GetAdvService() token.Service {
	return advService
}

func NewToken(app token.App) token.Token {
	return token.New(&app, firstService)
}

func NewAdvToken(app token.App) token.Token {
	return token.New(&app, advService)
}

var _ token.Service = (*acsReq)(nil)

type acsReq struct {
	check  token.CheckErrorFunc
	get    token.GenFunc
	ref    token.GenFunc
	oauth2 token.GenFunc
}

// Execute
// token.Service interface
func (req *acsReq) Execute(tk token.Token, cmd string, d data.D) (interface{}, error) {
	if cmd == token.NAccessToken {
		return req.get(tk, d)
	}
	if cmd == token.NRefreshToken {
		return req.ref(tk, d)
	}
	if cmd == token.NOauth2 {
		acs, err := req.oauth2(tk, d)
		if err != nil {
			return nil, err
		}
		if ad, ok := acs.(*AcsData); ok {
			aliasId, realId := genAlias(ad.AdIds)
			ch := &token.Child{
				App: token.App{
					ParentId: tk.GetId(),
					IsCache:  tk.IsCache(),
					Type:     token.OceanAdvType,
					Id:       token.GenId(tk.GetAppId(), aliasId),
					AppId:    aliasId,
					Name:     "adv",
				},
				VMap: token.VMap{},
			}
			token.SyncCreateChild(tk, advService, ch, acs, realId, func() {
			}) // 需要把广告主ID 与 别名ID 对应起来，几个id 对应一个 别名id
		}
		return acs, err
	}
	if cmd == token.NOauthAccount {
		return GetAuthAccount(tk, d)
	}
	return nil, token.ErrNotSupported
}

func (req *acsReq) BuildUrl(uri string, acsKey string, d data.D) string {
	return uri
}

func (req *acsReq) SetHead(h *http.Header, acsKey string) {
	h.Set("Content-Type", "application/json")
}

func (req *acsReq) CreateForm(w *multipart.Writer, field string, form interface{}) error {
	return nil
}

func (req *acsReq) SetCheckError(check token.CheckErrorFunc) {
	req.check = check
}

func (req *acsReq) CheckError(err error) error {
	if res, ok := err.(token.Result); ok {
		if req.check != nil {
			return req.check(&res)
		}
		if res.Code == 40103 {
			return token.ErrTokenExpired
		}
	}
	return err
}
