package service

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/gothird/weixin/work/structs"
	"gitee.com/h79/goutils/common/http"
)

//https://developer.work.weixin.qq.com/document

// GetPreAuthCode 获取预授权码
func GetPreAuthCode(api *token.Api) (*structs.PreAuthCode, error) {
	url := fmt.Sprintf("%s/get_pre_auth_code?", consts.WorkServiceApiPrefixUrl)
	type Result struct {
		response.Response
		structs.PreAuthCode
	}
	res := Result{}
	err := api.Request("GET", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	return &res.PreAuthCode, res.ErrorIf()
}

// GetAuthInfo 获取企业授权信息 POST（HTTPS）
// https://qyapi.weixin.qq.com/cgi-bin/service/get_auth_info?suite_access_token=SUITE_ACCESS_TOKEN
func GetAuthInfo(api *token.Api, authCorpId, permanentCode string) (*structs.Permanent, error) {
	url := fmt.Sprintf("%s/get_auth_info?", consts.WorkServiceApiPrefixUrl)

	var in = authCorp{
		AuthCorpId:    authCorpId,
		PermanentCode: permanentCode,
	}
	buf, _ := json.Marshal(&in)

	var res = struct {
		response.Response
		structs.Permanent
	}{}
	err := api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	return &res.Permanent, res.ErrorIf()
}

// GetAdminList 获取应用的管理员列表
// 第三方服务商可以用此接口获取授权企业中某个第三方应用的管理员列表(不包括外部管理员)，以便服务商在用户进入应用主页之后根据是否管理员身份做权限的区分。
// 该应用必须与SUITE_ACCESS_TOKEN对应的suiteid对应，否则没权限查看
// POST（HTTPS）
// https://qyapi.weixin.qq.com/cgi-bin/service/get_admin_list?suite_access_token=SUITE_ACCESS_TOKEN
func GetAdminList(api *token.Api, authCorpId string, agentId int32) (*structs.AdminList, error) {
	url := fmt.Sprintf("%s/get_admin_list?", consts.WorkServiceApiPrefixUrl)

	var in = struct {
		AuthCorpId string `json:"auth_corpid"`
		AgentId    int32  `json:"agentid"`
	}{
		AuthCorpId: authCorpId,
		AgentId:    agentId,
	}
	buf, _ := json.Marshal(&in)

	var res = struct {
		response.Response
		structs.AdminList
	}{}
	err := api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	return &res.AdminList, res.ErrorIf()
}

// CorpIdToOpenCorpId 明文corpid转换为加密corpid POST（HTTPS）
// 为更好地保护企业与用户的数据，第三方应用获取的corpid不再是明文的corpid，将升级为第三方服务商级别的加密corpid（了解更多）。第三方可以将已有的明文corpid转换为第三方的加密corpid。
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/service/corpid_to_opencorpid?provider_access_token=ACCESS_TOKEN
func CorpIdToOpenCorpId(api *token.Api, corpId string) (string, error) {
	url := fmt.Sprintf("%s/corpid_to_opencorpid?", consts.WorkServiceApiPrefixUrl)

	var in = struct {
		CorpId string `json:"corpid"`
	}{
		CorpId: corpId,
	}
	buf, _ := json.Marshal(&in)
	var res = struct {
		response.Response
		OpenCorpId string `json:"open_corpid"`
	}{}
	err := api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return "", err
	}
	return res.OpenCorpId, res.ErrorIf()
}

// GetAppQRCode 获取应用二维码 POST（HTTPS）
// 用于获取第三方应用二维码。
// 要求第三方应用是已上线的第三方应用,不能是代应用
// https://qyapi.weixin.qq.com/cgi-bin/service/get_app_qrcode?suite_access_token=SUITE_ACCESS_TOKEN
func GetAppQRCode(api *token.Api, qr structs.QRCodeReq) (string, error) {
	url := fmt.Sprintf("%s/get_app_qrcode?", consts.WorkServiceApiPrefixUrl)

	buf, _ := json.Marshal(&qr)
	var res = struct {
		response.Response
		Qrcode string `json:"qrcode"`
	}{}
	err := api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return "", err
	}
	return res.Qrcode, res.ErrorIf()
}

// GetCustomizedAuthUrl 获取带参授权链接
// 该API用于获取代开发自建应用授权链接，用于生成带参临时二维码。
// 可用来生成二维码的授权url，需要开发者自行生成为二维码
// 请求方式：POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/service/get_customized_auth_url?provider_access_token=PROVIDER_ACCESS_TOKEN
func GetCustomizedAuthUrl(api *token.Api, state string, templateId []string) (*structs.CustomizedAuthUrl, error) {

	url := fmt.Sprintf("%s/get_customized_auth_url?", consts.WorkServiceApiPrefixUrl)
	var req = struct {
		State          string   `json:"state"`
		TemplateIdList []string `json:"templateid_list"`
	}{
		State:          state,
		TemplateIdList: templateId,
	}
	buf, _ := json.Marshal(&req)
	var res = struct {
		response.Response
		structs.CustomizedAuthUrl
	}{}
	err := api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	return &res.CustomizedAuthUrl, res.ErrorIf()
}

// GetUserInfo3rd 获取访问用户身份
// 请求方式：GET（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/service/auth/getuserinfo3rd?suite_access_token=SUITE_ACCESS_TOKEN&code=CODE
func GetUserInfo3rd(api *token.Api, code string) (*structs.UserInfo3rd, error) {
	url := fmt.Sprintf("%s/auth/getuserinfo3rd?code=%s&", consts.WorkServiceApiPrefixUrl, code)
	var res = struct {
		response.Response
		structs.UserInfo3rd
	}{}
	err := api.Request("GET", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	return &res.UserInfo3rd, res.ErrorIf()
}

// GetUserDetail3rd 获取访问用户敏感信息
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/service/auth/getuserdetail3rd?suite_access_token=SUITE_ACCESS_TOKEN
func GetUserDetail3rd(api *token.Api, ticket string) (*structs.UserDetail3rd, error) {
	url := fmt.Sprintf("%s/get_customized_auth_url?", consts.WorkServiceApiPrefixUrl)
	var req = struct {
		Ticket string `json:"user_ticket"`
	}{
		Ticket: ticket,
	}
	buf, _ := json.Marshal(&req)
	var res = struct {
		response.Response
		structs.UserDetail3rd
	}{}
	err := api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	return &res.UserDetail3rd, res.ErrorIf()
}

// GetLoginInfo 获取登录用户信息 [扫码授权登录]
// 第三方可通过如下接口，获取登录用户的信息。建议用户以返回信息中的corpid及userid为主键匹配用户
// 授权登录服务商的网站时，使用应用提供商的provider_access_token
// 请求方式：POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/service/get_login_info?access_token=PROVIDER_ACCESS_TOKEN
func GetLoginInfo(api *token.Api, authCode string) (*structs.LoginInfo, error) {
	url := fmt.Sprintf("%s/get_login_info?", consts.WorkServiceApiPrefixUrl)

	var in = struct {
		AuthCode string `json:"auth_code"`
	}{
		AuthCode: authCode,
	}
	buf, _ := json.Marshal(&in)

	var res = struct {
		response.Response
		structs.LoginInfo
	}{}
	err := api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	return &res.LoginInfo, res.ErrorIf()
}

type authCorp struct {
	AuthCorpId    string `json:"auth_corpid"`
	PermanentCode string `json:"permanent_code"`
}

// SetSessionInfo 设置授权配置
// 该接口可对某次授权进行配置。可支持测试模式（应用未发布时）。
// 请求方式：POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/service/set_session_info?suite_access_token=SUITE_ACCESS_TOKEN
func SetSessionInfo(api *token.Api, preAuthCode string, sessionInfo structs.SessionInfo) error {
	url := fmt.Sprintf("%s/set_session_info?", consts.WorkServiceApiPrefixUrl)

	var in = struct {
		PreAuthCode string              `json:"pre_auth_code"`
		SessionInfo structs.SessionInfo `json:"session_info"`
	}{
		PreAuthCode: preAuthCode,
		SessionInfo: sessionInfo,
	}
	buf, _ := json.Marshal(&in)

	var res = response.Response{}
	err := api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return err
	}
	return res.ErrorIf()
}

// GetAgentPermissions 获取应用权限详情
// 该API用于获取代开发应用或第三方应用用户授权的权限详情。
// 请求方式：POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/agent/get_permissions?access_token=ACCESS_TOKEN
// 权限说明： https://developer.work.weixin.qq.com/document/path/99052
func GetAgentPermissions(api *token.Api) ([]string, error) {
	url := fmt.Sprintf("%s/agent/get_permissions?", consts.WorkApiPrefixUrl)
	var res = struct {
		response.Response
		Permissions []string `json:"app_permissions"`
	}{}
	err := api.Request("POST", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	return res.Permissions, res.ErrorIf()
}
