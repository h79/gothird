package weixin

import (
	"encoding/json"
	"gitee.com/h79/gothird/weixin/iv"
	"gitee.com/h79/gothird/weixin/mini"
	"testing"
)

func TestNewMsgCrypt(t *testing.T) {

	//crypt := algorithm2.NewCrypt("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ99876543")
	//
	////nonce := random.GenerateString(8)
	////timestamp := stringutil.Int64ToString(time.Now().Unix())
	////
	////msg := "122242FSFSAFSAFS"
	////
	////t.Log("input msg:", msg, "nonce: ", nonce, ",timestamp: ", timestamp)
	////
	////sign, en, err := crypt.Encrypt(msg, nonce, timestamp)
	////if err != nil {
	////	t.Log("encrypt fail", err)
	////}
	//////to xml
	////xml := crypt.GenerateXmlMsg(msg, timestamp, nonce)
	////
	////t.Log("xml: ", xml)
	////signature=52720539dfc4f74ad47c35ffbcc847cb11e0c1f5&timestamp=1609751689&nonce=1120740874&encrypt_type=aes&msg_signature=91466319d9b8e4597cd2702bd367f7b8983899a8
	//nonce := "1528346011"
	//timestamp := "1611562124"
	//en := "PG+q5frK3otJWT7C4SWzwVAVbFxgyB/xIfztK+FFImE5x2SQ+hTGOivP8dyvpTUN0KDSVUFCS3sU7Iaj+JTc15NBKE5MvI9G0DKV63P/lVzqtiXPZ6qGlpZzu9QK6tUm2+kCTtiaziN4xRFAoJA3NNRM2TesZAuZ7rAIznMbuCOQaiA+N5KkEyF+fj3JXztaZbQpzkW5mfiEMvKb3WFEKpLKgi55vXAxrQRYlNabtObpEtLwcucfi4BKRNCaGkrneaBaNP3rp2xkSl5ggv8tmCSdRh9T06vghLfo2PC46cdgdYfeAFQVoXjj0SlE2I1AvUtkt3rGfV6unf1FDh2PVBCiUnxytnMF8U6TLu/Rtjz22txQJY4Xggq8alKyr4xE1ddsHooFMJd9H+FJf7kr0MvZeQ7SQ9HmrZ1eNTMpWTk="
	//sign := "6698c939c272395e109d5de32f4c42fceb17fba3"
	////sign := "52720539dfc4f74ad47c35ffbcc847cb11e0c1f5"
	//content, from_id, _ := crypt.DecryptMsg(sign, timestamp, nonce, en)
	//t.Log("decrypt msg: ", string(content), ",from_id: ", from_id)
	//
	//sessionKey := "SE/BLocg+sMlvcKmxm8vQA=="
	//encryptedData := "7SfFtStsHqKZYhbIkke3BH2bCRzGD15T0jEiUtuksrl9lDeHm9LsPmswJymBXuinPCiXkZhd/uq7s7pACTvbWuvvoKEwz5fAJ6Vr9bTx79XVxiIN4r+Fwm6QHO9DjPkFrxTGAZvMYLyH6IOyOV/nmmlMoBM3G4peSnBi1qCYukwlyCMNp67lb93wSiPAoI7eRhYYw8ayPTsZ/MAJ9CBBUiCwM5aFOUWrMKNTikeq7YVjNCv7KCz0LJTrMKda0YMS0J/034L8x9vJ1OnIkxlWVMQEy/f55IfWVHI1I1fSKd5azzyVKXCbWDpU0PLJnU8XM/l4L7ZUlDOcRMR5KQVGhB9rIjVkykdXUPQK87v8lpnitslK06XceOJqDjK6mRkhJWOYpFUozZa6idFV6xmLZX8bkBsLxczzp1h/satEH7rIz3nKbxd3O1c+3dI2soSt8qFtaumcGdwhenTm+at0gxccAp8JD8PZiB5ZDLTofZIQ4RmI004SIExYUDZUje9mZO+3aC8McVwzrEyK7NKD/NZ5/dYPgDRwzBl1Vm99niY="
	//ivx := "z3tGYrgMcbLzd0qXqZuduQ=="

	//sessionKey := "XlgQzuTnZSURC2jztWl1gw=="
	//ivx := "/SRo6Lxc73ncvXszMAhgTA=="
	//encryptedData := "gtk6g4Z3vqNgppCletBkYdP+ZAY7LMbOgfsGMPmjzD1IgXyHYa+TSEmt0s8WlUBbiitc6x0LEyhCSd8PzvArJPSgqVngTnWQ09ccP3y/bzKOCWGcuT2gwakXKNOwYkYGyQv2XDqUbUPXjGJMMdj5cdAk95UyN8nTznEyO19h/pft0N3m1heWlfZuh1uWZGfNOksl72+sA8x1GcO0d0kv7w=="
	sessionKey := "tiihtNczf5v6AKRyjwEUhQ=="
	encryptedData := "CiyLU1Aw2KjvrjMdj8YKliAjtP4gsMZMQmRzooG2xrDcvSnxIMXFufNstNGTyaGS9uT5geRa0W4oTOb1WT7fJlAC+oNPdbB+3hVbJSRgv+4lGOETKUQz6OYStslQ142dNCuabNPGBzlooOmB231qMM85d2/fV6ChevvXvQP8Hkue1poOFtnEtpyxVLW1zAo6/1Xx1COxFvrc2d7UL/lmHInNlxuacJXwu0fjpXfz/YqYzBIBzD6WUfTIF9GRHpOn/Hz7saL8xz+W//FRAUid1OksQaQx4CMs8LOddcQhULW4ucetDf96JcR3g0gfRK4PC7E/r7Z6xNrXd2UIeorGj5Ef7b1pJAYB6Y5anaHqZ9J6nKEBvB4DnNLIVWSgARns/8wR2SiRS7MNACwTyrGvt9ts8p12PKFdlqYTopNHR1Vf7XjfhQlVsAJdNiKdYmYVoKlaRv85IfVunYzO0IKXsyl7JCUjCpoG20f0a04COwfneQAGGwd5oa+T8yO5hzuyDb/XcxxmK01EpqOyuxINew=="
	ivx := "r7BXXKkLb8qrSNn05n0qiA=="

	d, _ := iv.Decode(encryptedData, sessionKey, ivx)
	phone := mini.Phone{}
	if err2 := json.Unmarshal(d, &phone); err2 != nil {
		t.Error(err2)
	}
	t.Log(phone)
}
