package order

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/fxg/consts"
	"gitee.com/h79/gothird/fxg/params"
	"gitee.com/h79/gothird/fxg/response"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/goutils/common/data"
	"gitee.com/h79/goutils/common/http"
)

// batchSensitive
// order/batchSensitive?
func batchSensitive(api *token.Api, d data.D) (interface{}, error) {

	uri := fmt.Sprintf("%s/order/batchSensitive?%s", consts.ApiPrefixUrl, params.UrlParams(api.AppId(), api.Secret(), "order.batchSensitive", d))
	type sensResult struct {
		response.Response
		Data interface{} `json:"data,omitempty"`
	}
	res := sensResult{}
	er := api.Request("POST", uri, nil, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})

	return res.Data, er
}
