package ocean

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/token/access"
	"gitee.com/h79/goutils/common/data"
	"gitee.com/h79/goutils/common/http"
	"gitee.com/h79/goutils/common/logger"
)

func GetAccessToken(tk token.Token, d data.D) (access.Token, error) {
	p := tk.Parent()
	req := struct {
		AppId     string `json:"app_id"`
		Secret    string `json:"secret"`
		GrantType string `json:"grant_type,omitempty"`
		Code      string `json:"auth_code"`
	}{
		AppId:     p.GetAppId(),
		Secret:    p.GetSecret(),
		GrantType: "auth_code",
		Code:      d.String("auth_code"),
	}
	uri := fmt.Sprintf("%s/oauth2/access_token/", ApiPrefixUrl)

	return getToken(uri, req.AppId, &req)
}

func RefreshToken(tk token.Token, d data.D) (access.Token, error) {
	p := tk.Parent()
	req := struct {
		AppId     string `json:"app_id"`
		Secret    string `json:"secret"`
		GrantType string `json:"grant_type,omitempty"`
		RefToken  string `json:"refresh_token"`
	}{
		AppId:     p.GetAppId(),
		Secret:    p.GetSecret(),
		GrantType: "refresh_token",
		RefToken:  d.String("refToken"),
	}
	uri := fmt.Sprintf("%s/oauth2/refresh_token/", ApiPrefixUrl)

	return getToken(uri, req.AppId, &req)
}

func GetAuthAccount(tk token.Token, d data.D) (*AuthInfo, error) {
	parent := tk.Parent()
	req := struct {
		AppId  string `json:"app_id"`
		Secret string `json:"secret"`
		Token  string `json:"access_token"`
	}{
		AppId:  parent.GetAppId(),
		Secret: parent.GetSecret(),
		Token:  d.String("access_token"),
	}
	uri := fmt.Sprintf("%s/oauth2/advertiser/get/", ApiPrefixUrl)
	buf, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}
	hp := http.Http{}
	body, err := hp.DoBytes("GET", uri, buf)
	if err != nil {
		return nil, err
	}

	type listResult struct {
		Response
		D AuthInfo `json:"data,omitempty"`
	}
	res := listResult{}
	if err = json.Unmarshal(body, &res); err != nil {
		return nil, err
	}
	logger.NDebug("Ocean: get oauth2 advertiser data: %+v", res)
	if res.Code != 0 {
		return nil, token.Error(res.Code, res.Msg)
	}
	return &res.D, nil
}

func getToken(uri string, appId string, r interface{}) (*AcsData, error) {

	buf, err := json.Marshal(r)
	if err != nil {
		return nil, err
	}
	hp := http.Http{}
	body, err := hp.DoBytes("POST", uri, buf)
	if err != nil {
		return nil, err
	}

	res := tokenResult{}
	if err = json.Unmarshal(body, &res); err != nil {
		return nil, err
	}
	logger.Debug("Ocean: get token for appid= %s, response data: %+v", appId, res)
	if res.Code != 0 {
		return nil, token.Error(res.Code, res.Msg)
	}
	return &res.D, nil
}

type appInfo struct {
	AppId     string `json:"app_id"`
	Secret    string `json:"secret"`
	GrantType string `json:"grant_type,omitempty"`
}

type tokenResult struct {
	Response
	D AcsData `json:"data"`
}

type AcsData struct {
	AccessToken
	AdIds []int64 `json:"advertiser_ids,omitempty"`
}

type AuthInfo struct {
	Advertisers []AuthAdvertiser `json:"list,omitempty"`
}

type AuthAdvertiser struct {
	AdvertiserId   int64 `json:"advertiser_id"`
	AdvertiserName int64 `json:"advertiser_name"`
	AdvertiserRole int64 `json:"advertiser_role"`
	IsValid        int64 `json:"is_valid"`
	AccountRole    int64 `json:"account_role"`
}
