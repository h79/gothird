package app3

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/token/access"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/goutils/common/data"
	"gitee.com/h79/goutils/common/http"
)

func GetComponentAccessToken(tk token.Token, d data.D) (access.Token, error) {
	ticket, err := tk.GetCode("component_verify_ticket")
	if err != nil {
		return nil, err
	}
	d["appid"] = tk.GetAppId()
	d["ticket"] = ticket
	return GetAccessToken(tk, d)
}

// GetAccessToken
// https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/api/component_access_token.html
func GetAccessToken(tk token.Token, d data.D) (access.Token, error) {

	url := fmt.Sprintf("%s/cgi-bin/component/api_component_token", consts.ApiPrefixUrl)
	type info struct {
		AppId  string `json:"component_appid"`
		Secret string `json:"component_appsecret,omitempty"`
		Ticket string `json:"component_verify_ticket,omitempty"`
	}
	in := info{
		AppId:  tk.GetAppId(),
		Secret: tk.GetSecret(),
		Ticket: d.String("ticket"),
	}
	buf, err := json.Marshal(&in)
	if err != nil {
		return nil, err
	}
	hp := http.Http{}
	body, err := hp.DoBytes("POST", url, buf)
	if err != nil {
		return nil, err
	}
	type Result struct {
		response.Response
		AccessToken
	}

	res := Result{}
	if er := json.Unmarshal(body, &res); er != nil {
		return nil, er
	}
	return res.AccessToken, res.ErrorIf()
}

// ClearQuota
// 所有api调用（包括第三方帮其调用）次数进行清零,每月10次
// https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/Official_Accounts/Official_account_interface.html
func ClearQuota(api *token.Api) error {
	uri := fmt.Sprintf("%s/cgi-bin/component/clear_quota?", consts.ApiPrefixUrl)

	// 清除API次数
	type quotaReq struct {
		AppId string `json:"appid"`
	}
	Req := quotaReq{
		AppId: api.AppId(),
	}
	buf, err := json.Marshal(&Req)
	if err != nil {
		return err
	}
	res := response.Response{}
	err = api.Request("POST", uri, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	return err
}
