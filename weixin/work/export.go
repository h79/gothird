package work

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/gothird/weixin/work/structs"
	"gitee.com/h79/goutils/common/http"
)

// ExportSimpleUser 导出成员
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/export/simple_user?access_token=ACCESS_TOKEN
func ExportSimpleUser(api *token.Api, req *structs.Export) (string, error) {
	var url = fmt.Sprintf("%s/cgi-bin/export/simple_user?", consts.WorkApiPrefixUrl)
	return export(api, url, req)
}

// ExportUserDetail 导出成员详情
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/export/user?access_token=ACCESS_TOKEN
func ExportUserDetail(api *token.Api, req *structs.Export) (string, error) {
	var url = fmt.Sprintf("%s/cgi-bin/export/user?", consts.WorkApiPrefixUrl)

	return export(api, url, req)
}

// ExportDepartment 导出部门
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/export/user?access_token=ACCESS_TOKEN
func ExportDepartment(api *token.Api, req *structs.Export) (string, error) {
	var url = fmt.Sprintf("%s/cgi-bin/export/department?", consts.WorkApiPrefixUrl)

	return export(api, url, req)
}

// ExportTagUser 导出标签成员
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/export/taguser?access_token=ACCESS_TOKEN
func ExportTagUser(api *token.Api, req *structs.Export, tagId int) (string, error) {
	var url = fmt.Sprintf("%s/cgi-bin/export/taguser?", consts.WorkApiPrefixUrl)
	var body = struct {
		TagId          int    `json:"tagid"`
		EncodingAesKey string `json:"encoding_aeskey"`
		BlockSize      int    `json:"block_size"`
	}{
		TagId:          tagId,
		EncodingAesKey: req.EncodingAesKey,
		BlockSize:      req.BlockSize,
	}
	return export(api, url, &body)
}

func ExportResult(api *token.Api, jobId string) (*structs.ExportResult, error) {
	var url = fmt.Sprintf("%s/cgi-bin/export/get_result?jobid=%s&", consts.WorkApiPrefixUrl, jobId)

	var res = struct {
		response.Response
		structs.ExportResult
	}{}
	if err := api.Request("GET", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return nil, err
	}
	return &res.ExportResult, res.ErrorIf()
}

func export(api *token.Api, url string, req interface{}) (string, error) {

	buf, err := json.Marshal(req)
	if err != nil {
		return "", err
	}

	var res = struct {
		response.Response
		JobId string `json:"jobid"`
	}{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return "", err
	}
	return res.JobId, res.ErrorIf()
}
