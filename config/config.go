package config

import (
	"gitee.com/h79/goutils/common/server"
)

type Options struct {
	Email     Email     `json:"email"`
	Group     Group     `json:"group"`
	SmsConfig SmsConfig `json:"sms"`
}

type Email struct {
	Server   server.Address `json:"server"`
	Username string         `json:"user"`
	Password string         `json:"pwd"`
	EmailTo  string         `json:"emailTo"` //;分开
}

type Group struct {
	Method  string `json:"method"` // = chat, agent = agentid, root
	CorpId  string `json:"corpid"`
	ChatId  string `json:"chatid"`  // 群ID(运维监控）
	User    string `json:"user"`    // "HongChenLianXin|huqiuyun"
	AgentId int32  `json:"agentid"` //企业应用
}

type SmsConfig struct {
	Service   string `json:"service"` //是阿里，还是别的家
	RegionId  string `json:"regionid"`
	Domain    string `json:"domain"`
	AccessKey string `json:"acskey"`
	Secret    string `json:"secret"`
	SignName  string `json:"sign"`
	TempCode  string `json:"tempcode"`
	PhoneNums string `json:"phoneTo"` //,分开
}

func (c *SmsConfig) IsEmpty() bool {
	return c.PhoneNums == "" || c.Service == "" || c.RegionId == "" || c.AccessKey == "" || c.Secret == ""
}
