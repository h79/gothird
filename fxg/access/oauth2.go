package access

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/fxg/consts"
	"gitee.com/h79/gothird/fxg/params"
	"gitee.com/h79/gothird/fxg/response"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/token/access"
	"gitee.com/h79/goutils/common/data"
	"gitee.com/h79/goutils/common/http"
)

// GetAccessToken
// https://openapi-fxg.jinritemai.com/token/create?app_key=your_app_key&method=token.create&param_json={"code":"your_code","grant_type":"authorization_code"}&timestamp=2021-06-07 17:34:46&v=2&sign=your_sign_here
func GetAccessToken(tk token.Token, d data.D) (access.Token, error) {
	parent := tk.Parent()

	param := data.D{}
	param["code"] = d.String("code")
	param["grant_type"] = "authorization_code"

	pa := params.UrlParams(parent.GetAppId(), parent.GetSecret(), "token.create", param)

	url := fmt.Sprintf("%s/token/create?%s", consts.ApiPrefixUrl, pa)

	return getToken(url)
}

// RefreshToken
// https://openapi-fxg.jinritemai.com/token/refresh?app_key=your_app_key&method=token.refresh&param_json={"grant_type":"refresh_token","refresh_token":"your_refresh_token"}&timestamp=2021-06-07 17:36:56&v=2&sign=your_sign
func RefreshToken(tk token.Token, d data.D) (access.Token, error) {
	parent := tk.Parent()

	param := data.D{}
	param["refresh_token"] = d.String("refToken")
	param["grant_type"] = "refresh_token"

	pa := params.UrlParams(parent.GetAppId(), parent.GetSecret(), "token.refresh", param)

	url := fmt.Sprintf("%s/token/refresh?%s", consts.ApiPrefixUrl, pa)
	return getToken(url)
}

func getToken(uri string) (*AcsData, error) {
	hp := http.Http{}
	body, err := hp.DoBytes("GET", uri, nil)
	if err != nil {
		return nil, err
	}

	res := tokenResult{}
	if err = json.Unmarshal(body, &res); err != nil {
		return nil, err
	}
	return &res.D, res.ErrorIf()
}

type tokenResult struct {
	response.Response
	D AcsData `json:"data"`
}

type AcsData struct {
	Token
	ShopId   int64  `json:"shop_id,omitempty"`
	ShopName string `json:"shop_name,omitempty"`
}
