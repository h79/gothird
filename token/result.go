package token

import "fmt"

const (
	codeTryCount = -20000
)

var _ ResultObject = (*Result)(nil)

type Result struct {
	Code    int32
	SubCode string
	Msg     string
	Raw     interface{}
}

func (r Result) Is(err error) bool {
	if res, ok := err.(Result); ok {
		return r.Code == res.Code
	}
	return false
}

func (r Result) Error() string {
	return fmt.Sprintf("errcode: %v, subcode: %s, errmsg: %s, Raw: '%+v'", r.Code, r.SubCode, r.Msg, r.Raw)
}

func (r Result) GetCode() int32 {
	return r.Code
}
func (r Result) GetSubCode() string {
	return r.SubCode
}

func (r Result) GetMsg() string {
	return r.Msg
}

func (r Result) Clone(subCode string, err string) Result {
	return Result{Code: r.Code, SubCode: subCode, Msg: err}
}

func Error(code int32, err string) Result {
	return Result{
		Code: code,
		Msg:  err,
	}
}

func SubError(code int32, subCode string, err string) Result {
	return Result{
		Code:    code,
		SubCode: subCode,
		Msg:     err,
	}
}

func TryCountError() Result {
	return Result{
		Code: codeTryCount,
		Msg:  "",
	}
}

func Succeed() Result {
	return Result{
		Code: 0,
		Msg:  "",
	}
}

func (r Result) Ok() bool {
	return r.Code == 0
}

func (r Result) IsTokenError() bool {
	return false
}

func (r Result) IsTryCount() bool {
	return r.Code == codeTryCount
}
