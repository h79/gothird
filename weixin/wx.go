package weixin

import (
	"gitee.com/h79/gothird/token"
	wxerr "gitee.com/h79/gothird/weixin/errors"
	"gitee.com/h79/gothird/weixin/offiaccount"
	"gitee.com/h79/goutils/common/data"
	"net/http"
	"strconv"
)

var firstService *acsReq
var webService *acsReq

func init() {
	firstService = &acsReq{
		name: token.WxType,
		get:  GetAccessToken,
		ref:  token.GetEmpty,
		auth: offiaccount.Oauth2,
	}

	webService = &acsReq{
		name: token.WxWebType,
		get:  token.GetEmpty,
		ref:  offiaccount.Oauth2RefreshToken,
		auth: token.GetEmpty,
	}
}

func GetService() token.Service {
	return firstService
}

func GetWebService() token.Service {
	return webService
}

func NewToken(app token.App) token.Token {
	return token.New(&app, firstService)
}

func NewWebToken(app token.App) token.Token {
	return token.New(&app, webService)
}

type acsReq struct {
	name  string
	check token.CheckErrorFunc
	get   token.GenFunc
	ref   token.GenFunc
	auth  token.GenFunc
}

func (req *acsReq) GetName() string {
	return req.name
}

// Execute token.Service interface
func (req *acsReq) Execute(tk token.Token, cmd string, d data.D) (interface{}, error) {

	if cmd == token.NTicket {
		return offiaccount.GetTicket(tk, d)
	}
	if cmd == token.NAccessToken {
		return req.get(tk, d)
	}
	if cmd == token.NRefreshToken {
		return req.ref(tk, d)
	}
	if cmd == token.NOauth2 { // 网页，个人授权，返回一个 openid
		acs, err := req.auth(tk, d)
		if err != nil {
			return nil, err
		}
		return acs, err
	}
	return nil, token.ErrNotSupported
}

func (req *acsReq) BuildUrl(uri string, acsKey string, d data.D) string {
	return token.DefaultUrl(acsKey, uri)
}

func (req *acsReq) SetHead(h *http.Header, acsKey string) {
}

func (req *acsReq) SetCheckError(check token.CheckErrorFunc) {
	req.check = check
}

func (req *acsReq) CheckError(err error) error {
	if res, ok := err.(token.Result); ok {
		if req.check != nil {
			return req.check(&res)
		}
		if wxerr.IsExpired(res.Code) {
			return token.ErrTokenExpired.Clone(strconv.FormatInt(int64(res.Code), 10), res.Msg)
		}
	}
	return err
}
