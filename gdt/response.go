package gdt

import (
	"gitee.com/h79/gothird/token"
)

type Response struct {
	Code int32  `json:"code"`
	Msg  string `json:"message"`
}

func (r Response) ErrorIf() error {
	if r.Code == Success {
		return nil
	}
	if AccessTokenExpire == r.Code {
		return token.ErrTokenExpired
	}
	if InvalidAccessToken == r.Code {
		return token.ErrTokenInvalid
	}
	return token.Result{Code: r.Code, Msg: r.Msg}
}

func (r Response) ReturnIf(api *token.Api) error {
	if r.Code == 0 {
		return nil
	}
	if AccessTokenExpire == r.Code {
		api.ResetToken()
		return token.ErrTokenExpired
	}
	if InvalidAccessToken == r.Code {
		api.ResetToken()
		return token.ErrTokenInvalid
	}
	return token.Result{Code: r.Code, Msg: r.Msg}
}
