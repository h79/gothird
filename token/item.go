package token

import (
	"fmt"
	"gitee.com/h79/gothird/token/validate"
	"gitee.com/h79/goutils/common/logger"
	"sync"
)

type item struct {
	App    *App `json:"app,omitempty"`
	VMap   VMap `json:"vp,omitempty"`
	vmu    sync.RWMutex
	vMapAt updateTime
	appAt  updateTime
	tk     Token
}

func (it *item) read(key string) *validate.Data {
	if it == nil {
		return &validate.Empty
	}
	it.vmu.RLock()
	if v, found := it.VMap[key]; found {
		it.vmu.RUnlock()
		return v
	}
	it.vmu.RUnlock()
	return nil
}

func (it *item) readData(key string) string {
	if it == nil {
		return ""
	}
	it.vmu.RLock()
	if v, found := it.VMap[key]; found {
		it.vmu.RUnlock()
		return v.Data
	}
	it.vmu.RUnlock()
	return ""
}

func (it *item) write(key string, v *validate.Data) {
	logger.D("Token", "write, app= %s,validate= %v", it.App.String(), v)
	it.vmu.Lock()
	defer it.vmu.Unlock()
	if d, found := it.VMap[key]; found {
		d.Data = v.Data
		d.Expire = v.Expire
	} else {
		it.VMap[key] = v
	}
	it.vMapAt.Update()
}

func (it *item) clear(key string) (*validate.Data, error) {
	it.vmu.Lock()
	defer it.vmu.Unlock()
	if d, found := it.VMap[key]; found {
		delete(it.VMap, key)
		it.vMapAt.Update()
		return d, nil
	}
	return nil, fmt.Errorf("not found for id= %s, key= '%s'", it.App.Id, key)
}

func (it *item) vMapExpire() bool {
	var ac string
	var err error
	var fSave = false //需要保存
	var vd *validate.Data
	var keys = make([]string, 0)

	// 这个如果数据太大，会有效率问题，需要改进。
	it.vmu.RLock()
	for k, v := range it.VMap {
		if v.IsValid() && v.IsExpired() {
			keys = append(keys, k)
		}
	}
	it.vmu.RUnlock()

	for i := range keys {
		logger.W("Token", "expire, app= %s, key= %s", it.App.String(), keys[i])
		switch keys[i] {
		case NRefreshToken:
			ac = it.readData(keys[i])
			if ac == "" {
				continue
			}
			ac, err = it.tk.RefreshAccessToken(ac)
			if err != nil {
				logger.E("Token", "expire=>'%s' to refresh failure for id= %s, err= %s", keys[i], it.App.Id, err)
			} else {
				logger.N("Token", "expire=>'%s' to refresh ok for id= %s, ac= %s", keys[i], it.App.Id, ac)
			}
		case NAccessToken:
			vd, err = it.clear(keys[i])
			if err == nil {
				logger.D("Token", "expire=>'%s' to clear for id= %s, old value= %s", keys[i], it.App.Id, vd.String())
				fSave = true
			} else {
				logger.W("Token", "expire=>'%s' to clear failure for id= %s, err= %s", keys[i], it.App.Id, err)
			}
			ac, err = it.tk.RefreshAccessToken("")
			if err != nil {
				logger.E("Token", "expire=>'%s' to refresh failure for id= %s, err= %s", keys[i], it.App.Id, err)
			} else {
				logger.N("Token", "expire=>'%s' to refresh ok for id= %s, ac= %s", keys[i], it.App.Id, ac)
			}
		default:
			vd, err = it.clear(keys[i])
			if err == nil {
				logger.D("Token", "expire=>'%s' to clear ok for id= %s, old value= %s", keys[i], it.App.Id, vd.String())
				fSave = true
			} else {
				logger.W("Token", "expire=>'%s' to clear failure for id= %s, err= %s", keys[i], it.App.Id, err)
			}
		}
	}
	return fSave
}
