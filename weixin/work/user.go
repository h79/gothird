package work

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/gothird/weixin/work/structs"
	"gitee.com/h79/goutils/common/http"
)

// UserCreate 创建成员
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/create?access_token=ACCESS_TOKEN
// Deprecated: this function simply calls UserCreateV2.
func UserCreate(api *token.Api, user *structs.CreateUpdateDepMember) error {
	return UserCreateV2(api, user)
}

// UserCreateV2 创建成员
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/create?access_token=ACCESS_TOKEN
func UserCreateV2(api *token.Api, user interface{}) error {
	url := fmt.Sprintf("%s/cgi-bin/user/create?", consts.WorkApiPrefixUrl)
	buf, _ := json.Marshal(user)

	var res = response.Response{}
	if err := api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return err
	}
	return res.ErrorIf()
}

// UserGetByUserId 读取成员
// 应用只能获取可见范围内的成员信息，且每种应用获取的字段有所不同，在返回结果说明中会逐个说明。企业通讯录安全特别重要，企业微信将持续升级加固通讯录接口的安全机制，以下是关键的变更点：
// 从2022年6月20号20点开始，除通讯录同步以外的基础应用（如客户联系、微信客服、会话存档、日程等），以及新创建的自建应用与代开发应用，调用该接口时，不再返回以下字段：头像、性别、手机、邮箱、企业邮箱、员工个人二维码、地址，应用需要通过oauth2手工授权的方式获取管理员与员工本人授权的字段。
// 【重要】从2022年8月15日10点开始，“企业管理后台 - 管理工具 - 通讯录同步”的新增IP将不能再调用此接口，企业可通过「获取成员ID列表」和「获取部门ID列表」接口获取userid和部门ID列表。查看调整详情。
// 请求方式：GET（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token=ACCESS_TOKEN&userid=USERID
func UserGetByUserId(api *token.Api, userid string) (*structs.DepMember, error) {
	url := fmt.Sprintf("%s/cgi-bin/user/get?userid=%s&", consts.WorkApiPrefixUrl, userid)

	var res = struct {
		response.Response
		structs.DepMember
	}{}
	if err := api.Request("GET", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return nil, err
	}
	return &res.DepMember, res.ErrorIf()
}

// UserUpdate 更新成员
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/update?access_token=ACCESS_TOKEN
// Deprecated: this function simply calls UserUpdateV2.
func UserUpdate(api *token.Api, user *structs.CreateUpdateDepMember) error {
	return UserUpdateV2(api, user)
}

// UserUpdateV2 更新成员
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/update?access_token=ACCESS_TOKEN
func UserUpdateV2(api *token.Api, user any) error {

	url := fmt.Sprintf("%s/cgi-bin/user/update?", consts.WorkApiPrefixUrl)
	buf, _ := json.Marshal(user)

	var res = response.Response{}
	if err := api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return err
	}
	return res.ErrorIf()
}

// UserDelete 删除成员
// 请求方式：GET（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/delete?access_token=ACCESS_TOKEN&userid=USERID
func UserDelete(api *token.Api, userid string) error {
	url := fmt.Sprintf("%s/cgi-bin/user/delete?userid=%s&", consts.WorkApiPrefixUrl, userid)

	var res = response.Response{}
	if err := api.Request("GET", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return err
	}
	return res.ErrorIf()
}

// UserBatchDelete 批量删除成员
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/batchdelete?access_token=ACCESS_TOKEN
func UserBatchDelete(api *token.Api, userid []string) error {
	url := fmt.Sprintf("%s/cgi-bin/user/batchdelete?", consts.WorkApiPrefixUrl)

	var in = struct {
		UserId []string `json:"useridlist"`
	}{
		UserId: userid,
	}
	buf, _ := json.Marshal(&in)

	var res = response.Response{}
	if err := api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return err
	}
	return res.ErrorIf()
}

// UserGetSimpleByDepartId 获取部门成员
// 企业通讯录安全特别重要，企业微信将持续升级加固通讯录接口的安全机制，以下是关键的变更点：
// 【重要】从2022年8月15日10点开始，“企业管理后台 - 管理工具 - 通讯录同步”的新增IP将不能再调用此接口，企业可通过「获取成员ID列表」和「获取部门ID列表」接口获取userid和部门ID列表。查看调整详情。
// 请求方式：GET（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/simplelist?access_token=ACCESS_TOKEN&department_id=DEPARTMENT_ID
func UserGetSimpleByDepartId(api *token.Api, departId string) ([]structs.SimpleUser, error) {
	url := fmt.Sprintf("%s/cgi-bin/user/simplelist?department_id=%s&", consts.WorkApiPrefixUrl, departId)

	var res = struct {
		response.Response
		SimpleUser []structs.SimpleUser `json:"userlist"`
	}{}
	if err := api.Request("GET", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return nil, err
	}
	return res.SimpleUser, res.ErrorIf()
}

// UserGetDetailByDepartId 获取部门成员详情
// 应用只能获取可见范围内的成员信息，且每种应用获取的字段有所不同，在返回结果说明中会逐个说明。企业通讯录安全特别重要，企业微信持续升级加固通讯录接口的安全机制，以下是关键的变更点：
// 从2022年6月20号20点开始，除通讯录同步以外的基础应用（如客户联系、微信客服、会话存档、日程等），以及新创建的自建应用与代开发应用，调用该接口时，不再返回以下字段：头像、性别、手机、邮箱、企业邮箱、员工个人二维码、地址，应用需要通过oauth2手工授权的方式获取管理员与员工本人授权的字段。
// 【重要】从2022年8月15日10点开始，“企业管理后台 - 管理工具 - 通讯录同步”的新增IP将不能再调用此接口，企业可通过「获取成员ID列表」和「获取部门ID列表」接口获取userid和部门ID列表。查看调整详情。
// 请求方式：GET（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/list?access_token=ACCESS_TOKEN&department_id=DEPARTMENT_ID
func UserGetDetailByDepartId(api *token.Api, departId string) ([]structs.DepMember, error) {
	url := fmt.Sprintf("%s/cgi-bin/user/list?department_id=%s&", consts.WorkApiPrefixUrl, departId)

	var res = struct {
		response.Response
		DepMember []structs.DepMember `json:"userlist"`
	}{}
	if err := api.Request("GET", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return nil, err
	}
	return res.DepMember, res.ErrorIf()
}

// UserGetListId 获取成员ID列表
// 获取企业成员的userid与对应的部门ID列表，预计于2022年8月8号发布。若需要获取其他字段，参见「适配建议」。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/list_id?access_token=ACCESS_TOKEN
func UserGetListId(api *token.Api, cursor string, limit int) ([]structs.DepUser, string, error) {
	url := fmt.Sprintf("%s/cgi-bin/user/list_id?", consts.WorkApiPrefixUrl)

	var in = struct {
		Cursor string `json:"cursor,omitempty"`
		Limit  int    `json:"limit"`
	}{
		Cursor: cursor,
		Limit:  limit,
	}
	buf, _ := json.Marshal(&in)

	var res = struct {
		response.Response
		NextCursor string            `json:"next_cursor"`
		DeptUser   []structs.DepUser `json:"dept_user"`
	}{}
	if err := api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return nil, "", err
	}
	return res.DeptUser, res.NextCursor, res.ErrorIf()
}

// UserGetByMobile 手机号获取userid
// 通过手机号获取其所对应的userid。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/getuserid?access_token=ACCESS_TOKEN
func UserGetByMobile(api *token.Api, mobile string) (string, error) {
	url := fmt.Sprintf("%s/cgi-bin/user/getuserid?", consts.WorkApiPrefixUrl)

	var in = struct {
		Mobile string `json:"mobile"`
	}{
		Mobile: mobile,
	}
	buf, _ := json.Marshal(&in)

	return getUserid(api, url, buf)
}

// UserGetByEmail 邮箱获取userid
// 通过邮箱获取其所对应的userid。
// emailType 邮箱类型：1-企业邮箱（默认）；2-个人邮箱
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/get_userid_by_email?access_token=ACCESS_TOKEN
func UserGetByEmail(api *token.Api, email string, emailType int) (string, error) {
	url := fmt.Sprintf("%s/cgi-bin/user/get_userid_by_email?", consts.WorkApiPrefixUrl)

	var in = struct {
		Email     string `json:"email"`
		EmailType int    `json:"email_type"`
	}{
		Email:     email,
		EmailType: emailType,
	}
	buf, _ := json.Marshal(&in)
	return getUserid(api, url, buf)
}

func getUserid(api *token.Api, url string, buf []byte) (string, error) {

	var res = struct {
		response.Response
		UserId string `json:"userid"`
	}{}
	if err := api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return "", err
	}
	return res.UserId, res.ErrorIf()
}

// UserUserId2OpenId userid转openid
// 该接口使用场景为企业支付，在使用企业红包和向员工付款时，需要自行将企业微信的userid转成openid。
// 注：需要成员使用微信登录企业微信或者关注微信插件（原企业号）才能转成openid;
// 如果是外部联系人，请使用外部联系人openid转换转换openid
// 请求方式：POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/user/convert_to_openid?access_token=ACCESS_TOKEN
func UserUserId2OpenId(api *token.Api, userid string) (string, error) {
	url := fmt.Sprintf("%s/cgi-bin/user/convert_to_openid?", consts.WorkApiPrefixUrl)

	var in = struct {
		Userid string `json:"userid"`
	}{
		Userid: userid,
	}
	buf, _ := json.Marshal(&in)

	var res = struct {
		response.Response
		Openid string `json:"openid"`
	}{}
	if err := api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return "", err
	}
	return res.Openid, res.ErrorIf()
}

// UserMemberAuthList 获取成员授权列表
// 当企业当前授权模式为成员授权时，可调用该接口获取成员授权列表。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/list_member_auth?access_token=ACCESS_TOKEN
func UserMemberAuthList(api *token.Api, cursor string, limit int) (*structs.MemberAuthResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/user/list_member_auth?", consts.WorkApiPrefixUrl)

	var in = struct {
		Cursor string `json:"cursor,omitempty"`
		Limit  int    `json:"limit"`
	}{
		Cursor: cursor,
		Limit:  limit,
	}
	buf, _ := json.Marshal(&in)

	var res = struct {
		response.Response
		structs.MemberAuthResult
	}{}
	if err := api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return nil, err
	}
	return &res.MemberAuthResult, res.ErrorIf()
}

// UserCheckMemberAuth 查询成员用户是否已授权
// 当企业当前授权模式为成员授权时，可调用该接口查询成员用户是否已授权
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/check_member_auth?access_token=ACCESS_TOKEN
func UserCheckMemberAuth(api *token.Api, openUserId string) (bool, error) {
	url := fmt.Sprintf("%s/cgi-bin/user/check_member_auth?", consts.WorkApiPrefixUrl)

	var in = struct {
		OpenUserId string `json:"open_userid"`
	}{
		OpenUserId: openUserId,
	}
	buf, _ := json.Marshal(&in)

	var res = struct {
		response.Response
		IsMemberAuth bool `json:"is_member_auth"`
	}{}
	if err := api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return false, err
	}
	return res.IsMemberAuth, res.ErrorIf()
}

// UserSelectTicket 获取选人ticket对应的用户
// 当企业以成员授权的方式安装了第三方应用，成员在企业微信终端通过选人jsapi选择通讯录，应用获取到selectedTicket后，可调用该接口获取SelectedTicket对应的用户open_userid列表。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/list_selected_ticket_user?access_token=ACCESS_TOKEN
func UserSelectTicket(api *token.Api, selectedTicket string) (*structs.SelectedTicketResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/user/list_selected_ticket_user?", consts.WorkApiPrefixUrl)

	var in = struct {
		SelectedTicket string `json:"selected_ticket"`
	}{
		SelectedTicket: selectedTicket,
	}
	buf, _ := json.Marshal(&in)

	var res = struct {
		response.Response
		structs.SelectedTicketResult
	}{}
	if err := api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return nil, err
	}
	return &res.SelectedTicketResult, res.ErrorIf()
}
