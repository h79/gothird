package offiaccount

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	commonhttp "gitee.com/h79/goutils/common/http"
	"net/http"
	"net/url"
)

// QRGenerateReq
//
//   - 临时二维码
//     QRGenerateReq(6000,"QR_SCENE","", 1) OR  GenReq(6000,"QR_STR_SCENE","test", 0)
//
//   - 永久二维码
//     QRGenerateReq(0,"QR_LIMIT_SCENE","", 1) OR  GenReq(0,"QR_LIMIT_STR_SCENE","test", 0)
func QRGenerateReq(expireSeconds int64, actionName, sceneStr string, sceneId int32) QRCodeReq {
	return QRCodeReq{
		Expire:     expireSeconds,
		ActionName: actionName,
		ActionInfo: ActionInfo{
			Scene: Scene{
				SceneId:  sceneId,
				SceneStr: sceneStr,
			},
		},
	}
}

// QRCreate 临时二维码请求说明
//
// http请求方式: POST
// URL: https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=TOKEN
// POST数据格式：json
// POST数据例子：{"expire_seconds": 604800, "action_name": "QR_SCENE", "action_info": {"scene": {"scene_id": 123}}}
//
// 或者也可以使用以下POST数据创建字符串形式的二维码参数：
// {"expire_seconds": 604800, "action_name": "QR_STR_SCENE", "action_info": {"scene": {"scene_str": "test"}}}
// 2 永久二维码请求说明
//
// http请求方式: POST
// URL: https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=TOKEN
// POST数据格式：json
// POST数据例子：{"action_name": "QR_LIMIT_SCENE", "action_info": {"scene": {"scene_id": 123}}}
//
// 或者也可以使用以下POST数据创建字符串形式的二维码参数：
// {"action_name": "QR_LIMIT_STR_SCENE", "action_info": {"scene": {"scene_str": "test"}}}
// expire_seconds	该二维码有效时间，以秒为单位。 最大不超过2592000（即30天），此字段如果不填，则默认有效期为30秒。
// action_name	二维码类型，QR_SCENE为临时的整型参数值，QR_STR_SCENE为临时的字符串参数值，QR_LIMIT_SCENE为永久的整型参数值，QR_LIMIT_STR_SCENE为永久的字符串参数值
// action_info	二维码详细信息
// scene_id	场景值ID，临时二维码时为32位非0整型，永久二维码时最大值为100000（目前参数只支持1--100000）
// scene_str	场景值ID（字符串形式的ID），字符串类型，长度限制为1到64
//
// 返回
// {"ticket":"gQH47joAAAAAAAAAASxodHRwOi8vd2VpeGluLnFxLmNvbS9xL2taZ2Z3TVRtNzJXV1Brb3ZhYmJJAAIEZ23sUwMEmm
// ticket	获取的二维码ticket，凭借此ticket可以在有效时间内换取二维码。
// expire_seconds	该二维码有效时间，以秒为单位。 最大不超过2592000（即30天）。
// url	二维码图片解析后的地址，开发者可根据该地址自行生成需要的二维码图片
func QRCreate(api *token.Api, qr *QRCodeReq) (*QRCode, error) {
	uri := fmt.Sprintf("%s/cgi-bin/qrcode/create?", consts.ApiPrefixUrl)

	buf, err := json.Marshal(qr)
	if err != nil {
		return nil, err
	}

	type qrCodeResult struct {
		response.Response
		QRCode
	}
	res := qrCodeResult{}
	er := api.Request("POST", uri, buf, func(hp *commonhttp.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	return &res.QRCode, er
}

// QRGetBitmap 获取二维码ticket后，开发者可用ticket换取二维码图片。请注意，本接口无须登录态即可调用。
// HTTP GET请求（请使用https协议）https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=TICKET
// 提醒：TICKET记得进行UrlEncode
// ticket正确情况下，http 返回码是200，是一张图片，可以直接展示或者下载。
//
// HEAD
// Accept-Ranges:bytes
// IsCache-control:max-age=604800
// Connection:keep-alive
// Content-Length:28026
// Content-Name:image/jpg
func QRGetBitmap(ticket string) ([]byte, error) {

	params := url.Values{}
	params.Add("ticket", ticket)

	Url := fmt.Sprintf("%s/cgi-bin/showqrcode?%s", consts.ApiPrefixUrl, params.Encode())

	hp := commonhttp.Http{}
	return hp.Do("GET", Url, nil, func(h *http.Header) {
		h.Set("Content-Type", "image/jpg")
		h.Set("Accept-Ranges", "bytes")
	})
}
