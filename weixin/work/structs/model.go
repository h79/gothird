package structs

import (
	"gitee.com/h79/gothird/weixin/work/access"
	"gitee.com/h79/goutils/common/stringutil"
)

type PreAuthCode struct {
	Code      string `json:"pre_auth_code"`
	ExpiresIn int64  `json:"expires_in"`
}

type Ticket struct {
	Ticket   string `json:"ticket"`
	ExpireIn int64  `json:"expires_in"`
}

type Department struct {
	Id       int64    `json:"id,omitempty"`       //部门id，32位整型，指定时必须大于1。若不填该参数，将自动生成id
	ParentId int64    `json:"parentid,omitempty"` //父部门id，32位整型
	Order    int      `json:"order,omitempty"`    //在父部门中的次序值。order值大的排序靠前。有效的值范围是[0, 2^32)
	Name     string   `json:"name,omitempty"`     //部门名称。同一个层级的部门名称不能重复,长度限制为1~64个UTF-8字符，字符不能包括\:*?"<>
	NameEn   string   `json:"name_en,omitempty"`  //英文名称。同一个层级的部门名称不能重复。需要在管理后台开启多语言支持才能生效。长度限制为1~64个字符，
	Leader   []string `json:"department_leader,omitempty"`
}

type DepUser struct {
	OpenUserId string `json:"open_userid"`
	Department int64  `json:"department"`
}

type SimpleUser struct {
	Userid     string  `json:"userid"`      //成员UserID
	Name       string  `json:"name"`        //成员名称，代开发自建应用需要管理员授权才返回；此字段从2019年12月30日起，对新创建第三方应用不再返回真实name，使用userid代替name，2020年6月30日起，对所有历史第三方应用不再返回真实name，使用userid代替name，后续第三方仅通讯录应用可获取，未返回名称的情况需要通过通讯录展示组件来展示名字
	OpenUserid string  `json:"open_userid"` //全局唯一。对于同一个服务商，不同应用获取到企业内同一个成员的open_userid是相同的，最多64个字节。仅第三方应用可获取
	Department []int64 `json:"department"`  //成员所属部门列表。列表项为部门ID，32位整型
}

type UserInfoDetail struct {
	Userid  string `json:"userid"`
	Name    string `json:"name"`
	Gender  string `json:"gender"`
	Avatar  string `json:"avatar"`
	QrCode  string `json:"qr_code"`
	Mobile  string `json:"mobile"`
	Email   string `json:"email"`
	BizMail string `json:"biz_mail"`
	Address string `json:"address"`
}

type UserInfo struct {
	Userid         string `json:"userid"`
	UserTicket     string `json:"user_ticket"`     // 当用户为企业成员时
	OpenId         string `json:"openid"`          // 非企业成员时,非企业成员的标识，对当前企业唯一
	ExternalUserid string `json:"external_Userid"` // 非企业成员时,外部联系人id，
	// 当且仅当用户是企业的客户，且跟进人在应用的可见范围内时返回。
	// 如果是第三方应用调用，针对同一个客户，同一个服务商不同应用获取到的id相同
}

type UserIdReq struct {
	UseridList []string `json:"userid_list"`
}

type OpenUserId struct {
	OpenUseridList    []UserIdPair `json:"open_userid_list"`
	InvalidUseridList []string     `json:"invalid_userid_list"`
}

type UserIdResult struct {
	UseridList        []UserIdPair `json:"userid_list"`
	InvalidUseridList []string     `json:"invalid_open_userid_list"`
}

type UserIdPair struct {
	Userid     string `json:"userid"`
	OpenUserid string `json:"open_userid"`
}

type ExternalUserIds struct {
	Items []ExternalUserIdItem `json:"items"`
}

type ExternalUserIdItem struct {
	ExternalUserid    string `json:"external_userid"`
	NewExternalUserid string `json:"new_external_userid"`
}

type BatchUserId struct {
	UseridList []string `json:"userid_list"`
	Cursor     string   `json:"cursor,omitempty"`
	Limit      int      `json:"limit"`
}

type BatchUserIdRes struct {
	ExternalContactList []ExternalContactDetail `json:"external_contact_list"`
	NextCursor          string                  `json:"next_cursor"`
}

type WechatChannels struct {
	Nickname string `json:"nickname"`
	Source   int    `json:"source,omitempty"`
}

type ExternalProfile struct {
	ExternalAttr     []Attribute    `json:"external_attr,omitempty"`
	WechatChannels   WechatChannels `json:"wechat_channels,omitempty"`
	ExternalCorpName string         `json:"external_corp_name,omitempty"`
}

type Attribute struct {
	Type        int                   `json:"type"`
	Name        string                `json:"name"`
	Text        *AttributeText        `json:"text,omitempty"`
	Web         *AttributeWeb         `json:"web,omitempty"`
	MiniProgram *AttributeMiniProgram `json:"miniprogram,omitempty"`
}

type AttributeText struct {
	Value string `json:"value"`
}

type AttributeWeb struct {
	Url   string `json:"url"`
	Title string `json:"title"`
}

type AttributeMiniProgram struct {
	Appid    string `json:"appid"`
	PagePath string `json:"pagepath"`
	Title    string `json:"title"`
}

// CreateUpdateDepMember 成员创建与更新
type CreateUpdateDepMember struct {
	Userid           string           `json:"userid"`
	Name             string           `json:"name"`
	Alias            string           `json:"alias"`
	Mobile           string           `json:"mobile"`
	Position         string           `json:"position"`
	Gender           string           `json:"gender"`
	Email            string           `json:"email"`
	BizMail          string           `json:"biz_mail"`
	AvatarMediaId    string           `json:"avatar_mediaid,omitempty"`
	Telephone        string           `json:"telephone"`
	Address          string           `json:"address"`
	MainDepartment   string           `json:"main_department"`
	ToInvite         string           `json:"to_invite"`
	ExternalPosition string           `json:"external_position"`
	Enable           int              `json:"enable"`
	Order            []string         `json:"order"`
	Department       []string         `json:"department"`
	IsLeaderInDept   []string         `json:"is_leader_in_dept,omitempty"`
	DirectLeader     []string         `json:"direct_leader,omitempty"`
	ExtAttr          *Attribute       `json:"extattr,omitempty"`
	ExternalProfile  *ExternalProfile `json:"external_profile,omitempty"`
}

type DepMember struct {
	Userid           string           `json:"userid"`
	Name             string           `json:"name,omitempty"`
	Alias            string           `json:"alias,omitempty"`
	Mobile           string           `json:"mobile,omitempty"`
	Position         string           `json:"position,omitempty"`
	Gender           string           `json:"gender,omitempty"`
	Email            string           `json:"email,omitempty"`
	BizMail          string           `json:"biz_mail,omitempty"`
	AvatarMediaId    string           `json:"avatar_mediaid,omitempty"`
	OpenUserid       string           `json:"open_userid,omitempty"`
	Avatar           string           `json:"avatar,omitempty"`
	ThumbAvatar      string           `json:"thumb_avatar,omitempty"`
	Telephone        string           `json:"telephone,omitempty"`
	Address          string           `json:"address,omitempty"`
	QrCode           string           `json:"qr_code,omitempty"`
	ExternalPosition string           `json:"external_position"`
	ToInvite         bool             `json:"to_invite,omitempty"`
	Enable           int              `json:"enable"`
	Status           int              `json:"status,omitempty"`
	MainDepartment   int64            `json:"main_department"`
	Department       []int64          `json:"department,omitempty"`
	IsLeaderInDept   []int64          `json:"is_leader_in_dept,omitempty"`
	Order            []int            `json:"order,omitempty"`
	DirectLeader     []string         `json:"direct_leader,omitempty"`
	ExtAttr          *Attribute       `json:"extattr,omitempty"`
	ExternalProfile  *ExternalProfile `json:"external_profile,omitempty"`
}

type OwnerFilter struct {
	UseridList []string `json:"userid_list,omitempty"`
}

type GroupChat struct {
	ChatId string `json:"chat_id"`
	Status int    `json:"status"`
}

type GroupChatDetail struct {
	ChatId     string        `json:"chat_id"`        // 客户群ID
	Name       string        `json:"name"`           // 群名
	Owner      string        `json:"owner"`          // 群主ID
	Notice     string        `json:"notice"`         // 群公告
	MemVer     string        `json:"member_version"` // 当前群成员版本号。可以配合客户群变更事件减少主动调用本接口的次数
	CreateTime int64         `json:"create_time"`    // 群的创建时间
	MemberList []GroupMember `json:"member_list"`    // 群成员列表
	AdminList  []GroupAdmin  `json:"admin_list"`     // 群管理员列表
}

type GroupMember struct {
	Invitor       Invitor `json:"invitor"`        // 邀请者。目前仅当是由本企业内部成员邀请入群时会返回该值
	Userid        string  `json:"userid"`         // 群成员id
	GroupNickname string  `json:"group_nickname"` // 在群里的昵称
	Name          string  `json:"name"`           // 名字。仅当 need_name = 1 时返回 如果是微信用户，则返回其在微信中设置的名字 如果是企业微信联系人，则返回其设置对外展示的别名或实名
	UnionId       string  `json:"unionid"`        // 外部联系人在微信开放平台的唯一身份标识（微信unionid）
	Type          int     `json:"type"`           // 1企业成员 2外部联系人
	JoinScene     int     `json:"join_scene"`     // 入群方式。 1由群成员邀请入群（直接邀请入群） 2由群成员邀请入群（通过邀请链接入群） 3通过扫描群二维码入群
	JoinTime      int64   `json:"join_time"`      // 入群时间
}

type Invitor struct {
	Userid string `json:"userid"` //邀请者的userid
}

type GroupAdmin struct {
	Userid string `json:"userid"` //群管理员userid
}

type GroupMsgReq struct {
	ChatType   string `json:"chat_type"` //群发任务的类型，默认为single，表示发送给客户，group表示发送给客户群
	Creator    string `json:"creator"`
	Cursor     string `json:"cursor,omitempty"`
	StartTime  int64  `json:"start_time"`
	EndTime    int64  `json:"end_time"`
	FilterType int    `json:"filter_type"`
	Limit      int    `json:"limit"`
}

type GroupMsgSearch struct {
	MsgId  string `json:"msgid"`
	Userid string `json:"userid,omitempty"`
	Limit  int    `json:"limit"`
	Cursor string `json:"cursor,omitempty"`
}

type GroupMsg struct {
	MsgId       string       `json:"msgid"`
	Creator     string       `json:"creator"`
	CreateTime  string       `json:"create_time"`
	CreateType  int          `json:"create_type"`
	Text        *ExText      `json:"text,omitempty"`
	Attachments []Attachment `json:"attachments"`
}

type Attachment struct {
	MsgType string     `json:"msgtype"`
	Image   *ExImage   `json:"image,omitempty"`
	Link    *ExLink    `json:"link,omitempty"`
	Program *ExProgram `json:"miniprogram,omitempty"`
	Video   *ExVideo   `json:"video,omitempty"`
	File    *ExFile    `json:"file,omitempty"`
}

type GroupMsgTask struct {
	Userid   string `json:"userid"`
	Status   int    `json:"status"`
	SendTime int64  `json:"send_time"`
}

type GroupMsgSendResult struct {
	ExternalUserid string `json:"external_userid"`
	ChatId         string `json:"chat_id"`
	Userid         string `json:"userid"`
	Status         int    `json:"status"`
	SendTime       int64  `json:"send_time"`
}

type NewExternalUserid struct {
	ExternalUserid    string `json:"external_userid"`
	NewExternalUserid string `json:"new_external_userid"`
}

type RoomInfo struct {
	Name       string       `json:"roomname"`
	Creator    string       `json:"creator"`
	Notice     string       `json:"notice"`
	CreateTime int64        `json:"room_create_time"`
	Members    []RoomMember `json:"members"`
}

type RoomMember struct {
	MemberId string `json:"memberid"`
	JoinTime int64  `json:"jointime"`
}

type SingleAgree struct {
	Userid         string `json:"userid"`
	ExternalOpenId string `json:"externalopenid"`
}

type AgreeInfo struct {
	StatusChangeTime int64  `json:"status_change_time"` //同意状态改变的具体时间，utc时间
	Userid           string `json:"userid"`
	ExternalOpenId   string `json:"externalopenid"`
	AgreeStatus      string `json:"agree_status"` //同意:"Agree"，不同意:"Disagree"
}

// AgreeNotify 客户同意进行聊天内容存档事件回调
type AgreeNotify struct {
	ToUserName     string `json:"ToUserName" xml:"ToUserName"`
	FromUserName   string `json:"FromUserName" xml:"FromUserName"`
	UserID         string `json:"UserID" xml:"UserID"`
	ExternalUserID string `json:"ExternalUserID" xml:"ExternalUserID"`
	WelcomeCode    string `json:"WelcomeCode" xml:"WelcomeCode"`
}

type AddMomentTask struct {
	Text         ExText       `json:"text"` //不能与Attachment同时为空
	Attachments  []Attachment `json:"attachments"`
	VisibleRange VisibleRange `json:"visible_range"`
}

type VisibleRange struct {
	SenderList struct {
		UserList       []string `json:"user_list"`
		DepartmentList []int32  `json:"department_list"`
	} `json:"sender_list"`
	ExternalContactList struct {
		TagList []string `json:"tag_list"`
	} `json:"external_contact_list"`
}

type MomentTaskResult struct {
	Status int    `json:"status"`
	Type   string `json:"type"`
	Result struct {
		ErrCode           int32  `json:"errcode"`
		ErrMsg            string `json:"errmsg"`
		MomentId          string `json:"moment_id"`
		InvalidSenderList struct {
			UserList       []string `json:"user_list"`
			DepartmentList []int32  `json:"department_list"`
		} `json:"invalid_sender_list"`
		InvalidExternalContactList struct {
			TagList []string `json:"tag_list"`
		} `json:"invalid_external_contact_list"`
	} `json:"result"`
}

type MomentListReq struct {
	StartTime  int64  `json:"start_time"`
	EndTime    int64  `json:"end_time"`
	Creator    string `json:"creator"`
	FilterType int    `json:"filter_type"`
	Cursor     string `json:"cursor,omitempty"`
	Limit      int32  `json:"limit"`
}

type MomentListResp struct {
	NextCursor string       `json:"next_cursor"`
	MomentList []MomentInfo `json:"moment_list"`
}

type MomentInfo struct {
	MomentId    string `json:"moment_id"`
	Creator     string `json:"creator"`
	CreateTime  int64  `json:"create_time"`
	CreateType  int    `json:"create_type"`
	VisibleType int    `json:"visible_type"`
	Text        struct {
		Content string `json:"content"`
	} `json:"text"`
	Image []struct {
		MediaId string `json:"media_id"`
	} `json:"image"`
	Video struct {
		MediaId      string `json:"media_id"`
		ThumbMediaId string `json:"thumb_media_id"`
	} `json:"video"`
	Link struct {
		Title string `json:"title"`
		Url   string `json:"url"`
	} `json:"link"`
	Location struct {
		Latitude  string `json:"latitude"`
		Longitude string `json:"longitude"`
		Name      string `json:"name"`
	} `json:"location"`
}

type Export struct {
	EncodingAesKey string `json:"encoding_aeskey"`
	BlockSize      int    `json:"block_size"`
}

type ExportResult struct {
	Status   int          `json:"status"`
	DataList []ExportItem `json:"data_list"`
}

type ExportItem struct {
	Url  string      `json:"url"`
	Size interface{} `json:"size"`
	Md5  string      `json:"md5"`
}

const (
	KEBatchJobResult = "batch_job_result"
)

// ExportNotify 导出任务完成通知
type ExportNotify struct {
	JobId   string `json:"SuiteId" xml:"JobId"`
	JobType string `json:"AuthCorpId" xml:"JobType"` //操作类型，字符串，在异步导出的场景下分别有：export_user(导出成员详情)、 export_simple_user(导出成员）、export_department(导出部门）、export_tag(导出标签成员)
	ErrCode int32  `json:"ErrCode" xml:"ErrCode"`
	ErrMsg  string `json:"ErrMsg" xml:"ErrMsg"`
}

type ImportReq struct {
	MediaId  string `json:"media_id"`
	ToInvite bool   `json:"to_invite"`
	Callback struct {
		Url            string `json:"url"`
		Token          string `json:"token"`
		EncodingAesKey string `json:"encodingaeskey"`
	} `json:"callback"`
}

type ImportResult struct {
	Status     int           `json:"status"`
	Type       string        `json:"type"`
	Total      int           `json:"total"`
	Percentage int           `json:"percentage"`
	Result     []interface{} `json:"result"`
}

type MemberAuthResult struct {
	NextCursor     string `json:"next_cursor"`
	MemberAuthList []struct {
		OpenUserid string `json:"open_userid"`
	} `json:"member_auth_list"`
}

type SelectedTicketResult struct {
	OperatorOpenUserid   string   `json:"operator_open_userid"`
	OpenUseridList       []string `json:"open_userid_list"`
	UnAuthOpenUseridList []string `json:"unauth_open_userid_list"`
	Total                int32    `json:"total"`
}

// https://developer.work.weixin.qq.com/document/path/92275

// UserBehaviorDataReq 获取「联系客户统计」数据
type UserBehaviorDataReq struct {
	UserId    []string `json:"userid"`  //成员ID列表，最多100个
	PartyId   []int32  `json:"partyid"` //部门ID列表，最多100个
	StartTime int64    `json:"start_time"`
	EndTime   int64    `json:"end_time"`
}

type BehaviorDataResult struct {
	StatTime            int64   `json:"stat_time"`             //数据日期，为当日0点的时间戳
	ChatCnt             int32   `json:"chat_cnt"`              //聊天总数， 成员有主动发送过消息的单聊总数。
	MessageCnt          int32   `json:"message_cnt"`           //发送消息数，成员在单聊中发送的消息总数。
	ReplyPercentage     float64 `json:"reply_percentage"`      //已回复聊天占比，浮点型，
	AvgReplyTime        int32   `json:"avg_reply_time"`        //平均首次回复时长，单位为分钟
	NegativeFeedbackCnt int32   `json:"negative_feedback_cnt"` //删除/拉黑成员的客户数，即将成员删除或加入黑名单的客户数。
	NewApplyCnt         int32   `json:"new_apply_cnt"`         //发起申请数，成员通过
	NewContactCnt       int32   `json:"new_contact_cnt"`       //新增客户数，成员新添加的客户数量。
}

// https://developer.work.weixin.qq.com/document/path/93476

type GroupChatStatisticReq struct {
	DayBeginTime int64       `json:"day_begin_time"`
	DayEndTime   int64       `json:"day_end_time"`
	OwnerFilter  OwnerFilter `json:"owner_filter"`
	OrderBy      int32       `json:"order_by"`  //排序方式,默认为1 1 - 新增群的数量 2 - 群总数 3 - 新增群人数 4 - 群总人数
	OrderAsc     int32       `json:"order_asc"` //是否升序。0-否；1-是。默认降序
	Offset       int32       `json:"offset"`    //分页，偏移量, 默认为0
	Limit        int32       `json:"limit"`     //分页，预期请求的数据量，默认为500，取值范围 1 ~ 1000
}

type GroupChatDayStatisticReq struct {
	DayBeginTime int64       `json:"day_begin_time"`
	DayEndTime   int64       `json:"day_end_time"`
	OwnerFilter  OwnerFilter `json:"owner_filter"` //群主ID列表。最多100个,群主过滤。 如果不填，表示获取应用可见范围内全部群主的数据（但是不建议这么用，如果可见范围人数超过1000人，为了防止数据包过大，会报错 81017）
}

type GroupChatStatisticResult struct {
	Total      int32                    `json:"total"`
	NextOffset int32                    `json:"next_offset"` //当前分页的下一个offset。当next_offset和total相等时，说明已经取完所有
	Items      []GroupChatStatisticItem `json:"items"`
}

type GroupChatStatisticItem struct {
	Owner string                 `json:"owner"`
	Data  GroupChatStatisticData `json:"data"`
}

type GroupChatDayStatisticResult struct {
	Items []GroupChatDayStatisticItem `json:"items"`
}

type GroupChatDayStatisticItem struct {
	StatTime int64                  `json:"stat_time"`
	Data     GroupChatStatisticData `json:"data"`
}

type GroupChatStatisticData struct {
	NewChatCnt            int32 `json:"new_chat_cnt"`
	ChatTotal             int32 `json:"chat_total"`
	ChatHasMsg            int32 `json:"chat_has_msg"`
	NewMemberCnt          int32 `json:"new_member_cnt"`
	MemberTotal           int32 `json:"member_total"`
	MemberHasMsg          int32 `json:"member_has_msg"`
	MsgTotal              int32 `json:"msg_total"`
	MigrateTraineeChatCnt int32 `json:"migrate_trainee_chat_cnt"`
}

type CustomerAcquisition struct {
	LinkName   string                   `json:"link_name"`
	Range      CustomerAcquisitionRange `json:"range"`
	SkipVerify bool                     `json:"skip_verify"`
}

type CustomerAcquisitionResult struct {
	LinkIdList []string `json:"link_id_list"`
	NextCursor string   `json:"next_cursor"`
}

type CustomerAcquisitionInfo struct {
	Link       CustomerAcquisitionLink  `json:"link"`
	Range      CustomerAcquisitionRange `json:"range"`
	SkipVerify bool                     `json:"skip_verify"`
}

type CustomerAcquisitionLink struct {
	LinkId     string `json:"link_id"`
	LinkName   string `json:"link_name"`
	Url        string `json:"url"`
	CreateTime int64  `json:"create_time"`
}

type CustomerAcquisitionRange struct {
	UserList       []string `json:"user_list"`
	DepartmentList []int64  `json:"department_list"`
}

type CustomerAcquisitionSearch struct {
	LinkId string `json:"link_id,omitempty"`
	Limit  int32  `json:"limit"`
	Cursor string `json:"cursor,omitempty"`
}

type CustomerAcquisitionCustomerList struct {
	NextCursor   string                        `json:"next_cursor"`
	CustomerList []CustomerAcquisitionCustomer `json:"customer_list"`
}

type CustomerAcquisitionCustomer struct {
	ExternalUserid string `json:"external_userid"`
	Userid         string `json:"userid"`
	ChatStatus     int32  `json:"chat_status"`
	State          string `json:"state"`
}

type CustomerAcquisitionQuotaList struct {
	Total     int64                      `json:"total"`
	Balance   int64                      `json:"balance"`
	QuotaList []CustomerAcquisitionQuota `json:"quota_list"`
}

type CustomerAcquisitionQuota struct {
	ExpireDate int64 `json:"expire_date"`
	Balance    int64 `json:"balance"`
}

type CustomerAcquisitionStatisticSearch struct {
	LinkId    string `json:"link_id"`
	StartTime int64  `json:"start_time"`
	EndTime   int64  `json:"end_time"`
}

type CustomerAcquisitionStatisticResult struct {
	ClickLinkCustomerCnt int32 `json:"click_link_customer_cnt"`
	NewCustomerCnt       int32 `json:"new_customer_cnt"`
}

type CustomerAcquisitionChatInfoSearch struct {
	ChatKey string `json:"chat_key"`
}

type CustomerAcquisitionChatInfoResult struct {
	Userid         string `json:"userid"`
	ExternalUserid string `json:"external_userid"`
	ChatInfo       struct {
		RecvMsgCnt int    `json:"recv_msg_cnt"`
		LinkId     string `json:"link_id"`
		State      string `json:"state"`
	} `json:"chat_info"`
}

type GroupTransfer struct {
	ChatIdList []string `json:"chat_id_list"`
	NewOwner   string   `json:"new_owner"`
}

type GroupTransferResult struct {
	ChatId  string `json:"chat_id"`
	Errcode int32  `json:"errcode"`
	Errmsg  string `json:"errmsg"`
}

type TransferReq struct {
	HandoverUserid     string   `json:"handover_userid"`                //原跟进成员的userid
	TakeoverUserid     string   `json:"takeover_userid"`                //接替成员的userid
	ExternalUserid     []string `json:"external_userid"`                //客户的external_userid列表，每次最多分配100个客户
	TransferSuccessMsg string   `json:"transfer_success_msg,omitempty"` //转移成功后发给客户的消息，最多200个字符
}

type TransferResult struct {
	ExternalUserid string `json:"external_userid"` //客户的external_userid
	Errcode        int32  `json:"errcode"`         //对此客户进行分配的结果,0表示成功发起接替,待24小时后自动接替,并不代表最终接替成功
}

type Conclusions struct {
	Text        *ExText    `json:"text,omitempty"`
	Image       *ExImage   `json:"image,omitempty"`
	Link        *ExLink    `json:"link,omitempty"`
	MiniProgram *ExProgram `json:"miniprogram,omitempty"`
}

type AddContactWay struct {
	Type          int         `json:"type"`            //联系方式类型,1-单人, 2-多人
	Scene         int         `json:"scene"`           //场景，1-在小程序中联系，2-通过二维码联系
	Style         int         `json:"style"`           //联系方式的备注信息，用于助记，不超过30个字符
	Remark        string      `json:"remark"`          //联系方式的备注信息，用于助记，不超过30个字符
	SkipVerify    bool        `json:"skip_verify"`     //外部客户添加时是否无需验证，默认为true
	State         string      `json:"state"`           //企业自定义的state参数，用于区分不同的添加渠道，在调用“获取外部联系人详情”时会返回该参数值，不超过30个字符
	User          []string    `json:"user"`            //使用该联系方式的用户userID列表，在type为1时为必填，且只能有一个
	Party         []int       `json:"party"`           //使用该联系方式的部门id列表，只在type为2时有效
	IsTemp        bool        `json:"is_temp"`         //是否临时会话模式，true表示使用临时会话模式，默认为false
	ExpiresIn     int64       `json:"expires_in"`      //临时会话二维码有效期，以秒为单位。该参数仅在is_temp为true时有效，默认7天，最多为14天
	ChatExpiresIn int64       `json:"chat_expires_in"` //临时会话有效期，以秒为单位。该参数仅在is_temp为true时有效，默认为添加好友后24小时，最多为14天
	UnionId       string      `json:"unionid"`         //可进行临时会话的客户unionid，该参数仅在is_temp为true时有效，如不指定则不进行限制
	IsExclusive   bool        `json:"is_exclusive"`    //是否开启同一外部企业客户只能添加同一个员工，默认为否，开启后，同一个企业的客户会优先添加到同一个跟进人
	Conclusions   Conclusions `json:"conclusions"`     //结束语，会话结束时自动发送给客户，可参考“结束语定义”，仅在is_temp为true时有效
}

type AddContactWayResult struct {
	ConfigId string `json:"config_id"`
	QrCode   string `json:"qr_code"`
}

type ContactWay struct {
	ConfigId      string      `json:"config_id"`
	Type          int         `json:"type"`
	Scene         int         `json:"scene"`
	Style         int         `json:"style"`
	Remark        string      `json:"remark"`
	SkipVerify    bool        `json:"skip_verify"`
	State         string      `json:"state"`
	QrCode        string      `json:"qr_code"`
	User          []string    `json:"user"`
	Party         []int       `json:"party"`
	IsTemp        bool        `json:"is_temp"`
	ExpiresIn     int64       `json:"expires_in"`
	ChatExpiresIn int64       `json:"chat_expires_in"`
	UnionId       string      `json:"unionid"`
	Conclusions   Conclusions `json:"conclusions"`
}

type CursorSearchReq struct {
	StartTime int64  `json:"start_time,omitempty"`
	EndTime   int64  `json:"end_time,omitempty"`
	Cursor    string `json:"cursor,omitempty"`
	Limit     int32  `json:"limit,omitempty"`
}

type WayItem struct {
	ConfigId string `json:"config_id"`
}

type UpdateContactWay struct {
	ConfigId      string      `json:"config_id"`
	Remark        string      `json:"remark"`
	SkipVerify    bool        `json:"skip_verify"`
	Style         int         `json:"style"`
	State         string      `json:"state"`
	User          []string    `json:"user"`
	Party         []int       `json:"party"`
	ExpiresIn     int64       `json:"expires_in"`
	ChatExpiresIn int64       `json:"chat_expires_in"`
	UnionId       string      `json:"unionid"`
	Conclusions   Conclusions `json:"conclusions"`
}

type DealerCorpInfo struct {
	CorpId   string `json:"corpid"`
	CorpName string `json:"corp_name"`
}

type AuthCorpInfo struct {
	CorpId            string `json:"corpid"`
	CorpName          string `json:"corp_name"`
	CorpType          string `json:"corp_type"`
	CorpSquareLogoUrl string `json:"corp_square_logo_url"`
	CorpUserMax       int    `json:"corp_user_max"`
	CorpFullName      string `json:"corp_full_name"`
	VerifiedEndTime   int64  `json:"verified_end_time"`
	SubjectType       int    `json:"subject_type"`
	CorpWxqrcode      string `json:"corp_wxqrcode"`
	CorpScale         string `json:"corp_scale"`
	CorpIndustry      string `json:"corp_industry"`
	CorpSubIndustry   string `json:"corp_sub_industry"`
}

// Privilege
// Level 1:通讯录基本信息只读
// 3:通讯录全部信息读写
// 4:单个基本信息只读
type Privilege struct {
	Level      int      `json:"level"`
	AllowParty []int64  `json:"allow_party"`
	AllowUser  []string `json:"allow_user"`
	AllowTag   []int64  `json:"allow_tag"`
	ExtraParty []int64  `json:"extra_party"`
	ExtraUser  []string `json:"extra_user"`
	ExtraTag   []int64  `json:"extra_tag"`
}

type SharedFrom struct {
	CorpId    string `json:"corpid"`
	ShareType int    `json:"share_type"`
}

type Agent struct {
	AgentId          int32      `json:"agentid"`
	Appid            int64      `json:"appid,omitempty"` //旧的多应用套件中的对应应用id，新开发者请忽略
	AuthMode         int        `json:"auth_mode"`
	Name             string     `json:"name"`
	RoundLogoUrl     string     `json:"round_logo_url"`
	SquareLogoUrl    string     `json:"square_logo_url"`
	IsCustomizedApp  bool       `json:"is_customized_app"`
	AuthFromThirdApp bool       `json:"auth_from_thirdapp"`
	Privilege        Privilege  `json:"privilege"`
	SharedFrom       SharedFrom `json:"shared_from"`
}

type AuthInfo struct {
	Agent []Agent `json:"agent"`
}

type AuthUserInfo struct {
	Userid     string `json:"userid"`
	OpenUserid string `json:"open_userid"`
	Name       string `json:"name"`
	Avatar     string `json:"avatar"`
}

type RegisterCodeInfo struct {
	RegisterCode string `json:"register_code"`
	TemplateId   string `json:"template_id"`
	State        string `json:"state"`
}

type Edition struct {
	EditionID        string `json:"edition_id"`
	EditionName      string `json:"edition_name"`
	AgentId          int32  `json:"agentid"`
	AppStatus        int    `json:"app_status"`
	UserLimit        uint32 `json:"user_limit"` //旧的多应用套件中的对应应用id，新开发者请忽略(max=4294967295)
	ExpiredTime      int64  `json:"expired_time"`
	IsVirtualVersion bool   `json:"is_virtual_version"`
}

type EditionInfo struct {
	Agent []Edition `json:"agent"`
}

// 不同于自建应用开发模式，secret由企业管理员登录管理端获取，代开发的自建应用secret获取方式如下：
//（1）企业管理员扫代开发模版授权码时，授权完成后会推送授权成功通知到应用代开发模版回调url。
//（2）收到回调后，开发者通过获取企业永久授权码接口获取到的permanent_code，即为代开发应用的secret。
//（注意：此种情况获取企业永久授权码接口会多返回is_customized_app字段，且值为true，表示是代开发模版授权，另外接口不返回access_token字段）。

type Permanent struct {
	access.PermanentAccessToken
	PermanentInfo
}

type PermanentInfo struct {
	PermanentCode    string           `json:"permanent_code"`
	DealerCorpInfo   DealerCorpInfo   `json:"dealer_corp_info"`
	AuthCorpInfo     AuthCorpInfo     `json:"auth_corp_info"`
	AuthInfo         AuthInfo         `json:"auth_info"`
	AuthUserInfo     AuthUserInfo     `json:"auth_user_info"`
	RegisterCodeInfo RegisterCodeInfo `json:"register_code_info"`
	EditionInfo      EditionInfo      `json:"edition_info"`
	State            string           `json:"state"`
}

// IsCustomizedApp 是否为代开发自建应用，另外接口不返回access_token字段
// 授权的应用信息，注意是一个数组，但仅旧的多应用套件授权时会返回多个agent，对新的单应用授权，永远只返回一个agent
func (p PermanentInfo) IsCustomizedApp() bool {
	if len(p.AuthInfo.Agent) <= 0 {
		return false
	}
	return p.AuthInfo.Agent[0].IsCustomizedApp
}

func (p PermanentInfo) AgentId() string {
	if len(p.AuthInfo.Agent) <= 0 {
		return ""
	}
	return stringutil.Int32ToString(p.AuthInfo.Agent[0].AgentId)
}

func (p PermanentInfo) AgentName() string {
	if len(p.AuthInfo.Agent) == 0 {
		return ""
	}
	return p.AuthInfo.Agent[0].Name
}

func (p PermanentInfo) Agent() Agent {
	if len(p.AuthInfo.Agent) == 0 {
		return Agent{}
	}
	return p.AuthInfo.Agent[0]
}

type AdminInfo struct {
	Userid     string `json:"userid"`
	OpenUserid string `json:"open_userid,omitempty"`
	AuthType   int    `json:"auth_type"`
}

type AdminList struct {
	Admins []AdminInfo `json:"admin"`
}

type QRCodeReq struct {
	SuiteId    string `json:"suite_id"` //第三方应用id
	State      string `json:"state"`    //state值，用于区分不同的安装渠道，可以填写a-zA-Z0-9，长度不可超过32个字节，默认为空。扫应用带参二维码授权安装后，获取企业永久授权码接口会返回该state值
	Appid      int64  `json:"appid,omitempty"`
	Style      int    `json:"style"`       //二维码样式选项，默认为不带说明外框小尺寸。0：带说明外框的二维码，适合于实体物料，1：带说明外框的二维码，适合于屏幕类，2：不带说明外框（小尺寸），3：不带说明外框（中尺寸），4：不带说明外框（大尺寸）。具体样式与服务商管理端获取到的应用二维码样式一一对应，参见下文二维码样式说明
	ResultType int    `json:"result_type"` //结果返回方式，默认为返回二维码图片buffer。1：二维码图片buffer，2：二维码图片url
}

type CustomizedAuthUrl struct {
	QrcodeUrl string `json:"qrcode_url"` //可用来生成二维码的授权url，需要开发者自行生成为二维码
	ExpiresIn int64  `json:"expires_in"` //有效期（秒）。10天过期。
}

type UserInfo3rd struct {
	CorpId     string `json:"corpid"`
	Userid     string `json:"userid"`
	UserTicket string `json:"user_ticket"`
	ExpiresIn  int64  `json:"expires_in"`
	OpenUserid string `json:"open_userid"`
}

type UserDetail3rd struct {
	CorpId string `json:"corpid"`
	Userid string `json:"userid"`
	Name   string `json:"name"`
	Gender string `json:"gender"`
	Avatar string `json:"avatar"`
	QrCode string `json:"qr_code"`
}

type LoginInfo struct {
	Usertype int           `json:"usertype"`
	UserInfo AuthUserInfo  `json:"user_info"`
	CorpInfo AuthCorpInfo  `json:"corp_info"`
	Agent    []LoginAgent  `json:"agent"`
	AuthInfo LoginAuthInfo `json:"auth_info"`
}

type LoginAgent struct {
	AgentId  int32 `json:"agentid"`
	AuthType int   `json:"auth_type"`
}

type LoginDepartment struct {
	Id       int64 `json:"id"`
	Writable bool  `json:"writable"`
}

type LoginAuthInfo struct {
	Department []LoginDepartment `json:"department"`
}

type SessionInfo struct {
	AuthType int `json:"auth_type"`
}

type UnAssignedInfo struct {
	HandoverUserid string `json:"handover_userid"` //离职成员的userid
	ExternalUserid string `json:"external_userid"` //外部联系人userid
	DimissionTime  int64  `json:"dimission_time"`  //成员离职时间
}

type UnAssignedList struct {
	Info       []UnAssignedInfo `json:"info"`
	IsLast     bool             `json:"is_last"`
	NextCursor string           `json:"next_cursor"`
}

type TransferResultReq struct {
	HandoverUserid string `json:"handover_userid"`
	TakeoverUserid string `json:"takeover_userid"`
	Cursor         string `json:"cursor,omitempty"`
}

type TransferResultResp struct {
	ExternalUserid string `json:"external_userid"`
	Status         int    `json:"status"`
	TakeoverTime   int64  `json:"takeover_time"`
}

type ChatIdConvertResult struct {
	Items             []ChatIdConvertItem `json:"items"`
	InvalidChatIdList []string            `json:"invalid_chat_id_list"`
}

type ChatIdConvertItem struct {
	ChatId    string `json:"chat_id"`
	NewChatId string `json:"new_chat_id"`
}
