package gdt

import (
	"gitee.com/h79/gothird/token/access"
)

// AccessToken
/**
"access_token": "c17348106551c8755165fc090a23ac49",
		"refresh_token": "67994d762a30165630e6e66223e72d4d",
		"access_token_expires_in": 604800,
		"refresh_token_expires_in": 2592000
*/
type AccessToken struct {
	AcsToken    string `json:"access_token"`
	RefToken    string `json:"refresh_token"`
	AcsExpireIn int64  `json:"access_token_expires_in"`
	RefExpireIn int64  `json:"refresh_token_expires_in"`
}

// access.Token interface implement
func (a AccessToken) GetAccessToken() access.Value {
	return access.Value{
		Data:     a.AcsToken,
		ExpireIn: access.ExpireSecond(a.AcsExpireIn),
	}
}

func (a AccessToken) GetRefAccessToken() access.Value {
	return access.Value{
		Data:     a.RefToken,
		ExpireIn: access.RefExpireSecond(a.RefExpireIn),
	}
}
