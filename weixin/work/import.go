package work

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/gothird/weixin/work/structs"
	"gitee.com/h79/goutils/common/http"
)

// ImportBatchSYncUser 增量更新成员
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/batch/syncuser?access_token=ACCESS_TOKEN
func ImportBatchSYncUser(api *token.Api, req *structs.ImportReq) (string, error) {
	var url = fmt.Sprintf("%s/cgi-bin/batch/syncuser?", consts.WorkApiPrefixUrl)
	return importInfo(api, url, req)
}

// ImportBatchReplaceUser 全量覆盖成员
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/batch/replaceuser?access_token=ACCESS_TOKEN
func ImportBatchReplaceUser(api *token.Api, req *structs.ImportReq) (string, error) {
	var url = fmt.Sprintf("%s/cgi-bin/batch/replaceuser?", consts.WorkApiPrefixUrl)
	return importInfo(api, url, req)
}

// ImportBatchReplaceDepart 全量覆盖部门
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/batch/replaceparty?access_token=ACCESS_TOKEN
func ImportBatchReplaceDepart(api *token.Api, req *structs.ImportReq) (string, error) {
	var url = fmt.Sprintf("%s/cgi-bin/batch/replaceparty?", consts.WorkApiPrefixUrl)
	return importInfo(api, url, req)
}

// ImportBatchResult 获取异步任务结果
// 请求方式：GET（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/batch/getresult?access_token=ACCESS_TOKEN&jobid=JOBID
func ImportBatchResult(api *token.Api, jobId string) (*structs.ImportResult, error) {
	var url = fmt.Sprintf("%s/cgi-bin/batch/getresult?jobid=%s&", consts.WorkApiPrefixUrl, jobId)
	var res = struct {
		response.Response
		structs.ImportResult
	}{}
	if err := api.Request("GET", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return nil, err
	}
	return &res.ImportResult, res.ErrorIf()
}

func importInfo(api *token.Api, url string, req interface{}) (string, error) {

	buf, err := json.Marshal(req)
	if err != nil {
		return "", err
	}

	var res = struct {
		response.Response
		JobId string `json:"jobid"`
	}{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return "", err
	}
	return res.JobId, res.ErrorIf()
}
