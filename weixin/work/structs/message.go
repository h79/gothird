package structs

type ToWho interface {
	IsGroup() (bool, *ToGroup)
	IsAgent() (bool, *ToAgent)
}

type ToGroup struct {
	ChatId string `json:"chatid,omitempty"`
}

func (to *ToGroup) IsGroup() (bool, *ToGroup) {
	return to.ChatId != "", to
}

func (to *ToGroup) IsAgent() (bool, *ToAgent) {
	return false, nil
}

type ToAgent struct {
	AgentId int32  `json:"agentid,omitempty"` //应用id
	ToUser  string `json:"touser,omitempty"`  //企业号中的用户帐号
	ToTag   string `json:"totag,omitempty"`   //企业号中的标签id，群发使用（推荐）
	ToParty string `json:"toparty,omitempty"` //企业号中的部门id，群发时使用。
}

func (to *ToAgent) IsGroup() (bool, *ToGroup) {
	return false, nil
}

func (to *ToAgent) IsAgent() (bool, *ToAgent) {
	return to.AgentId != 0, to
}

func (to *ToAgent) IsRoot() (bool, *ToRoot) {
	return false, nil
}

type ToRoot struct {
}

func (to *ToRoot) IsGroup() (bool, *ToGroup) {
	return false, nil
}

func (to *ToRoot) IsAgent() (bool, *ToAgent) {
	return false, nil
}

func (to *ToRoot) IsRoot() (bool, *ToRoot) {
	return true, nil
}

type Message interface {
	IsGroup() bool
	IsAgent() bool
	IsWelcome() bool
	GetType() string
}

var _ Message = (*Head)(nil)

// {
// "chatid": "",
// "touser": "UserID1|UserID2|UserID3",
// "toparty": " PartyID1 | PartyID2 ",
// "totag": " TagID1 | TagID2 ",
// "agentid": 1,
// "msgtype": "text",
// "text": {
// "content": "Holiday Service For Pony(http://xxxxx)"
// },
// "safe":0
// }
type Head struct {
	ChatId                 string `json:"chatid,omitempty"`
	ToUser                 string `json:"touser,omitempty"`  //企业号中的用户帐号 指定接收消息的成员，成员ID列表（多个接收者用‘|’分隔，最多支持1000个）。 特殊情况：指定为"@all"，则向该企业应用的全部成员发送
	ToParty                string `json:"toparty,omitempty"` //企业号中的部门id，群发时使用。指定接收消息的部门，部门ID列表，多个接收者用‘|’分隔，最多支持100个。 当touser为"@all"时忽略本参数
	ToTag                  string `json:"totag,omitempty"`   //企业号中的标签id，群发使用（推荐）,指定接收消息的标签，标签ID列表，多个接收者用‘|’分隔，最多支持100个。 当touser为"@all"时忽略本参数
	Type                   string `json:"msgtype"`
	AgentId                int32  `json:"agentid,omitempty"`        //应用id
	Safe                   int    `json:"safe"`                     //表示是否是保密消息;0表示可对外分享，1表示不能分享且内容显示水印，默认为0
	EnableIdTrans          int    `json:"enable_id_trans"`          //表示是否开启id转译，0表示否，1表示是，默认0。仅第三方应用需要用到，企业自建应用可以忽略。
	EnableDuplicateCheck   int    `json:"enable_duplicate_check"`   //表示是否开启重复消息检查，0表示否，1表示是，默认0
	DuplicateCheckInterval int    `json:"duplicate_check_interval"` //表示是否重复消息检查的时间间隔，默认1800s，最大不超过4小时
}

func (h *Head) IsGroup() bool {
	return h.ChatId != ""
}

func (h *Head) IsAgent() bool {
	return h.AgentId > 0
}

func (h *Head) IsWelcome() bool {
	return false
}

func (h *Head) GetType() string {
	return h.Type
}

// TextMsg 文本消息
type TextMsg struct {
	Head
	Text ExText `json:"text"`
}

// ImageMsg 图片消息
type ImageMsg struct {
	Head
	Image ExImage `json:"image"`
}

type MarkdownMsg struct {
	Head
	Markdown ExText `json:"markdown"`
}

// VoiceMsg 语音消息
type VoiceMsg struct {
	Head
	Voice ExVoice `json:"voice"`
}

// VideoMsg 视频消息
type VideoMsg struct {
	Head
	Video ExVideo `json:"video"`
}

// TextCardMsg 文本卡片消息
type TextCardMsg struct {
	Head
	TextCard TextCard `json:"textcard"`
}

// NewsMsg 图文消息
type NewsMsg struct {
	Head
	News News `json:"news"`
}

var _ Message = (*ExWelcomeMsg)(nil)

// ExWelcomeMsg 客户欢迎语
type ExWelcomeMsg struct {
	WelcomeCode string         `json:"welcome_code"`
	Text        ExText         `json:"text"`
	Attachments []ExAttachment `json:"attachments,omitempty"`
}

func (h *ExWelcomeMsg) IsGroup() bool {
	return false
}

func (h *ExWelcomeMsg) IsAgent() bool {
	return false
}

func (h *ExWelcomeMsg) IsWelcome() bool {
	return true
}

func (h *ExWelcomeMsg) GetType() string {
	return "welcome"
}

// TextCard
// "textcard" : {
// "title" : "领奖通知",
// "description" : "<div class=\"gray\">2016年9月26日</div> <div class=\"normal\">恭喜你抽中iPhone 7一台，领奖码：xxxx</div><div class=\"highlight\">请于2016年10月10日前联系行政同事领取</div>",
// "url" : "URL",
// "btntxt":"更多"
// }
type TextCard struct {
	Title  string `json:"title"`
	Desc   string `json:"description"`
	Url    string `json:"url"`
	BtnTxt string `json:"btntxt"`
}

// News
// "news" : {
// "articles" : [
// {
// "title" : "中秋节礼品领取",
// "description" : "今年中秋节公司有豪礼相送",
// "url" : "URL",
// "picurl" : "http://res.mail.qq.com/node/ww/wwopenmng/images/independent/doc/test_pic_msg1.png"
// }
type News struct {
	Articles []Articles `json:"articles"`
}

type Articles struct {
	Title  string `json:"title"`
	Desc   string `json:"description"`
	Url    string `json:"url"`
	Picurl string `json:"picurl"`
}

// Group 群组
type Group struct {
	Id    string   `json:"chatid"`
	Name  string   `json:"name"`
	Owner string   `json:"owner"`
	Users []string `json:"userlist"`
}

type ExAttachment struct {
	MsgType     string     `json:"msgtype"`
	Image       *ExImage   `json:"image,omitempty"`
	Link        *ExLink    `json:"link,omitempty"`
	Video       *ExVideo   `json:"video,omitempty"`
	File        *ExFile    `json:"file,omitempty"`
	MiniProgram *ExProgram `json:"miniprogram,omitempty"`
}

type ExLink struct {
	Title   string `json:"title"`
	Desc    string `json:"desc"`
	PicUrl  string `json:"picurl,omitempty"`
	Url     string `json:"url,omitempty"`
	MediaId string `json:"media_id,omitempty"`
}

type ExProgram struct {
	Title   string `json:"title"`        //"title": "消息标题",
	MediaId string `json:"pic_media_id"` //"pic_media_id": "MEDIA_ID",
	AppId   string `json:"appid"`        //"appid": "wx8bd80126147df384",
	Page    string `json:"page"`         //"page": "/path/index"
}

type ExText struct {
	Content string `json:"content"` //消息内容，最长不超过2048个字节，超过将截断（支持id转译）
}

type ExImage struct {
	MediaId string `json:"media_id"`
	PicUrl  string `json:"pic_url,omitempty"`
}

type ExVoice struct {
	MediaId string `json:"media_id"`
}

type ExVideo struct {
	MediaId string `json:"media_id"`
	Title   string `json:"title,omitempty"`
	Desc    string `json:"description,omitempty"`
}

type ExFile struct {
	MediaId string `json:"media_id"`
}

//企业群发消息模板
//{
//"external_userid": [
//"woAJ2GCAAAXtWyujaWJHDDGi0mACas1w",
//"wmqfasd1e1927831291723123109r712"
//],
//"sender":"zhangsan",
//"text": {
//"content":"文本消息内容"
//},
//"image": {
//"media_id": "MEDIA_ID"
//},
//"link": {
//"title": "消息标题",
//"picurl": "https://example.pic.com/path",
//"desc": "消息描述",
//"url": "https://example.link.com/path"
//},
//"miniprogram": {
//"title": "消息标题",
//"pic_media_id": "MEDIA_ID",
//"appid": "wx8bd80126147df384",
//"page": "/path/index"
//}
//}

type ExGroupMsg struct {
	ChatType    string       `json:"chat_type"`
	Sender      string       `json:"sender"`
	ChatIdList  []string     `json:"chat_id_list"`
	ExUseIds    []string     `json:"external_userid"`
	AllowSelect bool         `json:"allow_select"`
	Text        *ExText      `json:"text,omitempty"`
	Image       *ExImage     `json:"image,omitempty"`
	Link        *ExLink      `json:"link,omitempty"`
	Program     *ExProgram   `json:"miniprogram,omitempty"`
	TagFilter   TagFilter    `json:"tag_filter"`
	Attachments []Attachment `json:"attachments,omitempty"`
}
type TagFilter struct {
	GroupList []TagList `json:"group_list"`
}

type TagList struct {
	TagList []string `json:"tag_list"`
}

type ExGroupMsgResult struct {
	FailList []string `json:"fail_list"`
	MsgId    string   `json:"msgid"`
}

type MessageResponse struct {
	ErrCode        int32  `json:"errcode"`
	ErrMsg         string `json:"errmsg"`
	InvalidUser    string `json:"invaliduser"`
	InvalidParty   string `json:"invalidparty"`
	InvalidTag     string `json:"invalidtag"`
	UnlicensedUser string `json:"unlicenseduser"`
	MsgId          string `json:"msgid"`
	ResponseCode   string `json:"response_code"`
}
