package work

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/gothird/weixin/work/structs"
	"gitee.com/h79/goutils/common/http"
)

// ECCustomerStrategyList 获取规则组列表
// 企业可通过此接口获取企业配置的所有客户规则组id列表。
// 请求方式: POST(HTTP)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/customer_strategy/list?access_token=ACCESS_TOKEN
func ECCustomerStrategyList(corp *token.Api, cursor structs.Cursor) (*structs.StrategyList, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/customer_strategy/list?", consts.WorkApiPrefixUrl)
	buf, _ := json.Marshal(cursor)

	var res = struct {
		response.Response
		structs.StrategyList
	}{}
	err := corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return &res.StrategyList, res.ErrorIf()
}

// ECCustomerStrategyDetail 获取规则组详情
// 企业可以通过此接口获取某个客户规则组的详细信息。
// 请求方式: POST(HTTP)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/customer_strategy/get?access_token=ACCESS_TOKEN
func ECCustomerStrategyDetail(corp *token.Api, strategyId int32) (*structs.StrategyDetail, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/customer_strategy/get?", consts.WorkApiPrefixUrl)

	buf, _ := json.Marshal(struct {
		StrategyId int32 `json:"strategy_id"`
	}{
		StrategyId: strategyId,
	})
	var res = struct {
		response.Response
		Strategy structs.StrategyDetail `json:"strategy"`
	}{}
	err := corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return &res.Strategy, res.ErrorIf()
}

// ECCustomerStrategyRange 获取规则组管理范围
// 企业可通过此接口获取某个客户规则组管理的成员和部门列表
// 请求方式: POST(HTTP)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/customer_strategy/get_range?access_token=ACCESS_TOKEN
func ECCustomerStrategyRange(corp *token.Api, strategyId int32, cursor structs.Cursor) (*structs.StrategyRangeList, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/customer_strategy/get_range?", consts.WorkApiPrefixUrl)

	buf, _ := json.Marshal(struct {
		StrategyId int32 `json:"strategy_id"`
		structs.Cursor
	}{
		StrategyId: strategyId,
		Cursor:     cursor,
	})
	var res = struct {
		response.Response
		structs.StrategyRangeList
	}{}
	err := corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return &res.StrategyRangeList, res.ErrorIf()
}

// ECCustomerStrategyCreate 创建新的规则组
// 企业可通过此接口创建一个新的客户规则组。该接口仅支持串行调用，请勿并发创建规则组。
// 请求方式: POST(HTTP)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/customer_strategy/create?access_token=ACCESS_TOKEN
func ECCustomerStrategyCreate(corp *token.Api, req structs.StrategyCreate) (int32, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/customer_strategy/create?", consts.WorkApiPrefixUrl)

	buf, _ := json.Marshal(req)
	var res = struct {
		response.Response
		StrategyId int32 `json:"strategy_id"`
	}{}
	err := corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return 0, err
	}
	return res.StrategyId, res.ErrorIf()
}

// ECCustomerStrategyEdit 编辑规则组及其管理范围
// 企业可通过此接口编辑规则组的基本信息和修改客户规则组管理范围。该接口仅支持串行调用，请勿并发修改规则组。
// 请求方式: POST(HTTP)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/customer_strategy/edit?access_token=ACCESS_TOKEN
func ECCustomerStrategyEdit(corp *token.Api, req structs.StrategyEdit) error {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/customer_strategy/edit?", consts.WorkApiPrefixUrl)

	buf, _ := json.Marshal(req)
	var res = response.Response{}
	err := corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return err
	}
	return res.ErrorIf()
}

// ECCustomerStrategyDelete 删除规则组
// 企业可通过此接口删除某个规则组。
// 请求方式: POST(HTTP)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/customer_strategy/del?access_token=ACCESS_TOKEN
func ECCustomerStrategyDelete(corp *token.Api, strategyId int32) error {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/customer_strategy/del?", consts.WorkApiPrefixUrl)

	buf, _ := json.Marshal(struct {
		StrategyId int32 `json:"strategy_id"`
	}{
		StrategyId: strategyId,
	})
	var res = response.Response{}
	err := corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return err
	}
	return res.ErrorIf()
}
