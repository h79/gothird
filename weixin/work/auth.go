package work

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/gothird/weixin/work/structs"
	"gitee.com/h79/goutils/common/http"
)

//https://developer.work.weixin.qq.com/document/path/91120

//构造网页授权链接
//如果企业需要在打开的网页里面携带用户的身份信息，第一步需要构造如下的链接来获取code参数：
//https://open.weixin.qq.com/connect/oauth2/authorize?appid=CORPID&redirect_uri=REDIRECT_URI&response_type=code&scope=snsapi_base&state=STATE&agentid=AGENTID#wechat_redirect

// GetUserDetail 获取访问用户敏感信息
// 自建应用与代开发应用可通过该接口获取成员授权的敏感字段
// 对于自建应用与代开发应用，敏感字段需要管理员在应用详情里选择，且成员oauth2授权时确认后才返回。
// 敏感字段包括：性别、头像、员工个人二维码、手机、邮箱、企业邮箱、地址。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/auth/getuserdetail?access_token=ACCESS_TOKEN
func GetUserDetail(api *token.Api, ticket string) (*structs.UserInfoDetail, error) {
	url := fmt.Sprintf("%s/cgi-bin/auth/getuserdetail?", consts.WorkApiPrefixUrl)

	var in = struct {
		Ticket string `json:"user_ticket"`
	}{
		Ticket: ticket,
	}
	buf, _ := json.Marshal(&in)

	var res = struct {
		response.Response
		structs.UserInfoDetail
	}{}
	err := api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	return &res.UserInfoDetail, res.ErrorIf()
}

// GetUserInfo 获取访问用户身份
// 该接口用于根据code获取成员信息
// 请求方式：GET（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/auth/getuserinfo?access_token=ACCESS_TOKEN&code=CODE
func GetUserInfo(api *token.Api, code string) (*structs.UserInfo, error) {
	url := fmt.Sprintf("%s/cgi-bin/auth/getuserinfo?code=%s&", consts.WorkApiPrefixUrl, code)

	var res = struct {
		response.Response
		structs.UserInfo
	}{}
	err := api.Request("GET", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	return &res.UserInfo, res.ErrorIf()
}
