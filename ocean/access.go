package ocean

import (
	"gitee.com/h79/gothird/token/access"
)

type AccessToken struct {
	AcsToken    string `json:"access_token"`
	RefToken    string `json:"refresh_token"`
	AcsExpireIn int64  `json:"expires_in"`
	RefExpireIn int64  `json:"refresh_token_expires_in"`
}

// access.Token interface implement
func (a AccessToken) GetAccessToken() access.Value {
	return access.Value{
		Data:     a.AcsToken,
		ExpireIn: access.ExpireSecond(a.AcsExpireIn),
	}
}

func (a AccessToken) GetRefAccessToken() access.Value {
	return access.Value{
		Data:     a.RefToken,
		ExpireIn: access.RefExpireSecond(a.RefExpireIn),
	}
}
