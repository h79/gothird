package work

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/gothird/weixin/work/structs"
	"gitee.com/h79/goutils/common/http"
)

// AGGetDetail 获取指定的应用详情
// 对于互联企业的应用，如果需要获取应用可见范围内其他互联企业的部门与成员，请调用互联企业-获取应用可见范围接口
// 请求方式：GET（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/agent/get?access_token=ACCESS_TOKEN&agentid=AGENTID
func AGGetDetail(api *token.Api, agentId string) (structs.AgentDetail, error) {
	url := fmt.Sprintf("%s/cgi-bin/agent/get?agentid=%s&", consts.WorkApiPrefixUrl, agentId)
	var res = struct {
		response.Response
		structs.AgentDetail
	}{}
	err := api.Request("GET", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return structs.AgentDetail{}, err
	}
	return res.AgentDetail, res.ErrorIf()
}

// AGGetList 请求方式：GET（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/agent/list?access_token=ACCESS_TOKEN
func AGGetList(api *token.Api) ([]structs.AgentInfo, error) {
	url := fmt.Sprintf("%s/cgi-bin/agent/list?", consts.WorkApiPrefixUrl)
	var res = struct {
		response.Response
		List []structs.AgentInfo `json:"agentlist"`
	}{}
	err := api.Request("GET", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	return res.List, res.ErrorIf()
}
