package dingtalk

import (
	"gitee.com/h79/goutils/common/data"
	"testing"
)

type dingTalkToken struct {
}

func (dt *dingTalkToken) GetAppId() string {
	return ""
}

func (dt *dingTalkToken) GetSecret() string {
	return ""
}

func (dt *dingTalkToken) GetAccessToken(options ...data.OptionsFunc) (string, error) {
	return "", nil
}

func TestSend(t *testing.T) {
	tk := &dingTalkToken{}

	toWho := ToRoot{}
	//msg := NewMessage(&toWho, "text", nil, Content{Content: "crm call(option, content)"})
	//res, _ := SendMessage(tk, msg)
	//t.Logf("SendMessage: %s", res)

	msg1 := NewMessage(&toWho, "link", nil, Link{Title: "Hi", Picurl: "https://lupic.cdn.bcebos.com/20210629/31261794_14.jpg", Text: "crm我是测试机器人", Url: "https://www.baidu.com/"})
	res1, _ := SendMessage(tk, msg1)

	t.Logf("SendMessage: %s", res1)
}
