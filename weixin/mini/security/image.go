package security

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/media"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/goutils/common/http"
)

//POST https://api.weixin.qq.com/wxa/img_sec_check?access_token=ACCESS_TOKEN

func Image(api *token.Api, filename string) error {
	uri := fmt.Sprintf("%s/wxa/img_sec_check?", consts.ApiPrefixUrl)
	var img = media.Image{
		FileName: filename,
	}
	var res = response.Response{}
	er := api.Upload(uri, "media", &img, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})
	return er
}
