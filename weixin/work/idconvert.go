package work

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/gothird/weixin/work/structs"
	"gitee.com/h79/goutils/common/http"
)

// IDCTmpExternalUserId tmp_external_userid的转换
// 将应用获取的外部用户临时id，tmp_external_userid，转换为external_userid。
//
// 支持将以下业务类型（business_type）对应接口获取到的tmp_external_userid进行转换：
//
// 业务类型	描述	相关接口
// 1	会议	获取会议详情
// 2	收集表	收集表的统计信息查询
// 读取收集表答案
// 支持对以下用户类型（user_type）进行转换：
//
// 用户类型	描述	转换范围
// 1	客户	应用可见范围内，配置了客户联系功能的企业成员所添加的外部联系人
// 2	企业互联	共享应用配置共享范围内的成员
// 3	上下游	共享应用配置共享范围内的成员
// 4	互联企业（圈子）	管理后台配置的应用可见范围内的成员
//
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/idconvert/convert_tmp_external_userid?access_token=ACCESS_TOKEN
func IDCTmpExternalUserId(api *token.Api, req structs.TmpExternalUser) (*structs.TmpExternalUserResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/idconvert/convert_tmp_external_userid?", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		structs.TmpExternalUserResult
	}{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return nil, err
	}
	return &res.TmpExternalUserResult, res.ErrorIf()
}

// IDCKfId 微信客服ID的转换
// 最后更新：2022/09/21
// 将企业主体下的微信客服ID转换成服务商主体下的微信客服ID。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/idconvert/open_kfid?access_token=ACCESS_TOKEN
func IDCKfId(api *token.Api, id []string) (*structs.KFIDResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/idconvert/open_kfid?", consts.WorkApiPrefixUrl)
	req := struct {
		Ids []string `json:"open_kfid_list"`
	}{
		Ids: id,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		structs.KFIDResult
	}{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return nil, err
	}
	return &res.KFIDResult, res.ErrorIf()
}

// IDCTagId 客户标签ID的转换
// 最后更新：2023/05/19
// 将企业主体下的客户标签ID(含标签组ID)转换成服务商主体下的客户标签ID。最多不超过1000个
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/idconvert/external_tagid?access_token=ACCESS_TOKEN
func IDCTagId(api *token.Api, tagId []string) (*structs.ExternalTagIDResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/idconvert/external_tagid?", consts.WorkApiPrefixUrl)
	req := struct {
		Ids []string `json:"external_tagid_list"`
	}{
		Ids: tagId,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		structs.ExternalTagIDResult
	}{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return nil, err
	}
	return &res.ExternalTagIDResult, res.ErrorIf()
}

// IDCUnionIdToExternalUserId unionid转换为第三方external_userid
// 当微信用户进入服务商的小程序或公众号时，服务商可通过此接口，将微信客户的unionid转为第三方主体的external_userid，若该微信用户尚未成为企业的客户，则返回pending_id。
// 小程序或公众号的主体名称可以是企业的，也可以是服务商的。
// 该接口有调用频率限制，当subject_type为0时，按企业作如下的限制：10万次/小时、48万次/天、750万次/月；（注意这里是所有服务商共用企业额度的）
// 当subject_type为1时，按服务商作如下的限制：10万次/小时、48万次/天、750万次/月。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/idconvert/unionid_to_external_userid?access_token=ACCESS_TOKEN
func IDCUnionIdToExternalUserId(api *token.Api, req structs.UnionIdTo) (*structs.UnionIdToResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/idconvert/unionid_to_external_userid?", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		structs.UnionIdToResult
	}{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return nil, err
	}
	return &res.UnionIdToResult, res.ErrorIf()
}

// IDCExternalUserIdToPendingId external_userid查询pending_id
// 该接口可用于当一个微信用户成为企业客户前已经使用过服务商服务（服务商侧曾经调用过unionid转换为第三方external_userid）的场景。
// 本接口获取到的pending_id可以维持unionid和external_userid的关联关系。
// pending_id有效期为90天，超过有效期之后，将无法通过该接口将external_userid换取对应的pending_id。
// 仅认证企业可调用
// 该客户的跟进人或其所在客户群群主必须在应用的可见范围之内
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/idconvert/batch/external_userid_to_pending_id?access_token=ACCESS_TOKEN
func IDCExternalUserIdToPendingId(api *token.Api, req structs.ExtToPendingId) ([]structs.ExtToPendingIdResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/idconvert/batch/external_userid_to_pending_id?", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		Result []structs.ExtToPendingIdResult `json:"result"`
	}{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return nil, err
	}
	return res.Result, res.ErrorIf()
}

// IDCApplyMassCallTicket 获取接口高频调用凭据
// 权限说明：
// 仅限授权三个月内的企业获取，且每个企业最多仅能获取一次
// 该凭据获取成功后，有效期为7天，请在7天内完成企业的初始化操作
// 使用该凭据可以不受业务频率限制，但是依然受到基础频率限制
// 请求方式：GET（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/corp/apply_mass_call_ticket?access_token=ACCESS_TOKEN
func IDCApplyMassCallTicket(api *token.Api) (string, error) {
	url := fmt.Sprintf("%s/cgi-bin/corp/apply_mass_call_ticket?", consts.WorkApiPrefixUrl)
	var res = struct {
		response.Response
		Ticket string `json:"mass_call_ticket"`
	}{}
	if err := api.Request("POST", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return "", err
	}
	return res.Ticket, res.ErrorIf()
}

// IDCApplyToUpgradeChatId 申请群ID的加密升级
// 设置代开发应用群ID完成加密升级的时间。在调用该接口后，用户可以使用新旧两种群ID调用相关接口。在到达设置的完成升级的时间点后，用户必须使用升级后的群ID调用接口，企业微信会返回升级后的群ID。
//
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/idconvert/apply_to_upgrade_chatid?access_token=ACCESS_TOKEN
func IDCApplyToUpgradeChatId(api *token.Api, upgradeTime int64) error {
	url := fmt.Sprintf("%s/cgi-bin/idconvert/apply_to_upgrade_chatid?", consts.WorkApiPrefixUrl)
	var req = struct {
		Time int64 `json:"upgrade_time"`
	}{
		Time: upgradeTime,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return err
	}
	var res = response.Response{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return err
	}
	return res.ErrorIf()
}

// IDCChatId 群ID转换接口
// 用户可以将升级前的群ID转换成升级后的群ID。如果传入升级后的群ID则原样返回。
// chatId 需要转换的群ID列表，最多输入100个
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/idconvert/chatid?access_token=ACCESS_TOKEN
func IDCChatId(api *token.Api, chatId []string) (structs.ChatIdConvertResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/idconvert/chatid?", consts.WorkApiPrefixUrl)
	var req = struct {
		List []string `json:"chat_id_list"`
	}{
		List: chatId,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return structs.ChatIdConvertResult{}, err
	}
	var res = struct {
		response.Response
		structs.ChatIdConvertResult
	}{}
	err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return res.ChatIdConvertResult, err
	}
	return res.ChatIdConvertResult, res.ErrorIf()
}

// IDBatchUserIdToService userid的转换
// doc: https://developer.work.weixin.qq.com/document/path/95435
// 将企业主体下的明文userid转换为服务商主体下的密文userid。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/batch/userid_to_openuserid?access_token=ACCESS_TOKEN
func IDBatchUserIdToService(api *token.Api, user *structs.UserIdReq) (*structs.OpenUserId, error) {

	url := fmt.Sprintf("%s/cgi-bin/batch/userid_to_openuserid?", consts.WorkApiPrefixUrl)

	buf, err := json.Marshal(user)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		structs.OpenUserId
	}{}
	err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	return &res.OpenUserId, res.ErrorIf()
}

// IDUnionIdToExternalUserid 企业主体unionid转换为第三方external_userid
// https://developer.work.weixin.qq.com/document/path/93274
// 针对服务商为企业代开发微信小程序的场景，服务商可通过此接口，将微信客户的unionid转为external_userid
// 服务商代开发小程序指企业使用的小程序为企业主体的，非服务商主体的小程序。
// 场景：企业客户在微信端从企业主体的小程序（非服务商应用）登录，同时企业在企业微信安装了服务商的第三方应用或代开发应用
// 服务商可以调用该接口将登录用户的unionid转换为服务商全局唯一的外部联系人id
// 仅认证企业可调用
// unionid（即微信开放平台账号主体）与openid（即小程序账号主体）需要认证
// unionid（unionid的主体为绑定了该小程序的微信开放平台账号主体）与openid（即小程序账号主体）的主体需与当前企业的主体一致。
// openid与unionid必须是在同一个小程序获取到的
// 该客户的跟进人或其所在客户群群主必须在应用的可见范围之内
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/unionid_to_external_userid?access_token=ACCESS_TOKEN
func IDUnionIdToExternalUserid(corp *token.Api, unionId, openId string) (string, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/unionid_to_external_userid?", consts.WorkApiPrefixUrl)
	var body = struct {
		UnionId string `json:"unionid"`
		Openid  string `json:"openid"`
	}{
		UnionId: unionId,
		Openid:  openId,
	}
	buf, _ := json.Marshal(&body)
	var res = struct {
		response.Response
		ExternalUserId string `json:"external_userid"`
	}{}
	err := corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return "", err
	}
	return res.ExternalUserId, res.ErrorIf()
}

// IDUnionIdToExternalUserId3rd 第三方主体unionid转换为第三方external_userid
// https://developer.work.weixin.qq.com/document/path/95878
// 当微信用户在微信中使用第三方应用的小程序或公众号时，第三方可将获取到的unionid与openid，
// 1. 该企业授权了该服务商第三方应用
// 2. 调用频率最大为5万次/小时，24万次/天
// 3. unionid和openid的主体需与服务商的主体一致
// 4. openid与unionid必须是在同一个小程序或同一个公众号获取到的
// 5. 相应外部成员跟进人在应用可见范围内 或 相应外部成员所属外联群群主在应用可见范围内。
// 6. 当微信用户为家长时，仅返回应用的可见范围内的家长。
// 7. 外联群场景中，本接口corpid为必填项。
// 8. 微信客服场景中，仅返回48小时内客服会话的external_userid。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/unionid_to_external_userid_3rd?suite_access_token=ACCESS_TOKEN
func IDUnionIdToExternalUserId3rd(corp *token.Api, corpId, unionId, openId string) ([]structs.ExternalUserIdInfo, error) {
	url := fmt.Sprintf("%s/cgi-bin/service/externalcontact/unionid_to_external_userid_3rd?", consts.WorkApiPrefixUrl)
	var body = struct {
		UnionId string `json:"unionid"`
		Openid  string `json:"openid"`
		CorpId  string `json:"corpid,omitempty"` //建议尽可能传入corpid参数，可获得更好的性能
	}{
		UnionId: unionId,
		Openid:  openId,
		CorpId:  corpId,
	}
	buf, _ := json.Marshal(&body)
	var res = struct {
		response.Response
		ExternalUserIdInfos []structs.ExternalUserIdInfo `json:"external_userid_info"`
	}{}
	err := corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return res.ExternalUserIdInfos, res.ErrorIf()
}

// IDExternalUserIdToService 代开发应用external_userid转换
// 企业同时授权了服务商的第三方应用与代开发应用，服务商可使用该接口将代开发应用获取到的external_userid转换为第三方应用的external_userid， 从而进行关联映射。
// 若代开发自建应用已升级（升级说明见 代开发应用安全性升级），则获取到的external_userid与第三方应用一致，故无须调用该接口。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/to_service_external_userid?access_token=ACCESS_TOKEN
func IDExternalUserIdToService(corp *token.Api, externalUserid string) (string, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/to_service_external_userid?", consts.WorkApiPrefixUrl)
	var body = struct {
		ExternalUserid string `json:"external_userid"`
	}{
		ExternalUserid: externalUserid,
	}
	buf, _ := json.Marshal(&body)
	var res = struct {
		response.Response
		ExternalUserId string `json:"external_userid"`
	}{}
	err := corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return "", err
	}
	return res.ExternalUserId, res.ErrorIf()
}

// IDOpenGroupIdToChatId 客户群opengid转换
// 用户在微信里的客户群里打开小程序时，某些场景下可以获取到群的opengid，如果该群是企业微信的客户群，则企业或第三方可以调用此接口将一个opengid转换为客户群chat_id
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/opengid_to_chatid?access_token=ACCESS_TOKEN
func IDOpenGroupIdToChatId(corp *token.Api, openGroupId string) (string, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/opengid_to_chatid?", consts.WorkApiPrefixUrl)

	var body = struct {
		GroupId string `json:"opengid"` //小程序在微信获取到的群ID
	}{
		GroupId: openGroupId,
	}
	buf, err := json.Marshal(&body)
	if err != nil {
		return "", err
	}
	var res = struct {
		response.Response
		ChatId string `json:"chat_id"` //	客户群ID，可以用来调用获取客户群详情
	}{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return "", err
	}
	return res.ChatId, res.ErrorIf()
}

// IDBatchUserIdFromService userid转换
// 将代开发应用或第三方应用获取的密文open_userid转换为明文userid。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/batch/openuserid_to_userid?access_token=ACCESS_TOKEN
func IDBatchUserIdFromService(corp *token.Api, openUseridList []string, sourceAgentId int32) (*structs.UserIdResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/batch/openuserid_to_userid?", consts.WorkApiPrefixUrl)

	var req = struct {
		SourceAgentId  int32    `json:"source_agentid"`
		OpenUserIdList []string `json:"open_userid_list"`
	}{
		SourceAgentId:  sourceAgentId,
		OpenUserIdList: openUseridList,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		Userid structs.UserIdResult
	}{}
	if err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	}); err != nil {
		return nil, err
	}
	return &res.Userid, res.ErrorIf()
}

// IDExternalUserIdFromService external_userid转换
// 将代开发应用或第三方应用获取的externaluserid转换成自建应用的externaluserid。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/from_service_external_userid?access_token=ACCESS_TOKEN
func IDExternalUserIdFromService(corp *token.Api, externalUserid string, sourceAgentId int32) (*structs.NewExternalUserid, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/from_service_external_userid?", consts.WorkApiPrefixUrl)

	var req = struct {
		SourceAgentId  int32  `json:"source_agentid"`
		ExternalUserid string `json:"external_userid"`
	}{
		SourceAgentId:  sourceAgentId,
		ExternalUserid: externalUserid,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		NewExternalUserid string `json:"external_userid"`
	}{}
	if err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	}); err != nil {
		return nil, err
	}
	return &structs.NewExternalUserid{ExternalUserid: externalUserid, NewExternalUserid: res.NewExternalUserid}, res.ErrorIf()
}

// IDNewExternalUserIdToService external_userid的转换
// https://developer.work.weixin.qq.com/document/path/95435
// 将企业主体下的external_userid转换为服务商主体下的external_userid。
// 转换客户external_userid
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_new_external_userid?access_token=ACCESS_TOKEN
func IDNewExternalUserIdToService(corp *token.Api, externalUseridList []string) ([]structs.NewExternalUserid, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/get_new_external_userid?", consts.WorkApiPrefixUrl)

	return newExternalUserId(corp, url, "", externalUseridList)
}

// IDGroupNewExternalUserIdToService 转换客户群成员external_userid
// 转换客户external_userid接口不支持客户群的场景，如果需要转换客户群中无好友关系的群成员external_userid，需要调用本接口，调用时需要传入客户群的chat_id。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/groupchat/get_new_external_userid?access_token=ACCESS_TOKEN
func IDGroupNewExternalUserIdToService(corp *token.Api, chatId string, externalUseridList []string) ([]structs.NewExternalUserid, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/groupchat/get_new_external_userid?", consts.WorkApiPrefixUrl)

	return newExternalUserId(corp, url, chatId, externalUseridList)
}

func newExternalUserId(corp *token.Api, url string, chatId string, externalUseridList []string) ([]structs.NewExternalUserid, error) {
	var req = struct {
		ChatId             string   `json:"chat_id,omitempty"`
		ExternalUseridList []string `json:"external_userid_list"`
	}{
		ChatId:             chatId,
		ExternalUseridList: externalUseridList,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		Items []structs.NewExternalUserid `json:"items"`
	}{}
	if err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	}); err != nil {
		return nil, err
	}
	return res.Items, res.ErrorIf()
}

// IDFinishOpenidMigration ID迁移完成状态的设置
// 最后更新：2023/08/03
// 服务商完成企业下所有第三方应用的新旧id（userid/corpid/external_userid)迁移后，即可主动将该企业设置为“迁移完成”。设置后，第三方应用获取到的是升级后的id。注意，该接口需要使用provider_access_token来调用。
// 1.当服务商第三方应用授权给本企业时，无需调用本接口;
// 2.当服务商对某企业设置external_userid迁移完成后，接口不再吐出该企业相关的unionid;
// 3.userid与corpid只能同时设置为迁移完成，external_userid可以单独设置。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/service/finish_openid_migration?provider_access_token=ACCESS_TOKEN
// openidType 1-userid与corpid; 3-external_userid
func IDFinishOpenidMigration(api *token.Api, corpId string, openidType []int) error {
	url := fmt.Sprintf("%s/cgi-bin/service/finish_openid_migration?", consts.WorkApiPrefixUrl)
	var req = struct {
		CorpId     string `json:"corpid"`
		OpenidType []int  `json:"openid_type"`
	}{
		OpenidType: openidType,
		CorpId:     corpId,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return err
	}
	var res = response.Response{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return err
	}
	return res.ErrorIf()
}
