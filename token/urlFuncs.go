package token

import (
	"fmt"
	"strings"
)

func ComponentUrl(acsKey string, uri string) string {
	if strings.HasSuffix(uri, "&") || strings.HasSuffix(uri, "?") {
		return fmt.Sprintf("%scomponent_access_token=%s", uri, acsKey)
	}
	return fmt.Sprintf("%s&component_access_token=%s", uri, acsKey)
}

func DefaultUrl(acsKey string, uri string) string {
	if strings.HasSuffix(uri, "&") || strings.HasSuffix(uri, "?") {
		return fmt.Sprintf("%saccess_token=%s", uri, acsKey)
	}
	return fmt.Sprintf("%s&access_token=%s", uri, acsKey)
}

func EmptyUrl(acsKey string, uri string) string {
	return uri
}
