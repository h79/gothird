package structs

type Rule struct {
	RuleId          string          `json:"rule_id,omitempty"`
	RuleName        string          `json:"rule_name"`
	WordList        []string        `json:"word_list"`
	SemanticsList   []int32         `json:"semantics_list"`   //额外的拦截语义规则，1：手机号、2：邮箱地:、3：红包
	InterceptType   int32           `json:"intercept_type"`   //拦截方式，1:警告并拦截发送；2:仅发警告
	ApplicableRange ApplicableRange `json:"applicable_range"` //敏感词适用范围，userid与department不能同时为不填
}

type ApplicableRange struct {
	UserList       []string `json:"user_list"`
	DepartmentList []int32  `json:"department_list"`
}

type RuleInfo struct {
	RuleId     string `json:"rule_id"`
	RuleName   string `json:"rule_name"`
	CreateTime int64  `json:"create_time"`
}

type RuleChange struct {
	RuleId                string          `json:"rule_id"`
	RuleName              string          `json:"rule_name"`
	WordList              []string        `json:"word_list"` //敏感词列表，敏感词长度1~32个utf8字符，列表大小不能超过300个；若为空忽略该字段
	ExtraRule             RuleExtra       `json:"extra_rule"`
	InterceptType         int32           `json:"intercept_type"`
	AddApplicableRange    ApplicableRange `json:"add_applicable_range"`
	RemoveApplicableRange ApplicableRange `json:"remove_applicable_range"`
}

type RuleExtra struct {
	SemanticsList []int32 `json:"semantics_list"`
}
