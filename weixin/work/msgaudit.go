package work

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/gothird/weixin/work/structs"
	"gitee.com/h79/goutils/common/http"
)

// MAGetGroupChat 获取会话内容存档内部群信息
// 最后更新：2021/01/07
// 企业可通过此接口，获取会话内容存档本企业的内部群信息，包括群名称、群主id、公告、群创建时间以及所有群成员的id与加入时间。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/msgaudit/groupchat/get?access_token=ACCESS_TOKEN
func MAGetGroupChat(api *token.Api, roomId string) (*structs.RoomInfo, error) {
	var url = fmt.Sprintf("%s/cgi-bin/msgaudit/groupchat/get?", consts.WorkApiPrefixUrl)
	var body = struct {
		RoomId string `json:"roomid"`
	}{
		RoomId: roomId,
	}
	buf, _ := json.Marshal(&body)
	var res = struct {
		response.Response
		structs.RoomInfo
	}{}
	err := api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	return &res.RoomInfo, res.ErrorIf()
}

// 获取会话同意情况

// MACheckSingleAgree 目前一次请求只支持最多100个查询条目
// 最后更新：2021/04/29
// 企业可通过下述接口，获取会话中外部成员的同意情况
// 单聊请求地址：https://qyapi.weixin.qq.com/cgi-bin/msgaudit/check_single_agree?access_token=ACCESS_TOKEN
// 请求方式：POST（HTTPS）
func MACheckSingleAgree(api *token.Api, agrees []structs.SingleAgree) ([]structs.AgreeInfo, error) {
	var url = fmt.Sprintf("%s/cgi-bin/msgaudit/check_single_agree?", consts.WorkApiPrefixUrl)
	var body = struct {
		Agrees []structs.SingleAgree `json:"info"`
	}{
		Agrees: agrees,
	}
	buf, _ := json.Marshal(&body)
	var res = struct {
		response.Response
		AgreeInfo []structs.AgreeInfo `json:"agreeinfo"`
	}{}
	err := api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	return res.AgreeInfo, res.ErrorIf()
}

// MACheckRoomAgree 群聊
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/msgaudit/check_room_agree?access_token=ACCESS_TOKEN
// 请求方式：POST（HTTPS）
func MACheckRoomAgree(api *token.Api, roomId string) ([]structs.AgreeInfo, error) {
	var url = fmt.Sprintf("%s/cgi-bin/msgaudit/check_room_agree?", consts.WorkApiPrefixUrl)
	var body = struct {
		RoomId string `json:"roomid"`
	}{
		RoomId: roomId,
	}
	buf, _ := json.Marshal(&body)
	var res = struct {
		response.Response
		AgreeInfo []structs.AgreeInfo `json:"agreeinfo"`
	}{}
	err := api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	return res.AgreeInfo, res.ErrorIf()
}

// MAGetPermitUserList 获取会话内容存档开启成员列表
// 最后更新：2020/08/27
// 企业可通过此接口，获取企业开启会话内容存档的成员列表
// ty: 拉取对应版本的开启成员列表。1表示办公版；2表示服务版；3表示企业版。非必填，不填写的时候返回全量成员列表。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/msgaudit/get_permit_user_list?access_token=ACCESS_TOKEN
func MAGetPermitUserList(api *token.Api, ty int) ([]string, error) {
	var url = fmt.Sprintf("%s/cgi-bin/msgaudit/get_permit_user_list?", consts.WorkApiPrefixUrl)
	var body = struct {
		Type int `json:"type,omitempty"`
	}{
		Type: ty,
	}
	buf, _ := json.Marshal(&body)
	var res = struct {
		response.Response
		Id []string `json:"ids"`
	}{}
	err := api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	return res.Id, res.ErrorIf()
}

//301052	会话存档已过期
