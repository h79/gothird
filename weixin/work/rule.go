package work

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/gothird/weixin/work/structs"
	"gitee.com/h79/goutils/common/http"
)

// ECAddInterceptRule 新建敏感词规则
// 企业和第三方应用可以通过此接口新建敏感词规则
// 请求方式：POST(HTTPS)
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/add_intercept_rule?access_token=ACCESS_TOKEN
func ECAddInterceptRule(corp *token.Api, req *structs.Rule) (string, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/add_intercept_rule?", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(req)
	if err != nil {
		return "", err
	}
	var res = struct {
		response.Response
		RuleId string `json:"rule_id"`
	}{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return "", err
	}
	return res.RuleId, res.ErrorIf()
}

// ECGetInterceptRule 获取敏感词规则列表
// 企业和第三方应用可以通过此接口获取敏感词规则列表
// 请求方式：GET(HTTPS)
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_intercept_rule_list?access_token=ACCESS_TOKEN
func ECGetInterceptRule(corp *token.Api) ([]structs.RuleInfo, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/get_intercept_rule_list?", consts.WorkApiPrefixUrl)
	var res = struct {
		response.Response
		Result []structs.RuleInfo `json:"rule_list"`
	}{}
	err := corp.Request("GET", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return res.Result, res.ErrorIf()
}

// ECGetInterceptRuleDetail 获取敏感词规则详情
// 企业和第三方应用可以通过此接口获取敏感词规则详情
// 请求方式：POST(HTTPS)
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_intercept_rule?access_token=ACCESS_TOKEN
func ECGetInterceptRuleDetail(corp *token.Api, ruleId string) (structs.Rule, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/get_intercept_rule?", consts.WorkApiPrefixUrl)
	req := struct {
		RuleId string `json:"rule_id"`
	}{
		RuleId: ruleId,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return structs.Rule{}, err
	}
	var res = struct {
		response.Response
		Result structs.Rule `json:"rule"`
	}{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return structs.Rule{}, err
	}
	return res.Result, res.ErrorIf()
}

// ECUpdateInterceptRule 修改敏感词规则
// 企业和第三方应用可以通过此接口修改敏感词规则
// 请求方式：POST(HTTPS)
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/update_intercept_rule?access_token=ACCESS_TOKEN
func ECUpdateInterceptRule(corp *token.Api, req *structs.RuleChange) error {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/update_intercept_rule?", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(req)
	if err != nil {
		return err
	}
	var res = response.Response{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return err
	}
	return res.ErrorIf()
}

// ECDeleteInterceptRule 删除敏感词规则
// 企业和第三方应用可以通过此接口修改敏感词规则
// 请求方式：POST(HTTPS)
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/del_intercept_rule?access_token=ACCESS_TOKEN
func ECDeleteInterceptRule(corp *token.Api, ruleId string) error {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/del_intercept_rule?", consts.WorkApiPrefixUrl)
	req := struct {
		RuleId string `json:"rule_id"`
	}{
		RuleId: ruleId,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return err
	}
	var res = response.Response{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return err
	}
	return res.ErrorIf()
}
