package feishu

import (
	"gitee.com/h79/gothird/token/access"
)

type AccessToken struct {
	Data     string `json:"tenant_access_token"`
	ExpireIn int64  `json:"expire"`
}

func (a AccessToken) GetAccessToken() access.Value {
	return access.Value{
		Data:     a.Data,
		ExpireIn: access.ExpireSecond(a.ExpireIn),
	}
}

func (a AccessToken) GetRefAccessToken() access.Value {
	return access.Value{}
}
