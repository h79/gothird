package service

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/token/access"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	workaccess "gitee.com/h79/gothird/weixin/work/access"
	"gitee.com/h79/gothird/weixin/work/structs"
	"gitee.com/h79/goutils/common/data"
	"gitee.com/h79/goutils/common/http"
)

// GetProviderToken 获取服务商凭证
// 开发者需要缓存provider_access_token，用于后续接口的调用（注意：不能频繁调用get_provider_token接口，否则会受到频率拦截）。
// 当provider_access_token失效或过期时，需要重新获取。
// provider_access_token的有效期通过返回的expires_in来传达，正常情况下为7200秒（2小时），有效期内重复获取返回相同结果，过期后获取会返回新的provider_access_token。
// provider_access_token至少保留512字节的存储空间。
// 企业微信可能会出于运营需要，提前使provider_access_token失效，开发者应实现provider_access_token失效时重新获取的逻辑
func GetProviderToken(corp token.Token, d data.D) (access.Token, error) {
	return getProviderToken(corp)
}

// GetSuiteToken 获取第三方应用凭证
// ticket suite_ticket
// 由于第三方服务商可能托管了大量的企业，其安全问题造成的影响会更加严重，故API中除了合法来源IP校验之外，还额外增加了suite_ticket作为安全凭证。
// 获取suite_access_token时，需要suite_ticket参数。suite_ticket由企业微信后台定时推送给“指令回调URL”，每十分钟更新一次，见推送suite_ticket。
// suite_ticket实际有效期为30分钟，可以容错连续两次获取suite_ticket失败的情况，但是请永远使用最新接收到的suite_ticket。
// 通过本接口获取的suite_access_token有效期为2小时，开发者需要进行缓存，不可频繁获取。
func GetSuiteToken(corp token.Token, d data.D) (access.Token, error) {
	ticket, err := corp.GetCode("suite_ticket")
	if err != nil {
		return nil, err
	}
	return getSuiteToken(corp, ticket)
}

func getSuiteToken(corp token.Base, suiteTicket string) (access.Token, error) {

	var req = struct {
		SuiteId     string `json:"suite_id"`
		SuiteSecret string `json:"suite_secret"`
		SuiteTicket string `json:"suite_ticket"`
	}{
		SuiteId:     corp.GetAppId(),
		SuiteSecret: corp.GetSecret(),
		SuiteTicket: suiteTicket,
	}
	if req.SuiteId == "" {
		return nil, fmt.Errorf("suite id is empty")
	}
	if req.SuiteSecret == "" {
		return nil, fmt.Errorf("suite secret is empty")
	}
	if req.SuiteTicket == "" {
		return nil, fmt.Errorf("suite ticket is empty")
	}
	buf, _ := json.Marshal(&req)
	url := fmt.Sprintf("%s/get_suite_token", consts.WorkServiceApiPrefixUrl)
	hp := http.Http{}
	body, err := hp.DoBytes("POST", url, buf)
	if err != nil {
		return nil, err
	}

	type tokenResult struct {
		response.Response
		workaccess.SuiteAccessToken
	}

	res := tokenResult{}

	if err = json.Unmarshal(body, &res); err != nil {
		return nil, err
	}
	return &res.SuiteAccessToken, res.ErrorIf()
}

// getProviderToken 获取服务商凭证 https://qyapi.weixin.qq.com/cgi-bin/service/get_provider_token
func getProviderToken(corp token.Base) (access.Token, error) {
	var req = struct {
		CorpId string `json:"corpid"`
		Secret string `json:"provider_secret"`
	}{
		CorpId: corp.GetAppId(),
		Secret: corp.GetSecret(),
	}
	if req.CorpId == "" {
		return nil, fmt.Errorf("corpId is empty")
	}
	if req.Secret == "" {
		return nil, fmt.Errorf("secret is empty")
	}
	buf, _ := json.Marshal(&req)
	url := fmt.Sprintf("%s/get_provider_token", consts.WorkServiceApiPrefixUrl)
	hp := http.Http{}
	body, err := hp.DoBytes("POST", url, buf)
	if err != nil {
		return nil, err
	}

	type tokenResult struct {
		response.Response
		workaccess.ProviderAccessToken
	}

	res := tokenResult{}

	if err = json.Unmarshal(body, &res); err != nil {
		return nil, err
	}
	return &res.ProviderAccessToken, res.ErrorIf()
}

// GetPermanentCode 获取企业永久授权码
// POST（HTTPS）https://qyapi.weixin.qq.com/cgi-bin/service/get_permanent_code
func GetPermanentCode(corp token.Token, d data.D) (access.Token, error) {
	acs, err := corp.GetAccessToken()
	if err != nil {
		return nil, err
	}
	url := fmt.Sprintf("%s/get_permanent_code?suite_access_token=%s", consts.WorkServiceApiPrefixUrl, acs)

	var req = struct {
		AuthCode string `json:"auth_code"`
	}{
		AuthCode: d.String("auth_code"),
	}
	if req.AuthCode == "" {
		return nil, fmt.Errorf("auth code is empty")
	}
	buf, _ := json.Marshal(&req)
	hp := http.Http{}
	body, err := hp.DoBytes("POST", url, buf)
	if err != nil {
		return nil, err
	}

	type tokenResult struct {
		response.Response
		structs.Permanent
	}

	res := tokenResult{}

	if err = json.Unmarshal(body, &res); err != nil {
		return nil, err
	}
	return &res.Permanent, res.ErrorIf()
}

// GetCorpToken 获取企业凭证 POST（HTTPS）
// 第三方服务商在取得企业的永久授权码后，通过此接口可以获取到企业的access_token。
// 获取后可通过通讯录、应用、消息等企业接口来运营这些应用。
// 此处获得的企业access_token与企业获取access_token拿到的token，本质上是一样的，只不过获取方式不同。
// 获取之后，就跟普通企业一样使用token调用API接口
// https://qyapi.weixin.qq.com/cgi-bin/service/get_corp_token?suite_access_token=SUITE_ACCESS_TOKEN
func GetCorpToken(corp token.Token, d data.D) (access.Token, error) {
	parent := corp.Parent()
	acs, err := parent.GetAccessToken()
	if err != nil {
		return nil, err
	}
	url := fmt.Sprintf("%s/get_corp_token?suite_access_token=%s", consts.WorkServiceApiPrefixUrl, acs)

	var in = authCorp{
		AuthCorpId:    corp.GetAppId(),
		PermanentCode: corp.GetSecret(),
	}
	if in.AuthCorpId == "" {
		return nil, fmt.Errorf("auth corp id is empty")
	}
	if in.PermanentCode == "" {
		return nil, fmt.Errorf("permanent code is empty")
	}
	buf, _ := json.Marshal(&in)

	hp := http.Http{}
	body, err := hp.DoBytes("POST", url, buf)
	if err != nil {
		return nil, err
	}

	var res = struct {
		response.Response
		workaccess.Token
	}{}
	if err = json.Unmarshal(body, &res); err != nil {
		return nil, err
	}
	return &res.Token, res.ErrorIf()
}
