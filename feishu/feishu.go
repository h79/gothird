package feishu

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/token/access"
	"gitee.com/h79/goutils/common/data"
	"gitee.com/h79/goutils/common/http"
)

func GetAccessToken(tk token.Token, d data.D) (access.Token, error) {

	url := fmt.Sprintf("%s/auth/v3/tenant_access_token/internal/", ApiPrefixUrl)

	type appSecret struct {
		AppId  string `json:"app_id"`
		Secret string `json:"app_secret"`
	}
	r := appSecret{
		AppId:  tk.GetAppId(),
		Secret: tk.GetSecret(),
	}
	buf, err := json.Marshal(&r)
	if err != nil {
		return nil, err
	}

	hp := http.Http{}
	body, err := hp.DoBytes("POST", url, buf)
	if err != nil {
		return nil, err
	}

	type tokenResult struct {
		Response
		AccessToken
	}
	res := tokenResult{}
	if err = json.Unmarshal(body, &res); err != nil {
		return nil, err
	}
	return &res.AccessToken, res.ErrorIf()
}

// GetDepartmentList
/**
* 获取授权的部门id列表
* @return array
 */
func GetDepartmentList(api *token.Api) (*DepartmentList, error) {
	uri := fmt.Sprintf("%s/contact/v1/scope/get", ApiPrefixUrl)

	type List struct {
		Response
		D DepartmentList `json:"data,omitempty"`
	}
	res := List{}
	er := api.Request("GET", uri, nil, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})
	return &res.D, er
}

// GetDepartmentInfo
/**
* 获取部门信息
* @return
 */
func GetDepartmentInfo(api *token.Api, odId string) (*DepartmentInfo, error) {

	uri := fmt.Sprintf("%s/contact/v1/department/info/get?open_department_id=%s", ApiPrefixUrl, odId)

	type Data struct {
		Info DepartmentInfo `json:"department_info"`
	}
	type List struct {
		Response
		D Data `json:"data"`
	}
	res := List{}
	er := api.Request("GET", uri, nil, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})
	return &res.D.Info, er
}

// GetDepartmentUser
/**
* 获取部门成员
 */
func GetDepartmentUser(api *token.Api) {
	//uri := fmt.Sprintf("%s/contact/v1/department/user/list?open_department_id=%s&page_size=%s&fetch_child=%s",ApiPrefixUrl)

}

// GetSubDepartmentList
/**
* 获取子部门列表
 */
func GetSubDepartmentList(api *token.Api) {
	//	uri := fmt.Sprintf("%s/contact/v1/department/simple/list?department_id=%s&page_size=%s&fetch_child=%s",ApiPrefixUrl)

}

// GetDepartmentUserDetail
/**
* 获取部门成员详细信息
 */
func GetDepartmentUserDetail(api *token.Api) {
	//uri := fmt.Sprintf("%s/contact/v1/department/user/detail/list?open_department_id=%s&page_size=%s&fetch_child=%s",ApiPrefixUrl)
}

// GetUserDetailByBatch /*批量获取用户信息*/
func GetUserDetailByBatch(api *token.Api, openIds ...string) ([]UserDetail, error) {

	uri := fmt.Sprintf("%s/contact/v1/user/batch_get?", ApiPrefixUrl)
	for i, m := range openIds {
		if i == 0 {
			uri += "open_ids=" + m
		} else {
			uri += "&open_ids=" + m
		}
	}
	type List struct {
		Details []UserDetail `json:"user_infos"`
	}

	type Data struct {
		Response
		D List `json:"data"`
	}
	res := Data{}
	er := api.Request("GET", uri, nil, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})
	if er != nil {
		return nil, er
	}
	return res.D.Details, nil
}

// GetUserWithMobile
/* 根据手机查单一用户 */
func GetUserWithMobile(api *token.Api, mobile string) (User, error) {
	users, err := GetUserWithMobileByBatch(api, mobile)
	if err != nil {
		return User{}, err
	}
	if user, ok := users[mobile]; ok {
		return user, nil
	}
	return User{}, err
}

// GetUserWithMobileByBatch
/**
 * 根据用户邮箱或手机号查询用户 open_id 和 user_id，支持批量查询。
 * 调用该接口需要申请 通过手机号或者邮箱获取用户ID 权限
 */
func GetUserWithMobileByBatch(api *token.Api, mobiles ...string) (map[string]User, error) {

	uri := fmt.Sprintf("%s/user/v1/batch_get_id?", ApiPrefixUrl)
	for i, m := range mobiles {
		if i == 0 {
			uri += "mobiles=" + m
		} else {
			uri += "&mobiles=" + m
		}
	}
	type Data struct {
		Response
		D map[string]interface{} `json:"data"`
	}
	res := Data{}
	er := api.Request("GET", uri, nil, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})
	if er != nil {
		return nil, er
	}
	users := make(map[string]User)
	if mobileUsers, ok := res.D["mobile_users"]; ok {
		for mobile, v := range mobileUsers.(map[string]interface{}) {
			item := v.([]interface{})
			u := item[0].(map[string]interface{})
			user := User{
				OpenId: u["open_id"].(string),
				UserId: u["user_id"].(string),
			}
			users[mobile] = user
		}
	}
	if mobilesNotExist, ok := res.D["mobiles_not_exist"]; ok {
		for _, mobile := range mobilesNotExist.([]interface{}) {
			fmt.Println("not exist mobile: " + mobile.(string))
		}
	}
	return users, nil
}

// GetUserWithEmail
/* 根据邮箱查单一用户*/
func GetUserWithEmail(api *token.Api, email string) (User, error) {
	users, err := GetUserWithEmailByBatch(api, email)
	if err != nil {
		return User{}, err
	}
	if user, ok := users[email]; ok {
		return user, nil
	}
	return User{}, err
}

func GetUserWithEmailByBatch(api *token.Api, emails ...string) (map[string]User, error) {

	uri := fmt.Sprintf("%s/user/v1/batch_get_id?", ApiPrefixUrl)
	for i, m := range emails {
		if i == 0 {
			uri += "emails=" + m
		} else {
			uri += "&emails=" + m
		}
	}
	type Data struct {
		Response
		D map[string]interface{} `json:"data"`
	}
	res := Data{}
	er := api.Request("GET", uri, nil, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})
	if er != nil {
		return nil, er
	}

	users := make(map[string]User)
	if emailUsers, ok := res.D["email_users"]; ok {
		for email, v := range emailUsers.(map[string]interface{}) {
			item := v.([]interface{})
			u := item[0].(map[string]interface{})
			user := User{
				OpenId: u["open_id"].(string),
				UserId: u["user_id"].(string),
			}
			users[email] = user
		}
	}
	if emailsNotExist, ok := res.D["emails_not_exist"]; ok {
		for _, email := range emailsNotExist.([]string) {
			fmt.Println("not exist email: " + email)
		}
	}
	return users, nil
}

// GetUserGroupWithName
/**
 * 根据群名，获取用户所在的指定群
 */
func GetUserGroupWithName(api *token.Api, groupName string) (Group, error) {
	groups, _, err := GetUserGroups(api, 100)
	if err != nil {
		return Group{}, err
	}
	for _, gr := range groups {
		if gr.Name == groupName {
			return gr, nil
		}
	}
	return Group{}, fmt.Errorf("not exist")
}

// GetUserGroups
/**获取用户所在的群列表*/
func GetUserGroups(api *token.Api, page_size int) ([]Group, *Page, error) {

	uri := fmt.Sprintf("%s/user/v4/group_list?page_size=%v", ApiPrefixUrl, page_size)

	type List struct {
		Page
		Group []Group `json:"groups"`
	}

	type Data struct {
		Response
		List `json:"data"`
	}
	res := Data{}
	er := api.Request("GET", uri, nil, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})
	if er != nil {
		return nil, nil, er
	}

	return res.List.Group, &res.Page, nil
}

// GetRootGroupWithName
/**
 * 根据群名，获取机器人所在的指定群
 */
func GetRootGroupWithName(api *token.Api, groupName string) (Group, error) {
	groups, _, err := GetRootGroups(api, 100)
	if err != nil {
		return Group{}, err
	}
	for _, gr := range groups {
		if gr.Name == groupName {
			return gr, nil
		}
	}
	return Group{}, fmt.Errorf("not exist")
}

// GetRootGroups
/** 获取机器人所在的群列表 */
func GetRootGroups(api *token.Api, page_size int) ([]Group, *Page, error) {

	uri := fmt.Sprintf("%s/chat/v4/list?page_size=%v", ApiPrefixUrl, page_size)

	type List struct {
		Page
		Group []Group `json:"groups"`
	}

	type Data struct {
		Response
		List `json:"data"`
	}
	res := Data{}
	er := api.Request("GET", uri, nil, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})
	if er != nil {
		return nil, nil, er
	}
	return res.List.Group, &res.Page, nil
}

// SendMessage
/**
 * 发送消息
 */
func SendMessage(api *token.Api, msg *MsgText) (string, error) {

	uri := fmt.Sprintf("%s/message/v4/send/", ApiPrefixUrl)
	buf, err := json.Marshal(msg)
	if err != nil {
		return "", err
	}

	type Data struct {
		MsgId string `json:"message_id"`
	}

	type msgResponse struct {
		Response
		D Data `json:"data"`
	}
	res := msgResponse{}
	er := api.Request("POST", uri, buf, func(hp *http.Http, body []byte) error {
		if err = json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})
	return res.D.MsgId, er
}

// RobotToChat
/**
* 拉机器人进入群聊
 */
func RobotToChat(api *token.Api, chatId string) error {
	uri := fmt.Sprintf("%s/bot/v4/add", ApiPrefixUrl)

	type Body struct {
		ChatId string `json:"chat_id"`
	}
	body := Body{
		ChatId: chatId,
	}
	buf, err := json.Marshal(body)
	if err != nil {
		return err
	}
	res := Response{}
	return api.Request("POST", uri, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
}

// PutImage
/**
图片上传
*/
func PutImage(api *token.Api, image Image) (string, error) {
	uri := fmt.Sprintf("%s/image/v4/put/", ApiPrefixUrl)

	type img struct {
		Key string `json:"image_key"`
	}
	var res = struct {
		Response
		Data img `json:"data"`
	}{}
	er := api.Upload(uri, "image", &image, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})
	return res.Data.Key, er
}
