package token

const (
	NAccessToken  = "access:token"         /*自己本身的access token*/
	NRefreshToken = "access:token:refresh" /*刷新自己本身access token */
	NTicket       = "get:ticket"
	NOauth2       = "oauth2" /*网页，个人授权*/
	NOauthAccount = "oauth2:account"
	NOauth2App3   = "oauth2:app3"        /*第三方应用授权，比把公众号授权给某个应用*/
	NPreAuthCode  = "app3:pre_auth_code" /* for 公众号授权给第三方，需要先获取一个 预授权码，然后调用前端授权页面进行授权,授权成功后会给一个授权码 "authorization_code"
	然后用这个 authorization_code去调用 NOauth2App3，获取到 access token 相关信息 */

	NAccessTokenProxy = "access:token:proxy" /* 进行代理，中转,从别的服务器获取token相关信息 */

	NGetOwner = "get:owner"
)

const (
	PermanentInfo = "permanentInfo"
	PermanentCode = "permanent_code"
	AgentId       = "agentId"
	SuiteType     = "suiteType"
	Owner         = "owner"
	Parent        = "parent"
	Secret        = "secret"
	True          = "true"
	Encoder       = "encoder"
	Decoder       = "decoder"
	TicketType    = "ticket_type"
)

const (
	BaiduType          = "baidu"
	FeiShuType         = "feishu"
	FxgType            = "fxg"
	GdtType            = "gdt"
	GdtAdvType         = "gdt.adv"
	OceanType          = "ocean"
	OceanAdvType       = "ocean.adv"
	WxType             = "wx"
	WxWebType          = "wx.web"
	WxApp3Type         = "wx.app3"
	WxApp3IdType       = "wx.app3.id"
	WxApp3WebType      = "wx.app3.web"
	WxWorkType         = "wx.work"                  //企业微信自建应用
	WxWorkMsgAuditType = "wx.work.msg.audit"        //会话内容存档,可以ignore,与自建应用一样
	WxWorkProviderType = "wx.work.service.provider" //服务商
	WxWorkSuiteType    = "wx.work.service.suite"    //第三方应用及模板
	WxWorkCorpType     = "wx.work.service.corp"     //第三方应用授权企业
	WxWorkAgentType    = "wx.work.service.agent"    //代应用
)
