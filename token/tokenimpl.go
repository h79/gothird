package token

import (
	"fmt"
	"gitee.com/h79/gothird/token/access"
	"gitee.com/h79/gothird/token/validate"
	"gitee.com/h79/goutils/common/coder"
	"gitee.com/h79/goutils/common/data"
	"gitee.com/h79/goutils/common/logger"
)

var _ Token = (*tokenImpl)(nil)
var _ Base = (*tokenImpl)(nil)

type tokenImpl struct {
	App     *App
	parent  Token
	service Service
}

func (t *tokenImpl) IsCache() bool {
	if t == nil {
		return false
	}
	return t.App.IsCache
}

func (t *tokenImpl) GetParentId() string {
	if t == nil {
		return ""
	}
	return t.App.ParentId
}

func (t *tokenImpl) GetId() string {
	if t == nil {
		return ""
	}
	return t.App.Id
}

func (t *tokenImpl) GetAppType() string {
	if t == nil {
		return ""
	}
	return t.App.Type
}

func (t *tokenImpl) GetAppId() string {
	if t == nil {
		return ""
	}
	return t.App.AppId
}

func (t *tokenImpl) GetName() string {
	if t == nil {
		return ""
	}
	return t.App.Name
}

func (t *tokenImpl) GetSecret() string {
	if t == nil {
		return ""
	}
	return t.App.Secret
}

func (t *tokenImpl) SetSecret(secret string) {
	if t == nil {
		return
	}
	t.App.Secret = secret
}

func (t *tokenImpl) Parent() Token {
	if t.parent != nil {
		return t.parent
	}
	if len(t.App.ParentId) == 0 {
		return t
	}
	if tk, err := Mgr().GetToken(t.App.ParentId); err == nil {
		t.parent = tk
		return tk
	}
	return t
}

func (t *tokenImpl) CreateChild(app *App, service Service) Token {
	if t == nil {
		return nil
	}
	tk := &tokenImpl{
		parent:  t,
		App:     app,
		service: service,
	}
	return tk
}

func (t *tokenImpl) GetRequest() Request {
	if t == nil {
		return nil
	}
	return t.service
}

func (t *tokenImpl) Execute(cmd string, data data.D) (interface{}, error) {
	if t == nil {
		return nil, ErrObjectNil
	}
	if cmd == NGetOwner {
		var p Token = nil
		if data.String(Parent) == True {
			if pp := t.Parent(); pp != t {
				//确实有父结点
				p = pp
			}
		}
		var secret = false
		if data.String(Secret) == True {
			secret = true
		}
		var encoder coder.Encoder = nil
		var en = data.Value(Encoder)
		if cdr, ok := en.(coder.Encoder); ok {
			encoder = cdr
		}
		var res = struct {
			Apps map[string]*App `json:"apps"`
		}{
			Apps: make(map[string]*App),
		}
		if app := getApp(t, secret, encoder); app != nil {
			res.Apps[Owner] = app
		}
		if app := getApp(p, secret, encoder); app != nil {
			res.Apps[Parent] = app
		}
		return &res, nil
	} else if cmd == NTicket {
		return t.GetTicket(data)
	}
	return t.service.Execute(t, cmd, data)
}

func (t *tokenImpl) GetTicket(data data.D) (Ticket, error) {
	if t == nil {
		return Ticket{}, ErrObjectNil
	}
	var key = ""
	var agentId = data.String(AgentId)
	if agentId != "" {
		key = fmt.Sprintf("ticket:agent_config:%s", t.GetAppId())
	} else {
		key = fmt.Sprintf("ticket:%s", t.GetAppId())
	}
	ticket, err := t.GetCodeV2(key)
	if err == nil && ticket.Data != "" {
		return Ticket{Ticket: ticket.Data, ExpireIn: ticket.RemainExpired()}, nil
	}
	r, err := t.service.Execute(t, NTicket, data)
	if err != nil {
		return Ticket{}, err
	}
	ti := r.(*Ticket)
	v := ti.GetAccessToken()
	err = t.SetCode(key, v.Data, v.ExpireIn)
	return Ticket{Ticket: v.Data, ExpireIn: ti.ExpireIn}, err
}

func (t *tokenImpl) GetCode(name string) (string, error) {
	if t == nil {
		return "", ErrObjectNil
	}
	v, err := Mgr().Read(t.App.Id, name)
	if err != nil {
		return "", err
	}
	return v.Data, nil
}

func (t *tokenImpl) GetCodeV2(name string) (validate.Data, error) {
	if t == nil {
		return validate.Data{}, ErrObjectNil
	}
	return Mgr().Read(t.App.Id, name)
}

func (t *tokenImpl) ClearCode(name string) error {
	if t == nil {
		return ErrObjectNil
	}
	return Mgr().Clear(t.App.Id, name)
}

func (t *tokenImpl) SetCode(name, code string, expire int64) error {
	if t == nil {
		return ErrObjectNil
	}
	ex := validate.New(name, code, expire)
	return Mgr().Write(t.App.Id, ex.Key, ex)
}

func (t *tokenImpl) SetAccessTokenWithAcs(acs access.Token) string {
	if !t.App.IsCache {
		return acs.GetAccessToken().Data
	}
	return Mgr().SetAccessToken(t.App.Id, acs)
}

func (t *tokenImpl) SetAccessToken(acs string, expire int64) error {
	if t == nil {
		return ErrObjectNil
	}
	if !t.App.IsCache {
		return nil
	}
	ex := validate.New(NAccessToken, acs, expire)
	return Mgr().Write(t.App.Id, ex.Key, ex)
}

func (t *tokenImpl) ClearAccessToken() {
	if t == nil {
		return
	}
	err := t.ClearCode(NAccessToken)
	if err != nil {
		logger.E("Token", "clean access token failure, app= %s,err= %s", t.App.String(), err)
	}
	return
}

func (t *tokenImpl) GetAccessToken(options ...data.OptionsFunc) (string, error) {
	if t == nil {
		return "", ErrObjectNil
	}
	if v, err := Mgr().Read(t.App.Id, NAccessToken); err == nil && v.Data != "" {
		return v.Data, nil
	}

	opts := data.Options{Data: data.D{"appid": t.App.AppId}}
	for i := range options {
		options[i](&opts)
	}
	d, err := t.service.Execute(t, NAccessToken, opts.Data)

	logger.D("Token", "GetAccessToken result, app= %s, err= %v", t.App.String(), err)

	if ErrEq(t.service.CheckError(err), ErrTokenExpired) {
		ref, er := t.GetRefreshAccessToken()
		if er == nil && ref != "" {
			return t.RefreshAccessToken(ref)
		}
	}
	if ErrEq(t.service.CheckError(err), ErrTokenInvalid) {
		t.ClearAccessToken()
	}
	if err != nil {
		return "", err
	}
	return t.SetAccessTokenWithAcs(d.(access.Token)), nil
}

func (t *tokenImpl) SetRefreshAccessToken(tk string, expire int64) error {
	if t == nil {
		return ErrObjectNil
	}
	if !t.App.IsCache {
		return nil
	}
	ex := validate.New(NRefreshToken, tk, expire)
	return Mgr().Write(t.App.Id, ex.Key, ex)
}

func (t *tokenImpl) GetRefreshAccessToken() (string, error) {
	if t == nil {
		return "", ErrObjectNil
	}
	v, err := Mgr().Read(t.App.Id, NRefreshToken)
	if err != nil {
		return "", err
	}
	return v.Data, nil
}

func (t *tokenImpl) RefreshAccessToken(refToken string) (string, error) {
	da := data.D{}
	da["appid"] = t.App.AppId
	da["refToken"] = refToken
	d, err := t.service.Execute(t, NRefreshToken, da)
	if err == nil {
		return t.SetAccessTokenWithAcs(d.(access.Token)), nil
	}
	logger.E("Token", "RefreshAccessToken failure, app= %s, err= %s", t.App.String(), err)
	if ErrEq(t.service.CheckError(err), ErrTokenExpired) {
		_ = Mgr().Clear(t.App.Id, NRefreshToken)
	}
	return "", err
}

func (t *tokenImpl) String() string {
	return t.App.String()
}

func getApp(t Token, secret bool, encoder coder.Encoder) *App {
	if t == nil {
		return nil
	}
	res := &App{ParentId: t.GetParentId(), Id: t.GetId(), AppId: t.GetAppId(), Type: t.GetAppType(), Name: t.GetName()}
	if secret {
		res.Secret = t.GetSecret()
		if encoder != nil {
			res.Secret = encoder.Encode(res.Secret)
		}
	}
	return res
}
