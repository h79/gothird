package feishu

import (
	"gitee.com/h79/gothird/token"
)

type Response struct {
	Code int32  `json:"code"`
	Msg  string `json:"msg"`
}

func (r Response) ErrorIf() error {
	if r.Code == 0 {
		return nil
	}
	return token.Result{Code: r.Code, Msg: r.Msg}
}

func (r Response) ReturnIf(api *token.Api) error {
	if r.Code == 0 {
		return nil
	}
	return token.Result{Code: r.Code, Msg: r.Msg}
}
