/**
 * 事件数据结构
 *
 * 微信服务器在五秒内收不到响应会断掉连接，并且重新发起请求，总共重试三次。
 *
 * 1 关注/取消关注事件
 * 2 扫描带参数二维码事件
 * 3 上报地理位置事件
 * 4 自定义菜单事件
 * 5 点击菜单拉取消息时的事件推送
 * 6 点击菜单跳转链接时的事件推送
 */
package offiaccount

type Head struct {
	ToAppId    string
	ToUser     string `xml:"ToUserName" json:"ToUserName"`     //开发者微信号
	FromUser   string `xml:"FromUserName" json:"FromUserName"` //OpenId
	CreateTime int64  `xml:"CreateTime" json:"CreateTime"`
	MsgType    string `xml:"MsgType" json:"MsgType"`
}

type EvData struct {
	Event string `xml:"Event,omitempty" json:"Event,omitempty"`

	//for subscribe  unsubscribe, SCAN, LOCATION, CLICK, VIEW
	EventKey string `xml:"EventKey,omitempty"  json:"EventKey,omitempty"`

	//for 扫二维码
	Ticket string `xml:"Ticket,omitempty"  json:"Ticket,omitempty"`

	//上报地理位置事件
	Latitude  float64 `xml:"Latitude" json:"Latitude,omitempty"`   //地理位置纬度
	Longitude float64 `xml:"Longitude" json:"Longitude,omitempty"` //地理位置经度
	Precision float64 `xml:"Precision" json:"Precision,omitempty"` //地理位置精度

	//点击菜单跳转链接时的事件推送 .EventMsg = "VIEW"
	MenuID string `xml:"MenuID" json:"MenuID,omitempty"` //指菜单ID，如果是个性化菜单，则可以通过这个字段，知道是哪个规则的菜单被点击了。

	Status string `xml:"Status" json:"Status,omitempty"`

	//for 客户 user_enter_tempsession
	SessionFrom string `xml:"SessionFrom" json:"SessionFrom,omitempty"`
}

const (
	EMSubscribe   = "subscribe"
	EMUnsubscribe = "unsubscribe"
	EMScan        = "SCAN"
	EMLocation    = "LOCATION"
	EMClick       = "CLICK"
	EMView        = "VIEW"
)

//关注事件
// .EventMsg = "subscribe"

//取消关注事件
// .EventMsg = "unsubscribe"

// 扫二维码 .EventMsg = "SCAN"
// .EventKey 事件KEY值，是一个32位无符号整数，即创建二维码时的二维码scene_id
// .Ticket  二维码的ticket，可用来换取二维码图片

//上报地理位置事件 .EventMsg = "LOCATION"

//用户点击自定义菜单后，微信会把点击事件推送给开发者，请注意，点击菜单弹出子菜单，不会产生上报。
//点击菜单拉取消息时的事件推送  .EventMsg ="CLICK"
// .EventKey 事件KEY值，与自定义菜单接口中KEY值对应

//点击菜单跳转链接时的事件推送  .EventMsg = "VIEW"
//.EventKey 事件KEY值，设置的跳转URL
//.MenuID	指菜单ID，如果是个性化菜单，则可以通过这个字段，知道是哪个规则的菜单被点击了。

type MsgData struct {
	MsgId string `xml:"MsgId,omitempty" json:"MsgId,omitempty"`

	// text
	Content string `xml:"Content,omitempty" json:"Content,omitempty"`

	// media
	MediaId string `xml:"MediaId,omitempty" json:"MediaId,omitempty"`

	// image
	PicUrl string `xml:"PicUrl,omitempty" json:"PicUrl,omitempty"`

	// voice
	Format      string `xml:"Format,omitempty" json:"Format,omitempty"`
	Recognition string `xml:"Recognition,omitempty" json:"Recognition,omitempty"`

	// video
	ThumbMediaId string `xml:"ThumbMediaId,omitempty" json:"ThumbMediaId,omitempty"`

	//地理位置
	LocationX float64 `xml:"Location_X,omitempty" json:"Location_X,omitempty"` //地理位置纬度
	LocationY float64 `xml:"Location_Y,omitempty" json:"Location_Y,omitempty"` //地理位置经度
	Scale     float64 `xml:"Scale,omitempty" json:"Scale,omitempty"`           //地理位置精度
	Label     string  `xml:"Label,omitempty" json:"Label,omitempty"`           //地址

	//link
	Title string `xml:"Title" json:"Title,omitempty"`
	Desc  string `xml:"Description" json:"Description,omitempty"`
	Url   string `xml:"Url,omitempty" json:"Url,omitempty"`

	//小卡片
	AppId    string `xml:"AppId" json:"AppId,omitempty"`
	PagePath string `xml:"PagePath" json:"PagePath,omitempty"`
	ThumbUrl string `xml:"ThumbUrl,omitempty" json:"ThumbUrl,omitempty"`
}

type Location struct {
	LocationX float64 //地理位置纬度
	LocationY float64 //地理位置经度
	Scale     float64 //地理位置精度
	Label     string  //地址
}

type Voice struct {
	MediaId string
	Format  string
}

type Video struct {
	MediaId      string
	ThumbMediaId string
}

type Link struct {
	Title string
	Desc  string
	Url   string
}

// .MsgType = "event"
type EventMsg struct {
	Head
	EvData
}

// .MsgType = text
// .MsgType = image
// .MsgType = voice
// .MsgType = location
// .MsgType = shortvideo
type NormalMsg struct {
	Head
	MsgData
}

// All pack
type AllMsg struct {
	Head
	EvData
	MsgData
}

func (m AllMsg) IsEvent() bool {
	return m.MsgType == "event"
}

func (m AllMsg) ToEvent() *EventMsg {
	return &EventMsg{
		Head:   m.Head,
		EvData: m.EvData,
	}
}

func (m AllMsg) ToNormal() *NormalMsg {
	return &NormalMsg{
		Head:    m.Head,
		MsgData: m.MsgData,
	}
}
