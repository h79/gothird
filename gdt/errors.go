package gdt

//https://developers.e.qq.com/docs/reference/errorcode?version=1.3&_preview=1
const (
	Success            = 0
	AccessTokenExpire  = 11000 //	access token过期
	EmptyAccessToken   = 11005 //access token为空
	InvalidAccessToken = 11004 //access token错误
	RefreshTokenExpire = 11012 //	refresh token过期
	AuthCodeUsed       = 11013 //Authorization code can only be used once.
)

func IsTokenExpired(code int32) bool {
	return code == RefreshTokenExpire || code == AuthCodeUsed
}
