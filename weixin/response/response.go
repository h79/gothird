package response

import (
	"gitee.com/h79/gothird/token"
	wxerr "gitee.com/h79/gothird/weixin/errors"
	"strconv"
)

type Response struct {
	ErrCode int32  `json:"errcode"`
	ErrMsg  string `json:"errmsg"`
}

func (r Response) Error() string {
	return r.ErrMsg
}

func (r Response) ErrorIf() error {
	if 0 == r.ErrCode {
		return nil
	}
	if wxerr.IsInvalid(r.ErrCode) {
		return token.ErrTokenInvalid.Clone(strconv.FormatInt(int64(r.ErrCode), 10), r.Error())
	} else if wxerr.IsExpired(r.ErrCode) {
		return token.ErrTokenExpired.Clone(strconv.FormatInt(int64(r.ErrCode), 10), r.Error())
	}
	return token.Result{Code: r.ErrCode, Msg: r.ErrMsg}
}

func (r Response) ReturnIf(api *token.Api) error {
	if 0 == r.ErrCode {
		return nil
	}
	if wxerr.IsInvalid(r.ErrCode) {
		api.ResetToken()
		return token.ErrTokenInvalid.Clone(strconv.FormatInt(int64(r.ErrCode), 10), r.ErrMsg)
	} else if wxerr.IsExpired(r.ErrCode) {
		api.ResetToken()
		return token.ErrTokenExpired.Clone(strconv.FormatInt(int64(r.ErrCode), 10), r.ErrMsg)
	}
	return token.Result{Code: r.ErrCode, Msg: r.ErrMsg}
}

//https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/login/auth.code2Session.html
//https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140842
