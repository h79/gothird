package offiaccount

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/goutils/common/http"
)

// ClearQuota 所有api调用（包括第三方帮其调用）次数进行清零,每月10次
// https://api.weixin.qq.com/cgi-bin/clear_quota?access_token=ACCESS_TOKEN
func ClearQuota(api *token.Api) error {
	uri := fmt.Sprintf("%s/cgi-bin/clear_quota?", consts.ApiPrefixUrl)

	type quotaReq struct {
		AppId string `json:"appid"`
	}
	Req := quotaReq{
		AppId: api.AppId(),
	}

	buf, err := json.Marshal(&Req)
	if err != nil {
		return err
	}
	res := response.Response{}
	err = api.Request("POST", uri, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	return err
}

// GetCallbackIP 获取微信服务器IP地址
// https://api.weixin.qq.com/cgi-bin/getcallbackip?access_token=ACCESS_TOKEN
func GetCallbackIP(api *token.Api) ([]string, error) {
	uri := fmt.Sprintf("%s/cgi-bin/getcallbackip?", consts.ApiPrefixUrl)
	type ipResult struct {
		response.Response
		Ip []string `json:"ip_list"`
	}
	res := ipResult{}
	er := api.Request("GET", uri, nil, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})
	return res.Ip, er
}

// GenerateShortUrl 长链接转短链接接口
// https://api.weixin.qq.com/cgi-bin/shorturl?access_token=ACCESS_TOKEN
func GenerateShortUrl(api *token.Api, longUrl string) (string, error) {
	uri := fmt.Sprintf("%s/cgi-bin/shorturl?", consts.ApiPrefixUrl)

	Req := ShortUrlReq{
		Action: "long2short",
		Url:    longUrl,
	}
	buf, err := json.Marshal(&Req)
	if err != nil {
		return "", err
	}
	type shortUrlResult struct {
		response.Response
		Url string `json:"short_url"`
	}
	res := shortUrlResult{}
	er := api.Request("POST", uri, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	return res.Url, er
}
