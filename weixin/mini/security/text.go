package security

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/goutils/common/http"
)

//POST https://api.weixin.qq.com/wxa/msg_sec_check?access_token=ACCESS_TOKEN

func Text(api *token.Api, txt string) error {
	uri := fmt.Sprintf("%s/wxa/msg_sec_check?", consts.ApiPrefixUrl)

	type content struct {
		Content string `json:"content"`
	}
	c := content{Content: txt}
	bu, err := json.Marshal(&c)
	if err != nil {
		return err
	}
	res := response.Response{}
	return api.Request("POST", uri, bu, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
}
