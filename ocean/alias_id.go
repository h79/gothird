package ocean

import (
	"encoding/binary"
	"gitee.com/h79/goutils/common/algorithm"
	"strconv"
)

/** 把一组id 生成一个别名id */
func genAlias(ids []int64) (string, []string) {
	var buf = make([]byte, 8*len(ids))
	var strId = make([]string, 0)
	for i := range ids {
		id := ids[i]
		strId = append(strId, strconv.FormatInt(id, 10))
		binary.BigEndian.PutUint64(buf, uint64(id))
	}
	return "as" + algorithm.BytesToMd5(buf), strId
}
