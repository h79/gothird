package mini

type SessionKey struct {
	SessionKey string `json:"session_key,omitempty"`
	OpenId     string `json:"openid,omitempty"`  //每个应用有对应的openid
	UnionId    string `json:"unionid,omitempty"` //开发者最好保存用户unionID信息，以便以后在不同应用中进行用户信息互通。
	AppId      string `json:"-"`
}

type WxaQrcReq struct {
	Scene string `json:"scene"`
	Page  string `json:"page,omitempty"`
}

type User struct {
	OpenId    string   `json:"openId"`
	UnionId   string   `json:"unionId,omitempty"` //开发者最好保存用户unionID信息，以便以后在不同应用中进行用户信息互通。
	Nick      string   `json:"nickName,omitempty"`
	Gender    int8     `json:"gender,omitempty"`
	AvatarUrl string   `json:"avatarUrl,omitempty"`
	Province  string   `json:"province,omitempty"`
	City      string   `json:"city,omitempty"`
	Country   string   `json:"country,omitempty"`
	Language  string   `json:"language,omitempty"`
	Privilege []string `json:"privilege,omitempty"`
	AppId     string   `json:"-"`
}

// implement common.data.Base interface
func (u *User) DataType() string {
	return "mini.user"
}

type Phone struct {
	User

	//用户绑定的手机号（国外手机号会有区号）
	PhoneNumber string `json:"phoneNumber,omitempty"`

	//没有区号的手机号
	PurePhoneNumber string `json:"purePhoneNumber,omitempty"`

	//区号
	CountryCode string `json:"countryCode,omitempty"`
}

// implement common.data.Base interface
func (p *Phone) DataType() string {
	return "mini.phone"
}
