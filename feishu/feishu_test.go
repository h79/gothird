package feishu

import (
	"gitee.com/h79/gothird/token"
	"testing"
)

func TestFeiShu(t *testing.T) {
	tk := NewToken(token.App{Type: "feishu",
		AppId:  "cli_9fb826af85fed00b",             //机器人
		Secret: "1DCmcZqKmKEm62ynwpIZJ3vma2Du81Ai", //机器人
	})

	api := token.NewApi(tk)

	li, err := GetDepartmentList(api)
	if err != nil {
		t.Error("failure: ", err)
		return
	}
	//[95g5537g94gbe2d9 fc7bfb95g9d68793 od-3ce0bb175b6586c37fec2b427d37cc0d od-2df619017fffb2c6bbb05df478063b29 od-40df2eecb96487e6ac55e22c12ac5b59 17ggb5b42fdd1f1d od-8d50da59fc84c282c8e6b22794d84773 od-2ee791bdeb8270558922d508c3dd126f od-213944e2c399b0c5cb955964729f1d22 od-64169dc8237e0ac8941442b00336b06d]
	//[od-c62557580e67e6c94d97c2ae6c02d719 od-30fa5c5d49ed7149f6b8443737171a54 od-3ce0bb175b6586c37fec2b427d37cc0d od-2df619017fffb2c6bbb05df478063b29 od-40df2eecb96487e6ac55e22c12ac5b59 od-d014c52321653501d59034342d129fa2 od-8d50da59fc84c282c8e6b22794d84773 od-2ee791bdeb8270558922d508c3dd126f od-213944e2c399b0c5cb955964729f1d22 od-64169dc8237e0ac8941442b00336b06d]

	for _, dep := range li.AuthedOpenDepartments {
		info, err := GetDepartmentInfo(api, dep)
		if err != nil {
			t.Error("failure: ", err)
			return
		}
		t.Log("dep info: ", info)

	}
	t.Log("DEP: ", li)
}

func TestMobile(t *testing.T) {
	tk := NewToken(token.App{Type: "feishu",
		AppId:  "cli_9fb826af85fed00b",
		Secret: "1DCmcZqKmKEm62ynwpIZJ3vma2Du81Ai",
	})
	api := token.NewApi(tk)

	li, err := GetUserWithMobileByBatch(api, "13822283547")
	if err != nil {
		t.Error("failure: ", err)
		return
	}
	t.Log("User: ", li)
}

func TestGroup(t *testing.T) {
	tk := NewToken(token.App{Type: "feishu",
		AppId:  "cli_9fb826af85fed00b",
		Secret: "1DCmcZqKmKEm62ynwpIZJ3vma2Du81Ai",
	})
	api := token.NewApi(tk)

	//groups,_,err := GetUserGroups(api,100)
	groups, _, err := GetRootGroups(api, 100)
	if err != nil {
		t.Error("failure: ", err)
		return
	}
	t.Log("groups: ", groups)
}

func TestPutImage(t *testing.T) {
	tk := NewToken(token.App{Type: "feishu",
		AppId:  "cli_9fb826af85fed00b",
		Secret: "1DCmcZqKmKEm62ynwpIZJ3vma2Du81Ai",
	})
	api := token.NewApi(tk)

	key, err := PutImage(api, Image{
		Type:     "message",
		FileName: "./1.jpeg",
	})
	if err != nil {
		t.Error("failure: ", err)
		return
	}
	t.Log("image key: ", key)
	//img_db1aed19-17f5-44ca-8aaf-bf9cfbcc30bg
}

func TestSendMessage(t *testing.T) {
	tk := NewToken(token.App{Type: "feishu",
		AppId:  "cli_9fb826af85fed00b",
		Secret: "1DCmcZqKmKEm62ynwpIZJ3vma2Du81Ai",
	})
	api := token.NewApi(tk)

	group, er := GetRootGroupWithName(api, "护发交流群2.0")
	if er != nil {
		return
	}
	t.Log(group)
	head := MsgHead{
		MsgType: "text",
		ChatId:  group.ChatId,
	}
	msg_id, err := SendMessage(api, &MsgText{
		MsgHead: head,
		Content: ContentText{
			Text: "夏侯惇报警服务测试",
		},
	})
	if err != nil {
		t.Error("failure: ", err)
		return
	}
	t.Log("msg id: ", msg_id)
}
