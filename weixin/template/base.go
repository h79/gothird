package template

type KeyWord struct {
	Value string `json:"value"`
	Color string `json:"color,omitempty"`
}

// Base
// 模板消息
type Base struct {
	AppId  string   `json:"-"`
	ToUser string   `json:"touser"`
	Id     string   `json:"template_id"`
	Url    string   `json:"url,omitempty"`
	Mini   *Program `json:"miniprogram,omitempty"`
}

// Template interface
func (b *Base) IsMini() bool {
	return false
}

// Template interface
func (b *Base) SetAppId(appid string) {
	b.AppId = appid
}

func (b *Base) GetAppId() string {
	return b.AppId
}

// Template interface
func (b *Base) SetFormId(formid string) {
}

// Template interface
func (b *Base) SetToUser(user string) {
	b.ToUser = user
}

// Template interface
func (b *Base) GetToUser() string {
	return b.ToUser
}

// Template interface
func (b *Base) SetId(id string) {
	b.Id = id
}

// Template interface
func (b *Base) SetUrl(url string) {
	b.Url = url
}

// Program
// 小程序卡片
type Program struct {
	Appid string `json:"appid"`
	Page  string `json:"pagepath,omitempty"`
}

type Data struct {
	First  KeyWord `json:"first"`
	Remark KeyWord `json:"remark"`
}
