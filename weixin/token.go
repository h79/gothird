package weixin

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	tokenaccess "gitee.com/h79/gothird/token/access"
	"gitee.com/h79/gothird/weixin/access"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/goutils/common/data"
	"gitee.com/h79/goutils/common/http"
)

// 1 公众号: 全局唯一接口调用凭据
// 2 小程序: 全局唯一后台接口调用凭据（access_token）。调用绝大多数后台接口时都需使用 access_token
// 这种没有刷新refreshToken, 只有到期时，再调用一次，取一个新的access token
func getTokenUrl(appId, secret string) string {
	return fmt.Sprintf("%s/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s", consts.ApiPrefixUrl, appId, secret)
}

func GetAccessToken(tk token.Token, d data.D) (tokenaccess.Token, error) {
	var appId = tk.GetAppId()
	var secret = tk.GetSecret()
	if appId == "" {
		return nil, fmt.Errorf("app id is empty")
	}
	if secret == "" {
		return nil, fmt.Errorf("secret is empty")
	}
	var hp = http.Http{}
	var body, err = hp.DoBytes("GET", getTokenUrl(appId, secret), nil)
	if err != nil {
		return nil, err
	}

	type tokenResult struct {
		response.Response
		access.Token
	}

	res := tokenResult{}

	if err = json.Unmarshal(body, &res); err != nil {
		return nil, err
	}

	return &res.Token, res.ErrorIf()
}

// GetStableAccessToken 获取稳定版接口调用凭据
// 获取公众号全局后台接口调用凭据，有效期最长为7200s，开发者需要进行妥善保存；
// 有两种调用模式: 1. 普通模式，access_token 有效期内重复调用该接口不会更新 access_token，绝大部分场景下使用该模式；2. 强制刷新模式，会导致上次获取的 access_token 失效，并返回新的 access_token；
// 该接口调用频率限制为 1万次 每分钟，每天限制调用 50w 次；
// 与获取Access token获取的调用凭证完全隔离，互不影响。该接口仅支持 POST JSON 形式的调用；
func GetStableAccessToken(tk token.Token, d data.D) (tokenaccess.Token, error) {
	var appId = tk.GetAppId()
	var secret = tk.GetSecret()
	if appId == "" {
		return nil, fmt.Errorf("app id is empty")
	}
	if secret == "" {
		return nil, fmt.Errorf("secret is empty")
	}
	var forceRefresh = d.Value("force_refresh").(bool)
	var req = stableAccessTokenReq{
		GrantType:    "client_credential",
		Appid:        appId,
		Secret:       secret,
		ForceRefresh: forceRefresh,
	}
	buf, er := json.Marshal(&req)
	if er != nil {
		return nil, er
	}
	var hp = http.Http{}
	var body, err = hp.DoBytes("POST", fmt.Sprintf("%s/cgi-bin/stable_token", consts.ApiPrefixUrl), buf)
	if err != nil {
		return nil, err
	}

	var res = struct {
		response.Response
		access.Token
	}{}

	if err = json.Unmarshal(body, &res); err != nil {
		return nil, err
	}

	return &res.Token, res.ErrorIf()
}

type stableAccessTokenReq struct {
	GrantType    string `json:"grant_type"`
	Appid        string `json:"appid"`
	Secret       string `json:"secret"`
	ForceRefresh bool   `json:"force_refresh"`
}
