package access

import (
	"time"
)

// Nanosecond,纳秒
const refExpire = time.Hour * time.Duration(24*6)

// ExpireSecond
/**
 * @param expire 秒
 * @return Nanosecond,纳秒
 */
func ExpireSecond(expire int64) int64 {
	return (time.Second * time.Duration(expire)).Nanoseconds()
}

// RefExpireSecond
/**
 * @param expire 秒
 * @return Nanosecond,纳秒
 */
func RefExpireSecond(expire int64) int64 {
	ex := ExpireSecond(expire)
	if ex == 0 {
		ex = refExpire.Nanoseconds()
	}
	return ex
}

type Value struct {
	Data     string
	ExpireIn int64
}

func (v Value) IsValidOf() bool {
	return v.Data != ""
}

type Token interface {
	GetAccessToken() Value
	GetRefAccessToken() Value
}
