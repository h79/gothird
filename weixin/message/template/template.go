package template

// 订阅消息
type Subscribe struct {
	AppId  string `json:"-"`
	ToUser string `json:"touser"`
	Id     string `json:"template_id"`
	Page   string `json:"page"`
}

// template.Template interface
func (b *Subscribe) IsMini() bool {
	return true
}

// template.Template interface
func (b *Subscribe) SetAppId(appid string) {
	b.AppId = appid
}

func (b *Subscribe) GetAppId() string {
	return b.AppId
}

func (b *Subscribe) SetFormId(formid string) {
}

// template.Template interface
func (b *Subscribe) SetToUser(user string) {
	b.ToUser = user
}

// template.Template interface
func (b *Subscribe) GetToUser() string {
	return b.ToUser
}

// template.Template interface
func (b *Subscribe) SetId(id string) {
	b.Id = id
}

// template.Template interface
func (b *Subscribe) SetUrl(url string) {
	b.Page = url
}

// 模板消息
type Base struct {
	AppId           string `json:"-"`
	ToUser          string `json:"touser"`
	Id              string `json:"template_id"`
	Page            string `json:"page"`                       //点击模板卡片后的跳转页面，仅限本小程序内的页面。支持带参数,（示例index?foo=bar）。该字段不填则模板无跳转。
	FormId          string `json:"form_id"`                    //表单提交场景下，为 submit 事件带上的 formId；支付场景下，为本次支付的 prepay_id
	EmphasisKeyword string `json:"emphasis_keyword,omitempty"` //模板需要放大的关键词，不填则默认无放大
}

// template.Template interface
func (b *Base) IsMini() bool {
	return true
}

// template.Template interface
func (b *Base) SetAppId(appid string) {
	b.AppId = appid
}

func (b *Base) GetAppId() string {
	return b.AppId
}

func (b *Base) SetFormId(formid string) {
	b.FormId = formid
}

// template.Template interface
func (b *Base) SetToUser(user string) {
	b.ToUser = user
}

// template.Template interface
func (b *Base) GetToUser() string {
	return b.ToUser
}

// template.Template interface
func (b *Base) SetId(id string) {
	b.Id = id
}

// template.Template interface
func (b *Base) SetUrl(url string) {
	b.Page = url
}
