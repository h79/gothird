package iv

import (
	"encoding/base64"
	"gitee.com/h79/goutils/common/algorithm"
)

/**
 * 解析小程序敏感数据
 */
func Decode(encryptData, sessionKey, iv string) ([]byte, error) {

	aesKey, err := base64.StdEncoding.DecodeString(sessionKey)
	if err != nil {
		return nil, err
	}
	Iv, er := base64.StdEncoding.DecodeString(iv)
	if er != nil {
		return nil, er
	}
	pk := algorithm.NewPKCS7()
	return pk.DecryptIv(encryptData, aesKey, Iv)
}
