package access

import (
	"gitee.com/h79/gothird/token/access"
)

type Token struct {
	AcsToken    string `json:"access_token"`
	RefToken    string `json:"refresh_token"`
	AcsExpireIn int64  `json:"expires_in"`
	RefExpireIn int64  `json:"refresh_expires_in"`
}

// GetAccessToken access.Token interface implement
func (a Token) GetAccessToken() access.Value {
	return access.Value{
		Data:     a.AcsToken,
		ExpireIn: access.ExpireSecond(a.AcsExpireIn),
	}
}

func (a Token) GetRefAccessToken() access.Value {
	return access.Value{
		Data:     a.RefToken,
		ExpireIn: access.RefExpireSecond(a.RefExpireIn),
	}
}
