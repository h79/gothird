package work

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/gothird/weixin/work/structs"
	"gitee.com/h79/goutils/common/http"
)

// ECFollowUserList 获取配置了客户联系功能的成员列表
// 企业和第三方服务商可通过此接口获取配置了客户联系功能的成员列表。
// 请求方式：GET（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_follow_user_list?access_token=ACCESS_TOKEN
func ECFollowUserList(corp *token.Api) ([]string, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/get_follow_user_list?", consts.WorkApiPrefixUrl)
	var res = struct {
		response.Response
		FollowUsers []string `json:"follow_user"`
	}{}
	err := corp.Request("GET", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return res.FollowUsers, res.ErrorIf()
}

// ECGetList
// 获取外部联系人列表
// 企业可通过此接口获取指定成员添加的客户列表。客户是指配置了客户联系功能的成员所添加的外部联系人。
func ECGetList(corp *token.Api, userid string) ([]string, error) {

	url := fmt.Sprintf("%s/cgi-bin/externalcontact/list?userid=%s&", consts.WorkApiPrefixUrl, userid)

	var res = struct {
		response.Response
		ExternalUserid []string `json:"external_userid"`
	}{}
	err := corp.Request("GET", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return res.ExternalUserid, res.ErrorIf()
}

// ECGetDetail 获取客户详情
// 企业可通过此接口，根据外部联系人的userid（如何获取?），拉取客户详情。
// 请求方式：GET（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get?access_token=ACCESS_TOKEN&external_userid=EXTERNAL_USERID&cursor=CURSOR
func ECGetDetail(corp *token.Api, externalUserid string, cursor string) (*structs.ExternalContactDetail, string, error) {

	url := fmt.Sprintf("%s/cgi-bin/externalcontact/get?external_userid=%s&cursor=%s&", consts.WorkApiPrefixUrl, externalUserid, cursor)
	var res = struct {
		response.Response
		structs.ExternalContactDetail
		NextCursor string `json:"next_cursor"`
	}{}
	err := corp.Request("GET", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, "", err
	}
	return &res.ExternalContactDetail, res.NextCursor, res.ErrorIf()
}

// ECBatchDetail 批量获取客户详情
// 企业/第三方可通过此接口获取指定成员添加的客户信息列表。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/batch/get_by_user?access_token=ACCESS_TOKEN
func ECBatchDetail(corp *token.Api, batch *structs.BatchUserId) ([]structs.ExternalContactDetail, string, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/batch/get_by_user?", consts.WorkApiPrefixUrl)

	buf, _ := json.Marshal(batch)

	var res = struct {
		response.Response
		ExternalContactDetail []structs.ExternalContactDetail `json:"external_contact_list"`
		NextCursor            string                          `json:"next_cursor"`
	}{}
	err := corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, "", err
	}
	return res.ExternalContactDetail, res.NextCursor, res.ErrorIf()
}

// ECRemark 修改客户备注信息
// 企业可通过此接口修改指定用户添加的客户的备注信息。
// 请求方式: POST(HTTP)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/remark?access_token=ACCESS_TOKEN
func ECRemark(corp *token.Api, remark *structs.ExternalRemark) error {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/remark?", consts.WorkApiPrefixUrl)
	buf, _ := json.Marshal(remark)
	var res = response.Response{}
	err := corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return err
	}
	return res.ErrorIf()
}

// ECGetTagList 获取企业标签库
// 企业可通过此接口获取企业客户标签详情。
// 请求方式: POST(HTTP)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_corp_tag_list?access_token=ACCESS_TOKEN
func ECGetTagList(corp *token.Api, tags []string, groups []string) ([]structs.ExternalContactTagGroup, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/get_corp_tag_list?", consts.WorkApiPrefixUrl)
	var body = struct {
		TagIds   []string `json:"tag_id,omitempty"`
		GroupIds []string `json:"group_id,omitempty"`
	}{
		TagIds:   tags,
		GroupIds: groups,
	}
	buf, _ := json.Marshal(&body)
	var res = struct {
		response.Response
		TagGroup []structs.ExternalContactTagGroup `json:"tag_group"`
	}{}
	err := corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return res.TagGroup, res.ErrorIf()
}

// ECAddTag 添加企业客户标签
// 企业可通过此接口向客户标签库中添加新的标签组和标签，每个企业最多可配置3000个企业标签。
// 请求方式: POST(HTTP)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/add_corp_tag?access_token=ACCESS_TOKEN
// req = *ExternalContactAddTagReq
func ECAddTag(corp *token.Api, req interface{}) (structs.ExternalContactTagGroup, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/add_corp_tag?", consts.WorkApiPrefixUrl)

	buf, _ := json.Marshal(req)
	var res = struct {
		response.Response
		TagGroup structs.ExternalContactTagGroup `json:"tag_group"`
	}{}
	err := corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return structs.ExternalContactTagGroup{}, err
	}
	return res.TagGroup, res.ErrorIf()
}

// ECEditTag  编辑企业客户标签
// 企业可通过此接口编辑客户标签/标签组的名称或次序值。
// 请求方式: POST(HTTP)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/edit_corp_tag?access_token=ACCESS_TOKEN
// req = *structs.ExternalContactEditTagReq
func ECEditTag(corp *token.Api, req interface{}) error {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/edit_corp_tag?", consts.WorkApiPrefixUrl)

	buf, _ := json.Marshal(req)
	var res = response.Response{}
	err := corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return err
	}
	return res.ErrorIf()
}

// ECDeleteTag 删除企业客户标签
// 企业可通过此接口删除客户标签库中的标签，或删除整个标签组。
// 请求方式: POST(HTTP)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/del_corp_tag?access_token=ACCESS_TOKEN
func ECDeleteTag(corp *token.Api, tags *structs.ExternalContactTagId, agentId string) error {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/del_corp_tag?", consts.WorkApiPrefixUrl)
	var req = struct {
		TagIds   []string `json:"tag_id,omitempty"`
		GroupIds []string `json:"group_id,omitempty"`
		AgentId  string   `json:"agent_id,omitempty"`
	}{
		TagIds:   tags.TagId,
		GroupIds: tags.GroupId,
		AgentId:  agentId,
	}
	buf, _ := json.Marshal(&req)
	var res = response.Response{}
	err := corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return err
	}
	return res.ErrorIf()
}

// ECStrategyTagList 获取指定规则组下的企业客户标签
// 企业可通过此接口获取某个规则组内的企业客户标签详情。
// 请求方式: POST(HTTP)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_strategy_tag_list?access_token=ACCESS_TOKEN
func ECStrategyTagList(corp *token.Api, tags *structs.ExternalContactTagId, strategyId string) ([]structs.ExternalContactTagGroup, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/get_strategy_tag_list?", consts.WorkApiPrefixUrl)

	var body = struct {
		TagIds     []string `json:"tag_id,omitempty"`
		GroupIds   []string `json:"group_id,omitempty"`
		StrategyId string   `json:"strategy_id,omitempty"`
	}{
		TagIds:     tags.TagId,
		GroupIds:   tags.GroupId,
		StrategyId: strategyId,
	}
	buf, _ := json.Marshal(&body)
	var res = struct {
		response.Response
		TagGroup []structs.ExternalContactTagGroup `json:"tag_group"`
	}{}
	err := corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return res.TagGroup, res.ErrorIf()
}

// ECAddStrategyTag 为指定规则组创建企业客户标签
// 企业可通过此接口向规则组中添加新的标签组和标签，每个企业的企业标签和规则组标签合计最多可配置3000个。注意，仅可在一级规则组下添加标签。
// 请求方式: POST(HTTP)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/add_strategy_tag?access_token=ACCESS_TOKEN
func ECAddStrategyTag(corp *token.Api, tag *structs.ExternalContactAddStrategyTagReq) (*structs.ExternalContactTagGroup, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/add_strategy_tag?", consts.WorkApiPrefixUrl)

	buf, _ := json.Marshal(tag)
	var res = struct {
		response.Response
		TagGroup structs.ExternalContactTagGroup `json:"tag_group"`
	}{}
	err := corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return &res.TagGroup, res.ErrorIf()
}

// ECEditStrategyTag 编辑指定规则组下的企业客户标签
// 企业可通过此接口编辑指定规则组下的客户标签/标签组的名称或次序值，但不可重新指定标签/标签组所属规则组。
// 请求方式: POST(HTTP)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/edit_strategy_tag?access_token=ACCESS_TOKEN
func ECEditStrategyTag(corp *token.Api, tag *structs.ExternalContactEditTagReq) error {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/edit_strategy_tag?", consts.WorkApiPrefixUrl)

	buf, _ := json.Marshal(tag)
	var res = response.Response{}
	err := corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return err
	}
	return res.ErrorIf()
}

// ECDeleteStrategyTag 删除指定规则组下的企业客户标签
// 企业可通过此接口删除某个规则组下的标签，或删除整个标签组。
// 请求方式: POST(HTTP)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/del_strategy_tag?access_token=ACCESS_TOKEN
func ECDeleteStrategyTag(corp *token.Api, tag *structs.ExternalContactTagId) error {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/del_strategy_tag?", consts.WorkApiPrefixUrl)

	buf, _ := json.Marshal(tag)
	var res = response.Response{}
	err := corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return err
	}
	return res.ErrorIf()
}

// ECMarkTag 编辑客户企业标签
// 企业可通过此接口为指定成员的客户添加上由企业统一配置的标签。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/mark_tag?access_token=ACCESS_TOKEN
func ECMarkTag(corp *token.Api, tags *structs.ExternalContactMarkTagReq) error {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/mark_tag?", consts.WorkApiPrefixUrl)

	buf, _ := json.Marshal(tags)
	var res = response.Response{}
	err := corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return err
	}
	return res.ErrorIf()
}

// ECGroupList 获取客户群列表
// 该接口用于获取配置过客户群管理的客户群列表。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/groupchat/list?access_token=ACCESS_TOKEN
func ECGroupList(corp *token.Api, req *structs.ExternalGroupListReq) ([]structs.GroupChat, string, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/groupchat/list?", consts.WorkApiPrefixUrl)

	buf, _ := json.Marshal(req)
	var res = struct {
		response.Response
		GroupChats []structs.GroupChat `json:"group_chat_list"`
		NextCursor string              `json:"next_cursor"`
	}{}
	err := corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, "", err
	}
	return res.GroupChats, res.NextCursor, res.ErrorIf()
}

// ECGroupDetail 获取客户群详情
// 通过客户群ID，获取详情。包括群名、群成员列表、群成员入群时间、入群方式。（客户群是由具有客户群使用权限的成员创建的外部群）
// 需注意的是，如果发生群信息变动，会立即收到群变更事件，但是部分信息是异步处理，可能需要等一段时间调此接口才能得到最新结果
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/groupchat/get?access_token=ACCESS_TOKEN
func ECGroupDetail(corp *token.Api, chatId string, needName int) (*structs.GroupChatDetail, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/groupchat/get?", consts.WorkApiPrefixUrl)

	var body = struct {
		ChatId   string `json:"chat_id"`
		NeedName int    `json:"need_name"`
	}{
		ChatId:   chatId,
		NeedName: needName,
	}
	buf, err := json.Marshal(&body)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		GroupChatDetail structs.GroupChatDetail `json:"group_chat"`
	}{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return &res.GroupChatDetail, res.ErrorIf()
}

// ECGroupMsgCreate
// 添加企业群发
// https://developer.work.weixin.qq.com/document/path/92135
// 企业可通过此接口添加企业群发消息的模板并通知客服人员发送给相关客户。（注：企业微信终端需升级到2.7.5版本及以上）
// 注意：调用该接口并不会直接发送消息给客户，需要相关的客服人员操作以后才会实际发送（客服人员的企业微信需要升级到2.7.5及以上版本）
// 群发消息可以是企业统一创建发送的，也可以是成员自己创建发送的；每位客户/每个客户群每月最多可接收条数为当月天数，超过接收上限的客户/客户群将无法再收到群发消息。
func ECGroupMsgCreate(corp *token.Api, req *structs.ExGroupMsg) (*structs.ExGroupMsgResult, error) {

	url := fmt.Sprintf("%s/cgi-bin/externalcontact/add_msg_template?", consts.WorkApiPrefixUrl)

	buf, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		structs.ExGroupMsgResult
	}{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return &res.ExGroupMsgResult, res.ErrorIf()
}

// ECRemindGroupMsgSend 提醒成员群发
// 最后更新：2022/11/30
// 企业和第三方应用可调用此接口，重新触发群发通知，提醒成员完成群发任务，24小时内每个群发最多触发三次提醒。
// 请求方式:POST(HTTPS)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/remind_groupmsg_send?access_token=ACCESS_TOKEN
func ECRemindGroupMsgSend(corp *token.Api, msgId string) error {

	url := fmt.Sprintf("%s/cgi-bin/externalcontact/remind_groupmsg_send?", consts.WorkApiPrefixUrl)
	var req = struct {
		MsgId string `json:"msgid"`
	}{
		MsgId: msgId,
	}
	buf, err := json.Marshal(req)
	if err != nil {
		return err
	}
	var res = response.Response{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return err
	}
	return res.ErrorIf()
}

// ECCancelGroupMsgSend 停止企业群发
// 最后更新：2022/11/30
// 企业和第三方应用可调用此接口，停止无需成员继续发送的企业群发
// 请求方式:POST(HTTPS)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/cancel_groupmsg_send?access_token=ACCESS_TOKEN
func ECCancelGroupMsgSend(corp *token.Api, msgId string) error {

	url := fmt.Sprintf("%s/cgi-bin/externalcontact/cancel_groupmsg_send?", consts.WorkApiPrefixUrl)
	var req = struct {
		MsgId string `json:"msgid"`
	}{
		MsgId: msgId,
	}
	buf, err := json.Marshal(req)
	if err != nil {
		return err
	}
	var res = response.Response{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return err
	}
	return res.ErrorIf()
}

// ECGetGroupMsgList 获取群发记录列表
// 企业和第三方应用可通过此接口获取企业与成员的群发记录。
// 请求方式：POST(HTTPS)
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_groupmsg_list_v2?access_token=ACCESS_TOKEN
func ECGetGroupMsgList(corp *token.Api, req *structs.GroupMsgReq) ([]structs.GroupMsg, string, error) {

	url := fmt.Sprintf("%s/cgi-bin/externalcontact/get_groupmsg_list_v2?", consts.WorkApiPrefixUrl)

	buf, err := json.Marshal(req)
	if err != nil {
		return nil, "", err
	}
	var res = struct {
		response.Response
		NextCursor string             `json:"next_cursor"`
		GroupMsg   []structs.GroupMsg `json:"group_msg_list"`
	}{}
	if err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	}); err != nil {
		return nil, "", err
	}
	return res.GroupMsg, res.NextCursor, res.ErrorIf()
}

// ECGetGroupMsgTask 获取群发成员发送任务列表
// 请求方式:POST(HTTPS)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_groupmsg_task?access_token=ACCESS_TOKEN
func ECGetGroupMsgTask(corp *token.Api, req *structs.GroupMsgSearch) ([]structs.GroupMsgTask, string, error) {

	url := fmt.Sprintf("%s/cgi-bin/externalcontact/get_groupmsg_task?", consts.WorkApiPrefixUrl)

	buf, err := json.Marshal(req)
	if err != nil {
		return nil, "", err
	}
	var res = struct {
		response.Response
		NextCursor string                 `json:"next_cursor"`
		GroupMsg   []structs.GroupMsgTask `json:"task_list"`
	}{}
	if err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	}); err != nil {
		return nil, "", err
	}
	return res.GroupMsg, res.NextCursor, res.ErrorIf()
}

// ECGroupMsgSendResult 获取企业群发成员执行结果
// 请求方式:POST(HTTPS)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_groupmsg_send_result?access_token=ACCESS_TOKEN
func ECGroupMsgSendResult(corp *token.Api, req *structs.GroupMsgSearch) ([]structs.GroupMsgSendResult, string, error) {

	url := fmt.Sprintf("%s/cgi-bin/externalcontact/get_groupmsg_send_result?", consts.WorkApiPrefixUrl)

	buf, err := json.Marshal(req)
	if err != nil {
		return nil, "", err
	}
	var res = struct {
		response.Response
		NextCursor string                       `json:"next_cursor"`
		GroupMsg   []structs.GroupMsgSendResult `json:"send_list"`
	}{}
	if err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	}); err != nil {
		return nil, "", err
	}
	return res.GroupMsg, res.NextCursor, res.ErrorIf()
}

// ECAddMomentTask 企业和第三方应用可通过该接口创建客户朋友圈的发表任务
// 企业可通过此接口，根据外部联系人的userid（如何获取?），拉取客户详情。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/add_moment_task?access_token=ACCESS_TOKEN
func ECAddMomentTask(corp *token.Api, req *structs.AddMomentTask) (string, error) {

	url := fmt.Sprintf("%s/cgi-bin/externalcontact/add_moment_task", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(req)
	if err != nil {
		return "", err
	}
	var res = struct {
		response.Response
		JobId string `json:"jobId"`
	}{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return "", err
	}
	return res.JobId, res.ErrorIf()
}

// ECGetMomentTaskResult 获取任务创建结果
// 请求方式：GET（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_moment_task_result?access_token=ACCESS_TOKEN
func ECGetMomentTaskResult(corp *token.Api, jobId string) (*structs.MomentTaskResult, error) {

	url := fmt.Sprintf("%s/cgi-bin/externalcontact/get_moment_task_result?jobid=%s&", consts.WorkApiPrefixUrl, jobId)

	var res = struct {
		response.Response
		structs.MomentTaskResult
	}{}
	err := corp.Request("GET", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return &res.MomentTaskResult, res.ErrorIf()
}

// ECCancelMomentTask 停止发表企业朋友圈
func ECCancelMomentTask(corp *token.Api, momentId string) error {

	url := fmt.Sprintf("%s/cgi-bin/externalcontact/cancel_moment_task?", consts.WorkApiPrefixUrl)
	var body = struct {
		MomentId string `json:"moment_id"`
	}{
		MomentId: momentId,
	}
	buf, err := json.Marshal(&body)
	if err != nil {
		return err
	}
	var res = response.Response{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return err
	}
	return res.ErrorIf()
}

// ECGetMomentList 获取企业全部的发表列表
func ECGetMomentList(corp *token.Api, req structs.MomentListReq) (*structs.MomentListResp, error) {

	url := fmt.Sprintf("%s/cgi-bin/externalcontact/get_moment_list?", consts.WorkApiPrefixUrl)

	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		structs.MomentListResp
	}{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return &res.MomentListResp, res.ErrorIf()
}

// ECGetUserBehaviorData 获取「联系客户统计」数据
// userid和partyid不可同时为空;
// 此接口提供的数据以天为维度，查询的时间范围为[start_time,end_time]，即前后均为闭区间，支持的最大查询跨度为30天；
// 用户最多可获取最近180天内的数据；
// 当传入的时间不为0点时间戳时，会向下取整，如传入1554296400(Wed Apr 3 21:00:00 CST 2019)会被自动转换为1554220800（Wed Apr 3 00:00:00 CST 2019）;
// 如传入多个userid，则表示获取这些成员总体的联系客户数据
// 企业可通过此接口获取成员联系客户的数据，包括发起申请数、新增客户数、聊天数、发送消息数和删除/拉黑成员的客户数等指标。
// 请求方式: POST(HTTP)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_user_behavior_data?access_token=ACCESS_TOKEN
func ECGetUserBehaviorData(corp *token.Api, req *structs.UserBehaviorDataReq) ([]structs.BehaviorDataResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/get_user_behavior_data?", consts.WorkApiPrefixUrl)

	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		Data []structs.BehaviorDataResult `json:"behavior_data"`
	}{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return res.Data, res.ErrorIf()
}

// ECGroupChatStatistic 获取「群聊数据统计」数据
// 获取指定日期的统计数据。注意，企业微信仅存储180天的数据。
// 按群主聚合的方式
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/groupchat/statistic?access_token=ACCESS_TOKEN
func ECGroupChatStatistic(corp *token.Api, req *structs.GroupChatStatisticReq) (*structs.GroupChatStatisticResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/groupchat/statistic?", consts.WorkApiPrefixUrl)

	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		structs.GroupChatStatisticResult
	}{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return &res.GroupChatStatisticResult, res.ErrorIf()
}

// ECGroupChatStatisticByDay 按自然日聚合的方式
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/groupchat/statistic_group_by_day?access_token=ACCESS_TOKEN
func ECGroupChatStatisticByDay(corp *token.Api, req *structs.GroupChatDayStatisticReq) (*structs.GroupChatDayStatisticResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/groupchat/statistic_group_by_day?", consts.WorkApiPrefixUrl)

	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		structs.GroupChatDayStatisticResult
	}{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return &res.GroupChatDayStatisticResult, res.ErrorIf()
}

// ECCustomerAcquisitionList 获取获客链接列表
// 企业可通过此接口获取当前仍然有效的获客链接。
// 请求方式: POST(HTTP)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/customer_acquisition/list_link?access_token=ACCESS_TOKEN
func ECCustomerAcquisitionList(corp *token.Api, req *structs.CustomerAcquisitionSearch) (*structs.CustomerAcquisitionResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/customer_acquisition/list_link?", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		structs.CustomerAcquisitionResult
	}{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return &res.CustomerAcquisitionResult, res.ErrorIf()
}

// ECCustomerAcquisitionDetail 获取获客链接详情
// 企业可通过此接口根据获客链接id获取链接配置详情。
// 请求方式: POST(HTTP)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/customer_acquisition/get?access_token=ACCESS_TOKEN
func ECCustomerAcquisitionDetail(corp *token.Api, linkId string) (*structs.CustomerAcquisitionInfo, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/customer_acquisition/get?", consts.WorkApiPrefixUrl)
	var req = struct {
		LinkId string `json:"link_id"`
	}{
		LinkId: linkId,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		structs.CustomerAcquisitionInfo
	}{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return &res.CustomerAcquisitionInfo, res.ErrorIf()
}

// ECCustomerAcquisitionCreate 创建获客链接
// 企业可通过此接口创建新的获客链接。
// 请求方式: POST(HTTP)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/customer_acquisition/create_link?access_token=ACCESS_TOKEN
func ECCustomerAcquisitionCreate(corp *token.Api, req *structs.CustomerAcquisition) (*structs.CustomerAcquisitionLink, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/customer_acquisition/create_link?", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		structs.CustomerAcquisitionLink `json:"link"`
	}{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return &res.CustomerAcquisitionLink, res.ErrorIf()
}

// ECCustomerAcquisitionUpdate 编辑获客链接
// 企业可通过此接口编辑获客链接，修改获客链接的关联范围或修改获客链接的名称。
// 请求方式: POST(HTTP)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/customer_acquisition/update_link?access_token=ACCESS_TOKEN
func ECCustomerAcquisitionUpdate(corp *token.Api, req *structs.CustomerAcquisitionInfo) error {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/customer_acquisition/update_link?", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(&req)
	if err != nil {
		return err
	}
	var res = response.Response{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return err
	}
	return res.ErrorIf()
}

// ECCustomerAcquisitionDelete 删除获客链接
// 企业可通过此接口删除获客链接，删除后的获客链接将无法继续使用。
// 请求方式: POST(HTTP)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/customer_acquisition/delete_link?access_token=ACCESS_TOKEN
func ECCustomerAcquisitionDelete(corp *token.Api, linkId string) error {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/customer_acquisition/delete_link?", consts.WorkApiPrefixUrl)
	var req = struct {
		LinkId string `json:"link_id"`
	}{
		LinkId: linkId,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return err
	}
	var res = response.Response{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return err
	}
	return res.ErrorIf()
}

// ECCustomerAcquisitionCustomerList 获取获客客户列表
// 企业可通过此接口获取到由指定的获客链接添加的客户列表。
// 请求方式: POST(HTTP)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/customer_acquisition/customer?access_token=ACCESS_TOKEN
func ECCustomerAcquisitionCustomerList(corp *token.Api, req *structs.CustomerAcquisitionSearch) (*structs.CustomerAcquisitionCustomerList, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/customer_acquisition/customer?", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		structs.CustomerAcquisitionCustomerList
	}{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return &res.CustomerAcquisitionCustomerList, res.ErrorIf()
}

// ECCustomerAcquisitionQuota 查询剩余使用量
// 企业可通过此接口查询当前剩余的使用量。
// 请求方式：GET（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/customer_acquisition_quota?access_token=ACCESS_TOKEN
func ECCustomerAcquisitionQuota(corp *token.Api) (*structs.CustomerAcquisitionQuotaList, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/customer_acquisition_quota?", consts.WorkApiPrefixUrl)
	var res = struct {
		response.Response
		structs.CustomerAcquisitionQuotaList
	}{}
	err := corp.Request("GET", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return &res.CustomerAcquisitionQuotaList, res.ErrorIf()
}

// ECCustomerAcquisitionStatistic 查询链接使用详情
// 企业可通过此接口查询指定获客链接在指定时间范围内的访问情况。
// 请求方式：GET（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/customer_acquisition/statistic?access_token=ACCESS_TOKEN
func ECCustomerAcquisitionStatistic(corp *token.Api, req *structs.CustomerAcquisitionStatisticSearch) (*structs.CustomerAcquisitionStatisticResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/customer_acquisition/statistic?", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		structs.CustomerAcquisitionStatisticResult
	}{}
	err = corp.Request("GET", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return &res.CustomerAcquisitionStatisticResult, res.ErrorIf()
}

// ECCustomerAcquisitionChatInfo 获取成员多次收消息详情
// 企业和服务商可通过此接口获取成员多次收消息情况，如次数、客户id等信息。。
// 请求方式: POST(HTTP)
// 请求地址:https://qyapi.weixin.qq.com/cgi-bin/externalcontact/customer_acquisition/get_chat_info?access_token=ACCESS_TOKEN
func ECCustomerAcquisitionChatInfo(corp *token.Api, req *structs.CustomerAcquisitionChatInfoSearch) (*structs.CustomerAcquisitionChatInfoResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/customer_acquisition/get_chat_info?", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		structs.CustomerAcquisitionChatInfoResult
	}{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return &res.CustomerAcquisitionChatInfoResult, res.ErrorIf()
}

// ECGroupTransfer 分配离职成员的客户群
// 最后更新：2020/12/16
// 企业可通过此接口，将已离职成员为群主的群，分配给另一个客服成员。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/groupchat/transfer?access_token=ACCESS_TOKEN
func ECGroupTransfer(corp *token.Api, req structs.GroupTransfer) ([]structs.GroupTransferResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/groupchat/transfer?", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		Result []structs.GroupTransferResult `json:"failed_chat_list"`
	}{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return res.Result, res.ErrorIf()
}

// ECGroupOnJobTransfer 分配在职成员的客户群
// 最后更新：2022/05/06
// 企业可通过此接口，将在职成员为群主的群，分配给另一个客服成员。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/groupchat/onjob_transfer?access_token=ACCESS_TOKEN
func ECGroupOnJobTransfer(corp *token.Api, req structs.GroupTransfer) ([]structs.GroupTransferResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/groupchat/onjob_transfer?", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		Result []structs.GroupTransferResult `json:"failed_chat_list"`
	}{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return res.Result, res.ErrorIf()
}

// ECTransferCustomer 分配在职成员的客户
// 最后更新：2022/08/31
// 企业可通过此接口，转接在职成员的客户给其他成员。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/transfer_customer?access_token=ACCESS_TOKEN
func ECTransferCustomer(corp *token.Api, req structs.TransferReq) ([]structs.TransferResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/transfer_customer?", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		Result []structs.TransferResult `json:"customer"`
	}{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return res.Result, res.ErrorIf()
}

// ECUnAssignedList 获取待分配的离职成员列表
// 最后更新：2023/12/01
// 企业和第三方可通过此接口，获取所有离职成员的客户列表，并可进一步调用分配离职成员的客户接口将这些客户重新分配给其他企业成员。
//
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_unassigned_list?access_token=ACCESS_TOKEN
func ECUnAssignedList(corp *token.Api, cursor structs.Cursor) (structs.UnAssignedList, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/get_unassigned_list?", consts.WorkApiPrefixUrl)
	req := struct {
		Cursor   string `json:"cursor,omitempty"`
		PageSize int    `json:"page_size"`
	}{
		Cursor:   cursor.Cursor,
		PageSize: cursor.Limit,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return structs.UnAssignedList{}, err
	}
	var res = struct {
		response.Response
		structs.UnAssignedList
	}{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return res.UnAssignedList, err
	}
	return res.UnAssignedList, res.ErrorIf()
}

// ECReSignedTransfer 分配离职成员的客户
// 企业可通过此接口，分配离职成员的客户给其他成员。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/resigned/transfer_customer?access_token=ACCESS_TOKEN
func ECReSignedTransfer(corp *token.Api, req structs.TransferReq) ([]structs.TransferResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/resigned/transfer_customer?", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		Result []structs.TransferResult `json:"customer"`
	}{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return res.Result, res.ErrorIf()
}

// ECReSignedTransferResult 查询客户接替状态
// 企业和第三方可通过此接口查询离职成员的客户分配情况。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/resigned/transfer_result?access_token=ACCESS_TOKEN
func ECReSignedTransferResult(corp *token.Api, req structs.TransferResultReq) ([]structs.TransferResultResp, string, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/resigned/transfer_result?", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, "", err
	}
	var res = struct {
		response.Response
		Result     []structs.TransferResultResp `json:"customer"`
		NextCursor string                       `json:"next_cursor"`
	}{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, "", err
	}
	return res.Result, res.NextCursor, res.ErrorIf()
}

// ECTransferResult 查询客户接替状态
// 企业和第三方可通过此接口查询在职成员的客户转接情况。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/transfer_result?access_token=ACCESS_TOKEN
func ECTransferResult(corp *token.Api, req structs.TransferResultReq) ([]structs.TransferResultResp, string, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/transfer_result?", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, "", err
	}
	var res = struct {
		response.Response
		Result     []structs.TransferResultResp `json:"customer"`
		NextCursor string                       `json:"next_cursor"`
	}{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, "", err
	}
	return res.Result, res.NextCursor, res.ErrorIf()
}

// ECAddContactWay 配置客户联系「联系我」方式
// 调用相关接口应满足如下的权限要求：
// 权限
// 企业需要使用“客户联系”secret或配置到“可调用应用”列表中的自建应用secret所获取的accesstoken来调用（accesstoken如何获取？）。
// 使用人员需要配置了客户联系功能。
// 第三方调用时，应用需具有“企业客户权限->客户联系->配置「联系我」二维码”权限。
// 第三方/自建应用调用时，传入的userid和partyid需要在此应用的可见范围内。
// 配置的使用成员必须在企业微信激活且已经过实名认证。
// 临时会话的二维码具有有效期，添加企业成员后仅能在指定有效期内进行会话，仅支持医疗行业企业创建。
// 临时会话模式可以配置会话结束时自动发送给用户的结束语。
//
// 企业可以在管理后台-客户联系-加客户中配置成员的「联系我」的二维码或者小程序按钮，客户通过扫描二维码或点击小程序上的按钮，即可获取成员联系方式，主动联系到成员。
// 企业可通过此接口为具有客户联系功能的成员生成专属的「联系我」二维码或者「联系我」按钮。
// 如果配置的是「联系我」按钮，需要开发者的小程序接入小程序插件。
//
// 注意:
// 通过API添加的「联系我」不会在管理端进行展示，每个企业可通过API最多配置50万个「联系我」。
// 用户需要妥善存储返回的config_id，config_id丢失可能导致用户无法编辑或删除「联系我」。
// 临时会话模式不占用「联系我」数量，但每日最多添加10万个，并且仅支持单人。
// 临时会话模式的二维码，添加好友完成后该二维码即刻失效。
//
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/add_contact_way?access_token=ACCESS_TOKEN
// req *structs.AddContactWay
func ECAddContactWay(corp *token.Api, req any) (*structs.AddContactWayResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/add_contact_way?", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		structs.AddContactWayResult
	}{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return &res.AddContactWayResult, res.ErrorIf()
}

// ECGetContactWay 获取企业已配置的「联系我」方式
// 获取企业配置的「联系我」二维码和「联系我」小程序按钮。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/get_contact_way?access_token=ACCESS_TOKEN
func ECGetContactWay(corp *token.Api, configId string) (*structs.ContactWay, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/get_contact_way?", consts.WorkApiPrefixUrl)
	req := struct {
		ConfigId string `json:"config_id"`
	}{
		ConfigId: configId,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		Result structs.ContactWay `json:"contact_way"`
	}{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return &res.Result, res.ErrorIf()
}

// ECListContactWay 获取企业已配置的「联系我」列表
// 获取企业配置的「联系我」二维码和「联系我」小程序插件列表。不包含临时会话。
// 注意，该接口仅可获取2021年7月10日以后创建的「联系我」
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/list_contact_way?access_token=ACCESS_TOKEN
func ECListContactWay(corp *token.Api, req *structs.CursorSearchReq) ([]structs.WayItem, string, error) {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/list_contact_way?", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, "", err
	}
	var res = struct {
		response.Response
		Item       []structs.WayItem `json:"contact_way"`
		NextCursor string            `json:"next_cursor"`
	}{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, "", err
	}
	return res.Item, res.NextCursor, res.ErrorIf()
}

// ECUpdateContactWay 更新企业已配置的「联系我」方式
// 更新企业配置的「联系我」二维码和「联系我」小程序按钮中的信息，如使用人员和备注等。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/update_contact_way?access_token=ACCESS_TOKEN
// req  *structs.UpdateContactWay
func ECUpdateContactWay(corp *token.Api, req any) error {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/update_contact_way?", consts.WorkApiPrefixUrl)
	buf, err := json.Marshal(req)
	if err != nil {
		return err
	}
	var res = response.Response{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return err
	}
	return res.ErrorIf()
}

// ECDeleteContactWay 删除企业已配置的「联系我」方式
// 删除一个已配置的「联系我」二维码或者「联系我」小程序按钮。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/del_contact_way?access_token=ACCESS_TOKEN
func ECDeleteContactWay(corp *token.Api, configId string) error {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/del_contact_way?", consts.WorkApiPrefixUrl)
	req := struct {
		ConfigId string `json:"config_id"`
	}{
		ConfigId: configId,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return err
	}
	var res = response.Response{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return err
	}
	return res.ErrorIf()
}

// ECCloseTempChat 结束临时会话
// 将指定的企业成员和客户之前的临时会话断开，断开前会自动下发已配置的结束语。
// 请求方式：POST（HTTPS）
// 请求地址：https://qyapi.weixin.qq.com/cgi-bin/externalcontact/close_temp_chat?access_token=ACCESS_TOKEN
func ECCloseTempChat(corp *token.Api, userid, externalUserId string) error {
	url := fmt.Sprintf("%s/cgi-bin/externalcontact/close_temp_chat?", consts.WorkApiPrefixUrl)
	req := struct {
		UserId         string `json:"userid"`
		ExternalUserId string `json:"external_userid"`
	}{
		UserId:         userid,
		ExternalUserId: externalUserId,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return err
	}
	var res = response.Response{}
	err = corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return err
	}
	return res.ErrorIf()
}
