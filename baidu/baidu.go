package baidu

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/token/access"
	"gitee.com/h79/goutils/common/data"
	"gitee.com/h79/goutils/common/http"
)

func GetAccessToken(tk token.Token, d data.D) (access.Token, error) {

	p := tk.Parent()

	url := fmt.Sprintf("%s/oauth/2.0/token?grant_type=client_credentials&client_id=%s&client_secret=%s", ApiPrefixUrl, p.GetAppId(), p.GetSecret())

	hp := http.Http{}
	body, err := hp.DoBytes("POST", url, nil)
	if err != nil {
		return nil, err
	}

	type tokenResult struct {
		Response
		AccessToken
	}
	res := tokenResult{}
	if err = json.Unmarshal(body, &res); err != nil {
		return nil, err
	}
	return &res.AccessToken, res.ErrorIf()
}
