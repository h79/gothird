package dingtalk

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/token/access"
	"gitee.com/h79/goutils/common/data"
	"gitee.com/h79/goutils/common/http"
	http2 "net/http"
)

func GetAccessToken(tk token.Token, d data.D) (access.Token, error) {

	url := fmt.Sprintf("%s/gettoken?appkey=%s&appsecret=%s", ApiPrefixUrl, tk.GetAppId(), tk.GetSecret())

	hp := http.Http{}
	body, err := hp.Do("GET", url, nil, func(h *http2.Header) {
	})
	if err != nil {
		return nil, err
	}

	type tokenResult struct {
		Response
		AccessToken
	}
	res := tokenResult{}
	if err = json.Unmarshal(body, &res); err != nil {
		return nil, err
	}
	if res.Code != 0 {
		return nil, token.Result{Code: res.Code, Msg: res.Msg}
	}
	return &res.AccessToken, nil
}
