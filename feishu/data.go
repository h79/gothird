package feishu

import (
	"gitee.com/h79/gothird/token"
	"mime/multipart"
)

type DepartmentList struct {
	AuthedDepartments     []string `json:"authed_departments,omitempty"`
	AuthedOpenDepartments []string `json:"authed_open_departments,omitempty"`
	AuthedEmployeeIds     []string `json:"authed_employee_ids,omitempty"`
	AuthedOpenIds         []string `json:"authed_open_ids,omitempty"`
}

type DepartmentInfo struct {
	Id                     string `json:"id"`
	OpenDepartmentId       string `json:"open_department_id"`
	LeaderEmployeeId       string `json:"leader_employee_id"`
	LeaderOpenId           string `json:"leader_open_id"`
	ChatId                 string `json:"chat_id"`
	MemberCount            int    `json:"member_count"`
	Name                   string `json:"name"`
	ParentId               string `json:"parent_id"`
	ParentOpenDepartmentId string `json:"parent_open_department_id"`
	Status                 int    `json:"status"`
}

/* 页码信息 */
type Page struct {
	HasMore   bool   `json:"has_more"`
	PageToken string `json:"page_token"`
}

// 群信息
type Group struct {
	Avatar      string `json:"avatar"`
	ChatId      string `json:"chat_id"`
	Desc        string `json:"description"`
	Name        string `json:"name"`
	OwnerOpenId string `json:"owner_open_id"`
	OwnerUserId string `json:"owner_user_id"`
}

type User struct {
	OpenId string `json:"open_id"`
	UserId string `json:"user_id"`
}

type Image struct {
	Type     string
	FileName string
}

func (img *Image) CreateForm(w *multipart.Writer, field string) error {
	if er := w.WriteField("image_type", img.Type); er != nil {
		return er
	}
	if er := token.DefaultForm(w, field, img.FileName); er != nil {
		return er
	}
	return nil
}

type UserDetail struct {
	User
	Name             string   `json:"name"`
	NamePy           string   `json:"name_py"`
	NameEn           string   `json:"en_name"`
	EmployeeId       string   `json:"employee_id"`
	EmployeeNo       string   `json:"employee_no"`
	OpenId           string   `json:"open_id"`
	UnionId          string   `json:"union_id"`
	Status           int      `json:"status"`
	EmployeeType     int      `json:"employee_type"`
	Email            string   `json:"email"`
	Mobile           string   `json:"mobile"`
	LeaderEmployeeId string   `json:"leader_employee_id"`
	LeaderOpenId     string   `json:"leader_open_id"`
	LeaderUnionId    string   `json:"leader_union_id"`
	Departments      []string `json:"departments"`
	OpenDepartments  []string `json:"open_departments"`
}

type MsgHead struct {
	OpenId  string `json:"open_id,omitempty"`
	RootId  string `json:"root_id,omitempty"`
	ChatId  string `json:"chat_id,omitempty"`
	UserId  string `json:"user_id,omitempty"`
	Email   string `json:"email,omitempty"`
	MsgType string `json:"msg_type"`
}

type ContentText struct {
	Text string `json:"text"`
}

// msg_type=text
type MsgText struct {
	MsgHead
	Content ContentText `json:"content"`
}

// 需要上传
type ContentImage struct {
	ImageK string `json:"image_key"`
}

// msg_type="image"
type MsgImage struct {
	MsgHead
	Content ContentImage `json:"content"`
}

type PostText struct {
	Title   string        `json:"title"`
	Content []interface{} `json:"content"`
}

type PostRichText struct {
	Zh PostText `json:"zh_cn"` //中文
	Jp PostText `json:"ja_jp"` //日
	En PostText `json:"en_us"` //英
}

type ContentRichText struct {
	Post PostRichText `json:"post"`
}

// msg_type="image"
type MsgRichText struct {
	MsgHead
	Content ContentRichText `json:"content"`
}
