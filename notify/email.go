package notify

import (
	"bytes"
	"fmt"
	"gitee.com/h79/gothird/config"
	"gitee.com/h79/goutils/common/result"
	"net/smtp"
	"strings"
)

type FillEmailCallback func(*config.Options, interface{}) *bytes.Buffer

func SendEmail(op *config.Options, content interface{}, call FillEmailCallback) error {

	if op.Email.EmailTo == "" {
		return result.Error(result.ErrEmailInternal, "to is empty")
	}
	if op.Email.Username == "" {
		return result.Error(result.ErrEmailInternal, "username is empty")
	}
	if op.Email.Password == "" {
		return result.Error(result.ErrEmailInternal, "password is empty")
	}
	if op.Email.Server.Host == "" {
		return result.Error(result.ErrEmailInternal, "host is empty")
	}
	auth := smtp.PlainAuth("",
		op.Email.Username,
		op.Email.Password,
		op.Email.Server.Host)
	addr := op.Email.Server.To()
	msg := call(op, content)
	emailTo := strings.Split(op.Email.EmailTo, ";")
	if err := smtp.SendMail(addr, auth, op.Email.Username, emailTo, msg.Bytes()); err != nil {
		return result.Error(result.ErrEmailInternal, fmt.Sprintf("%v:%v\r\n%v", addr, msg, err))
	}
	return nil
}
