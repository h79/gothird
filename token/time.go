package token

import "time"

type updateTime struct {
	createAt int64
	updateAt int64
}

func (u *updateTime) Update() {
	u.updateAt = time.Now().Unix()
}

func (u *updateTime) Complete() {
	u.createAt = time.Now().Unix()
	u.updateAt = u.createAt
}

func (u *updateTime) HasUpdate() bool {
	return u.updateAt != u.createAt
}
