package app3

import (
	"gitee.com/h79/gothird/token/access"
)

type AccessToken struct {
	Token     string `json:"component_access_token"`
	ExpiresIn int64  `json:"expires_in"`
}

// GetAccessToken access.Token interface implement
func (a AccessToken) GetAccessToken() access.Value {
	return access.Value{
		Data:     a.Token,
		ExpireIn: access.ExpireSecond(a.ExpiresIn),
	}
}

func (a AccessToken) GetRefAccessToken() access.Value {
	return access.Value{}
}
