package work

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/gothird/weixin/work/structs"
	"gitee.com/h79/goutils/common/http"
)

//https://work.weixin.qq.com/api/doc#90000/90135/90664

func genHead(toWho structs.ToWho, msgType string) structs.Head {
	h := structs.Head{
		Type: msgType,
		Safe: 0,
	}
	if yes, agent := toWho.IsAgent(); yes {
		h.AgentId = agent.AgentId
		h.ToTag = agent.ToTag
		h.ToParty = agent.ToParty
		h.ToUser = agent.ToUser
	}
	if yes, group := toWho.IsGroup(); yes {
		h.ChatId = group.ChatId
	}
	return h
}

func NewMessage(toWho structs.ToWho, msgType string, msg interface{}) structs.Message {

	switch msgType {
	case "text":
		return &structs.TextMsg{
			Head: genHead(toWho, msgType),
			Text: msg.(structs.ExText),
		}

	case "image":
		return &structs.ImageMsg{
			Head:  genHead(toWho, msgType),
			Image: msg.(structs.ExImage),
		}

	case "voice":
		return &structs.VoiceMsg{
			Head:  genHead(toWho, msgType),
			Voice: msg.(structs.ExVoice),
		}

	case "video":
		return &structs.VideoMsg{
			Head:  genHead(toWho, msgType),
			Video: msg.(structs.ExVideo),
		}

	case "news":
		return &structs.NewsMsg{
			Head: genHead(toWho, msgType),
			News: msg.(structs.News),
		}

	case "textcard":
		return &structs.TextCardMsg{
			Head:     genHead(toWho, msgType),
			TextCard: (msg).(structs.TextCard),
		}

	case "markdown":
		return &structs.MarkdownMsg{
			Head:     genHead(toWho, msgType),
			Markdown: msg.(structs.ExText),
		}

	default:
	}
	return nil
}

// SendMessage 发送应用消息,群聊消息,客户欢迎语
func SendMessage(api *token.Api, msg structs.Message) error {
	_, err := SendMessageEx(api, msg)
	if err != nil {
		return err
	}
	return nil
}

func SendMessageEx(api *token.Api, msg structs.Message) (structs.MessageResponse, error) {
	var url string
	if msg.IsAgent() {
		url = fmt.Sprintf("%s/cgi-bin/message/send?", consts.WorkApiPrefixUrl)
	} else if msg.IsGroup() {
		url = fmt.Sprintf("%s/cgi-bin/appchat/send?", consts.WorkApiPrefixUrl)
	} else if msg.IsWelcome() { //发送新客户欢迎语
		url = fmt.Sprintf("%s/cgi-bin/externalcontact/send_welcome_msg?", consts.WorkApiPrefixUrl)
	} else {
		return structs.MessageResponse{}, token.Error(-1, "[WX] SendMessage parameter is error")
	}
	buf, err := json.Marshal(msg)
	if err != nil {
		return structs.MessageResponse{}, token.Error(-1, "[WX] SendMessage Json marshal error")
	}
	res := structs.MessageResponse{}
	err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		r := response.Response{ErrCode: res.ErrCode, ErrMsg: res.ErrMsg}
		return r.ReturnIf(api)
	})
	if err != nil {
		return structs.MessageResponse{}, err
	}
	r := response.Response{ErrCode: res.ErrCode, ErrMsg: res.ErrMsg}
	return res, r.ErrorIf()
}
