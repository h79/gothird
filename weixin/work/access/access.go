package access

import (
	"gitee.com/h79/gothird/token/access"
)

type Token struct {
	AcsToken string `json:"access_token"`
	ExpireIn int64  `json:"expires_in"`
}

func (a Token) GetAccessToken() access.Value {
	return access.Value{
		Data:     a.AcsToken,
		ExpireIn: access.ExpireSecond(a.ExpireIn),
	}
}

func (a Token) GetRefAccessToken() access.Value {
	return access.Value{}
}

// ProviderAccessToken 服务商token
type ProviderAccessToken struct {
	AcsToken  string `json:"provider_access_token"`
	ExpiresIn int64  `json:"expires_in"`
}

func (a ProviderAccessToken) GetAccessToken() access.Value {
	return access.Value{
		Data:     a.AcsToken,
		ExpireIn: access.ExpireSecond(a.ExpiresIn),
	}
}

func (a ProviderAccessToken) GetRefAccessToken() access.Value {
	return access.Value{}
}

// SuiteAccessToken 第三方应用凭证token
type SuiteAccessToken struct {
	SuiteAcsToken string `json:"suite_access_token"`
	ExpiresIn     int64  `json:"expires_in"`
}

func (a SuiteAccessToken) GetAccessToken() access.Value {
	return access.Value{
		Data:     a.SuiteAcsToken,
		ExpireIn: access.ExpireSecond(a.ExpiresIn),
	}
}

func (a SuiteAccessToken) GetRefAccessToken() access.Value {
	return access.Value{}
}

type PermanentAccessToken struct {
	AcsToken  string `json:"access_token"`
	ExpiresIn int64  `json:"expires_in"`
}

func (a PermanentAccessToken) GetAccessToken() access.Value {
	return access.Value{
		Data:     a.AcsToken,
		ExpireIn: access.ExpireSecond(a.ExpiresIn),
	}
}

func (a PermanentAccessToken) GetRefAccessToken() access.Value {
	return access.Value{}
}
