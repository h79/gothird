/**
 * 企业微信处理（发消息，服务于报警）
 */
package work

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/goutils/common/http"
)

// MigrateToCustomizedApp 自建应用迁移成代开发自建应用
// 该API可以将企业的自建应用关联到服务商的代开发应用模版上，从而将自建应用转换成代开发应用。测试企业的任意自建应用均可调用该接口；若非测试企业，则仅服务商管理端“历史应用迁移”列表中的应用可调用。
// 请求方式：POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/agent/migrate_to_customized_app?access_token=ACCESS_TOKEN
func MigrateToCustomizedApp(api *token.Api, suiteAccessToken string) error {
	url := fmt.Sprintf("%s/cgi-bin/batch/migrate_to_customized_app?", consts.WorkApiPrefixUrl)

	var body = struct {
		SuiteAccessToken string `json:"suite_access_token"`
	}{
		SuiteAccessToken: suiteAccessToken,
	}
	buf, err := json.Marshal(&body)
	if err != nil {
		return err
	}
	var res = response.Response{}
	err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return err
	}
	return res.ErrorIf()
}

// GetJoinQrcode 获取加入企业二维码
// 支持企业用户获取实时成员加入二维码。
// size_type	否	qrcode尺寸类型，1: 171 x 171; 2: 399 x 399; 3: 741 x 741; 4: 2052 x 2052
// 请求方式：GET（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/corp/get_join_qrcode?access_token=ACCESS_TOKEN&size_type=SIZE_TYPE
func GetJoinQrcode(api *token.Api, sizeType int) (string, error) {
	url := fmt.Sprintf("%s/cgi-bin/corp/get_join_qrcode?size_type=%v&", consts.WorkApiPrefixUrl, sizeType)

	var res = struct {
		response.Response
		Qrcode string `json:"join_qrcode"`
	}{}
	if err := api.Request("GET", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return "", err
	}
	return res.Qrcode, res.ErrorIf()
}
