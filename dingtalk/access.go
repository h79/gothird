package dingtalk

import "gitee.com/h79/gothird/token/access"

type AccessToken struct {
	Token    string `json:"access_token"`
	ExpireIn int64  `json:"expires_in"`
}

func (a AccessToken) GetAccessToken() access.Value {
	return access.Value{
		Data:     a.Token,
		ExpireIn: access.ExpireSecond(a.ExpireIn),
	}
}

func (a AccessToken) GetRefAccessToken() access.Value {
	return access.Value{}
}
