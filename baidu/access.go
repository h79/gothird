package baidu

import "gitee.com/h79/gothird/token/access"

type AccessToken struct {
	AcsToken    string `json:"access_token"`
	RefToken    string `json:"refresh_token"`
	AcsExpireIn int64  `json:"expires_in"`
}

func (a AccessToken) GetAccessToken() access.Value {
	return access.Value{
		Data:     a.AcsToken,
		ExpireIn: access.ExpireSecond(a.AcsExpireIn),
	}
}

func (a AccessToken) GetRefAccessToken() access.Value {
	return access.Value{
		Data:     a.RefToken,
		ExpireIn: access.RefExpireSecond(0),
	}
}
