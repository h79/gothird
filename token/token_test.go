package token

import (
	"gitee.com/h79/gothird/token/validate"
	"gitee.com/h79/goutils/common/data"
	"gitee.com/h79/goutils/common/logger"
	"path/filepath"
	"testing"
	"time"
)

func TestCmdData_String(t *testing.T) {

	cmd := data.D{}

	cmd["name"] = "122"

	t.Log(cmd.String("name"))

	t.Log(filepath.Ext("xxxx.gob"))

	opts := data.Options{
		Data: data.D{"sfsf": "ssss"},
	}
	t.Log(opts)
}

func TestIrToken_Child(t *testing.T) {
	logger.Debug("dddd: %v,xx=%s", "333", "444")

	logger.Debug("time: %s", time.Hour*time.Duration(24*30)-time.Minute*time.Duration(30))

	child := Child{
		VMap: VMap{},
	}
	child.VMap["122"] = &validate.Data{}

	logger.Debug("child: %#v", child)

}
