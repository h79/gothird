package dingtalk

import (
	"gitee.com/h79/gothird/token"
)

type Response struct {
	Code int32  `json:"errcode"`
	Msg  string `json:"errmsg"`
}

func (r Response) ReturnIf(api *token.Api) error {
	if r.Code == 0 {
		return nil
	}
	return token.Result{Code: r.Code, Msg: r.Msg}
}
