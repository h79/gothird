package structs

type ExtToPendingId struct {
	ChatId         string   `json:"chat_id,omitempty"` //群id，如果有传入该参数，则只检查群主是否在可见范围，同时会忽略在该群以外的external_userid。如果不传入该参数，则只检查客户跟进人是否在可见范围内。
	ExternalUserid []string `json:"external_userid"`
}

type ExtToPendingIdResult struct {
	PendingId      string `json:"pending_id"`
	ExternalUserid string `json:"external_userid"`
}

type UnionIdTo struct {
	UnionId        string `json:"unionid"`
	OpenId         string `json:"openid"`
	SubjectType    int    `json:"subject_type"`
	MassCallTicket string `json:"mass_call_ticket,omitempty"` //高频
}

type UnionIdToResult struct {
	ExternalUserId string `json:"external_userid"`
	PendingId      string `json:"pending_id"`
}

type KFIDResult struct {
	Items           []KFIDItem `json:"items"`
	InvalidKfIdList []string   `json:"invalid_open_kfid_list"`
}

type KFIDItem struct {
	TmpExternalUserid string `json:"open_kfid"`
	ExternalUserid    string `json:"new_open_kfid"`
}

type TmpExternalUser struct {
	BusinessType          int      `json:"business_type"`
	UserType              int      `json:"user_type"`
	TmpExternalUseridList []string `json:"tmp_external_userid_list"`
}

// TmpExternalUserResult
// results[].tmp_external_userid	string	输入的tmp_external_userid
// results[].external_userid	string	转换后的userid，user_type为1时返回
// results[].corpid	string	userid对应的corpid，user_type为2、3、4时返回
// results[].userid	string	转换后的userid，user_type为2、3、4时返回
// invalid_tmp_external_userid_list	string[]	无法转换的tmp_external_userid。可能非法或没有权限
type TmpExternalUserResult struct {
	Results []struct {
		TmpExternalUserid string `json:"tmp_external_userid"`
		ExternalUserid    string `json:"external_userid,omitempty"`
		CorpId            string `json:"corpid,omitempty"`
		UserId            string `json:"userid,omitempty"`
	} `json:"results"`
	InvalidTmpExternalUseridList []string `json:"invalid_tmp_external_userid_list"`
}
