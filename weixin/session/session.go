package session

import (
	"encoding/json"
	"gitee.com/h79/gothird/weixin/access"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/goutils/common/http"
)

// 用户授权
// 官司说明 https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140842
// https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/Wechat_webpage_authorization.html
// https://developers.weixin.qq.com/doc/oplatform/Website_App/WeChat_Login/Authorized_Interface_Calling_UnionID.html

type Session struct {
	access.Token
	IsSnapShotUser int    `json:"is_snapshotuser"`  //是否为快照页模式虚拟账号，只有当用户是快照页模式虚拟账号时返回，值为1
	Scope          string `json:"scope,omitempty"`  //用户授权的作用域，使用逗号（,）分隔
	Openid         string `json:"openid,omitempty"` //每个应用有对应的openid
	Unionid        string `json:"unionid"`          //用户统一标识（针对一个微信开放平台账号下的应用，同一用户的 unionid 是唯一的），只有当scope为"snsapi_userinfo"时返回
	AppId          string `json:"internal_appid,omitempty"`
	ParentId       string `json:"internal_parentId,omitempty"`
	SessionId      string `json:"internal_sessionId,omitempty"`
}

func GetToken(appid string, url string) (*Session, error) {
	hp := http.Http{}
	body, err := hp.DoBytes("GET", url, nil)
	if err != nil {
		return &Session{}, err
	}
	/*
	   {
	       "access_token":"ACCESS_TOKEN",
	       "expires_in":7200,
	       "refresh_token":"REFRESH_TOKEN",
	       "openid":"OPENID",
	       "scope":"SCOPE"
	    }
	*/
	type sessionResult struct {
		response.Response
		Session
	}
	res := sessionResult{}
	res.AppId = appid

	if err = json.Unmarshal(body, &res); err != nil {
		return &res.Session, err
	}
	return &res.Session, res.ErrorIf()
}
