package app3

import (
	"gitee.com/h79/gothird/token"
	wxerr "gitee.com/h79/gothird/weixin/errors"
	"gitee.com/h79/goutils/common/data"
	"net/http"
	"strconv"
)

var firstService *acsReq
var app3IdService *acsReq
var app3WebService *acsReq

func init() {
	firstService = &acsReq{
		get:   GetComponentAccessToken,
		ref:   GetComponentAccessToken,
		auth2: token.GetEmpty,
		app3:  AuthorizerGet,
	}
	app3IdService = &acsReq{
		get:   token.GetEmpty,
		ref:   GetAuthorizerAccessToken,
		auth2: WebGetAccessToken,
		app3:  token.GetEmpty,
	}
	app3WebService = &acsReq{
		get:   token.GetEmpty,
		ref:   WebRefreshToken,
		auth2: WebGetAccessToken,
		app3:  token.GetEmpty,
	}
}

func GetService() token.Service {
	return firstService
}

func GetIdService() token.Service {
	return app3IdService
}

func GetWebService() token.Service {
	return app3WebService
}

func NewToken(app token.App) token.Token {
	return token.New(&app, firstService)
}

func NewWebToken(app token.App) token.Token {
	return token.New(&app, app3WebService)
}

func NewIdToken(app token.App) token.Token {
	return token.New(&app, app3IdService)
}

var _ token.Service = (*acsReq)(nil)

type acsReq struct {
	check token.CheckErrorFunc
	get   token.GenFunc
	ref   token.GenFunc
	auth2 token.GenFunc
	app3  token.GenFunc
}

// Execute
// token.Service interface
func (req *acsReq) Execute(tk token.Token, cmd string, d data.D) (interface{}, error) {
	if cmd == token.NTicket {
		return GetTicket(tk, d)
	}
	if cmd == token.NAccessToken {
		return req.get(tk, d)
	}
	if cmd == token.NRefreshToken {
		return req.ref(tk, d)
	}
	if cmd == token.NPreAuthCode {
		api := token.NewApi(tk)
		return GetPreAuthCode(api)
	}
	if cmd == token.NOauth2App3 { //把公众号或小程序 授权给第三方应用获取授权信息
		acs, err := req.app3(tk, d)
		if err != nil {
			return nil, err
		}
		if info, ok := acs.(*AuthorizationInfo); ok && info.Appid != "" {
			id := info.Appid
			ch := &token.Child{
				App: token.App{
					ParentId: tk.GetId(),
					IsCache:  tk.IsCache(),
					Type:     token.WxApp3IdType,
					Id:       token.GenId(tk.GetAppId(), id),
					AppId:    id,
					Name:     "app3:id",
				},
				VMap: token.VMap{},
			}
			token.SyncCreateChild(tk, app3IdService, ch, acs, []string{id}, func() {

			}) //需要把授权的id 保存
		}
		return acs, err
	}
	if cmd == token.NOauth2 {
		acs, err := req.auth2(tk, d)
		if err != nil {
			return nil, err
		}
		return acs, err
	}
	return nil, token.ErrNotSupported
}

func (req *acsReq) BuildUrl(uri string, acsKey string, d data.D) string {
	return token.ComponentUrl(acsKey, uri)
}

func (req *acsReq) SetHead(h *http.Header, acsKey string) {
}

func (req *acsReq) SetCheckError(check token.CheckErrorFunc) {
	req.check = check
}

func (req *acsReq) CheckError(err error) error {
	if res, ok := err.(token.Result); ok {
		if req.check != nil {
			return req.check(&res)
		}
		if wxerr.IsExpired(res.Code) {
			return token.ErrTokenExpired.Clone(strconv.FormatInt(int64(res.Code), 10), res.Msg)
		}
	}
	return err
}
