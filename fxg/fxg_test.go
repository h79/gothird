package fxg

import (
	"fmt"
	"gitee.com/h79/gothird/fxg/access"
	"gitee.com/h79/gothird/fxg/order"
	"gitee.com/h79/gothird/fxg/sign"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/goutils/common/data"
	"gitee.com/h79/goutils/common/json"
	"gitee.com/h79/goutils/common/stringutil"
	"testing"
)

var appid = "7063300499142936071"
var secret = "06774cf8-d7c9-4354-8517-98035ccbb2ea"
var accsToken = "824004b0-611a-477a-b581-ba662460fbad"

func createReq() *token.Api {
	return token.NewApi(nil).WithOptions(token.WithAppId(appid),
		token.WithSecret(secret),
		token.WithToken(accsToken),
		token.WithBuildUrl(func(uri string, acsToken string, d data.D) string {
			return fmt.Sprintf("%s&access_token=%s", uri, acsToken)
		}))
}

func TestOrderSearch(t *testing.T) {

	req := createReq()
	d := data.D{}
	d["update_time_start"] = stringutil.UInt64ToString(1654838167)
	d["size"] = 20
	d["page"] = 0
	res, err := order.SearchList(req, d)
	if err != nil {
		t.Error(err)
		return
	}
	t.Logf("data: %+v", json.ToString(res))
}

func TestOrderDetail(t *testing.T) {
	req := createReq()

	d := data.D{}
	d["shop_order_id"] = "4942734323716519270"
	res, err := order.Details(req, d)
	if err != nil {
		t.Error(err)
		return
	}
	t.Logf("data: %+v", json.ToString(res))

}

func TestTokenCode(t *testing.T) {
	tk := NewToken(token.App{
		Type:   "fxg",
		AppId:  appid,
		Secret: secret,
	})
	d := data.D{}
	d["code"] = "228dd925-c20d-45b6-813a-399579c4d1f0"
	acc, err := access.GetAccessToken(tk, d)
	if err != nil {
		t.Error(err)
		return
	}
	t.Logf("Token: %+v", acc)
}

func TestSign(t *testing.T) {
	// 2243e1f8e1a337156b451280f00558031b5d6a2a462decd0540c7a4d42a694d8
	ti := "2022-06-11 13:16:07" //stringutil.UInt64ToString(1654924567)

	d := data.D{}
	d["size"] = 20
	d["page"] = 0
	params := json.ToString(d)

	s := sign.Sign("7063300499142936071", "06774cf8-d7c9-4354-8517-98035ccbb2ea", "order.searchList", ti, params)

	t.Logf("sign: %s", s)
}
