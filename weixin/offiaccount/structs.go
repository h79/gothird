package offiaccount

// https://developers.weixin.qq.com/doc/oplatform/Website_App/WeChat_Login/Authorized_Interface_Calling_UnionID.html
// https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/Wechat_webpage_authorization.html#3

type User struct {
	OpenId    string   `json:"openid"`               //用户的唯一标识
	UnionId   string   `json:"unionid,omitempty"`    //只有在用户将公众号绑定到微信开放平台账号后，才会出现该字段
	Nick      string   `json:"nickname,omitempty"`   //用户昵称
	ImgUrl    string   `json:"headimgurl,omitempty"` //用户头像，最后一个数值代表正方形头像大小
	Province  string   `json:"province,omitempty"`   //省份
	City      string   `json:"city,omitempty"`       //城市
	Country   string   `json:"country,omitempty"`    //国家
	Privilege []string `json:"privilege,omitempty"`  //用户特权信息，json 数组
	Language  string   `json:"language,omitempty"`   // 语言,zh-CN
	AppId     string   `json:"appid"`
	Sex       int      `json:"sex,omitempty"` //用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
}

type Phone struct {
	User
	//用户绑定的手机号（国外手机号会有区号）
	PhoneNumber string `json:"phoneNumber,omitempty"`

	//没有区号的手机号
	PurePhoneNumber string `json:"purePhoneNumber,omitempty"`

	//区号
	CountryCode string `json:"countryCode,omitempty"`
}

type UserEx struct {
	User
	Subscribe      int     `json:"subscribe"`                 //用户是否订阅该公众号标识，值为0时，代表此用户没有关注该公众号，拉取不到其余信息
	SubscribeTime  int64   `json:"subscribe_time,omitempty"`  //用户关注时间，为时间戳。如果用户曾多次关注，则取最后关注时间
	Remark         string  `json:"remark,omitempty"`          //公众号运营者对粉丝的备注，公众号运营者可在微信公众平台用户管理界面对粉丝添加备注
	GroupId        int64   `json:"groupid,omitempty"`         //用户所在的分组ID（兼容旧的用户分组接口）
	TagIds         []int64 `json:"tagid_list,omitempty"`      //用户被打上的标签ID列表
	SubscribeScene string  `json:"subscribe_scene,omitempty"` //返回用户关注的渠道来源，ADD_SCENE_SEARCH 公众号搜索，ADD_SCENE_ACCOUNT_MIGRATION 公众号迁移，ADD_SCENE_PROFILE_CARD 名片分享，ADD_SCENE_QR_CODE 扫描二维码，ADD_SCENE_PROFILE_LINK 图文页内名称点击，ADD_SCENE_PROFILE_ITEM 图文页右上角菜单，ADD_SCENE_PAID 支付后关注，ADD_SCENE_WECHAT_ADVERTISEMENT 微信广告，ADD_SCENE_REPRINT 他人转载，ADD_SCENE_LIVESTREAM 视频号直播，ADD_SCENE_CHANNELS 视频号，ADD_SCENE_W
	QRSceneId      int32   `json:"qr_scene,omitempty"`        //二维码扫码场景
	QRSceneStr     string  `json:"qr_scene_str,omitempty"`    //二维码扫码场景描述
}

type UserOpenIdList struct {
	Total      int32  `json:"total"`
	Count      int32  `json:"count"`
	Data       OpenId `json:"data"`
	NextOpenId string `json:"next_openid"`
}

type OpenId struct {
	OpenId []string `json:"openid"`
}

type UserReq struct {
	Openid string `json:"openid"`
	Lang   string `json:"lang"`
}

// 所属行业
type Industry struct {
	FirstClass  string `json:"first_class"`
	SecondClass string `json:"second_class"`
}

type IndustryS struct {
	Primary   Industry `json:"primary_industry"`
	Secondary Industry `json:"secondary_industry"`
}

// TemplateInfo 模板信息
type TemplateInfo struct {
	Id              string `json:"template_id"`
	Title           string `json:"title"`
	PrimaryIndustry string `json:"primary_industry"`
	DeputyIndustry  string `json:"deputy_industry"`
	Content         string `json:"content"`
	Example         string `json:"example"`
}

// 二维码
type QRCodeReq struct {
	Expire     int64      `json:"expire_seconds,omitempty"`
	ActionName string     `json:"action_name"`
	ActionInfo ActionInfo `json:"action_info"`
}

type QRCode struct {
	Ticket string `json:"ticket"`
	Expire int64  `json:"expire_seconds"`
	Url    string `json:"url"`
}

type ActionInfo struct {
	Scene Scene `json:"scene"`
}

type Scene struct {
	SceneId  int32  `json:"scene_id,omitempty"`  //场景值ID，临时二维码时为32位非0整型，永久二维码时最大值为100000（目前参数只支持1--100000）
	SceneStr string `json:"scene_str,omitempty"` //场景值ID（字符串形式的ID），字符串类型，长度限制为1到64
}

// 长链接转短链接
type ShortUrlReq struct {
	Action string `json:"action"` //=long2short
	Url    string `json:"long_url"`
}

// 所属行业设置
type IndustryReq struct {
	Id1 string `json:"industry_id1"`
	Id2 string `json:"industry_id2"`
}

type BlackReq struct {
	OpenId string `json:"begin_openid,omitempty"`
}

type BatchReq struct {
	OpenIdList []string `json:"begin_openid,omitempty"`
}

type QrcBuffer struct {
	Type string `json:"contentType"`
	Buff []byte `json:"buffer"`
}
