package work

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/gothird/weixin/work/structs"
	"gitee.com/h79/goutils/common/http"
)

// ACCreate 创建群聊会话
// 权限说明：
// 只允许企业自建应用调用，且应用的可见范围必须是根部门。
// 限制说明：
// 群成员人数不可超过管理端配置的“群成员人数上限”，且最大不可超过2000人（含应用）。
// 每企业创建群数不可超过1000/天。
// 请求方式： POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/appchat/create?access_token=ACCESS_TOKEN
func ACCreate(corp *token.Api, req *structs.AppChatCreateRequest) (string, error) {
	url := fmt.Sprintf("%s/cgi-bin/appchat/create?", consts.WorkApiPrefixUrl)
	buf, _ := json.Marshal(req)
	var res = struct {
		response.Response
		ChatID string `json:"chatid"`
	}{}
	err := corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return "", err
	}
	return res.ChatID, res.ErrorIf()
}

// ACEdit 修改群聊会话
// 权限说明：
// 只允许企业自建应用调用，且应用的可见范围必须是根部门。
// 限制说明：
// chatid所代表的群必须是该应用所创建。
// 群成员人数不可超过2000人。
//
// 每企业变更群的次数不可超过1000次/小时。
// 请求方式： POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/appchat/update?access_token=ACCESS_TOKEN
func ACEdit(corp *token.Api, req *structs.AppChatEditRequest) error {
	url := fmt.Sprintf("%s/cgi-bin/appchat/update?", consts.WorkApiPrefixUrl)
	buf, _ := json.Marshal(req)
	var res = response.Response{}
	err := corp.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return err
	}
	return res.ErrorIf()
}

// ACGet 获取群聊会话
// 权限说明：
// 只允许企业自建应用调用，且应用的可见范围必须是根部门；
// chatid所代表的群必须是该应用所创建；
// 第三方不可调用。
// 请求方式： GET（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/appchat/get?access_token=ACCESS_TOKEN&chatid=CHATID
func ACGet(corp *token.Api, chatId string) (*structs.AppChatInfo, error) {
	url := fmt.Sprintf("%s/cgi-bin/appchat/get?chatid=%s&", consts.WorkApiPrefixUrl, chatId)
	var res = struct {
		response.Response
		Info structs.AppChatInfo `json:"chat_info"`
	}{}
	err := corp.Request("GET", url, nil, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(corp)
	})
	if err != nil {
		return nil, err
	}
	return &res.Info, res.ErrorIf()
}

// ACSendMessage 应用推送消息
// 应用支持推送文本、图片、视频、文件、图文等类型。
// 请求方式： POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/appchat/send?access_token=ACCESS_TOKEN
func ACSendMessage(api *token.Api, msg structs.Message) (structs.MessageResponse, error) {
	url := fmt.Sprintf("%s/cgi-bin/appchat/send?", consts.WorkApiPrefixUrl)
	buf, _ := json.Marshal(msg)
	res := structs.MessageResponse{}
	err := api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		r := response.Response{ErrCode: res.ErrCode, ErrMsg: res.ErrMsg}
		return r.ReturnIf(api)
	})
	if err != nil {
		return structs.MessageResponse{}, err
	}
	r := response.Response{ErrCode: res.ErrCode, ErrMsg: res.ErrMsg}
	return res, r.ErrorIf()
}
