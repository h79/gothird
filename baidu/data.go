package baidu

type Words struct {
	Words string `json:"words"`
}

type WordsResult struct {
	Words    []Words `json:"words_result"`
	WordsLen int     `json:"words_result_num"`
}

type PersonInfo struct {
	Height float64 `json:"height"`
	Width  float64 `json:"width"`
	Left   float64 `json:"left"`
	Top    float64 `json:"top"`
	Score  float64 `json:"score"`
}

type PersonResult struct {
	LabelMap   string       `json:"labelmap"`
	ScoreMap   string       `json:"scoremap"`
	ForeGround string       `json:"foreground"`
	PersonNum  int          `json:"person_num"`
	PersonInfo []PersonInfo `json:"person_info"`
}
