package notify

import (
	"gitee.com/h79/gothird/config"
	"gitee.com/h79/goutils/common/result"
	"github.com/aliyun/alibaba-cloud-sdk-go/sdk"
	"github.com/aliyun/alibaba-cloud-sdk-go/sdk/requests"
)

// https://help.aliyun.com/document_detail/55491.html?spm=a2c4g.11186623.6.658.2b573144YbPBi1
// https://api.aliyun.com/?spm=a2c4g.11186623.2.13.796556e0tRbdOf#/?product=Dysmsapi&api=SendSms
func SendAliyun(conf *config.SmsConfig, phoneNums string, content string) (string, error) {

	client, err := sdk.NewClientWithAccessKey(conf.RegionId, conf.AccessKey, conf.Secret)
	if err != nil {
		return "", result.Error(result.ErrSmsInternal, err.Error())
	}

	if phoneNums == "" {
		phoneNums = conf.PhoneNums
	}
	request := requests.NewCommonRequest()
	request.Method = "POST"
	request.Scheme = "https" // https | http
	request.ApiName = "SendSms"
	request.Version = "2017-05-25"
	request.Domain = conf.Domain
	request.QueryParams["RegionId"] = conf.RegionId
	request.QueryParams["SignName"] = conf.SignName     //短信的签名 签名名称
	request.QueryParams["TemplateCode"] = conf.TempCode //模板号
	request.QueryParams["PhoneNumbers"] = phoneNums
	request.QueryParams["TemplateParam"] = content

	res, err := client.ProcessCommonRequest(request)
	if err != nil {
		return "", result.Error(result.ErrEmailInternal, err.Error())
	}
	return res.GetHttpContentString(), nil
}
