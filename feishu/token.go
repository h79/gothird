package feishu

import (
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/goutils/common/data"
	"mime/multipart"
	"net/http"
	"reflect"
)

var firstService *acsReq

func init() {
	firstService = &acsReq{
		get: GetAccessToken,
	}
}

func GetService() token.Service {
	return firstService
}

func NewToken(app token.App) token.Token {
	return token.New(&app, firstService)
}

var _ token.Service = (*acsReq)(nil)

type acsReq struct {
	check token.CheckErrorFunc
	get   token.GenFunc
}

// Execute token.Service interface
func (req *acsReq) Execute(tk token.Token, cmd string, d data.D) (interface{}, error) {
	if cmd == token.NAccessToken {
		return req.get(tk, d)
	}
	return nil, token.ErrNotSupported
}

func (req *acsReq) BuildUrl(uri string, acsKey string, d data.D) string {
	return uri
}

func (req *acsReq) SetHead(h *http.Header, acsKey string) {
	h.Set("Authorization", "Bearer "+acsKey)
}

func (req *acsReq) CreateForm(w *multipart.Writer, field string, form interface{}) error {
	img, ok := form.(*Image)
	if !ok {
		return token.Result{Code: -1, Msg: "file object not is Image"}
	}
	val := reflect.ValueOf(img)
	if val.CanInterface() && val.Type().Implements(token.FormType) {
		fm := val.Interface().(token.Form)
		return fm.CreateForm(w, field)
	}
	return token.DefaultForm(w, field, img.FileName)
}

func (req *acsReq) SetCheckError(check token.CheckErrorFunc) {
	req.check = check
}

func (req *acsReq) CheckError(err error) error {
	return err
}
