package token

import (
	"encoding/gob"
	"gitee.com/h79/goutils/common/logger"
	"sync"
)

type RealId struct {
	at  updateTime
	mu  sync.Mutex
	ids map[string]string //广告组id 或 个人在公众号的 openid
}

func (rid *RealId) Add(realId []string, id string) {
	rid.mu.Lock()
	defer rid.mu.Unlock()
	for i := range realId {
		if len(realId[i]) == 0 {
			continue
		}
		if r, ok := rid.ids[realId[i]]; ok {
			logger.W("Token", "RealId is existed, id= %s", r)
			continue
		}
		rid.ids[realId[i]] = id
	}
	rid.at.Update()
}

func (rid *RealId) Get(realId string) (string, bool) {
	rid.mu.Lock()
	defer rid.mu.Unlock()
	if r, ok := rid.ids[realId]; ok {
		return r, true
	}
	return "", false
}

func (rid *RealId) Delete(realId string) bool {
	rid.mu.Lock()
	defer rid.mu.Unlock()
	if _, ok := rid.ids[realId]; ok {
		delete(rid.ids, realId)
		rid.at.Update()
		return true
	}
	return false
}

func (rid *RealId) load() {
	rid.mu.Lock()
	defer rid.mu.Unlock()

	if err := loadFile(realIdFileName, func(de *gob.Decoder) error {
		return de.Decode(&rid.ids)
	}); err != nil {
		logger.E("Token", "load realId failure, err= %s", err)
	} else {
		rid.at.Complete()
	}
}

func (rid *RealId) save() {

	rid.mu.Lock()
	defer rid.mu.Unlock()
	if !rid.at.HasUpdate() {
		return
	}
	if err := saveFile(realIdFileName, func(en *gob.Encoder) error {
		return en.Encode(rid.ids)
	}); err != nil {
		logger.E("Token", "save realId failure, err= %s", err)
	} else {
		rid.at.Complete()
	}
}
