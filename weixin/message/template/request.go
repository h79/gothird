package template

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	template2 "gitee.com/h79/gothird/weixin/template"
	"gitee.com/h79/goutils/common/http"
)

// 发送模板消息
// POST https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN
func Send(api *token.Api, req template2.Template) error {

	buf, err := json.Marshal(req)
	if err != nil {
		return err
	}
	return SendBuff(req.IsMini(), api, buf)
}

func SendBuff(mini bool, api *token.Api, buf []byte) error {
	uri := fmt.Sprintf("%s/cgi-bin/message/wxopen/template/send?", consts.ApiPrefixUrl)
	if !mini {
		uri = fmt.Sprintf("%s/cgi-bin/message/template/send?", consts.ApiPrefixUrl)
	}

	type msgResult struct {
		response.Response
		MsgId int64 `json:"msgid"`
	}

	res := msgResult{}
	return api.Request("POST", uri, buf, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})
}
