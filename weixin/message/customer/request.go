package customer

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/goutils/common/http"
)

// 发送小程序客服消息
// POST https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=ACCESS_TOKEN
func Send(api *token.Api, req Customer) error {

	buf, err := json.Marshal(req)
	if err != nil {
		return err
	}
	return SendWithBytes(api, buf)
}

func SendWithBytes(api *token.Api, buf []byte) error {

	uri := fmt.Sprintf("%s/cgi-bin/message/custom/send?", consts.ApiPrefixUrl)
	res := msgResult{}

	return api.Request("POST", uri, buf, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})
}

// 客服输入状态
// POST https://api.weixin.qq.com/cgi-bin/message/custom/typing?access_token=ACCESS_TOKEN
func SendTyping(api *token.Api, req *Typing) error {
	uri := fmt.Sprintf("%s/cgi-bin/message/custom/typing?", consts.ApiPrefixUrl)

	buf, err := json.Marshal(req)
	if err != nil {
		return err
	}
	res := msgResult{}
	return api.Request("POST", uri, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
}

type msgResult struct {
	response.Response
	MsgId int64 `json:"msgid"`
}
