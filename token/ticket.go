package token

import "gitee.com/h79/gothird/token/access"

type Ticket struct {
	Ticket   string `json:"ticket"`
	ExpireIn int64  `json:"expires_in"`
}

func (a Ticket) IsValid() bool {
	return a.Ticket != ""
}

func (a Ticket) GetAccessToken() access.Value {
	return access.Value{
		Data:     a.Ticket,
		ExpireIn: access.ExpireSecond(a.ExpireIn),
	}
}

func (a Ticket) GetRefAccessToken() access.Value {
	return access.Value{}
}
