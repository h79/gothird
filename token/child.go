package token

import (
	"fmt"
	"gitee.com/h79/gothird/token/access"
	"gitee.com/h79/gothird/token/validate"
	"gitee.com/h79/goutils/common/logger"
	"strings"
)

// VMap [string] = access_token, refresh_token, code...
type VMap map[string]*validate.Data

func (vp *VMap) String() string {
	var b strings.Builder
	for k, v := range *vp {
		b.WriteString("\"")
		b.WriteString(k)
		b.WriteString("\":{")
		b.WriteString(v.String())
		b.WriteString("}")
	}
	return b.String()
}

type Child struct {
	App
	VMap VMap `json:"data"`
}

func (c *Child) String() string {
	return fmt.Sprintf("App= %s,VMap = {%s}", c.App.String(), c.VMap.String())
}

type CreateChildFunc func(tk Token, sev Service, ch *Child, acsToken access.Token) (Token, error)

func CreateChildEmpty(tk Token, sev Service, ch *Child, acsToken access.Token) (Token, error) {
	logger.W("Token", "CreateChildEmpty, will return 'ErrNotSupported' for app= %s", tk.String())
	return nil, ErrNotSupported
}

func CreateChild(parent Token, service Service, ch *Child, acsToken access.Token) (Token, error) {
	logger.D("Token", "create child, info= %s", ch.String())
	tk, err := Mgr().CreateChild(parent, ch, service)
	if err != nil {
		return nil, err
	}
	tk.SetAccessTokenWithAcs(acsToken)
	return tk, nil
}

func SyncCreateChild(parent Token, service Service, ch *Child, acsToken access.Token, realId []string, syncFunc func()) {
	go func() {
		if syncFunc != nil {
			syncFunc()
		}
		if _, err := CreateChild(parent, service, ch, acsToken); err != nil {
			logger.E("Token", "create child failure, parentId= %v, child= %s , err= %+v", parent.GetId(), ch.String(), err)
		}
		if len(realId) == 0 {
			return
		}
		if err := Mgr().AddRealId(realId, ch.Id, ch.Type); err != nil {
			logger.E("Token", "add alias failure, realId= %+v, aliasId= %v, err= %+v", realId, ch.Id, err)
		}
	}()
}
