package response

import (
	"gitee.com/h79/gothird/fxg/errors"
	"gitee.com/h79/gothird/token"
	"strconv"
)

type Response struct {
	ErrNo   int32  `json:"err_no,omitempty"`
	Message string `json:"message"`
	Code    int32  `json:"code"`
	Msg     string `json:"msg,omitempty"`
	SubCode string `json:"sub_code,omitempty"`
	SubMsg  string `json:"sub_msg,omitempty"`
	LogId   string `json:"log_id,omitempty"`
}

func (r Response) ErrorIf() error {
	if r.Code == errors.Success {
		return nil
	}
	if errors.ScTokenExpire == r.SubCode {
		return token.ErrTokenExpired.Clone(strconv.FormatInt(int64(r.Code), 10), r.Message)
	}
	if errors.InvalidAccessToken == r.Code {
		return token.ErrTokenInvalid.Clone(strconv.FormatInt(int64(r.Code), 10), r.Message)
	}
	return token.Result{Code: r.Code, SubCode: r.SubCode, Msg: r.Msg, Raw: r}
}

func (r Response) ReturnIf(api *token.Api) error {
	if r.Code == errors.Success {
		return nil
	}
	if errors.ScTokenExpire == r.SubCode ||
		errors.InvalidAccessToken == r.Code {
		api.ResetToken()
		return token.TryCountError()
	}
	return token.Result{Code: r.Code, SubCode: r.SubCode, Msg: r.Msg, Raw: r}
}
