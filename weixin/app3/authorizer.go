package app3

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/goutils/common/http"
	"gitee.com/h79/goutils/common/logger"
)

// 1
//https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/api/pre_auth_code.html

// 2 然后去授权，授权成功后，会调用回调函数通知相关相信
//https://mp.weixin.qq.com/cgi-bin/componentloginpage?component_appid={$this->component_appid}&pre_auth_code={$this->pre_auth_code}&redirect_uri={$redirect_uri}

// GetPreAuthCode 预授权码（pre_auth_code）是第三方平台方实现授权托管的必备信息，每个预授权码有效期为 10 分钟。需要先获取令牌才能调用。
func GetPreAuthCode(api *token.Api) (*PreAuthCode, error) {
	url := fmt.Sprintf("%s/cgi-bin/component/api_create_preauthcode?", consts.ApiPrefixUrl)

	var in = struct {
		AppId string `json:"component_appid"`
	}{
		AppId: api.AppId(),
	}
	buf, err := json.Marshal(&in)
	if err != nil {
		return nil, err
	}
	type Result struct {
		response.Response
		PreAuthCode
	}
	res := Result{}
	err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	if res.ErrCode != 0 {
		return nil, res
	}
	return &res.PreAuthCode, nil
}

// GetAuthorizerInfo 获取授权方的帐号基本信息
// https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/api/api_get_authorizer_info.html
func GetAuthorizerInfo(api *token.Api, authorizerAppId string) (*AuthorizerInfo, *AuthorizationInfo, error) {

	url := fmt.Sprintf("%s/cgi-bin/component/api_get_authorizer_info?", consts.ApiPrefixUrl)

	var in = struct {
		AppId           string `json:"component_appid"`
		AuthorizerAppId string `json:"authorizer_appid"`
	}{
		AppId:           api.AppId(),
		AuthorizerAppId: authorizerAppId,
	}
	buf, err := json.Marshal(&in)
	if err != nil {
		return nil, nil, err
	}
	type Result struct {
		response.Response
		AuthorizerInfo    `json:"authorizer_info"`
		AuthorizationInfo `json:"authorization_info"`
	}
	res := Result{}

	err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, nil, err
	}
	logger.Debug("App3 GetAuthorizerInfo: %v", res)
	if res.ErrCode != 0 {
		return nil, nil, res
	}
	return &res.AuthorizerInfo, &res.AuthorizationInfo, nil
}

func GetAuthorizerList(api *token.Api, offset, count int) (*ListAuthInfo, error) {

	url := fmt.Sprintf("%s/cgi-bin/component/api_get_authorizer_list?", consts.ApiPrefixUrl)

	type info struct {
		AppId  string `json:"component_appid"`
		Offset int    `json:"offset"`
		Count  int    `json:"count"`
	}
	in := info{
		AppId:  api.AppId(),
		Offset: offset,
		Count:  count,
	}
	buf, err := json.Marshal(&in)
	if err != nil {
		return nil, err
	}

	type Result struct {
		response.Response
		ListAuthInfo
	}
	res := Result{}
	err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	logger.Debug("App3 GetAuthorizerList: %+v", res)
	if res.ErrCode != 0 {
		return nil, res
	}
	return &res.ListAuthInfo, nil
}
