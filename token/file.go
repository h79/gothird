package token

import (
	"encoding/gob"
	"gitee.com/h79/goutils/common/algorithm"
	"gitee.com/h79/goutils/common/logger"
	"io"
	"mime/multipart"
	"os"
	"path/filepath"
)

func DefaultForm(w *multipart.Writer, field, filename string) error {

	writer, err := w.CreateFormFile(field, filepath.Base(filename))
	if err != nil {
		return Error(-1, err.Error())
	}

	fh, err := os.Open(filename)
	if err != nil {
		return Error(-1, err.Error())
	}
	defer fh.Close()

	_, err = io.Copy(writer, fh)

	return err
}

func saveFile(filename string, writer func(en *gob.Encoder) error) error {

	file, er := os.Create(filename)
	if er != nil {
		return er
	}
	defer file.Close()

	encoder := gob.NewEncoder(file)

	return writer(encoder)
}

func loadFile(filename string, reader func(de *gob.Decoder) error) error {

	file, er := os.Open(filename)
	if er != nil {
		return er
	}
	defer file.Close()

	de := gob.NewDecoder(file)

	return reader(de)
}

func deleteFile(filename string) {
	if err := os.Remove(filename); err != nil {
		logger.Debug("Token: delete path failure,err= %v", err)
	}
}

func checkPath(path string) error {
	if len(path) == 0 {
		return ErrParam
	}
	if err := os.MkdirAll(path, os.ModePerm); err != nil {
		logger.Error("Token: mkdir the alias path failure, err= %v", err)
		return err
	}
	return nil
}

func fileId(id string) string {
	return algorithm.Md5String(id)
}
