package errors

// https://op.jinritemai.com/docs/guide-docs/161/1427

const (
	Success            = 10000
	InvalidAccessToken = 50001
)

const (
	ScTokenExpire  = "dop.token-generate-failed:token-expired"
	ScTokenNoExist = "dop.token-generate-failed:token-invalid"
)
