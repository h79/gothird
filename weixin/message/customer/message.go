package customer

type Head struct {
	ToUser string         `json:"touser"`
	Type   string         `json:"msgtype"`
	Custom *CustomService `json:"customservice,omitempty"`
}

// customer.Customer interface
func (h *Head) SetToUser(user string) {
	h.ToUser = user
}

func (h *Head) SetCustom(custom *CustomService) {
	h.Custom = custom
}

//文本消息
//{
//"touser": "openid",
//"msgtype": "text",
//"text": {
//"content": "Holiday Request For Pony(http://xxxxx)"
//},
//}
type TextMsg struct {
	Head
	Text Content `json:"text"`
}

func NewTextMsg(to string, content string) *TextMsg {
	return &TextMsg{
		Head: Head{
			ToUser: to,
			Type:   "text",
		},
		Text: Content{
			Content: content,
		},
	}
}

//图片消息
type ImageMsg struct {
	Head
	Image Image `json:"image"`
}

func NewImageMsg(to string, mediaId string) *ImageMsg {
	return &ImageMsg{
		Head: Head{
			ToUser: to,
			Type:   "image",
		},
		Image: Image{
			MediaId: mediaId,
		},
	}
}

//语音消息
type VoiceMsg struct {
	Head
	Voice Voice `json:"voice"`
}

func NewVoiceMsg(to string, mediaId string) *VoiceMsg {
	return &VoiceMsg{
		Head: Head{
			ToUser: to,
			Type:   "voice",
		},
		Voice: Voice{
			MediaId: mediaId,
		},
	}
}

//视频消息
type VideoMsg struct {
	Head
	Video Video `json:"video"`
}

func NewVideoMsg(to string, video Video) *VideoMsg {
	return &VideoMsg{
		Head: Head{
			ToUser: to,
			Type:   "voice",
		},
		Video: video,
	}
}

//{
//"touser":"OPENID",
//"msgtype":"miniprogrampage",
//"miniprogrampage": {
//"title":"title",
//"pagepath":"pagepath",
//"thumb_media_id":"thumb_media_id"
//}
//}
type ProgramMsg struct {
	Head
	Mp Program `json:"miniprogrampage"`
}

//{
//"touser": "OPENID",
//"msgtype": "link",
//"link": {
//"title": "Happy Day",
//"description": "Is Really A Happy Day",
//"url": "URL",
//"thumb_url": "THUMB_URL"
//}
type LinkMsg struct {
	Head
	Link Link `json:"link"`
}

type MusicMsg struct {
	Head
	Music Music `json:"music"`
}

type Music struct {
	Title      string `json:"title"`
	Desc       string `json:"description"`
	MusicUrl   string `json:"musicurl"`
	HQMusicUrl string `json:"hqmusicurl"`
	MediaId    string `json:"thumb_media_id,omitempty"`
}

type Link struct {
	Title  string `json:"title"`
	Picurl string `json:"thumb_url"`
	Desc   string `json:"description"`
	Url    string `json:"url"`
}

type Program struct {
	Title   string `json:"title"`                    //"title": "消息标题",
	AppId   string `json:"appid"`                    //"appid": "wx8bd80126147df384",
	Page    string `json:"pagepath"`                 //"page": "/path/index"
	MediaId string `json:"thumb_media_id,omitempty"` //"thumb_media_id": "MEDIA_ID",
}

type Content struct {
	Content string `json:"content"`
}

type Image struct {
	MediaId string `json:"media_id"`
}

type Voice struct {
	MediaId string `json:"media_id"`
}

type Video struct {
	MediaId  string `json:"media_id"`
	Title    string `json:"title"`
	Desc     string `json:"description"`
	ThumbMId string `json:"thumb_media_id,omitempty"`
}

type CustomService struct {
	Account string `json:"kf_account"`
}

type Typing struct {
	ToUser  string         `json:"touser"`
	Command string         `json:"command"` //"Typing","CancelTyping"
	Custom  *CustomService `json:"customservice,omitempty"`
}
