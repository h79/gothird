package baidu

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/goutils/common/data"
	"gitee.com/h79/goutils/common/http"
)

// GeneralBasic 通用文字识别（标准版）
// https://ai.baidu.com/ai-doc/OCR/zk3h7xz52
func GeneralBasic(api *token.Api, d data.D) (*WordsResult, error) {

	uri := fmt.Sprintf("%s/rest/2.0/ocr/v1/general_basic?", ApiPrefixUrl)

	buf := []byte(d.ToFormUrl().Encode())

	type result struct {
		Response2
		WordsResult
	}
	res := result{}
	err := api.Request("POST", uri, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	return &res.WordsResult, err
}

// AccurateBasic 通用文字识别（高精度版）
// https://ai.baidu.com/ai-doc/OCR/1k3h7y3db
func AccurateBasic(api *token.Api, d data.D) (*WordsResult, error) {

	uri := fmt.Sprintf("%s/rest/2.0/ocr/v1/accurate_basic?", ApiPrefixUrl)

	buf := []byte(d.ToFormUrl().Encode())

	type result struct {
		Response2
		WordsResult
	}
	res := result{}
	err := api.Request("POST", uri, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	return &res.WordsResult, err
}
