package media

import (
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/goutils/common"
	"gitee.com/h79/goutils/common/stringutil"
	"mime/multipart"
	"path"
	"path/filepath"
	"strings"
)

type ID struct {
	Type      string      `json:"type,omitempty"`
	MediaId   string      `json:"media_id"`
	CreatedAt interface{} `json:"created_at,omitempty"` //企业微信为字符串，公众号为int64
}

func (i *ID) StringCreateAt() string {
	return stringutil.ToString(i.CreatedAt)
}

func (i *ID) Int64CreateAt() int64 {
	return common.ToInt64(i.CreatedAt)
}

type News struct {
	Title              string `json:"title"`
	ThumbMediaId       string `json:"thumb_media_id"`
	Author             string `json:"author"`
	Digest             string `json:"digest"`
	ShowCoverPic       int8   `json:"show_cover_pic"`
	Content            string `json:"content"`
	ContentSourceUrl   string `json:"content_source_url"`
	NeedOpenComment    int8   `json:"need_open_comment"`
	OnlyFansCanComment int8   `json:"only_fans_can_comment"`
}

type Articles struct {
	Articles []News `json:"articles"`
}

type NewsResult struct {
	Title              string `json:"title"`
	ThumbMediaId       string `json:"thumb_media_id"`
	Author             string `json:"author"`
	Digest             string `json:"digest"`
	ShowCoverPic       int8   `json:"show_cover_pic"`
	Content            string `json:"content"`
	Url                string `json:"url"`
	Description        string `json:"description"`
	DownUrl            string `json:"down_url"`
	ContentSourceUrl   string `json:"content_source_url"`
	NeedOpenComment    int8   `json:"need_open_comment"`
	OnlyFansCanComment int8   `json:"only_fans_can_comment"`
}

type ArticlesResult struct {
	Items []NewsResult `json:"news_item"`
	FileName
}

type FileName struct {
	Raw  []byte // 其他类型的素材消息，则响应的直接为素材的内容，开发者可以自行保存为文件
	File string
}

type Image struct {
	Field    string
	FileName string
}

func (u *Image) CreateForm(w *multipart.Writer, field string) error {
	if u.Field != "" {
		field = u.Field
	}
	if field == "" {
		//获取文件
		field = filepath.Base(u.FileName)
		fileSuffix := path.Ext(field)
		field = strings.TrimSuffix(field, fileSuffix)
	}
	return token.DefaultForm(w, field, u.FileName)
}

type BigFile struct {
	Scene    int    `json:"scene"`
	Type     string `json:"type"`
	Filename string `json:"filename"`
	Url      string `json:"url"`
	Md5      string `json:"md5"`
}

type BigJob struct {
	Status int `json:"status"`
	Detail struct {
		Errcode   int    `json:"errcode"`
		Errmsg    string `json:"errmsg"`
		MediaId   string `json:"media_id"`
		CreatedAt string `json:"created_at"`
	} `json:"detail"`
}
