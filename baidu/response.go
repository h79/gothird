package baidu

import (
	"gitee.com/h79/gothird/token"
)

type Response struct {
	Code int32  `json:"error"`
	Msg  string `json:"error_description"`
}

func (r Response) ErrorIf() error {
	if r.Code == 0 {
		return nil
	}
	return token.Result{Code: r.Code, Msg: r.Msg}
}

func (r Response) ReturnIf(api *token.Api) error {
	if r.Code == 0 {
		return nil
	}
	return token.Result{Code: r.Code, Msg: r.Msg}
}

type Response2 struct {
	Code int32  `json:"error_code"`
	Msg  string `json:"error_msg"`
}

func (r Response2) ErrorIf() error {
	if r.Code == 0 {
		return nil
	}
	return token.Result{Code: r.Code, Msg: r.Msg}
}

func (r Response2) ReturnIf(api *token.Api) error {
	if r.Code == 0 {
		return nil
	}
	return token.Result{Code: r.Code, Msg: r.Msg}
}
