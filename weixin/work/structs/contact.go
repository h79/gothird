package structs

// ExternalRemark
// remark_company只在此外部联系人为微信用户时有效。
// remark，description，remark_company，remark_mobiles和remark_pic_mediaid不可同时为空。
// 如果填写了remark_mobiles，将会覆盖旧的备注手机号。
// 如果要清除所有备注手机号,请在remark_mobiles填写一个空字符串("")。
// remark_pic_mediaid可以通过素材管理接口获得。
type ExternalRemark struct {
	Userid           string      `json:"userid"`
	ExternalUserid   string      `json:"external_userid"`
	Remark           string      `json:"remark"`
	Description      string      `json:"description"`
	RemarkCompany    string      `json:"remark_company"`
	RemarkPicMediaId string      `json:"remark_pic_mediaid"`
	RemarkMobiles    interface{} `json:"remark_mobiles"` //正常是一个 []string, 如果要清除所有备注手机号,请在remark_mobiles填写一个空字符串("")
}

type ExternalContact struct {
	ExternalUserid  string           `json:"external_userid"`
	Name            string           `json:"name"`
	Position        string           `json:"position"`
	Avatar          string           `json:"avatar"`
	CorpName        string           `json:"corp_name"`
	CorpFullName    string           `json:"corp_full_name"`
	UnionId         string           `json:"unionid"`
	Type            int              `json:"type"`
	Gender          int              `json:"gender"`
	ExternalProfile *ExternalProfile `json:"external_profile,omitempty"`
}

type ExternalContactDetail struct {
	ExternalContact ExternalContact `json:"external_contact"`
	FollowInfo      *FollowUser     `json:"follow_info,omitempty"` //for batch
	FollowUser      []FollowUser    `json:"follow_user,omitempty"` // for detail
}

type ExternalUserIdInfo struct {
	CorpId         string `json:"corpid"`
	ExternalUserid string `json:"external_userid"`
}

type FollowUserTagGroup struct {
	GroupId    string `json:"group_id"`
	GroupName  string `json:"group_name"`
	Order      int    `json:"order"`
	CreateTime int64  `json:"create_time"`
	Deleted    bool   `json:"deleted"`
}

type FollowUserTag struct {
	TagId      string `json:"tag_id"`
	TagName    string `json:"tag_name"`
	GroupId    string `json:"group_id"`
	GroupName  string `json:"group_name"`
	Type       int    `json:"type"`
	Order      int    `json:"order"`       // 内部参数
	CreateTime int64  `json:"create_time"` // 内部参数
	Deleted    bool   `json:"deleted"`     // 内部参数
}

type FollowUser struct {
	Userid         string          `json:"userid"` //添加了此外部联系人的企业成员userid
	Remark         string          `json:"remark"` //该成员对此外部联系人的备注
	Description    string          `json:"description"`
	RemarkCorpName string          `json:"remark_corp_name,omitempty"` //该成员对此微信客户备注的企业名称（仅微信客户有该字段）
	OperUserid     string          `json:"oper_userid"`                //发起添加的userid，如果成员主动添加，为成员的userid；如果是客户主动添加，则为客户的外部联系人userid；如果是内部成员共享/管理员分配，则为对应的成员/管理员userid
	State          string          `json:"state,omitempty"`
	Tags           []FollowUserTag `json:"tags,omitempty"`
	TagIds         []string        `json:"tag_id,omitempty"`
	RemarkMobiles  []string        `json:"remark_mobiles,omitempty"` //该成员对此客户备注的手机号码，代开发自建应用需要管理员授权才可以获取，第三方不可获取，上游企业不可获取下游企业客户该字段
	CreateTime     int64           `json:"createtime"`
	AddWay         int             `json:"add_way"` //该成员添加此客户的来源
	WechatChannels *WechatChannels `json:"wechat_channels,omitempty"`
}

// add_way表示添加客户的来源，有固定的值，而state表示此客户的渠道，可以由企业进行自定义的配置，请注意二者的不同。
//
// 值	含义
// 0	未知来源
// 1	扫描二维码
// 2	搜索手机号
// 3	名片分享
// 4	群聊
// 5	手机通讯录
// 6	微信联系人
// 8	安装第三方应用时自动添加的客服人员
// 9	搜索邮箱
// 10	视频号添加
// 11	通过日程参与人添加
// 12	通过会议参与人添加
// 13	添加微信好友对应的企业微信
// 14	通过智慧硬件专属客服添加
// 15	通过上门服务客服添加
// 201	内部成员共享
// 202	管理员/负责人分配

type ExternalContactTagGroup struct {
	GroupId    string                        `json:"group_id"`
	GroupName  string                        `json:"group_name"`
	CreateTime int64                         `json:"create_time"`
	Order      int                           `json:"order"`
	StrategyId int32                         `json:"strategy_id,omitempty"`
	Deleted    bool                          `json:"deleted"`
	Tag        []ExternalContactTagGroupItem `json:"tag"`
}

type ExternalContactTagGroupItem struct {
	Id         string `json:"id"`
	Name       string `json:"name"`
	CreateTime int64  `json:"create_time"`
	Order      int    `json:"order"`
	Deleted    bool   `json:"deleted"`
}

type ExternalContactAddTagReq struct {
	GroupId   string                   `json:"group_id,omitempty"`
	GroupName string                   `json:"group_name,omitempty"`
	Order     int                      `json:"order,omitempty"`
	AgentId   int32                    `json:"agentid,omitempty"` //授权方安装的应用agentid。仅旧的第三方多应用套件需要填此参数
	Tag       []ExternalContactTagInfo `json:"tag"`
}

type ExternalContactAddStrategyTagReq struct {
	ExternalContactAddTagReq
	StrategyId int32 `json:"strategy_id"`
}

type ExternalContactTagInfo struct {
	Name  string `json:"name"`
	Order int    `json:"order,omitempty"`
}

type ExternalContactEditTagReq struct {
	Id      string `json:"id"`
	Name    string `json:"name"`
	Order   int    `json:"order"`
	AgentId int32  `json:"agentid,omitempty"` //授权方安装的应用agentid。仅旧的第三方多应用套件需要填此参数
}

type ExternalContactMarkTagReq struct {
	Userid         string   `json:"userid"`
	ExternalUserid string   `json:"external_userid"`
	AddTag         []string `json:"add_tag,omitempty"`
	RemoveTag      []string `json:"remove_tag,omitempty"`
}

type ExternalContactTagId struct {
	TagId   []string `json:"tag_id,omitempty"`
	GroupId []string `json:"group_id,omitempty"`
}

type ExternalGroupListReq struct {
	OwnerFilter  *OwnerFilter `json:"owner_filter,omitempty"`
	Cursor       string       `json:"cursor,omitempty"`
	Limit        int          `json:"limit"`
	StatusFilter int          `json:"status_filter"`
}

type ExternalTagIDResult struct {
	Items            []TagItem `json:"items"`
	InvalidTagIdList []string  `json:"invalid_external_tagid_list"`
}

type TagItem struct {
	TmpExternalUserid string `json:"external_tagid"`
	ExternalUserid    string `json:"open_external_tagid"`
}
