package structs

type AppChatCreateRequest struct {
	Name     string   `json:"name"`     //群聊名，最多50个utf8字符，超过将截断
	Owner    string   `json:"owner"`    //指定群主的id。如果不指定，系统会随机从userlist中选一人作为群主
	UserList []string `json:"userlist"` //群成员id列表。至少2人，至多2000人
	ChatId   string   `json:"chatid"`   //群聊的唯一标志，不能与已有的群重复；字符串类型，最长32个字符。只允许字符0-9及字母a-zA-Z。如果不填，系统会随机生成群id
}

type AppChatEditRequest struct {
	ChatId      string   `json:"chatid"`
	Name        string   `json:"name"`
	Owner       string   `json:"owner"`
	AddUserList []string `json:"add_user_list"`
	DelUserList []string `json:"del_user_list"`
}

type AppChatInfo struct {
	ChatId   string   `json:"chatid"`
	Name     string   `json:"name"`
	Owner    string   `json:"owner"`
	UserList []string `json:"userlist"`
	ChatType int      `json:"chat_type"`
}
