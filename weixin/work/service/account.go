package service

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/goutils/common/http"
)

// LICActiveAccount 激活帐号
// 下单购买帐号并支付完成之后，先调用获取订单中的帐号列表接口获取到帐号激活码，然后可以调用该接口将激活码绑定到某个企业员工，以对其激活相应的平台服务能力。
// 1. 一个userid允许激活一个基础帐号以及一个互通帐号。
// 2. 若userid已激活，使用同类型的激活码来激活后，则绑定关系变为新激活码，新激活码有效时长自动叠加上旧激活码剩余时长，同时旧激活码失效。
// 3. 为了避免服务商调用出错，多个同类型的激活码累加后的有效期不可超过5年，否则接口报错701030。
// 4.为了避免服务商调用出错，只有当旧的激活码的剩余使用小于等于20天，才可以使用新的同类型的激活码进行激活并续期
// 请求方式： POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/license/active_account?provider_access_token=ACCESS_TOKEN
func LICActiveAccount(api *token.Api, corpId string, active ActiveCode) error {
	url := fmt.Sprintf("%s/cgi-bin/license/active_account?", consts.WorkApiPrefixUrl)
	var req = struct {
		CorpId string `json:"corpid"`
		ActiveCode
	}{
		ActiveCode: active,
		CorpId:     corpId,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return err
	}
	var res = response.Response{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return err
	}
	return res.ErrorIf()
}

// LICBatchActiveAccount 批量激活帐号
// 可在一次请求里为一个企业的多个成员激活许可帐号，便于服务商批量化处理。
// 1.一个userid允许激活一个基础帐号以及一个互通帐号。
// 2. 若userid已激活，使用同类型的激活码来激活后，则绑定关系变为新激活码，新激活码有效时长自动叠加上旧激活码剩余时长，同时旧激活码失效。
// 3. 为了避免服务商调用出错，多个同类型的激活码累加后的有效期不可超过5年，否则接口报错701030。
// 4.为了避免服务商调用出错，只有当旧的激活码的剩余使用小于等于20天，才可以使用新的同类型的激活码进行激活并续期
// 5. 单次激活的员工数量不超过1000
// 请求方式： POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/license/batch_active_account?provider_access_token=ACCESS_TOKEN
func LICBatchActiveAccount(api *token.Api, corpId string, codes []ActiveCode) ([]ActiveCodeResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/license/batch_active_account?", consts.WorkApiPrefixUrl)
	var req = struct {
		CorpId  string       `json:"corpid"`
		Actives []ActiveCode `json:"active_list"`
	}{
		Actives: codes,
		CorpId:  corpId,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		ActiveResult []ActiveCodeResult `json:"active_result"`
	}{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return nil, err
	}
	return res.ActiveResult, res.ErrorIf()
}

// LICActiveCodeDetail 获取激活码详情
// 查询某个帐号激活码的状态以及激活绑定情况。
// 请求方式： POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/license/get_active_info_by_code?provider_access_token=ACCESS_TOKEN
func LICActiveCodeDetail(api *token.Api, corpId, activeCode string) (ActiveCodeDetail, error) {
	url := fmt.Sprintf("%s/cgi-bin/license/get_active_info_by_code?", consts.WorkApiPrefixUrl)
	var req = struct {
		CorpId string `json:"corpid"`
		Code   string `json:"active_code"`
	}{
		Code:   activeCode,
		CorpId: corpId,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return ActiveCodeDetail{}, err
	}
	var res = struct {
		response.Response
		Info ActiveCodeDetail `json:"active_info"`
	}{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return res.Info, err
	}
	return res.Info, res.ErrorIf()
}

// LICBatchActiveCodeDetail 批量获取激活码详情
// 批量查询帐号激活码的状态以及激活绑定情况。
// 请求方式： POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/license/batch_get_active_info_by_code?provider_access_token=ACCESS_TOKEN
func LICBatchActiveCodeDetail(api *token.Api, corpId string, codes []string) (ActiveCodeDetailResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/license/batch_get_active_info_by_code?", consts.WorkApiPrefixUrl)
	var req = struct {
		CorpId string   `json:"corpid"`
		Code   []string `json:"active_code_list"`
	}{
		Code:   codes,
		CorpId: corpId,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return ActiveCodeDetailResult{}, err
	}
	var res = struct {
		response.Response
		ActiveCodeDetailResult
	}{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return res.ActiveCodeDetailResult, err
	}
	return res.ActiveCodeDetailResult, res.ErrorIf()
}

// 获取企业的帐号列表
// 最后更新：2023/04/27
// 查询指定企业下的平台能力服务帐号列表。
// 若为上下游场景，corpid指定的为上游企业，仅返回上游企业激活的账号；若corpid指定为下游企业，若激活码为上游企业分享过来的且已绑定，也会返回。
// 请求方式： POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/license/list_actived_account?provider_access_token=ACCESS_TOKEN
func LICActivatedAccountList(api *token.Api, corpId string, cursor Cursor) (ActivatedAccountResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/license/list_actived_account?", consts.WorkApiPrefixUrl)
	var req = struct {
		CorpId string `json:"corpid"`
		Limit  int    `json:"limit"`
		Cursor string `json:"cursor,omitempty"`
	}{
		CorpId: corpId,
		Limit:  cursor.Limit,
		Cursor: cursor.Cursor,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return ActivatedAccountResult{}, err
	}
	var res = struct {
		response.Response
		ActivatedAccountResult
	}{}
	err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return res.ActivatedAccountResult, err
	}
	return res.ActivatedAccountResult, res.ErrorIf()
}

// LICGetActiveByUser 获取成员的激活详情
// 查询某个企业成员的激活情况。
// 请求方式： POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/license/get_active_info_by_user?provider_access_token=ACCESS_TOKEN
// return ActiveAccountDetailResult
func LICGetActiveByUser(api *token.Api, corpId, userId string) (ActiveAccountDetailResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/license/get_active_info_by_user?", consts.WorkApiPrefixUrl)
	var req = struct {
		CorpId string `json:"corpid"`
		UserId string `json:"userid"`
	}{
		UserId: userId,
		CorpId: corpId,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return ActiveAccountDetailResult{}, err
	}
	var res = struct {
		response.Response
		ActiveAccountDetailResult
	}{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return res.ActiveAccountDetailResult, err
	}
	return res.ActiveAccountDetailResult, res.ErrorIf()
}

// LICBatchTransfer 帐号继承
// 在企业员工离职或者工作范围的有变更时，允许将其许可帐号继承给其他员工。
// 调用限制：
// 转移成员和接收成员属于同一个企业
// 转移成员的帐号已激活，且在有效期
// 转移许可的成员为离职成员，或不在服务商应用的可见范围内时，不限制下次转移的时间间隔
// 转移许可的成员为在职成员且在服务商应用的可见范围内时，转移后30天后才可进行下次转移
// 接收成员许可不能与转移成员的许可重叠（同时拥有基础帐号或者互通帐号）
// 单次转移的帐号数限制在1000以内
// 请求方式： POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/license/batch_transfer_license?provider_access_token=ACCESS_TOKEN
// return []LicenseTransferResult
func LICBatchTransfer(api *token.Api, corpId string, transfer []LicenseTransfer) ([]LicenseTransferResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/license/batch_transfer_license?", consts.WorkApiPrefixUrl)
	var req = struct {
		CorpId string            `json:"corpid"`
		List   []LicenseTransfer `json:"transfer_list"`
	}{
		List:   transfer,
		CorpId: corpId,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		TransferResult []LicenseTransferResult `json:"transfer_result"`
	}{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return nil, err
	}
	return res.TransferResult, res.ErrorIf()
}

// LICBatchShareActiveCode 分配激活码给下游企业
// 服务商可调用该接口将为上游企业购买的激活码分配给下游企业使用。
// (1) 上游企业有共享该服务商的应用给下游企业，应用包括第三方应用和代开发应用
// (2) 分配给下游企业的激活码，当前未激活，且属于上游企业的，且未分配给其他下游企业
// (3) 分配的激活码帐号数的上限，单次分享激活码不可超过1000个, 不能超过下游企业在上下游通讯录中人数上限的两倍
// 请求方式： POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/license/batch_share_active_code?provider_access_token=ACCESS_TOKEN
func LICBatchShareActiveCode(api *token.Api, share *LicenseShareActiveCode) ([]LicenseShareResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/license/batch_share_active_code?", consts.WorkApiPrefixUrl)

	buf, err := json.Marshal(share)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		ShareResult []LicenseShareResult `json:"share_result"`
	}{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return nil, err
	}
	return res.ShareResult, res.ErrorIf()
}

// LICGetAppInfo 获取应用的接口许可状态
// 服务商可获取某个授权企业的应用接口许可试用期，免费试用期为企业首次安装应用后的90天。
// appid (旧的多应用套件中的应用id，新开发者请忽略)
// 请求方式： POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/license/get_app_license_info?provider_access_token=ACCESS_TOKEN
func LICGetAppInfo(api *token.Api, corpId, suiteId, appid string) (AppLicense, error) {
	url := fmt.Sprintf("%s/cgi-bin/license/get_app_license_info?", consts.WorkApiPrefixUrl)
	var req = struct {
		CorpId  string `json:"corpid"`
		SuiteId string `json:"suite_id"`
		AppId   string `json:"appid,omitempty"`
	}{
		SuiteId: suiteId,
		CorpId:  corpId,
		AppId:   appid,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return AppLicense{}, err
	}
	var res = struct {
		response.Response
		AppLicense
	}{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return res.AppLicense, err
	}
	return res.AppLicense, res.ErrorIf()
}

// LICSetAutoActiveStatus 设置企业的许可自动激活状态
// 设置企业的许可自动激活状态
// 服务商可以调用该接口设置授权企业的许可自动激活状态。设置为自动激活后，对应授权企业的员工使用服务商应用时，接口许可表现为自动激活。
// 请求方式： POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/license/set_auto_active_status?provider_access_token=ACCESS_TOKEN
func LICSetAutoActiveStatus(api *token.Api, corpId string, status int) error {
	url := fmt.Sprintf("%s/cgi-bin/license/set_auto_active_status?", consts.WorkApiPrefixUrl)
	var req = struct {
		CorpId string `json:"corpid"`
		Status int    `json:"auto_active_status"`
	}{
		Status: status,
		CorpId: corpId,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return err
	}
	var res = response.Response{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return err
	}
	return res.ErrorIf()
}

// LICGetAutoActiveStatus 查询企业的许可自动激活状态
// 服务商可以调用该接口查询授权企业的许可自动激活状态。
// 请求方式： POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/license/get_auto_active_status?provider_access_token=ACCESS_TOKEN
func LICGetAutoActiveStatus(api *token.Api, corpId string) (int, error) {
	url := fmt.Sprintf("%s/cgi-bin/license/get_auto_active_status?", consts.WorkApiPrefixUrl)
	var req = struct {
		CorpId string `json:"corpid"`
	}{
		CorpId: corpId,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return 0, err
	}
	var res = struct {
		response.Response
		Status int `json:"auto_active_status"`
	}{}
	if err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	}); err != nil {
		return 0, err
	}
	return res.Status, res.ErrorIf()
}

type ActiveCode struct {
	ActiveCode string `json:"active_code"`
	Userid     string `json:"userid,omitempty"`
	Type       int    `json:"type,omitempty"`
}

type ActiveCodeResult struct {
	ActiveCode string `json:"active_code"`
	Userid     string `json:"userid"`
	Errcode    int32  `json:"errcode"`
}

type ActiveCodeDetail struct {
	ActiveCode string     `json:"active_code"`
	Userid     string     `json:"userid,omitempty"` //账号绑定激活的企业成员userid，未激活则不返回该字段。返回加密的userid
	Merge      *MergeInfo `json:"merge_info"`       //合并信息
	Share      *ShareInfo `json:"share_info"`       //分配信息
	Type       int        `json:"type"`             //激活码帐号类型：1:基础帐号，2:互通帐号
	Status     int        `json:"status"`           //账号状态 1: 未绑定 2: 已绑定且有效 3: 已过期 4: 待转移(企业开启自动激活时，成员离职或者被移出可见范围，第二天凌晨会更新为该状态) 5: 已合并（激活码本身激活了userid，后续使用新的激活码重新激活了该userid，则该码变为已合并状态。若被合并时，该激活码未过期则合并后会重置expire_time为合并时间。若被合并时，激活码已过期则不重置expire_time。注：该状态的激活码是已经失效的，不能重新用于激活或者继承。） 6: 已分配给下游
	CreateTime int64      `json:"create_time"`      //创建时间，订单支付成功后立即创建
	ActiveTime int64      `json:"active_time"`      //激活时间
	ExpireTime int64      `json:"expire_time"`      //过期时间
}

type MergeInfo struct {
	ToActiveCode   string `json:"to_active_code"`
	FromActiveCode string `json:"from_active_code"`
}

type ShareInfo struct {
	ToCorpId   string `json:"to_corpid"`
	FromCorpId string `json:"from_corpid"`
}

type ActiveCodeDetailResult struct {
	List     []ActiveCodeDetail `json:"active_info_list"`
	Invalids []string           `json:"invalid_active_code_list"`
}

type ActiveAccount struct {
	Userid     string `json:"userid"`      //账号绑定激活的企业成员userid，企业的成员userid。返回加密的userid
	Type       int    `json:"type"`        //激活码帐号类型：1:基础帐号，2:互通帐号
	ActiveTime int64  `json:"active_time"` //激活时间
	ExpireTime int64  `json:"expire_time"` //过期时间
}

type ActiveAccountDetail struct {
	ActiveCode string `json:"active_code"`
	Userid     string `json:"userid"`      //账号绑定激活的企业成员userid，企业的成员userid。返回加密的userid
	Type       int    `json:"type"`        //激活码帐号类型：1:基础帐号，2:互通帐号
	CreateTime int64  `json:"create_time"` //创建时间，订单支付成功后立即创建
	ActiveTime int64  `json:"active_time"` //激活时间
	ExpireTime int64  `json:"expire_time"` //过期时间
}

type ActiveAccountDetailResult struct {
	ActiveStatus int                   `json:"active_status"` // -1表示无效,账号激活状态。0：未激活、 1：已激活
	List         []ActiveAccountDetail `json:"active_info_list"`
}

type ActivatedAccountResult struct {
	HasMore    int             `json:"has_more"`
	NextCursor string          `json:"next_cursor"`
	Accounts   []ActiveAccount `json:"account_list"`
}

type LicenseTransfer struct {
	HandoverUserid string `json:"handover_userid"`
	TakeoverUserid string `json:"takeover_userid"`
}

type LicenseTransferResult struct {
	HandoverUserid string `json:"handover_userid"`
	TakeoverUserid string `json:"takeover_userid"`
	Errcode        int32  `json:"errcode"`
}

type LicenseShareActiveCode struct {
	FromCorpId string       `json:"from_corpid"`
	ToCorpId   string       `json:"to_corpid"`
	ShareList  []ActiveCode `json:"share_list"`
}

type LicenseShareResult struct {
	ActiveCode string `json:"active_code"`
	Errcode    int32  `json:"errcode"`
	Errmsg     string `json:"errmsg"`
}

type AppLicense struct {
	Status    int              `json:"license_status"`     //license检查开启状态。 0：未开启license检查状态（未迁移的历史授权的第三方应用（接入版本付费）或者未达到拦截时间的历史授权的的第三方应用（未接入版本付费）以及代开发应用） 1：已开启license检查状态。若开启且已过试用期，则需要为企业购买license账号才可以使用
	CheckTime int64            `json:"license_check_time"` //接口开启拦截校验时间。开始拦截校验后，无接口许可将会被拦截，有接口许可将不会被拦截。
	TrailInfo LicenseTrailInfo `json:"trail_info"`         //试用
}

type LicenseTrailInfo struct {
	StartTime int64 `json:"start_time"`
	EndTime   int64 `json:"end_time"`
}
