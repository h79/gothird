package template

type Template interface {
	IsMini() bool
	SetAppId(appId string)
	GetAppId() string
	SetFormId(formId string)
	SetToUser(user string)
	GetToUser() string
	SetId(id string)
	SetUrl(url string)
	SetKeyWord(key string, word KeyWord)
	Clone() Template
}
