package notify

import (
	"fmt"
	"gitee.com/h79/gothird/config"
	"gitee.com/h79/goutils/common/result"
	"math/rand"
	"strings"
	"time"
)

type SmsFillContentCallback func(interface{}) string

func SendSms(config *config.SmsConfig, phone string, content interface{}, call SmsFillContentCallback) (string, error) {
	if config.IsEmpty() {
		return "", result.Error(result.ErrSmsInternal, "config is empty")
	}
	txt := call(content)
	if txt == "" {
		return "", result.Error(result.ErrSmsInternal, "content is empty")
	}
	fmt.Println(txt)
	if strings.EqualFold(config.Service, "aliyun") {
		return SendAliyun(config, phone, txt)
	} else if config.Service == "tencent" {
		return SendTencentYun(config, phone, txt)
	} else if config.Service == "qiniu" {
		return SendQiNiuYun(config, phone, txt)
	}
	return "", result.RErrNotSupport
}

// GenSmsValidateCode
// 随机几位数字验证码生成
func GenSmsValidateCode(width int) string {
	numeric := [10]byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	r := len(numeric)
	rand.Seed(time.Now().UnixNano())

	var sb strings.Builder
	for i := 0; i < width; i++ {
		_, _ = fmt.Fprintf(&sb, "%d", numeric[rand.Intn(r)])
	}
	return sb.String()
}
