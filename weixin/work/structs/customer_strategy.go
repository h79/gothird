package structs

//https://developer.work.weixin.qq.com/document/path/99543

type StrategyID struct {
	StrategyId int32 `json:"strategy_id"`
}

type StrategyList struct {
	Info       []StrategyID `json:"strategy"`
	NextCursor string       `json:"next_cursor"`
}

type StrategyDetail struct {
	StrategyId   int32             `json:"strategy_id"`
	ParentId     int32             `json:"parent_id"`
	CreateTime   int64             `json:"create_time"`
	StrategyName string            `json:"strategy_name"`
	AdminList    []string          `json:"admin_list"`
	Privilege    StrategyPrivilege `json:"privilege"`
}

type StrategyPrivilege struct {
	ViewCustomerList        bool `json:"view_customer_list"`
	ViewCustomerData        bool `json:"view_customer_data"`
	ViewRoomList            bool `json:"view_room_list"`
	ContactMe               bool `json:"contact_me"`
	JoinRoom                bool `json:"join_room"`
	ShareCustomer           bool `json:"share_customer"`
	OperResignCustomer      bool `json:"oper_resign_customer"`
	OperResignGroup         bool `json:"oper_resign_group"`
	SendCustomerMsg         bool `json:"send_customer_msg"`
	EditWelcomeMsg          bool `json:"edit_welcome_msg"`
	ViewBehaviorData        bool `json:"view_behavior_data"`
	ViewRoomData            bool `json:"view_room_data"`
	SendGroupMsg            bool `json:"send_group_msg"`
	RoomDeduplication       bool `json:"room_deduplication"`
	RapidReply              bool `json:"rapid_reply"`
	OnjobCustomerTransfer   bool `json:"onjob_customer_transfer"`
	EditAntiSpamRule        bool `json:"edit_anti_spam_rule"`
	ExportCustomerList      bool `json:"export_customer_list"`
	ExportCustomerData      bool `json:"export_customer_data"`
	ExportCustomerGroupList bool `json:"export_customer_group_list"`
	ManageCustomerTag       bool `json:"manage_customer_tag"`
}

type StrategyRange struct {
	UserId  string `json:"userid,omitempty"`
	Type    int32  `json:"type"`
	PartyId int32  `json:"partyid,omitempty"`
}

type StrategyRangeList struct {
	Range      []StrategyRange `json:"range"`
	NextCursor string          `json:"next_cursor"`
}

type StrategyCreate struct {
	ParentId     int32             `json:"parent_id"`
	StrategyName string            `json:"strategy_name"`
	AdminList    []string          `json:"admin_list"`
	Privilege    StrategyPrivilege `json:"privilege"`
	Range        []StrategyRange   `json:"range"`
}

type StrategyEdit struct {
	StrategyId   int32             `json:"strategy_id"`
	StrategyName string            `json:"strategy_name"`
	AdminList    []string          `json:"admin_list"`
	Privilege    StrategyPrivilege `json:"privilege"`
	RangeAdd     []StrategyRange   `json:"range_add"`
	RangeDel     []StrategyRange   `json:"range_del"`
}
