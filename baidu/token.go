package baidu

import (
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/goutils/common/data"
	"mime/multipart"
	"net/http"
)

var firstService *acsReq

func init() {
	firstService = &acsReq{
		get: GetAccessToken,
	}
}

func GetService() token.Service {
	return firstService
}

func NewToken(app token.App) token.Token {
	return token.New(&app, firstService)
}

var _ token.Service = (*acsReq)(nil)

type acsReq struct {
	check token.CheckErrorFunc
	get   token.GenFunc
}

func (req *acsReq) Execute(tk token.Token, cmd string, d data.D) (interface{}, error) {
	if cmd == token.NAccessToken {
		return req.get(tk, d)
	}
	return nil, token.ErrNotSupported
}

func (req *acsReq) BuildUrl(uri string, acsKey string, d data.D) string {
	return token.DefaultUrl(acsKey, uri)
}

func (req *acsReq) SetHead(h *http.Header, acsKey string) {
	h.Set("Content-Type", "application/x-www-form-urlencoded")
}

func (req *acsReq) CreateForm(w *multipart.Writer, field string, form interface{}) error {
	return token.ErrNotSupported
}

func (req *acsReq) SetCheckError(check token.CheckErrorFunc) {
	req.check = check
}

func (req *acsReq) CheckError(err error) error {
	return err
}
