package validate

import (
	"fmt"
	"time"
)

var Empty = Data{}

type Data struct {
	Key    string `form:"key" binding:"required" json:"key"`
	Data   string `form:"data" binding:"required" json:"data"`
	At     int64  `form:"at" binding:"-" json:"at"`
	Expire int64  `form:"expireIn" binding:"-" json:"expireIn"`
}

// New
/**
  @param expire Nanoseconds 纳秒
*/
func New(key string, data string, expire int64) *Data {
	now := time.Now()
	ex := expire
	if ex > 0 {
		ex = now.Add(time.Duration(expire)).UnixNano()
	}
	return &Data{
		Key:    key,
		Data:   data,
		At:     now.UnixNano(),
		Expire: ex,
	}
}

func (v *Data) IsValid() bool {
	if v == nil {
		return false
	}
	return v.Data != ""
}

func (v *Data) IsExpired() bool {
	if v == nil {
		return false
	}
	if v.Expire <= 0 {
		return false
	}
	return time.Now().UnixNano() > v.Expire
}

func (v *Data) RemainExpired() int64 {
	if v == nil {
		return 0
	}
	if v.Expire <= 0 {
		return 0
	}
	var e = time.Now().UnixNano() - v.Expire
	if e <= 0 {
		return 0
	}
	return int64(time.Duration(e) / time.Second)
}

func (v *Data) String() string {
	return fmt.Sprintf("<Key: %s, Data: %s,At: %v,Expire: %v>", v.Key, v.Data, v.At, v.Expire)
}
