package work

import (
	"gitee.com/h79/gothird/config"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/work/structs"
	"gitee.com/h79/goutils/common/result"
)

type FillContentCallback func(*config.Options, interface{}) string

func Notify(option *config.Options, content interface{}, call FillContentCallback) error {
	tk, err := token.GetToken(option.Group.CorpId)
	if err != nil {
		return err
	}
	api := token.NewApi(tk)
	if option.Group.Method == "chat" {
		toWho := structs.ToGroup{ChatId: option.Group.ChatId}
		msg := NewMessage(&toWho, "text", structs.ExText{Content: call(option, content)})
		err = SendMessage(api, msg)
		return err
	} else if option.Group.Method == "agent" {
		toWho := structs.ToAgent{AgentId: option.Group.AgentId, ToUser: option.Group.User}
		msg := NewMessage(&toWho, "text", structs.ExText{Content: call(option, content)})
		err = SendMessage(api, msg)
		return err
	}
	return result.Error(result.ErrGroupInternal, "Not support method")
}
