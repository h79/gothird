package work

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/token/validate"
	"gitee.com/h79/gothird/weixin/work/service"
	"gitee.com/h79/gothird/weixin/work/structs"
	"gitee.com/h79/goutils/common/data"
	"net/http"
	"strconv"
	"strings"
	"time"
)

// 企业内部
var firstService *acsReq

// 会话存档，与企业内部应用一样，可以ignore
var msgAuditService *acsReq

// 获取服务商
var providerService *acsReq

// 获取第三方应用
var suiteService *acsReq

// 第三方服务商在取得企业的永久授权码后，获取授权企业
var corpService *acsReq

func init() {
	firstService = &acsReq{get: GetAccessToken, urlGet: token.DefaultUrl, auth2: token.GetEmpty}
	providerService = &acsReq{get: service.GetProviderToken, urlGet: ProviderTokenUrl, auth2: service.GetPermanentCode}
	suiteService = &acsReq{get: service.GetSuiteToken, urlGet: SuiteTokenUrl, auth2: service.GetPermanentCode}
	corpService = &acsReq{get: service.GetCorpToken, urlGet: token.DefaultUrl, auth2: token.GetEmpty}
	msgAuditService = &acsReq{get: GetAccessToken, urlGet: token.DefaultUrl, auth2: token.GetEmpty}
}

func GetService() token.Service {
	return firstService
}

func GetProviderService() token.Service {
	return providerService
}

func GetSuiteService() token.Service {
	return suiteService
}

func GetCorpService() token.Service {
	return corpService
}

func GetMsgAuditService() token.Service {
	return msgAuditService
}

func NewToken(app token.App) token.Token {
	return token.New(&app, firstService)
}

func NewProviderToken(app token.App) token.Token {
	return token.New(&app, providerService)
}

func NewSuiteToken(app token.App) token.Token {
	return token.New(&app, suiteService)
}

func NewCorpToken(app token.App) token.Token {
	return token.New(&app, corpService)
}

func NewMsgAuditToken(app token.App) token.Token {
	return token.New(&app, msgAuditService)
}

var _ token.Service = (*acsReq)(nil)

type acsReq struct {
	check  token.CheckErrorFunc
	get    token.GenFunc
	auth2  token.GenFunc
	urlGet token.GetUrlFunc
}

func (req *acsReq) Execute(tk token.Token, cmd string, d data.D) (interface{}, error) {
	if cmd == token.NAccessToken {
		return req.get(tk, d)
	} else if cmd == token.NRefreshToken {
		return req.get(tk, d)
	} else if cmd == token.NPreAuthCode {
		api := token.NewApi(tk)
		return service.GetPreAuthCode(api)
	} else if cmd == token.NOauth2App3 {
		acs, err := req.auth2(tk, d)
		if err != nil {
			return nil, err
		}
		if per, ok := acs.(*structs.Permanent); ok {
			appId := per.AuthCorpInfo.CorpId
			ch := &token.Child{
				App: token.App{
					ParentId: tk.GetAppId(),
					IsCache:  tk.IsCache(),
					Type:     token.WxWorkCorpType,
					Id:       token.GenId(tk.GetAppId(), appId),
					Secret:   per.PermanentCode,
					AppId:    appId,
					Name:     "permanent",
				},
				VMap: token.VMap{
					token.PermanentCode: validate.New(token.PermanentCode, per.PermanentCode, -1),
					token.AgentId:       validate.New(token.AgentId, per.AgentId(), -1)},
			}
			var suiteType = "1" //第三方应用
			var tokenService token.Service = corpService
			if per.IsCustomizedApp() { //代应用
				ch.Type = token.WxWorkType
				suiteType = "0"
				tokenService = firstService
			}
			buf, _ := json.Marshal(&per.PermanentInfo)
			ch.VMap[token.PermanentInfo] = validate.New(token.PermanentInfo, string(buf), (time.Hour * 24 * 30).Nanoseconds())
			ch.VMap[token.SuiteType] = validate.New(token.SuiteType, suiteType, -1)
			token.SyncCreateChild(tk, tokenService, ch, acs, []string{appId}, func() {
				_ = tk.SetCode(token.PermanentCodeKey(appId), per.PermanentCode, -1)
			})
		}
		return acs, err
	} else if cmd == token.NTicket {
		api := token.NewApi(tk)
		return GetTicket(api, d)
	}
	return nil, token.ErrNotSupported
}

func (req *acsReq) BuildUrl(uri string, acsKey string, d data.D) string {
	return req.urlGet(acsKey, uri)
}

func (req *acsReq) SetHead(h *http.Header, acsKey string) {
}

func (req *acsReq) SetCheckError(check token.CheckErrorFunc) {
	req.check = check
}

func (req *acsReq) CheckError(err error) error {
	if res, ok := err.(token.Result); ok {
		if req.check != nil {
			return req.check(&res)
		}
		if IsTokenExpired(res.Code) {
			return token.ErrTokenExpired.Clone(strconv.FormatInt(int64(res.Code), 10), res.Msg)
		}
	}
	return err
}

func SuiteTokenUrl(acsKey string, uri string) string {
	if strings.HasSuffix(uri, "&") || strings.HasSuffix(uri, "?") {
		return fmt.Sprintf("%ssuite_access_token=%s", uri, acsKey)
	}
	return fmt.Sprintf("%s&suite_access_token=%s", uri, acsKey)
}

func ProviderTokenUrl(acsKey string, uri string) string {
	if strings.HasSuffix(uri, "&") || strings.HasSuffix(uri, "?") {
		return fmt.Sprintf("%sprovider_access_token=%s", uri, acsKey)
	}
	return fmt.Sprintf("%s&provider_access_token=%s", uri, acsKey)
}

//https://work.weixin.qq.com/api/doc/90000/90139/90313

func IsTokenExpired(code int32) bool {
	return false
}
