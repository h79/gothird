package token

import (
	"fmt"
	"gitee.com/h79/gothird/token/access"
	"gitee.com/h79/goutils/common"
	"gitee.com/h79/goutils/common/data"
	"gitee.com/h79/goutils/common/logger"
	"mime/multipart"
	"net/http"
	"reflect"
	"strings"
)

type Base interface {
	GetAppId() string
	GetSecret() string
	GetAccessToken(options ...data.OptionsFunc) (string, error)
}

type Token interface {
	Parent() Token
	CreateChild(app *App, service Service) Token

	String() string
	IsCache() bool
	GetParentId() string
	GetId() string
	GetName() string
	GetAppType() string
	GetAppId() string
	GetSecret() string
	SetSecret(secret string)

	GetAccessToken(options ...data.OptionsFunc) (string, error)
	SetAccessTokenWithAcs(acs access.Token) string
	SetAccessToken(tk string, expire int64) error
	ClearAccessToken()

	SetRefreshAccessToken(tk string, expire int64) error
	RefreshAccessToken(refToken string) (string, error)

	GetCode(name string) (string, error)
	ClearCode(name string) error
	SetCode(name, code string, expire int64) error

	GetRequest() Request
	// Execute data["appid"]
	Execute(cmd string, d data.D) (interface{}, error)
}

type CheckErrorFunc func(result ResultObject) error

type ResultObject interface {
	GetCode() int32
	GetSubCode() string
	GetMsg() string
}

// Request GetUrl for request
type Request interface {
	BuildUrl(uri string, acsKey string, d data.D) string
	SetHead(h *http.Header, acsKey string)
	SetCheckError(check CheckErrorFunc)
	CheckError(err error) error
}

type Form interface {
	CreateForm(w *multipart.Writer, field string) error
}

var FormType = reflect.TypeOf((*Form)(nil)).Elem()

type Service interface {
	Request
	Execute(tk Token, cmd string, d data.D) (interface{}, error)
}

type GenFunc func(tk Token, d data.D) (access.Token, error)
type GetUrlFunc func(acsKey string, uri string) string

func GetEmpty(tk Token, d data.D) (access.Token, error) {
	logger.W("Token", "GetEmpty, will return 'ErrTokenExpired' for app= %s", tk.String())
	return nil, ErrTokenExpired
}

func New(app *App, service Service) Token {
	if common.IsNil(service) {
		panic("New token, service is nil")
	}
	// ir 表示内部
	appId := strings.TrimSuffix(app.AppId, ":ir")
	tk := &tokenImpl{
		App:     app,
		parent:  nil,
		service: service,
	}
	tk.App.AppId = appId
	return tk
}

func AddToken(id string, app *AppItem) {
	Mgr().AddToken(id, app)
}

func DeleteToken(id string) error {
	return Mgr().DeleteToken(id)
}

func GetToken(id string) (Token, error) {
	return Mgr().GetToken(id)
}

func ChangeSecret(id string, secret string) error {
	return Mgr().ChangeSecret(id, secret)
}

func GetRealId(ty string, realId string) (Token, error) {
	id, ok := Mgr().GetRealId(ty, realId)
	if !ok {
		return nil, fmt.Errorf("not found real id for type= '%s',realId= '%s'", ty, realId)
	}
	return GetToken(id)
}

func GenId(componentId, appId string) string {
	return fmt.Sprintf("%s-%s", componentId, appId)
}

func GenIdWithType(ty, componentId, appId string) string {
	return fmt.Sprintf("%s-%s-%s", ty, componentId, appId)
}

func GetId(componentId, appId string) string {
	if len(componentId) > 0 {
		if len(appId) > 0 {
			return GenId(componentId, appId)
		}
		return componentId
	}
	return appId
}

func GetIdWithType(ty, componentId, appId string) string {
	if len(componentId) > 0 {
		if len(appId) > 0 {
			return GenIdWithType(ty, componentId, appId)
		}
		return GenId(ty, componentId)
	}
	return GenId(ty, appId)
}

func PermanentCodeKey(corpId string) string {
	return PermanentCode + ":" + corpId
}
