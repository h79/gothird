package app3

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/token/access"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/gothird/weixin/session"
	"gitee.com/h79/goutils/common/data"
	"gitee.com/h79/goutils/common/http"
)

// 针对个人,代替授权公众号发起网页授权

// WebGetAccessToken https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/Official_Accounts/official_account_website_authorization.html
// 第三方平台可以根据本文档相关说明，代替授权公众号发起网页授权
// ?appid=APPID&code=CODE&grant_type=authorization_code&component_appid=COMPONENT_APPID&
func WebGetAccessToken(tk token.Token, d data.D) (access.Token, error) {
	parent := tk.Parent()
	acs, err := parent.GetAccessToken()
	if err != nil {
		return nil, err
	}
	appid := d.String("appid")
	code := d.String("code")
	url := fmt.Sprintf("%s/sns/oauth2/component/access_token?appid=%s&code=%s&grant_type=authorization_code&component_appid=%s&component_access_token=%s", consts.ApiPrefixUrl, appid, code, parent.GetAppId(), acs)
	return session.GetToken(appid, url)
}

func WebRefreshToken(tk token.Token, d data.D) (access.Token, error) {
	parent := tk.Parent()
	acs, err := parent.GetAccessToken()
	if err != nil {
		return nil, err
	}
	appid := d.String("appid")
	refToken := d.String("refToken")
	url := fmt.Sprintf("%s/sns/component/oauth2/refresh_token?appid=%s&grant_type=refresh_token&refresh_token=%s&component_appid=%s&component_access_token=%s", consts.ApiPrefixUrl, appid, refToken, parent.GetAppId(), acs)

	return session.GetToken(appid, url)
}

// 针对一个公众号 授权给第三方应用
// 先获取 "预授权码" -> 生成授权链接->用户扫码->（auth_code)->AuthorizerGet

// AuthorizerGet 使用授权码获取授权信息
//
//	https://developers.weixin.qq.com/doc/oplatform/Third-party_Platforms/api/authorization_info.html
func AuthorizerGet(tk token.Token, d data.D) (access.Token, error) {
	parent := tk.Parent()
	acs, err := parent.GetAccessToken()
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("%s/cgi-bin/component/api_query_auth?component_access_token=%s", consts.ApiPrefixUrl, acs)

	type info struct {
		ComponentAppId string `json:"component_appid"`
		Code           string `json:"authorization_code"`
	}
	in := info{
		ComponentAppId: parent.GetAppId(),
		Code:           d.String("auth_code"),
	}
	buf, err := json.Marshal(&in)
	if err != nil {
		return nil, err
	}
	type Result struct {
		response.Response
		AuthorizationInfo `json:"authorization_info"`
	}
	res := Result{}
	hp := http.Http{}
	body, err := hp.DoBytes("POST", url, buf)
	if err != nil {
		return nil, err
	}
	if err = json.Unmarshal(body, &res); err != nil {
		return nil, err
	}
	return &res.AuthorizationInfo, res.ErrorIf()
}

// GetAuthorizerAccessToken 获取/刷新接口调用令牌(获取授权账号的authorizer_access_token)
func GetAuthorizerAccessToken(tk token.Token, d data.D) (access.Token, error) {
	parent := tk.Parent()
	acs, err := parent.GetAccessToken()
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("%s/cgi-bin/component/api_authorizer_token?component_access_token=%s", consts.ApiPrefixUrl, acs)

	authorizerAppId := tk.GetAppId()
	refToken := d.String("refToken")
	if refToken == "" {
		refToken, _ = tk.GetCode(token.NRefreshToken)
	}
	in := struct {
		ComponentAppId     string `json:"component_appid"`
		AuthorizerAppId    string `json:"authorizer_appid"`
		AuthorizerRefToken string `json:"authorizer_refresh_token"`
	}{
		ComponentAppId:     parent.GetAppId(),
		AuthorizerAppId:    authorizerAppId,
		AuthorizerRefToken: refToken,
	}
	buf, err := json.Marshal(&in)
	if err != nil {
		return nil, err
	}
	hp := http.Http{}
	body, err := hp.DoBytes("POST", url, buf)
	if err != nil {
		return nil, err
	}
	type Result struct {
		response.Response
		AuthorizerToken
	}
	res := Result{}
	if er := json.Unmarshal(body, &res); er != nil {
		return nil, er
	}
	res.AuthorizerAppId = authorizerAppId
	return &res.AuthorizerToken, res.ErrorIf()
}
