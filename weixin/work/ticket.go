package work

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/gothird/weixin/work/structs"
	"gitee.com/h79/goutils/common/data"
	"gitee.com/h79/goutils/common/http"
)

func GetTicket(api *token.Api, d data.D) (*token.Ticket, error) {
	var err error
	var ticket structs.Ticket
	if d.String(token.AgentId) != "" {
		ticket, err = JSApiGetAgentTicket(api)
	} else {
		ticket, err = JSApiGetCorpTicket(api)
	}
	if err != nil {
		return nil, err
	}
	return &token.Ticket{
		Ticket:   ticket.Ticket,
		ExpireIn: ticket.ExpireIn,
	}, nil
}

// JSApiGetCorpTicket 获取企业的jsapi_ticket
// 生成签名之前必须先了解一下jsapi_ticket，jsapi_ticket是H5应用调用企业微信JS接口的临时票据。正常情况下，jsapi_ticket的有效期为7200秒，通过access_token来获取。由于获取jsapi_ticket的api调用次数非常有限（一小时内，一个企业最多可获取400次，且单个应用不能超过100次），频繁刷新jsapi_ticket会导致api调用受限，影响自身业务，开发者必须在自己的服务全局缓存jsapi_ticket。
//
// 请求方式：GET（HTTPS）
// 请求URL：https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token=ACCESS_TOKEN
func JSApiGetCorpTicket(api *token.Api) (structs.Ticket, error) {

	uri := fmt.Sprintf("%s/cgi-bin/get_jsapi_ticket?", consts.WorkApiPrefixUrl)

	type ticketResult struct {
		response.Response
		structs.Ticket
	}
	res := ticketResult{}
	err := api.Request("GET", uri, nil, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return structs.Ticket{}, err
	}
	return res.Ticket, res.ErrorIf()
}

// JSApiGetAgentTicket 获取应用的jsapi_ticket
// 应用的jsapi_ticket用于计算agentConfig（参见“通过agentConfig注入应用的权限”）的签名，签名计算方法与上述介绍的config的签名算法完全相同，但需要注意以下区别：
//
// 签名的jsapi_ticket必须使用以下接口获取。且必须用wx.agentConfig中的agentid对应的应用去获取access_token。
// 签名用的noncestr和timestamp必须与wx.agentConfig中的nonceStr和timestamp相同。
// 正常情况下，应用的jsapi_ticket的有效期为7200秒，通过access_token来获取。由于获取jsapi_ticket的api调用次数非常有限（一小时内，每个应用不能超过100次），频繁刷新jsapi_ticket会导致api调用受限，影响自身业务，开发者必须在自己的服务全局缓存应用的jsapi_ticket。
//
// 请求方式：GET（HTTPS）
// 请求URL：https://qyapi.weixin.qq.com/cgi-bin/ticket/get?access_token=ACCESS_TOKEN&type=agent_config
func JSApiGetAgentTicket(api *token.Api) (structs.Ticket, error) {

	uri := fmt.Sprintf("%s/cgi-bin/ticket/get?type=agent_config&", consts.WorkApiPrefixUrl)

	type ticketResult struct {
		response.Response
		structs.Ticket
	}
	res := ticketResult{}
	err := api.Request("GET", uri, nil, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return structs.Ticket{}, err
	}
	return res.Ticket, res.ErrorIf()
}
