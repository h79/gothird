default: all

# Do a parallel build with multiple jobs, based on the number of CPUs online
# in this system: 'make -j8' on a 8-CPU system, etc.
ifeq ($(JOBS),)
  JOBS := $(shell grep -c ^processor /proc/cpuinfo 2>/dev/null)
  ifeq ($(JOBS),)
    JOBS := $(shell sysctl -n hw.logicalcpu 2>/dev/null)
    ifeq ($(JOBS),)
      JOBS := 1
    endif
  endif
endif

use_all_cores:
	make -j$(JOBS) all

# git information
GIT_COMMIT ?= $(shell git rev-parse --short HEAD)
GIT_DIRTY ?= $(shell test -n "`git status --porcelain`" && echo "+CHANGES" || true)
GIT_DESCRIBE ?= $(shell git describe --tags --always)

COMMIT := $(GIT_COMMIT)$(GIT_DIRTY)
VERSION := $(GIT_DESCRIBE)
SHIP_VERSION := $(shell docker image inspect -f "{{ .Config.Labels.version }}" $(IMAGE):latest 2>/dev/null)

MODULE := gitee.com/h79
BUILDER := h79/h79-go-builder
IMAGE := h79/h79-go
IMAGE_TAR := $(subst /,_,$(IMAGE)).$(SHIP_VERSION).tar
IMAGE_TAR_GZ := $(IMAGE_TAR).gz

branch := $(shell git rev-parse --abbrev-ref HEAD)
buildDate := $(shell date +%Y%m%d%H%M%S)

Uname := $(shell uname)

ifeq ($(Uname),Linux)
	platform := linux
else
  ifeq ($(Uname),Darwin)
	platform := darwin
  endif
endif

ifdef APPVERSION
	version := $(APPVERSION)-$(buildDate)
else
	version := $(branch)-$(GIT_COMMIT)-$(buildDate)
endif

tags := $(platform) release


GO_BUILD := CGO_ENABLED=0 go build -tags "$(tags)"

status:
	@echo "Commit: $(COMMIT) Version: $(VERSION) Ship Version: $(SHIP_VERSION)"

builder: status
	docker build \
		--tag $(BUILDER):$(VERSION) \
		--tag $(BUILDER):latest \
		--build-arg BUILD_ARG=release \
		-f docker/builder.Dockerfile \
		.

# 生成镜像文件
docker: builder
	docker build \
		--tag $(IMAGE):$(VERSION) \
		--tag $(IMAGE):latest \
		--build-arg COMMIT=$(COMMIT) \
		--build-arg VERSION=$(VERSION) \
		-f docker/Dockerfile \
		.

docker_clean: status
	docker rmi -f $(BUILDER):latest
	docker rmi -f $(IMAGE):latest
	docker rmi -f $(BUILDER):$(VERSION)
	docker rmi -f $(IMAGE):$(VERSION)

save: status
ifeq ($(SHIP_VERSION),)
	$(error No version to ship, please build first)
endif
	docker save $(IMAGE):$(SHIP_VERSION) > $(IMAGE_TAR)
	tar zcf $(IMAGE_TAR_GZ) $(IMAGE_TAR)

start:
	docker-compose down
	docker-compose up --no-start
	docker-compose start

stop:
	docker-compose down

logs:
	docker-compose logs -f --tail=10

push_testnet:
	docker tag $(IMAGE):$(VERSION) $(IMAGE):testnet
	docker push $(IMAGE):testnet

push_bench:
	docker tag $(IMAGE):$(VERSION) $(IMAGE):bench
	docker push $(IMAGE):bench

push_staging:
	docker tag $(IMAGE):$(VERSION) $(IMAGE):staging
	docker push $(IMAGE):staging

push:
	docker push $(IMAGE):$(VERSION)
	docker push $(IMAGE):latest

bin/utils:
	$(GO_BUILD)

all: bin/utils

release:
ifeq ($(Uname),Linux)
	make alpine_release;
else
	make -j$(JOBS) build_release;
	tar czvf h79-bin.tgz bin/*;
endif

build_release: all

alpine_release: build_release
	temp_container = $$(docker create $(BUILDER):$(VERSION)) ; \
	docker cp $${temp_container}:/go/src/giteee.com/h79/bin - | gzip > gothird-bin.tgz

clean:
	rm -rf bin/dy-*
	rm -f *.cover.out
	rm -f coverage.txt

.PHONY: vendor

vendor:
	go mod tidy
	go mod vendor
