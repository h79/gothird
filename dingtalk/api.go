package dingtalk

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/goutils/common/http"
)

func genHead(toWho ToWho, msgType string) head {
	h := head{
		Type: msgType,
	}
	if yes, agent := toWho.IsAgent(); yes {
		h.AgentId = agent.AgentId
		h.ToTag = agent.ToTag
		h.ToParty = agent.ToParty
		h.ToUser = agent.ToUser
	}
	if yes, group := toWho.IsGroup(); yes {
		h.ChatId = group.ChatId
	}
	if yes, _ := toWho.IsRoot(); yes {
		h.Root = yes
	}
	return h
}

func NewMessage(toWho ToWho, msgType string, at *At, msg interface{}) Message {

	switch msgType {
	case "text":
		return &textMsg{
			head: genHead(toWho, msgType),
			Text: msg.(Content),
			At:   at,
		}

	case "image":
		return &imageMsg{
			head:  genHead(toWho, msgType),
			Image: msg.(Image),
			At:    at,
		}

	case "voice":
		return &voiceMsg{
			head:  genHead(toWho, msgType),
			Voice: msg.(Voice),
			At:    at,
		}

	case "video":
		return &videoMsg{
			head:  genHead(toWho, msgType),
			Video: msg.(Video),
			At:    at,
		}

	case "markdown":
		return &markdownMsg{
			head:     genHead(toWho, msgType),
			Markdown: msg.(Content),
			At:       at,
		}

	case "link":
		return &linkMsg{
			head: genHead(toWho, msgType),
			Link: msg.(Link),
			At:   at,
		}

	default:
	}
	return nil
}

// SendMessage 发送应用消息,群聊消息,客户欢迎语
func SendMessage(corp token.Base, msg Message) (string, error) {
	acs, err := corp.GetAccessToken()
	if err != nil {
		return "", err
	}
	var url string
	if msg.IsAgent() {
		return "", token.Error(-1, "Not implement")
	} else if msg.IsGroup() {
		url = fmt.Sprintf("%s/chat/send?access_token=%s", ApiPrefixUrl, acs)
	} else if msg.IsRoot() {
		url = fmt.Sprintf("%s/robot/send?access_token=%s", ApiPrefixUrl, acs)
	} else {
		return "", token.Error(-1, "[DingTalk] SendMessage parameter is error")
	}
	buf, err := json.Marshal(msg)
	if err != nil {
		return "", token.Error(-1, "[WX] SendMessage Json marshal error")
	}
	hp := http.Http{}
	return hp.DoString("POST", url, buf)
}
