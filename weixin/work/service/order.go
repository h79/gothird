package service

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/goutils/common/http"
)

// LICCreateOrder 下单购买帐号
// 服务商下单为企业购买新的帐号，可以同时购买基础帐号与互通帐号。下单之后，需要到服务商管理端发起支付，支付完成之后，订单才能生效。
// 请求方式： POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/license/create_new_order?provider_access_token=ACCESS_TOKEN
func LICCreateOrder(api *token.Api, order *LicenseOrder) (string, error) {
	url := fmt.Sprintf("%s/cgi-bin/license/create_new_order?", consts.WorkApiPrefixUrl)

	buf, err := json.Marshal(order)
	if err != nil {
		return "", err
	}
	var res = struct {
		response.Response
		OrderId string `json:"order_id"`
	}{}
	err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return "", err
	}
	return res.OrderId, res.ErrorIf()
}

// LICCreateRenewOrderJob 创建续期任务
// 在同一个订单里，首次创建任务无须指定jobid，后续指定同一个jobid，表示往同一个订单任务追加续期的成员。
// 请求方式： POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/license/create_renew_order_job?provider_access_token=ACCESS_TOKEN
func LICCreateRenewOrderJob(api *token.Api, order *LicenseReOrder) (*LicenseReOrderResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/license/create_renew_order_job?", consts.WorkApiPrefixUrl)

	buf, err := json.Marshal(order)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		LicenseReOrderResult
	}{}
	err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	return &res.LicenseReOrderResult, res.ErrorIf()
}

// LICSubmitOrderJob 提交续期订单
// 创建续期任务之后，需要调用该接口，以提交订单任务。注意，提交之后，需要到服务商管理端发起支付，支付完成之后，订单才能生效。
// 请求方式： POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/license/submit_order_job?provider_access_token=ACCESS_TOKEN
func LICSubmitOrderJob(api *token.Api, order *LicenseSubOrder) (string, error) {
	url := fmt.Sprintf("%s/cgi-bin/license/submit_order_job?", consts.WorkApiPrefixUrl)

	buf, err := json.Marshal(order)
	if err != nil {
		return "", err
	}
	var res = struct {
		response.Response
		OrderId string `json:"order_id"`
	}{}
	err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return "", err
	}
	return res.OrderId, res.ErrorIf()
}

// LICGetOrderList 获取订单列表
// 服务商查询自己某段时间内的平台能力服务订单列表
// 请求方式： POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/license/list_order?provider_access_token=ACCESS_TOKEN
func LICGetOrderList(api *token.Api, cond *LicenseOrderListSearch) (*LicenseOrderListResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/license/list_order?", consts.WorkApiPrefixUrl)

	buf, err := json.Marshal(cond)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		LicenseOrderListResult
	}{}
	err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	return &res.LicenseOrderListResult, res.ErrorIf()
}

// LICGetOrderDetail 获取订单详情
// 查询某个订单的详情，包括订单的状态、基础帐号个数、互通帐号个数、帐号购买时长等。注意，该接口不返回订单中的帐号激活码列表或者续期的帐号成员列表，请调用获取订单中的帐号列表接口以获取帐号列表。
// 请求方式： POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/license/get_order?provider_access_token=ACCESS_TOKEN
func LICGetOrderDetail(api *token.Api, orderId string) (*LicenseOrderDetail, error) {
	url := fmt.Sprintf("%s/cgi-bin/license/get_order?", consts.WorkApiPrefixUrl)
	var req = struct {
		OrderId string `json:"order_id"`
	}{
		OrderId: orderId,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		Order LicenseOrderDetail `json:"order"`
	}{}
	err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	return &res.Order, res.ErrorIf()
}

// LICCancelOrder 取消订单
// 取消接口许可购买和续费订单，只可取消未支付且未失效的订单。
// 请求方式： POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/license/cancel_order?provider_access_token=ACCESS_TOKEN
func LICCancelOrder(api *token.Api, corpId, orderId string) error {
	url := fmt.Sprintf("%s/cgi-bin/license/cancel_order?", consts.WorkApiPrefixUrl)
	var req = struct {
		OrderId string `json:"order_id"`
		CorpId  string `json:"corpid"`
	}{
		OrderId: orderId,
		CorpId:  corpId,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return err
	}
	var res = response.Response{}
	err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return err
	}
	return res.ErrorIf()
}

// LICOrderAccountList 获取订单中的帐号列表
// 查询指定订单下的平台能力服务帐号列表。若为购买帐号的订单或者存量企业的版本付费迁移订单，则返回帐号激活码列表；若为续期帐号的订单，则返回续期帐号的成员列表。注意，若是购买帐号的订单，则仅订单支付完成时，系统才会生成帐号，故支付完成之前，该接口不会返回帐号激活码。
// 请求方式： POST（HTTPS）
// 请求地址： https://qyapi.weixin.qq.com/cgi-bin/license/list_order_account?provider_access_token=ACCESS_TOKEN
func LICOrderAccountList(api *token.Api, orderId string, cursor Cursor) (*LicenseAccountListResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/license/list_order_account?", consts.WorkApiPrefixUrl)
	var req = struct {
		OrderId string `json:"order_id"`
		Limit   int    `json:"limit"`
		Cursor  string `json:"cursor,omitempty"`
	}{
		OrderId: orderId,
		Limit:   cursor.Limit,
		Cursor:  cursor.Cursor,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		LicenseAccountListResult
	}{}
	err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	return &res.LicenseAccountListResult, res.ErrorIf()
}

// LICCreateNewOrderJob 创建多企业新购任务
func LICCreateNewOrderJob(api *token.Api, buyList []LicenseBuy, JobId string) (*LicenseReOrderResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/license/create_new_order_job?", consts.WorkApiPrefixUrl)
	var req = struct {
		BuyList []LicenseBuy `json:"buy_list"`
		JobId   string       `json:"jobid"`
	}{
		BuyList: buyList,
		JobId:   JobId,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		Invalid LicenseReOrderResult `json:"invalid_list"`
	}{}
	err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	return &res.Invalid, res.ErrorIf()
}

// LICGetUnionOrder 获取多企业订单详情
func LICGetUnionOrder(api *token.Api, orderId string, cursor Cursor) (*LicenseUnionOrderResult, error) {
	url := fmt.Sprintf("%s/cgi-bin/license/get_union_order?", consts.WorkApiPrefixUrl)
	var req = struct {
		OrderId string `json:"order_id"`
		Limit   int    `json:"limit"`
		Cursor  string `json:"cursor,omitempty"`
	}{
		OrderId: orderId,
		Limit:   cursor.Limit,
		Cursor:  cursor.Cursor,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	var res = struct {
		response.Response
		LicenseUnionOrderResult
	}{}
	err = api.Request("POST", url, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	if err != nil {
		return nil, err
	}
	return &res.LicenseUnionOrderResult, res.ErrorIf()
}

type LicenseUnionOrderResult struct {
	Order      LicenseOrderDetail `json:"order"`
	HasMore    int                `json:"has_more"`
	NextCursor string             `json:"next_cursor"`
	BuyList    []LicenseBuy       `json:"buy_list"`
}

type LicenseBuy struct {
	SubOrderId       string                      `json:"sub_order_id"`
	CorpId           string                      `json:"corpid"`
	AccountCount     LicenseOrderAccountCount    `json:"account_count"`
	AccountDuration  LicenseOrderAccountDuration `json:"account_duration"`
	AutoActiveStatus int                         `json:"auto_active_status"`
}

type LicenseOrder struct {
	CorpId          string                      `json:"corpid"`
	BuyerUserId     string                      `json:"buyer_userid"`
	AccountCount    LicenseOrderAccountCount    `json:"account_count"`
	AccountDuration LicenseOrderAccountDuration `json:"account_duration"` //账号购买时长
}

type LicenseOrderAccountCount struct {
	BaseCount            int `json:"base_count"`             //基础账号个数
	ExternalContactCount int `json:"external_contact_count"` //互通账号个数
}

type LicenseOrderAccountDuration struct {
	Months        int   `json:"months"`
	Days          int   `json:"days"`
	NewExpireTime int64 `json:"new_expire_time,omitempty"` //下单续期账号中指定新过期时间时返回
}

type LicenseReOrder struct {
	CorpId      string           `json:"corpid"`
	JobId       string           `json:"jobid,omitempty"`
	AccountList []LicenseAccount `json:"account_list"`
}

type LicenseAccount struct {
	ActiveCode string `json:"active_code,omitempty"` //账号码，订单类型为购买账号时，返回该字段
	Userid     string `json:"userid,omitempty"`      //企业续期成员userid，订单类型为续期账号时，返回该字段。返回加密的userid
	Type       int    `json:"type"`                  //账号类型：1:基础账号，2:互通账号
}

type LicenseReOrderResult struct {
	//任务id，请求包中未指定jobid时，会生成一个新的jobid返回
	JobId string `json:"jobid"`
	//不合法的续期账号列表
	InvalidAccountList []LicenseInvalidAccount `json:"invalid_account_list"`
}

type LicenseSubOrder struct {
	JobId           string                      `json:"jobid"`
	BuyerUserid     string                      `json:"buyer_userid"`
	AccountDuration LicenseOrderAccountDuration `json:"account_duration"`
}

type LicenseInvalidAccount struct {
	Errcode int32  `json:"errcode"`
	Errmsg  string `json:"errmsg"`
	Userid  string `json:"userid"`
	Type    int    `json:"type"`
}

type LicenseOrderListSearch struct {
	CorpId    string `json:"corpid"`
	StartTime int64  `json:"start_time,omitempty"`
	EndTime   int64  `json:"end_time,omitempty"`
	Cursor
}

type LicenseOrderListResult struct {
	NextCursor string             `json:"next_cursor"`
	HasMore    int                `json:"has_more"`
	OrderList  []LicenseOrderInfo `json:"order_list"`
}

type LicenseOrderInfo struct {
	OrderId   string `json:"order_id"`
	OrderType int    `json:"order_type"` //1：购买账号 2：续期账号 5：历史企业迁移订单 8：多企业新购订单(只返回父订单，且仅当corpid不填时返回)
}

type LicenseOrderDetail struct {
	CorpId          string                      `json:"corpid"`
	OrderId         string                      `json:"order_id"`
	OrderType       int                         `json:"order_type"`   //订单类型，1：购买账号，2：续期账号，5：应用版本付费迁移订单，6：历史合同迁移订单
	OrderStatus     int                         `json:"order_status"` //订单状态，0：待支付，1：已支付，2：已取消（未支付，订单已关闭）3：未支付，订单已过期，4：申请退款中，5：退款成功，6：退款被拒绝，7：订单已失效（将企业从服务商测试企业列表中移除时会将对应测试企业的所有测试订单置为已失效）
	Price           int32                       `json:"price"`        //订单金额，单位分
	CreateTime      int64                       `json:"create_time"`
	PayTime         int64                       `json:"pay_time"`         //迁移订单不返回该字段
	AccountCount    LicenseOrderAccountCount    `json:"account_count"`    //订单的账号数
	AccountDuration LicenseOrderAccountDuration `json:"account_duration"` //账号购买时长
}

type LicenseAccountListResult struct {
	HasMore     int              `json:"has_more"`
	NextCursor  string           `json:"next_cursor"`
	AccountList []LicenseAccount `json:"account_list"`
}

type Cursor struct {
	Limit  int    `json:"limit"`
	Cursor string `json:"cursor,omitempty"`
}
