package dingtalk

type ToWho interface {
	IsGroup() (bool, *ToGroup)
	IsAgent() (bool, *ToAgent)
	IsRoot() (bool, *ToRoot)
}

type ToGroup struct {
	ChatId string `json:"chatid,omitempty"`
}

func (to *ToGroup) IsGroup() (bool, *ToGroup) {
	return to.ChatId != "", to
}

func (to *ToGroup) IsAgent() (bool, *ToAgent) {
	return false, nil
}

func (to *ToGroup) IsRoot() (bool, *ToRoot) {
	return false, nil
}

type ToAgent struct {
	AgentId int32  `json:"agentid,omitempty"` //应用id
	ToUser  string `json:"touser,omitempty"`  //企业号中的用户帐号
	ToTag   string `json:"totag,omitempty"`   //企业号中的标签id，群发使用（推荐）
	ToParty string `json:"toparty,omitempty"` //企业号中的部门id，群发时使用。
}

func (to *ToAgent) IsGroup() (bool, *ToGroup) {
	return false, nil
}

func (to *ToAgent) IsAgent() (bool, *ToAgent) {
	return to.AgentId != 0, to
}

func (to *ToAgent) IsRoot() (bool, *ToRoot) {
	return false, nil
}

type ToRoot struct {
}

func (to *ToRoot) IsGroup() (bool, *ToGroup) {
	return false, nil
}

func (to *ToRoot) IsAgent() (bool, *ToAgent) {
	return false, nil
}

func (to *ToRoot) IsRoot() (bool, *ToRoot) {
	return true, nil
}

type Message interface {
	IsGroup() bool
	IsAgent() bool
	IsRoot() bool
}

// {
// "chatid": "",
// "touser": "UserID1|UserID2|UserID3",
// "toparty": " PartyID1 | PartyID2 ",
// "totag": " TagID1 | TagID2 ",
// "agentid": 1,
// "msgtype": "text",
// "text": {
// "content": "Holiday Service For Pony(http://xxxxx)"
// },
// "safe":0
// }
type head struct {
	Root    bool   `json:"-"`
	ChatId  string `json:"chatid,omitempty"`
	AgentId int32  `json:"agentid,omitempty"` //应用id
	ToUser  string `json:"touser,omitempty"`  //企业号中的用户帐号
	ToTag   string `json:"totag,omitempty"`   //企业号中的标签id，群发使用（推荐）
	ToParty string `json:"toparty,omitempty"` //企业号中的部门id，群发时使用。
	Type    string `json:"msgtype"`
}

func (h *head) IsGroup() bool {
	return h.ChatId != ""
}

func (h *head) IsAgent() bool {
	return h.AgentId > 0
}

func (h *head) IsRoot() bool {
	return h.Root
}

// 文本消息
type textMsg struct {
	head
	Text Content `json:"text"`
	At   *At     `json:"at,omitempty"`
}

// 图片消息
type imageMsg struct {
	head
	Image Image `json:"image"`
	At    *At   `json:"at,omitempty"`
}

type markdownMsg struct {
	head
	Markdown Content `json:"markdown"`
	At       *At     `json:"at,omitempty"`
}

// 语音消息
type voiceMsg struct {
	head
	Voice Voice `json:"voice"`
	At    *At   `json:"at,omitempty"`
}

// 视频消息
type videoMsg struct {
	head
	Video Video `json:"video"`
	At    *At   `json:"at,omitempty"`
}

type linkMsg struct {
	head
	Link Link `json:"link"`
	At   *At  `json:"at,omitempty"`
}

type Link struct {
	Title  string `json:"title"`
	Picurl string `json:"picUrl"`
	Text   string `json:"text"`
	Url    string `json:"messageUrl"`
}

type Content struct {
	Content string `json:"content"`
}

type Image struct {
	MediaId string `json:"media_id"`
}

type Voice struct {
	MediaId string `json:"media_id"`
}

type Video struct {
	MediaId string `json:"media_id"`
	Title   string `json:"title"`
	Desc    string `json:"description"`
}

/*
	"at": {
	          "atMobiles": [
	              "150XXXXXXXX"
	          ],
	          "atUserIds": [
	              "user123"
	          ],
	          "isAtAll": false
	      }
*/
type At struct {
	AtMobiles []string `json:"atMobiles,omitempty"`
	AtUserIds []string `json:"atUserIds,omitempty"`
	IsAtAll   bool     `json:"isAtAll,omitempty"`
}
