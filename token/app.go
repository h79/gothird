package token

import (
	"encoding/gob"
	"fmt"
	"gitee.com/h79/goutils/common/logger"
	"gitee.com/h79/goutils/common/system"
	"time"
)

const (
	fAll    = 1
	fReadId = 2
	fVMap   = 3
	fApp    = 4

	ownerGob = "/owner.gob"
)

type AppItem struct {
	item
	conf     Config
	itemPath string
	fUpdate  chan int
}

type ConfigOptions func(conf *Config)

func NewAppItem(app App, tk Token, options ...ConfigOptions) *AppItem {
	return newAppItem(&app, tk, VMap{}, options...)
}

func newAppItem(app *App, tk Token, vMap VMap, options ...ConfigOptions) *AppItem {
	//for create file directory
	ap := &AppItem{
		item: item{
			App:   app,
			tk:    tk,
			VMap:  vMap,
			appAt: updateTime{updateAt: time.Now().Unix()},
		},
		fUpdate: make(chan int, 10),
	}
	if len(vMap) > 0 {
		//为了触发保存
		ap.vMapAt.Update()
	}
	for i := range options {
		options[i](&ap.conf)
	}
	ap.itemPath = fmt.Sprintf("%s/%s/%s", rootPath, ap.App.Type, fileId(ap.App.Id))
	if err := checkPath(ap.itemPath); err != nil {
		logger.E("Token", "create directory path failure, err= %v", err)
	}
	return ap
}

func (app *AppItem) setSecret(secret string) {
	app.App.Secret = secret
	if app.tk != nil {
		app.tk.SetSecret(secret)
	}
	app.appAt.Update()
	app.fUpdate <- fApp
}

func (app *AppItem) start() {
	var count = 0
	var minute = time.NewTicker(time.Minute)
	for {
		select {
		case <-minute.C:
			app.checkExpire()
			if count%5 == 0 {
				//5分钟保存一次
				count = 0
				app.saveOwner()
			}
			count++

		case f := <-app.fUpdate:
			if f == fAll {
				app.save()
			}
			if f == fVMap {
				app.saveOwner()
			}
			if f == fApp {
				app.saveApp()
			}
		case _, ok := <-system.Closed():
			if !ok {
				return
			}
		}
	}
}

func (app *AppItem) checkExpire() {
	//自已的值
	if !app.vMapExpire() {
		return
	}
	app.itemUpdated(fVMap)
}

func (app *AppItem) itemUpdated(f int) {
	app.fUpdate <- f
}

func (app *AppItem) load() {
	if !app.conf.IsLocal() {
		return
	}
	if len(app.itemPath) == 0 {
		return
	}
	app.loadOwner()
}

func (app *AppItem) loadOwner() {
	app.vmu.Lock()
	defer app.vmu.Unlock()
	filename := app.itemPath + ownerGob
	err := loadFile(filename, func(de *gob.Decoder) error {
		return de.Decode(&app.VMap)
	})
	if err != nil {
		logger.E("Token", "loadOwner path= %s, app= %s, err= %s", app.itemPath, app.App.String(), err)
		return
	}
	app.vMapAt.Complete()

	logger.D("Token", "loadOwner path= %s, app= %s", app.itemPath, app.App.String())

	for _, v := range app.VMap {
		logger.D("Token", "---->loadOwner data= %s", v.String())
	}
}

func (app *AppItem) saveOwner() {
	app.vmu.Lock()
	defer app.vmu.Unlock()
	if !app.vMapAt.HasUpdate() {
		return
	}
	if err := app.checkPath(); err != nil {
		return
	}
	filename := app.itemPath + ownerGob
	err := saveFile(filename, func(en *gob.Encoder) error {
		return en.Encode(app.VMap)
	})
	if err != nil {
		logger.E("Token", "saveOwner path= %s, file= %s, app= %s, err= %s",
			filename, fileId(app.App.Id), app.App.String(), err)
		return
	}
	app.vMapAt.Complete()
}

func (app *AppItem) saveApp() {
	if !app.conf.IsLocal() {
		return
	}
	if !app.appAt.HasUpdate() {
		return
	}
	filename := fmt.Sprintf("%s/%s.gob", appPath, fileId(app.App.Id))
	err := saveFile(filename, func(en *gob.Encoder) error {
		return en.Encode(app.App)
	})
	if err != nil {
		logger.E("Token", "saveApp path= %s, file= %s, app= %s, err= %s", filename, fileId(app.App.Id), app.App.String(), err)
		return
	}
	app.appAt.Complete()
}

func (app *AppItem) save() {
	app.saveApp()
	app.saveOwner()
}

func (app *AppItem) checkPath() error {
	if !app.conf.IsLocal() {
		return ErrNotSupported
	}
	return checkPath(app.itemPath)
}

func (app *AppItem) Path() string {
	return fileId(app.App.Id)
}
