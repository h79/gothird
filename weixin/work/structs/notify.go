package structs

// 回调接口
// https://developer.work.weixin.qq.com/document/path/90613
// https://developer.work.weixin.qq.com/document/path/92277

// InfoType
const (
	ItCreateAuth         = "create_auth"
	ItChangeAuth         = "change_auth"
	ItCancelAuth         = "cancel_auth"
	ItChangeContact      = "change_contact"
	ItShareAgentChange   = "share_agent_change"
	ItResetPermanentCode = "reset_permanent_code"
)

// SuiteTicketNotify 推送suite_ticket
type SuiteTicketNotify struct {
	SuiteId     string `json:"SuiteId" xml:"SuiteId"`
	AuthCorpId  string `json:"AuthCorpId" xml:"AuthCorpId"`
	SuiteTicket string `json:"SuiteTicketNotify" xml:"SuiteTicketNotify"`
}

// AuthNotify 授权通知事件
// InfoType = create_auth, change_auth, cancel_auth
type AuthNotify struct {
	SuiteId    string `json:"SuiteId" xml:"SuiteId"`
	AuthCorpId string `json:"AuthCorpId" xml:"AuthCorpId"`
	AuthCode   string `json:"AuthCode" xml:"AuthCode"`
	State      string `json:"State" xml:"State"`
}

// 成员通知事件
// InfoType = change_contact, ChangeType= create_user, update_user,delete_user

const (
	CtCreateUser = "create_user"
	CtUpdateUser = "update_user"
	CtDeleteUser = "delete_user"
)

type ContactNotify struct {
	CtType         string         `json:"XCtType" xml:"XCtType"`
	SuiteId        string         `json:"SuiteId" xml:"SuiteId"`
	AuthCorpId     string         `json:"AuthCorpId" xml:"AuthCorpId"`
	UserID         string         `json:"UserID" xml:"UserID"`
	OpenUserID     string         `json:"OpenUserID" xml:"OpenUserID"`
	Name           string         `json:"Name" xml:"Name"`
	Mobile         string         `json:"Mobile" xml:"Mobile"`
	Position       string         `json:"Position" xml:"Position"`
	BizMail        string         `json:"BizMail" xml:"BizMail"`
	Email          string         `json:"Email" xml:"Email"`
	Avatar         string         `json:"Avatar" xml:"Avatar"`
	Alias          string         `json:"Alias" xml:"Alias"`
	Telephone      string         `json:"Telephone" xml:"Telephone"`
	Gender         int            `json:"Gender" xml:"Gender"`
	MainDepartment int64          `json:"MainDepartment" xml:"MainDepartment"`
	Department     []int64        `json:"Department" xml:"Department"`
	IsLeaderInDept []int          `json:"IsLeaderInDept" xml:"IsLeaderInDept"`
	DirectLeader   []string       `json:"DirectLeader" xml:"DirectLeader"`
	ExtAttribute   []ExtAttribute `json:"ExtAttr" xml:"ExtAttr"`
}

type ExtAttribute struct {
	Name string `json:"Name" xml:"Name"`
	Type int    `json:"Type" xml:"Type"`
}

// 部门通知事件
// InfoType = change_contact, ChangeType = create_party, update_party, delete_party

const (
	CtCreateParty = "create_party"
	CTUpdateParty = "update_party"
	CTDeleteParty = "delete_party"
)

// DepartmentNotify 部门通知事件内容
type DepartmentNotify struct {
	CtType     string `json:"XCtType" xml:"XCtType"`
	SuiteId    string `json:"SuiteId" xml:"SuiteId"`
	AuthCorpId string `json:"AuthCorpId" xml:"AuthCorpId"`
	Name       string `json:"Name" xml:"Name"`
	Id         int64  `json:"Id" xml:"Id"`
	ParentId   int64  `json:"ParentId" xml:"ParentId"`
	Order      int    `json:"Order" xml:"Order"`
}

// 成员 标签通知事件
// 回调设置在授权应用可见范围内的标签的变更事件。由第三方应用调用接口触发的标签变更事件不回调给该应用本身。
// InfoType = change_contact, ChangeType = update_tag

const (
	CtUpdateTag = "update_tag"
)

type ContactTagNotify struct {
	CtType        string   `json:"XCtType" xml:"XCtType"`
	SuiteId       string   `json:"SuiteId" xml:"SuiteId"`
	AuthCorpId    string   `json:"AuthCorpId" xml:"AuthCorpId"`
	TagId         int64    `json:"TagId" json:"TagId"`
	AddUserItems  []string `json:"AddUserItems" xml:"AddUserItems"`
	DelUserItems  []string `json:"DelUserItems" xml:"DelUserItems"`
	AddPartyItems []int64  `json:"AddPartyItems" xml:"AddPartyItems"`
	DelPartyItems []int64  `json:"DelPartyItems" xml:"DelPartyItems"`
}

// MsgType= event, Event字段
const (
	EtLICUnlicensed        = "unlicensed_notify" //接口许可失效通知
	EtLICPaySuccess        = "license_pay_success"
	EtLICRefund            = "license_refund"
	EtLICAutoActive        = "auto_activate"
	EtUploadMediaJobFinish = "upload_media_job_finish"
)

type UnLicensedNotify struct {
	AuthCorpId string //企业微信CorpID
	UserId     string //成员UserID
	AgentID    int    //企业应用的id
}

// OrderSuccessNotify 订单支付成功
// InfoType = license_pay_success
type OrderSuccessNotify struct {
	AuthCorpId    string `json:"AuthCorpId" xml:"AuthCorpId"`
	ServiceCorpId string `json:"ServiceCorpId" xml:"ServiceCorpId"`
	OrderId       string `json:"OrderId" xml:"OrderId"`
	BuyerUserId   string `json:"BuyerUserId" xml:"BuyerUserId"`
}

// OrderRefundNotify 退款结果通知
// InfoType = license_refund
type OrderRefundNotify struct {
	AuthCorpId    string `json:"AuthCorpId" xml:"AuthCorpId"`
	ServiceCorpId string `json:"ServiceCorpId" xml:"ServiceCorpId"`
	OrderId       string `json:"OrderId" xml:"OrderId"`
	OrderStatus   int    `json:"OrderStatus" xml:"OrderStatus"`
}

// ActiveNotify 自动激活回调通知
// InfoType = auto_activate
type ActiveNotify struct {
	AuthCorpId    string        `json:"AuthCorpId" xml:"AuthCorpId"`
	ServiceCorpId string        `json:"ServiceCorpId" xml:"ServiceCorpId"`
	Scene         string        `json:"Scene" xml:"Scene"`
	AccountList   []AccountList `json:"AccountList" xml:"AccountList"`
}

type AccountList struct {
	ActiveCode         string `json:"ActiveCode" xml:"ActiveCode"`
	UserId             string `json:"UserId" xml:"UserId"`
	PreviousActiveCode string `json:"PreviousActiveCode" xml:"PreviousActiveCode"`
	ExpireTime         int64  `json:"ExpireTime" xml:"ExpireTime"`
	Type               int    `json:"Type" xml:"Type"`
	PreviousStatus     int    `json:"PreviousStatus" xml:"PreviousStatus"`
}

// 客户事件通知
const (
	ItChangeExternalContact = "change_external_contact"
	ItCustomerAcquisition   = "customer_acquisition"

	//ChangeType 值

	CtAddExternalContact     = "add_external_contact"      //添加企业客户事件
	CtEditExternalContact    = "edit_external_contact"     //编辑企业客户事件(授权企业中配置了客户联系功能的成员编辑外部联系人的备注信息(不包括备注手机号码)或企业标签时，企业微信服务器会向应用的“指令回调URL”推送该事件，但仅修改外部联系人备注手机号时不会触发回调。)
	CtAddHalfExternalContact = "add_half_external_contact" //外部联系人免验证添加成员事件(外部联系人添加了配置了客户联系功能且开启了免验证的成员时（此时成员尚未确认添加对方为好友），企业微信服务器会向应用的“指令回调URL”推送该事件)
	CtDeleteExternalContact  = "del_external_contact"      //删除企业客户事件(授权企业中配置了客户联系功能的成员删除外部联系人时，企业微信服务器会向应用的“指令回调URL”推送该事件)
	CtDeleteFollowUser       = "del_follow_user"           //删除跟进成员事件(授权企业中配置了客户联系功能的成员被外部联系人删除时，企业微信服务器会向应用的“指令回调URL”推送该事件)
	CtTransferFail           = "transfer_fail"             //客户接替失败事件(企业将客户分配给新的成员接替后，当客户添加失败时，企业微信服务器会向应用的“指令回调URL”推送该事件)
	CtBalanceLow             = "balance_low"               //获客额度即将耗尽事件
	CtBalanceExhausted       = "balance_exhausted"         //获客额度已经耗尽事件
	CtDeleteLink             = "delete_link"               //删除获客链接事件
	CtQuotaExpireSoon        = "quota_expire_soon"         //获客额度即将过期事件
	CtCustomerStartChat      = "customer_start_chat"       //微信客户发起会话事件
	CtLinkUnavailable        = "link_unavailable"          //获客链接失效事件
)

type ExternalContactNotify struct {
	CtType         string `json:"XCtType" xml:"XCtType"`
	SuiteId        string `json:"SuiteId" xml:"SuiteId"`               //第三方应用ID
	AuthCorpId     string `json:"AuthCorpId" xml:"AuthCorpId"`         //授权企业的CorpID
	UserID         string `json:"UserID" xml:"UserID"`                 //企业服务人员的UserID
	ExternalUserID string `json:"ExternalUserID" xml:"ExternalUserID"` //外部联系人的userid，注意不是企业成员的账号(第三方应用可以根据ExternalUserID调用“获取客户详情”读取详情。)
	State          string `json:"State" xml:"State"`                   //添加此用户的「联系我」方式配置的state参数，可用于识别添加此用户的渠道
	WelcomeCode    string `json:"WelcomeCode" xml:"WelcomeCode"`
	FailReason     string `json:"FailReason" xml:"FailReason"` //transfer_fail
}

//客户群变更事件

const (
	ItChangeExternalChat = "change_external_chat"

	CtCreate  = "create"
	CtUpdate  = "update"
	CtDismiss = "dismiss"
	CtDelete  = "delete"
	CtShuffle = "shuffle" //重排 for tag
)

type ExternalChatNotify struct {
	CtType        string          `json:"XCtType" xml:"XCtType"`
	SuiteId       string          `json:"SuiteId" xml:"SuiteId"`
	AuthCorpId    string          `json:"AuthCorpId" xml:"AuthCorpId"`
	ChatId        string          `json:"ChatId" xml:"ChatId"`
	UpdateDetail  string          `json:"UpdateDetail" xml:"UpdateDetail"`
	LastMemVer    string          `json:"LastMemVer" xml:"LastMemVer"`
	CurMemVer     string          `json:"CurMemVer" xml:"CurMemVer"`
	MemChangeCnt  string          `json:"MemChangeCnt" xml:"MemChangeCnt"`
	MemChangeList []MemChangeList `json:"MemChangeList" xml:"MemChangeList"`
	JoinScene     int             `json:"JoinScene" xml:"JoinScene"` // 0 由成员邀请入群（包括直接邀请入群和通过邀请链接入群） 3通过扫描群二维码入群
	QuitScene     int             `json:"QuitScene" xml:"QuitScene"` // 当是成员退群时有值。表示成员的退群方式 0自己退群 1群主/群管理员移出
}

type MemChangeList struct {
	Item string `xml:"Item" json:"Item" log:"Item"` //当是成员入群或退群时有值。变更的成员列表
}

//UpdateDetail	变更详情。目前有以下几种：
//add_member : 成员入群
//del_member : 成员退群
//change_owner : 群主变更
//change_name : 群名变更
//change_notice : 群公告变更
//JoinScene	当是成员入群时有值。表示成员的入群方式
//0 - 由成员邀请入群（包括直接邀请入群和通过邀请链接入群）
//3 - 通过扫描群二维码入群
//QuitScene	当是成员退群时有值。表示成员的退群方式
//0 - 自己退群
//1 - 群主/群管理员移出
//MemChangeCnt	当是成员入群或退群时有值。表示成员变更数量

// 企业客户标签事件

const (
	ItChangeExternalTag = "change_external_tag"
)

type ExternalTagNotify struct {
	CtType     string `json:"XCtType" xml:"XCtType"`
	SuiteId    string `json:"SuiteId" xml:"SuiteId"`
	AuthCorpId string `json:"AuthCorpId" xml:"AuthCorpId"`
	Id         string `json:"Id" xml:"Id"`
	TagType    string `json:"TagType" xml:"TagType"`
}
