package gdt

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/token/access"
	"gitee.com/h79/goutils/common/data"
	"gitee.com/h79/goutils/common/http"
)

func GetAccessToken(tk token.Token, d data.D) (access.Token, error) {
	parent := tk.Parent()
	url := fmt.Sprintf("%s/oauth/token?client_id=%s&client_secret=%s&grant_type=authorization_code&authorization_code=%s&redirect_uri=%s",
		ApiPrefixUrl, parent.GetAppId(), parent.GetSecret(), d.String("code"), d.String("redirect_uri"))

	return getToken(url)
}

func RefreshToken(tk token.Token, d data.D) (access.Token, error) {
	parent := tk.Parent()
	url := fmt.Sprintf("%s/oauth/token?client_id=%s&client_secret=%s&grant_type=refresh_token&refresh_token=%s",
		ApiPrefixUrl, parent.GetAppId(), parent.GetSecret(), d.String("refToken"))
	return getToken(url)
}

func getToken(uri string) (*AcsData, error) {
	hp := http.Http{}
	body, err := hp.DoBytes("GET", uri, nil)
	if err != nil {
		return nil, err
	}

	res := tokenResult{}
	if err = json.Unmarshal(body, &res); err != nil {
		return nil, err
	}
	return &res.D, res.ErrorIf()
}

type tokenResult struct {
	Response
	D AcsData `json:"data"`
}

type AcsData struct {
	AccessToken
	AdInfo AuthorizerInfo `json:"authorizer_info,omitempty"`
}

type AuthorizerInfo struct {
	AccountUin      int64    `json:"account_uin,omitempty"`
	AccountId       int64    `json:"account_id,omitempty"`
	ScopeList       []string `json:"scope_list,omitempty"`
	WechatAccountId string   `json:"wechat_account_id,omitempty"`
	AccountRoleType string   `json:"account_role_type,omitempty"`
	AccountType     string   `json:"account_type,omitempty"`
	RoleType        string   `json:"role_type,omitempty"`
}
