package token

type Config struct {
	StoreType string `json:"storeType"`
}

func (c Config) IsLocal() bool {
	return c.StoreType == "local" || c.StoreType == ""
}
