package fxg

import (
	"fmt"
	"gitee.com/h79/gothird/fxg/access"
	"gitee.com/h79/gothird/fxg/errors"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/goutils/common/data"
	"mime/multipart"
	"net/http"
)

var firstService *acsReq

func init() {
	firstService = &acsReq{
		get:    token.GetEmpty,
		ref:    access.RefreshToken,
		oauth2: access.GetAccessToken,
	}
}

func GetService() token.Service {
	return firstService
}

func NewToken(app token.App) token.Token {
	return token.New(&app, firstService)
}

var _ token.Service = (*acsReq)(nil)

type acsReq struct {
	check  token.CheckErrorFunc
	get    token.GenFunc
	ref    token.GenFunc
	oauth2 token.GenFunc
}

// Execute
// token.Service interface
func (req *acsReq) Execute(tk token.Token, cmd string, d data.D) (interface{}, error) {
	if cmd == token.NAccessToken {
		return req.get(tk, d)
	}
	if cmd == token.NRefreshToken {
		return req.ref(tk, d)
	}
	if cmd == token.NOauth2 {
		acs, err := req.oauth2(tk, d)
		if err != nil {
			return nil, err
		}

		return acs, err
	}
	return nil, token.ErrNotSupported
}

func (req *acsReq) BuildUrl(uri string, acsKey string, d data.D) string {
	return fmt.Sprintf("%s&access_token=%s", uri, acsKey)
}

func (req *acsReq) SetHead(h *http.Header, acsKey string) {
	h.Set("Accept", "*/*")
	h.Set("Content-Type", "application/json;charset=UTF-8")
}

func (req *acsReq) CreateForm(w *multipart.Writer, field string, form interface{}) error {
	return nil
}

func (req *acsReq) SetCheckError(check token.CheckErrorFunc) {
	req.check = check
}

func (req *acsReq) CheckError(err error) error {
	if res, ok := err.(token.Result); ok {
		if req.check != nil {
			return req.check(&res)
		}
		if res.Code == errors.InvalidAccessToken {
			return token.ErrTokenInvalid
		}
		if res.SubCode == errors.ScTokenExpire {
			return token.ErrTokenExpired
		}
	}
	return err
}
