package app3

import (
	"gitee.com/h79/gothird/token/access"
)

type PreAuthCode struct {
	Code      string `json:"pre_auth_code"`
	ExpiresIn int64  `json:"expires_in"`
}

type AuthorizerInfo struct {
	NickName        string `json:"nick_name"` //昵称
	HeadImg         string `json:"head_img"`  //头像
	ServiceTypeInfo struct {
		Id int `json:"id"`
	} `json:"service_type_info"` //公众号类型
	VerifyTypeInfo struct {
		Id int `json:"id"`
	} `json:"verify_type_info"` //公众号认证类型
	UserName     string `json:"user_name"`  //原始 ID
	Alias        string `json:"alias"`      //公众号所设置的微信号，可能为空
	QrcodeUrl    string `json:"qrcode_url"` //二维码图片的 URL，开发者最好自行也进行保存
	BusinessInfo struct {
		OpenPay   int `json:"open_pay"`   //是否开通微信支付功能
		OpenShake int `json:"open_shake"` //是否开通微信摇一摇功能
		OpenScan  int `json:"open_scan"`  //是否开通微信扫商品功能
		OpenCard  int `json:"open_card"`  //是否开通微信卡券功能
		OpenStore int `json:"open_store"` //是否开通微信门店功能
	} `json:"business_info"` //用以了解功能的开通状况（0代表未开通，1代表已开通）
	Idc             int    `json:"idc"`
	PrincipalName   string `json:"principal_name"` //主体名称
	Signature       string `json:"signature"`
	MiniProgramInfo struct {
		Network struct {
			RequestDomain        []string      `json:"RequestDomain"`
			WsRequestDomain      []string      `json:"WsRequestDomain"`
			UploadDomain         []string      `json:"UploadDomain"`
			DownloadDomain       []string      `json:"DownloadDomain"`
			BizDomain            []interface{} `json:"BizDomain"`
			UDPDomain            []interface{} `json:"UDPDomain"`
			TCPDomain            []interface{} `json:"TCPDomain"`
			PrefetchDNSDomain    []interface{} `json:"PrefetchDNSDomain"`
			NewRequestDomain     []interface{} `json:"NewRequestDomain"`
			NewWsRequestDomain   []interface{} `json:"NewWsRequestDomain"`
			NewUploadDomain      []interface{} `json:"NewUploadDomain"`
			NewDownloadDomain    []interface{} `json:"NewDownloadDomain"`
			NewBizDomain         []interface{} `json:"NewBizDomain"`
			NewUDPDomain         []interface{} `json:"NewUDPDomain"`
			NewTCPDomain         []interface{} `json:"NewTCPDomain"`
			NewPrefetchDNSDomain []interface{} `json:"NewPrefetchDNSDomain"`
		} `json:"network"`
		Categories []struct {
			First  string `json:"first"`
			Second string `json:"second"`
		} `json:"categories"`
		VisitStatus int `json:"visit_status"`
	} `json:"MiniProgramInfo"` //小程序配置，根据这个字段判断是否为小程序类型授权
	RegisterType  int `json:"register_type"`  //小程序注册方式
	AccountStatus int `json:"account_status"` //账号状态，该字段小程序也返回
	BasicConfig   struct {
		IsPhoneConfigured bool `json:"is_phone_configured"` //是否已经绑定手机号
		IsEmailConfigured bool `json:"is_email_configured"` //是否已经绑定邮箱，不绑定邮箱账号的不可登录微信公众平台
	} `json:"basic_config"` //基础配置信息
}

type AuthorizationInfo struct {
	Appid        string        `json:"authorizer_appid"`
	AccessToken  string        `json:"authorizer_access_token"`
	RefreshToken string        `json:"authorizer_refresh_token"`
	ExpiresIn    int64         `json:"expires_in"`
	Func         []interface{} `json:"func_info"`
}

// GetAccessToken access.Token interface implement
func (a AuthorizationInfo) GetAccessToken() access.Value {
	return access.Value{
		Data:     a.AccessToken,
		ExpireIn: access.ExpireSecond(a.ExpiresIn),
	}
}

func (a AuthorizationInfo) GetRefAccessToken() access.Value {
	return access.Value{
		Data:     a.RefreshToken,
		ExpireIn: access.RefExpireSecond(0),
	}
}

type ListAuthInfo struct {
	TotalCount int    `json:"total_count"`
	List       []Auth `json:"list"`
}

type Auth struct {
	AuthorizerAppId        string `json:"authorizer_appid"`
	AuthorizerRefreshToken string `json:"refresh_token"`
	AuthTime               int64  `json:"auth_time"`
}

type AuthorizerToken struct {
	AuthorizerAppId string `json:"authorizer_appid,omitempty"`
	AcsToken        string `json:"authorizer_access_token"`
	ExpireIn        int64  `json:"expires_in"`
	RefToken        string `json:"authorizer_refresh_token"`
	RefExpireIn     int64  `json:"authorizer_refresh_expires_in,omitempty"`
}

// GetAccessToken access.Token interface implement
func (a AuthorizerToken) GetAccessToken() access.Value {
	return access.Value{
		Data:     a.AcsToken,
		ExpireIn: access.ExpireSecond(a.ExpireIn),
	}
}

func (a AuthorizerToken) GetRefAccessToken() access.Value {
	return access.Value{
		Data:     a.RefToken,
		ExpireIn: access.RefExpireSecond(a.RefExpireIn),
	}
}
