package mini

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/goutils/common/http"
	"gitee.com/h79/goutils/common/result"
)

// GetCode2Session 登录凭证校验。通过 wx.login 接口获得临时登录凭证 code 后传到开发者服务器调用此接口完成登录流程。更多使用方法详见 小程序登录。
// https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/login/auth.code2Session.html
func GetCode2Session(appId string, secret string, code string) (*SessionKey, error) {
	if appId == "" || secret == "" {
		return nil, result.Error(-1, "appid or secret is empty.")
	}
	url := fmt.Sprintf("%s/sns/jscode2session?appid=%s&secret=%s&grant_type=authorization_code&js_code=%s", consts.ApiPrefixUrl, appId, secret, code)
	hp := http.Http{}
	body, err := hp.DoBytes("GET", url, nil)
	if err != nil {
		return nil, err
	}
	/*
	   openid	string	用户唯一标识
	   session_key	string	会话密钥
	   unionid	string	用户在开放平台的唯一标识符，在满足 UnionID 下发条件的情况下会返回，详见 UnionID 机制说明。
	   errcode	number	错误码
	   errmsg	string	错误信息
	*/
	type sessionResult struct {
		response.Response
		SessionKey
	}
	res := sessionResult{}
	if err = json.Unmarshal(body, &res); err != nil {
		return nil, err
	}
	if res.ErrCode != 0 {
		return nil, result.Error(res.ErrCode, res.ErrMsg)
	}
	res.AppId = appId
	return &res.SessionKey, nil
}
