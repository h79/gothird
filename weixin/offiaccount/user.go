package offiaccount

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/goutils/common/http"
)

// UserGetInfo
// 2021年12月27日之后，不再输出头像、昵称信息。
// GET https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN
func UserGetInfo(api *token.Api, openid, lang string) (*UserEx, error) {
	if openid == "" {
		return nil, token.Error(-1, "openid is empty")
	}
	if lang == "" {
		lang = "zh_CN"
	}
	uri := fmt.Sprintf("%s/cgi-bin/user/info?openid=%s&lang=%s&", consts.ApiPrefixUrl, openid, lang)

	res := userExResult{}
	er := api.Request("GET", uri, nil, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})
	return &res.UserEx, er
}

// UserBatchGetInfo
// 批量获取用户基本信息
// 开发者可通过该接口来批量获取用户基本信息。最多支持一次拉取100条。
// http请求方式: POST https://api.weixin.qq.com/cgi-bin/user/info/batchget?access_token=ACCESS_TOKEN
func UserBatchGetInfo(api *token.Api, open []UserReq) ([]UserEx, error) {
	var r = struct {
		UserList []UserReq `json:"user_list"`
	}{
		UserList: append(make([]UserReq, 0), open...),
	}
	uri := fmt.Sprintf("%s/cgi-bin/user/info/batchget?", consts.ApiPrefixUrl)
	buf, _ := json.Marshal(&r)
	res := struct {
		response.Response
		List []UserEx `json:"user_info_list"` //2021年12月27日之后，不再输出头像、昵称信息。
	}{}
	er := api.Request("POST", uri, buf, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})
	return res.List, er
}

// UserGetUnionId 获取 unionid
// GET https://api.weixin.qq.com/wxa/getpaidunionid?access_token=ACCESS_TOKEN&openid=OPENID
func UserGetUnionId(api *token.Api, openid string) (string, error) {
	uri := fmt.Sprintf("%s/wxa/getpaidunionid?&openid=%s&", consts.ApiPrefixUrl, openid)
	type unionIdResult struct {
		response.Response
		UnionId string `json:"unionid"`
	}
	res := unionIdResult{}
	er := api.Request("GET", uri, nil, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})
	return res.UnionId, er
}

// UserGetList 获取用户列表
// 公众号可通过本接口来获取帐号的关注者列表，关注者列表由一串OpenID（加密后的微信号，每个用户对每个公众号的OpenID是唯一的）组成。一次拉取调用最多拉取10000个关注者的OpenID，可以通过多次拉取的方式来满足需求。
// GET https://api.weixin.qq.com/cgi-bin/user/get?access_token=ACCESS_TOKEN&next_openid=NEXT_OPENID
func UserGetList(api *token.Api, nextopenid string) (*UserOpenIdList, error) {
	var uri = ""
	if nextopenid != "" {
		uri = fmt.Sprintf("%s/cgi-bin/user/get?next_openid=%s&", consts.ApiPrefixUrl, nextopenid)
	} else {
		uri = fmt.Sprintf("%s/cgi-bin/user/get?", consts.ApiPrefixUrl)
	}
	res := userOpenIdListResult{}
	er := api.Request("GET", uri, nil, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})
	return &res.UserOpenIdList, er
}

// UserGetBlack 获取公众号的黑名单列表
//
// 公众号可通过该接口来获取帐号的黑名单列表，黑名单列表由一串 OpenId（加密后的微信号，每个用户对每个公众号的OpenID是唯一的）组成。
//
// 该接口每次调用最多可拉取 10000 个OpenID，当列表数较多时，可以通过多次拉取的方式来满足需求。
// POST https://api.weixin.qq.com/cgi-bin/tags/members/getblacklist?access_token=ACCESS_TOKEN
func UserGetBlack(api *token.Api, nextopenid string) (*UserOpenIdList, error) {
	uri := fmt.Sprintf("%s/cgi-bin/tags/members/getblacklist?", consts.ApiPrefixUrl)

	req := BlackReq{
		OpenId: nextopenid,
	}
	buf, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	res := userOpenIdListResult{}
	er := api.Request("POST", uri, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	return &res.UserOpenIdList, er
}

// UserSetBlack 拉黑用户
// 公众号可通过该接口来拉黑一批用户，黑名单列表由一串 OpenId （加密后的微信号，每个用户对每个公众号的OpenID是唯一的）组成。
// POST https://api.weixin.qq.com/cgi-bin/tags/members/batchblacklist?access_token=ACCESS_TOKEN
func UserSetBlack(api *token.Api, req *BatchReq) error {
	uri := fmt.Sprintf("%s/cgi-bin/tags/members/batchblacklist?", consts.ApiPrefixUrl)

	buf, err := json.Marshal(&req)
	if err != nil {
		return err
	}
	res := response.Response{}
	return api.Request("POST", uri, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
}

// UserCancelBlack 取消拉黑用户
// POST https://api.weixin.qq.com/cgi-bin/tags/members/batchunblacklist?access_token=ACCESS_TOKEN
func UserCancelBlack(api *token.Api, req *BatchReq) error {
	uri := fmt.Sprintf("%s/cgi-bin/tags/members/batchunblacklist?", consts.ApiPrefixUrl)

	buf, err := json.Marshal(&req)
	if err != nil {
		return err
	}
	res := response.Response{}
	return api.Request("POST", uri, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
}

type userExResult struct {
	response.Response
	UserEx //2021年12月27日之后，不再输出头像、昵称信息。
}

type userOpenIdListResult struct {
	response.Response
	UserOpenIdList
}
