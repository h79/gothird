package dingtalk

import (
	"gitee.com/h79/gothird/config"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/goutils/common/result"
)

type FillContentCallback func(*config.Options, interface{}) string

func Notify(option *config.Options, content interface{}, call FillContentCallback) error {
	tk, err := token.GetToken(option.Group.CorpId)
	if err != nil {
		return err
	}
	if option.Group.Method == "chat" {
		toWho := ToGroup{ChatId: option.Group.ChatId}
		msg := NewMessage(&toWho, "text", nil, Content{Content: call(option, content)})
		_, err = SendMessage(tk, msg)
		return err
	} else if option.Group.Method == "agent" {
		toWho := ToAgent{AgentId: option.Group.AgentId, ToUser: option.Group.User}
		msg := NewMessage(&toWho, "text", nil, Content{Content: call(option, content)})
		_, err = SendMessage(tk, msg)
		return err
	} else if option.Group.Method == "root" {
		toWho := ToRoot{}
		msg := NewMessage(&toWho, "text", nil, Content{Content: call(option, content)})
		_, err = SendMessage(tk, msg)
		return err
	}
	return result.Error(result.ErrGroupInternal, "Not support method")
}
