package offiaccount

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/token/access"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/gothird/weixin/response"
	"gitee.com/h79/gothird/weixin/session"
	"gitee.com/h79/goutils/common/data"
	"gitee.com/h79/goutils/common/http"
)

// 针对微信公众号，个人用户(这个是个人的access_token)
// 首先请注意，这里通过code换取的是一个特殊的网页授权access_token,与基础支持中的access_token（该access_token用于调用其他接口）不同。
// 公众号可通过下述接口来获取网页授权access_token。
// 如果网页授权的作用域为snsapi_base，则本步骤中获取到网页授权access_token的同时，也获取到了openid，snsapi_base式的网页授权流程即到此为止。
// 尤其注意：由于公众号的secret和获取到的access_token安全级别都非常高，必须只保存在服务器，不允许传给客户端。
// 后续刷新access_token、通过access_token获取用户信息等步骤，也必须从服务器发起。

// Oauth2 官司说明 https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140842
// 微信网页通过code 授权access_token， 这个授权只是某一个具体的app，某个具体openid对应的access_token
func Oauth2(tk token.Token, d data.D) (access.Token, error) {
	p := tk.Parent()
	code := d.String("code")
	url := fmt.Sprintf("%s/sns/oauth2/access_token?appid=%s&secret=%s&grant_type=authorization_code&code=%s", consts.ApiPrefixUrl, p.GetAppId(), p.GetSecret(), code)
	se, err := session.GetToken(p.GetAppId(), url)
	if err != nil {
		return nil, err
	}
	se.ParentId = tk.GetId()
	se.SessionId = token.GenId(tk.GetAppId(), se.Openid)
	return se, nil
}

// Oauth2RefreshToken 刷新access_token
// https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=AppID&grant_type=refresh_token&refresh_token=REFRESH_TOKEN
func Oauth2RefreshToken(tk token.Token, d data.D) (access.Token, error) {
	p := tk.Parent()
	refToken := d.String("refToken")
	url := fmt.Sprintf("%s/sns/oauth2/refresh_token?appid=%s&grant_type=refresh_token&refresh_token=%s", consts.ApiPrefixUrl, p.GetAppId(), refToken)
	se, err := session.GetToken(p.GetAppId(), url)
	if err != nil {
		return nil, err
	}
	se.ParentId = tk.GetId()
	se.SessionId = token.GenId(tk.GetAppId(), se.Openid)
	return se, nil
}

/**
 * 网站应用
 *
 *第一步：请求CODE
 * 第三方使用网站应用授权登录前请注意已获取相应网页授权作用域（scope=snsapi_login），则可以通过在PC端打开以下链接：
 *   https://open.weixin.qq.com/connect/qrconnect?appid=APPID&redirect_uri=REDIRECT_URI&response_type=code&scope=SCOPE&state=STATE#wechat_redirect
 *
 *   https://developers.weixin.qq.com/doc/oplatform/Website_App/WeChat_Login/Authorized_Interface_Calling_UnionID.html
 */

// Oauth2UserGet GET https://api.weixin.qq.com/sns/userinfo?access_token=%s&openid=%s
// 通过access_token和openid获取用户的基础信息，包括头像、昵称、性别、地区
func Oauth2UserGet(api *token.Api, openid, lang string) (*User, error) {
	if openid == "" {
		return nil, token.Error(-1, "openid is empty")
	}
	if lang == "" {
		lang = "zh_CN"
	}
	uri := fmt.Sprintf("%s/sns/userinfo?openid=%s&lang=%s&", consts.ApiPrefixUrl, openid, lang)
	type userResult struct {
		response.Response
		User
	}
	res := userResult{}
	if er := api.Request("GET", uri, nil, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	}); er != nil {
		return nil, er
	}
	res.AppId = api.AppId()
	if res.Language == "" {
		res.Language = lang
	}
	return &res.User, nil
}
