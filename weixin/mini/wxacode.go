package mini

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/gothird/weixin/consts"
	"gitee.com/h79/goutils/common/http"
)

// QrcCreate 获取小程序码，适用于需要的码数量极多的业务场景。通过该接口生成的小程序码，永久有效，数量暂无限制
// https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/qr-code/wxacode.getUnlimited.html
func QrcCreate(api *token.Api, qrc *WxaQrcReq) ([]byte, error) {

	uri := fmt.Sprintf("%s/wxa/getwxacodeunlimit?", consts.ApiPrefixUrl)

	buf, err := json.Marshal(qrc)
	if err != nil {
		return nil, err
	}
	var res []byte
	er := api.Request("POST", uri, buf, func(hp *http.Http, body []byte) error {
		res = body
		return nil
	})
	return res, er
}
