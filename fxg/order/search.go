package order

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/fxg/consts"
	"gitee.com/h79/gothird/fxg/params"
	"gitee.com/h79/gothird/fxg/response"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/goutils/common/data"
	"gitee.com/h79/goutils/common/http"
)

// SearchList
// order/searchList?app_key=your_appkey_here&method=order.searchList&access_token=your_accesstoken_here&param_json={}&timestamp=2018-06-19%2016:06:59&v=2&sign=your_sign_here
func SearchList(api *token.Api, d data.D) (interface{}, error) {

	uri := fmt.Sprintf("%s/order/searchList?%s", consts.ApiPrefixUrl, params.UrlParams(api.AppId(), api.Secret(), "order.searchList", d))
	type searchResult struct {
		response.Response
		Data interface{} `json:"data,omitempty"`
	}
	res := searchResult{}
	er := api.Request("POST", uri, nil, func(hp *http.Http, body []byte) error {
		if err := json.Unmarshal(body, &res); err != nil {
			return err
		}
		return res.ReturnIf(api)
	})

	return res.Data, er
}
