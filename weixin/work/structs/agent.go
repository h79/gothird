package structs

type AgentInfo struct {
	AgentId       int32  `json:"agentid"`
	Name          string `json:"name"`
	SquareLogoUrl string `json:"square_logo_url"`
}

type AgentDetail struct {
	AgentId                 int32          `json:"agentid"`
	Name                    string         `json:"name"`                      //企业应用名称
	SquareLogoUrl           string         `json:"square_logo_url"`           //企业应用方形头像
	Description             string         `json:"description"`               //企业应用详情
	RedirectDomain          string         `json:"redirect_domain"`           //企业应用可信域名
	HomeUrl                 string         `json:"home_url"`                  //应用主页url
	AllowUserInfos          AllowUserInfos `json:"allow_userinfos"`           //企业应用可见范围（人员），其中包括userid
	AllowParties            AllowParties   `json:"allow_partys"`              //企业应用可见范围（部门）
	AllowTags               AllowTags      `json:"allow_tags"`                //企业应用可见范围（标签）
	Close                   int            `json:"close"`                     //企业应用是否被停用。0：未被停用；1：被停用
	ReportLocationFlag      int            `json:"report_location_flag"`      //企业应用是否打开地理位置上报 0：不上报；1：进入会话上报；
	IsReportEnter           int            `json:"isreportenter"`             //是否上报用户进入应用事件。0：不接收；1：接收
	CustomizedPublishStatus int            `json:"customized_publish_status"` //代开发自建应用返回该字段，表示代开发发布状态。0：待开发（企业已授权，服务商未创建应用）；1：开发中（服务商已创建应用，未上线）；2：已上线（服务商已上线应用且不存在未上线版本）；3：存在未上线版本（服务商已上线应用但存在未上线版本）
}

type AllowUserInfos struct {
	User []AllowUser `json:"user"`
}

type AllowUser struct {
	Userid string `json:"userid"`
}

type AllowParties struct {
	PartyId []int `json:"partyid"`
}

type AllowTags struct {
	TagId []int `json:"tagid"`
}
