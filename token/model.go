package token

import "fmt"

type App struct {
	ParentId string `json:"parentId"`
	Id       string `json:"id"`
	AppId    string `json:"appid"`
	Secret   string `json:"secret,omitempty"`
	Type     string `json:"type,omitempty"`
	Name     string `json:"name,omitempty"`
	IsCache  bool   `json:"cache,omitempty"`
}

func (a *App) String() string {
	return fmt.Sprintf("<Id: %s,ParentId: %s, AppId: %s, Type: %s, Name: %s, Cache: %v>",
		a.Id, a.ParentId, a.AppId, a.Type, a.Name, a.IsCache)
}

type Item struct {
	Apps []App `json:"apps"`
}

type ResToken struct {
	ParentId string `json:"parentId"`
	Id       string `json:"id"`
	AppId    string `json:"appid"`
	Type     string `json:"type"`
	Token    string `json:"token"`
}

type ResCode struct {
	ParentId string `json:"parentId"`
	Id       string `json:"id"`
	AppId    string `json:"appid"`
	Type     string `json:"type"`
	Code     string `json:"code"`
}
