package params

import (
	"gitee.com/h79/gothird/fxg/sign"
	"gitee.com/h79/goutils/common/json"
	"net/url"
	"time"
)

func UrlParams(appKey, appSecret string, method string, param interface{}) string {

	timestamp := time.Now().Format("2006-01-02 15:04:05")
	paramJson := json.ToString(param)

	params := url.Values{}
	params.Add("app_key", appKey)
	params.Add("param_json", paramJson)
	params.Add("method", method)
	params.Add("v", "2")
	params.Add("timestamp", timestamp)
	params.Add("sign", sign.Sign(appKey, appSecret, method, timestamp, paramJson))
	params.Add("sign_method", "hmac-sha256")

	return params.Encode()
}
