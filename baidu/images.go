package baidu

import (
	"encoding/json"
	"fmt"
	"gitee.com/h79/gothird/token"
	"gitee.com/h79/goutils/common/data"
	"gitee.com/h79/goutils/common/http"
)

// PersonSeg https://ai.baidu.com/ai-doc/BODY/Fk3cpyxua
func PersonSeg(api *token.Api, d data.D) (*PersonResult, error) {

	uri := fmt.Sprintf("%s/rest/2.0/image-classify/v1/body_seg?", ApiPrefixUrl)

	buf := []byte(d.ToFormUrl().Encode())

	type result struct {
		Response2
		PersonResult
	}
	res := result{}
	err := api.Request("POST", uri, buf, func(hp *http.Http, body []byte) error {
		if er := json.Unmarshal(body, &res); er != nil {
			return er
		}
		return res.ReturnIf(api)
	})
	return &res.PersonResult, err
}
